# -*- coding: UTF-8 -*-
'''
cgi-bin

'''

import os
import string
import sys

import EW_HtmlLink

arg_file = ""
arg_full = "no"

def erreur(msg, status='500 Internal Server Error'):
    print 'Status:', status
    print
    print msg
    sys.exit(0)

def parseEnvString():
    global arg_file, arg_full

    inp = os.getenv("QUERY_STRING")
    if (inp == None or inp == ""): erreur("File name expected")
    tokens = string.split(inp, "&")

    for tok in tokens:
        if (tok == ""): continue
        stok = string.split(tok, "=")
        var = string.lower(stok[0])
        val = stok[1]
        if (var == "file"):  arg_file = val
        if (var == "full"):  arg_full = val.lower()

    if (arg_file == ""): erreur("File name expected")

def main():
    global arg_file, arg_full

    #---  Parse les info
    parseEnvString()

    #---  Construit l'algo d'ecriture
    if (arg_file <> ""):
        if (arg_full == "no"):
            algo = EW_HtmlLink.EWHtml_SummaryLog()
        else:
            algo = EW_HtmlLink.EWHtml_LinkerLog()
        algo.xeq(arg_file)


if __name__ == '__main__':
    main()
