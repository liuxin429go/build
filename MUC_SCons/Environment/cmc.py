# -*- coding: UTF-8 -*-
import SCons.Script
import SCons.Environment

from . import Environment
import MUC_platform

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

class EnvironmentCMC(MUCEnv):
    def __init__(self, tool, version, opts, *args, **kwargs):
        if (MUC_platform.is_unx()):
            tools =  ['c++', 'cc', 'fortran', 'link', 'ar']
            muc_target_plateform = 'unx64'
            plateform = 'unx64'
        else:
            raise SCons.Errors.UserError('Invalid CMC platform: %s' % MUC_platform.platform())

        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform = plateform,
                           MUC_TARGETPLATEFORM = muc_target_plateform)
        # Place les path posix en fin
        tmpPath = self['ENV']['PATH']
        self['ENV']['PATH'] = ' '
        MUCEnv.__init__(self, tool, version, *args, **kwargs)
        self.AppendENVPath('PATH', tmpPath)

        if (self['docpplink']):
            self['LINK'] = 's.cc'
        else:
            self['LINK'] = 's.f90'
        if (muc_target_plateform == 'unx32'):
            self['LINK'] = self.get('LINK') + ' -m32'
        else:
            self['LINK'] = self.get('LINK') + ' -m64'


    def setupEnv(self, ctx):
        # cmc ne comprend pas les -U de '-MODE_DYNAMIC'
        # ajoute le define pour spécifier un build cmc
        lnk = ctx.lnk
        dfn = ctx.dfn
        if (lnk == 'static'):
            try:
                ctx.dfn.remove('MODE_DYNAMIC')
            except:
                pass
            # ctx.lnk = 'CMC_undefined'
        if 'INRS_BUILD_CMC' not in ctx.dfn: ctx.dfn.append('INRS_BUILD_CMC')
        MUCEnv.setupEnv(self, ctx)
        ctx.dfn = dfn
        ctx.lnk = lnk

        if (ctx.bld == 'debug'):   self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-debug']
        if (ctx.bld == 'release'): self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-openmp']
        if ('mpi' in ctx.lxt): self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-mpi']
        # if (ctx.lnk == 'static'):  self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-static', '-static-intel']
        # if (ctx.lnk == 'dynamic'): self['SHLINKFLAGS'] = self.get('SHLINKFLAGS') + ['-fpic', '-shared']
        if (not self['docpplink']):
           try:
               ctx.sys.index('stdc++')
           except:
               ctx.sys.append('stdc++')

