# -*- coding: UTF-8 -*-
import EW_Info
from EW_Info import creeInfoDatedeXML
from EW_Util import EWHtmlFile
from EW_Info import *
from EW_Util import dbg

import os

rpdb2_imported = False
try:
    import rpdb2
    rpdb2_imported = True
except:
    pass

class EWInfoGlobal(EWInfo):
    def __init__(self, parent):
        EWInfo.__init__(self, parent, 'Global')

## --Hierarchie en X et en Y
_X_ = [ EWInfoDate(None), EWInfoPackage(None), EWInfoVersion(None), EWInfoModule(None), EWInfoFichier(None) ]
##_Y_ = [ EWInfoGlobal(None), EWInfoPlatform(None), EWInfoCompiler(None), EWInfoTypeLink(None), EWInfoTypeBuild(None) ]

class Table:
    def __init__(self):
        self.col_t = None       # Col type
        self.row_t = None      	# Row type
        self.tbl_col = []       # Col header
        self.tbl_row = []       # Row header
        self.tbl_col_sum = []   # Col sum       { 'Fichier' : [0,0,0], 'Link' : [0,0,0] }
        self.tbl_val = []       # Table values  { 'Fichier' : [0,0,0], 'Link' : [0,0,0] }

    def __getIdx(self, item, tbl):
        ix = None
        i = item
        while (i):
            try:
                ix = tbl.index( str(i) )
            except AttributeError:
                i = i.parent
        if (not ix):
            raise AttributeError

    def asm(self, item, first = True):
        if (item.type == 'Fichier' or item.type == 'Link'):
            try:
                ir = self.__getIdx(item, self.tbl_row)
                ic = self.__getIdx(item, self.tbl_col)
                self.tbl_val[ir][ic][item.type][0] += item.absent
                self.tbl_val[ir][ic][item.type][1] += item.errors
                self.tbl_val[ir][ic][item.type][2] += item.warnings
            except AttributeError:
                pass
        else:
            for i in item: self.asm(i, False)

        if (first):
            for ir in range(len(self.tbl_val)):
                for cv, ic in self.tbl_col_sum, self.tbl_val[ir]:
                    cv['Fichier'][0]  = ic['Fichier'][0]
                    cv['Fichier'][1]  = ic['Fichier'][1]
                    cv['Fichier'][2]  = ic['Fichier'][2]
                    cv['Link'][0]  = ic['Link'][0]
                    cv['Link'][1]  = ic['Link'][1]
                    cv['Link'][2]  = ic['Link'][2]

    def __xeq_r(self, item, typ, tbl):
        for i in item:
            if (i.type == typ):
                si = str(i)
                if (not si in tbl) : tbl.append(si)
            self.__xeq_r(i, typ, tbl)

    def build(self, head, row_t, col_t):
        self.row_t = row_t
        self.col_t = col_t
        self.tbl_row = []
        self.tbl_col = []
        if (row_t): self.__xeq_r(head, row_t, self.tbl_row)
        if (col_t == 'Global'):
            self.tbl_col.append('Compile')
            self.tbl_col.append('Link')
        else:
            self.__xeq_r(head, col_t, self.tbl_col)
        self.tbl_row.sort()
        self.tbl_col.sort()

        self.tbl_col_sum = []
        for c in self.tbl_col:
            self.tbl_col_sum.append( { 'Fichier' : [0,0,0], 'Link' : [0,0,0] } )      # compil et link
        for r in self.tbl_row:
            self.tbl_val.append( self.tbl_col_sum[:] )

    def get(self, i_row, i_col):
        ir = self.tbl_row.index( str(i_row) )
        ic = self.tbl_col.index( str(i_col) )
        return self.tbl_val[ir][ic]

    def __str__(self):
        return '\n'.join(self.tbl_row) + '\n'.join(self.tbl_col)

class EnteteTable:
    def __imprimeEnteteTable(self, idx, cols, os):
        itms = {}
        for c in cols:
            itm = '::'.join( c.split('::')[0:idx+1] )
            try:
                itms[itm] += 1
            except KeyError:
                itms.setdefault(itm, 1)

        ikeys = itms.keys()
        ikeys.sort()
        for k in ikeys:
            itm = k.split('::')[idx]
            os.write("<th colspan=%d  bgcolor=\"#FFE9BF\">%s" % (itms[k], itm))

    def __call__(self, cols, os):
        nline = len( cols[0].split('::') )
        os.write("<tr>")
        os.write( "<th rowspan=%d align=left bgcolor=\"#FFE9BF\">%s" % (nline, "Fichier") )
        for i in range(nline-2):
            self.__imprimeEnteteTable(i, cols, os)
            os.write("<tr>")
        self.__imprimeEnteteTable(nline-1, cols, os)


class EWBase:
    def __init__(self):
        self.row_t = None
        self.row_v = None
        self.col_t =None
        self.col_v = None
        self.ficHTML = None

    def __trouveTete(self, item):
        ret = None
        if (item.type != self.row_t and item.nom != self.row_v):
            ret = item
        else:
            for i in item: ret = self.trouveTete(i)
        return ret

    def charge(self, path, date, row_t, row_v, col_t, col_v):
        self.row_t = row_t
        self.row_v = row_v
        self.col_t =col_t
        self.col_v = col_v

        # ---  Charge l'infodate
        f = "EW_" + date + ".xml"
        info_date = creeInfoDatedeXML( os.path.normpath(os.path.join(path, f)) )

        # ---  Recherche la tete
        item = self.__trouveTete(info_date)
        return  item


class EWGenerique______ (EWBase):
    def __init__(self):
        EWBase.__init__(self)
        self.dte = None
        self.row_t = None
        self.col_t = None

    def __imprimeUneCellule(self, path, item):
        ferr = item['Fichier']
        dbg.printd("imprimeUnFichier : %s" % (path))
        href = "EW_GenHtmlCmpl.cgi?date=%s&%s=%s" % (self.dte, self.row_t, item)
        self.ficHTML.write("<td align=center " +
                           "bgcolor=" + self.ficHTML.getColor(ferr[0], ferr[1], ferr[2]) +
                           ">" +
                           "<a href=\"%s\">%d - %d - %d</a>" % (href, ferr[0], ferr[1], ferr[2]))

    def imprimeLink(self, cols):
        for i in cols:
            lerr = i['Link']
            self.ficHTML.write("<td align=center " +
                               "bgcolor=" + self.ficHTML.getColor(lerr[0], lerr[1], lerr[2]) +
                               ">" +
                               "%d - %d - %d" % (lerr[0], lerr[1], lerr[2]))

    def __imprimeTable(self, item):
        dbg.printd("imprime:")

        # ---  Monte l'info dans la table
        tbl = Table()
        tbl.build(item, self.row_t, self.col_t)
        tbl.asm(item, 'Fichier')

        # ---  L'entete
        self.ficActu = None
        hd = EnteteTable()(tbl.tbl_col, self.ficHTML)

        # ---  Le résultat de link ? ou  global ?
        self.ficHTML.write("<tr>")
        self.ficHTML.write("<th align=left bgcolor=\"#9f9f9f\">%s" % (item))
        self.imprimeLink(tbl.tbl_col_sum)

        # ---  Les lignes
        for ir in range( len(tbl.tbl_row) ):
            f = tbl.tbl_row[ir]
            href = "EW_GenHtmlCmpl.cgi?date=%s&" % self.dte
            self.ficHTML.write('<tr>')
            self.ficHTML.write('<th align=left><a href=%s>%s</a>' % (href, f.split('::')[-1]) )
            for ic in range( len(tbl.tbl_col) ):
                self.__imprimeUneCellule(f, tbl.tbl_val[ir][ic])


    def __imprimeEntete(self, item):
        # ---  Chemin vers la tete
        path = []
        p = item.parent
        while (p):
            path.append(p)
            p = p.parent
        path.reverse()

        # ---  La banniere
        head = ''
        for p in path:
            head = head + p.nom + ' '
        self.ficHTML.writeHeader( head.strip() )

        #---  L'entete
        self.ficHTML.write("<table cellspacing=20 border=0>")
        self.ficHTML.write("<tr>")
        for p in path:
            self.ficHTML.write("<td><b>%s: %s</b></td>" % (p.type, p.nom))
        self.ficHTML.write("</tr>")
        self.ficHTML.write("</table>")


    def xeq(self, path, dates, date, row_t, row_v, col_t, col_v):
        self.dte = date
        self.row_t = row_t
        self.col_t = col_t

        # ---  Charge les données
        item = self.charge(path, date, row_t, row_v, col_t, col_v)

        # ---  Génere le fichier HTML
        self.ficHTML = EWHtmlFile()
        self.__imprimeEntete(item)

        self.ficHTML.write("<center><table cellspacing=2 width=\"95%\" border=1>")
        self.__imprimeTable(item)
        self.ficHTML.write("</table></center>")
        self.ficHTML.writeFooter()

class EWUneDateUnModuleUneVersionToutesPlateformesToutesConfigs(EWBase):
    def __init__(self, date, pkg, version, module, platform, config):
        EWBase.__init__(self, date, pkg, version, module, platform, config)

    def imprimeUnFichier(self, path, err):
        dbg.printd("imprimeUnFichier : %s" % (path))
        href = "EW_GenHtmlCmpl.cgi?full=no&file=%s" % path
        self.ficHTML.write("<td align=center " +
                           "bgcolor=" + self.ficHTML.getColor(err[0], err[1], err[2]) +
                           ">" +
                           "<a href=\"%s\">%d - %d - %d</a>" % (href, err[0], err[1], err[2]))

    def imprimeUnLink(self, item):
        if (item == None):
            self.ficHTML.write("<td>")
        else:
            if (item.errors < 0):
                self.ficHTML.write("<td align=center " +
                                   "bgcolor=" + self.ficHTML.getColor(item.absent, item.errors, item.warnings) +
                                   ">-----")
            else:
                href = "EW_GenHtmlLink.cgi?full=no&file=%s" % item.path
                self.ficHTML.write("<td align=center " +
                               "bgcolor=" + self.ficHTML.getColor(item.absent, item.errors, item.warnings) +
                               ">" +
                               "<a href=\"%s\">%d - %d - %d</a>" % (href, item.absent, item.errors, item.warnings))

    def imprimeLink(self, item):
        dbg.printd("imprimeLink: %s %s" % (item.type, item.nom))
        if (item.type == 'Date' and item.nom != self.dte): return
        if (item.type == 'Package' and item.nom != self.pkg): return
        if (item.type == 'Version' and item.nom != self.ver): return
        if (item.type == 'Module' and item.nom != self.mdl): return
        if (item.type == 'Link'):
            self.imprimeUnLink(item)
        else:
            for i in item: self.imprimeLink(i)

    def imprime(self, item):
        dbg.printd("imprime:")

        # ---  Monte l'info dans la table
        tbl = Table()
        tbl.build(item, 'Fichier', 'TypeBuild')
        tbl.asm(item, 'Fichier')

        # ---  L'entete
        self.ficActu = None
        hd = EnteteTable()(tbl.tbl_col, self.ficHTML)

        # ---  Le résultat de link
        self.ficHTML.write("<tr>")
        self.ficHTML.write("<th align=left bgcolor=\"#9f9f9f\">%s" % (self.mdl))
        self.imprimeLink(item)

        # ---  Les fichiers
        for ir in range( len(tbl.tbl_row) ):
            f = tbl.tbl_row[ir]
            self.ficHTML.write("<tr>")
            self.ficHTML.write("<th align=left>%s" % os.path.splitext(f)[0])
            for ic in range( len(tbl.tbl_col) ):
                self.imprimeUnFichier(f, tbl.tbl_val[ir][ic])

    def xeq(self, dates, versions, path):
        # ---  Charge les données
        item = self.load(dates, version, path)

        # ---  Génere le fichier HTML
        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("%s %s %s" % (self.dte, self.mdl, self.ver))
        self.ficHTML.write("<table cellspacing=20 border=0>")
        self.ficHTML.write("<tr>")
        self.ficHTML.write("<td><b>Date: %s</b></td>" % self.dte)
        self.ficHTML.write("<td><b>Package: %s</b></td>" % self.pkg)
        self.ficHTML.write("<td><b>Module: %s</b></td>" % self.mdl)
        self.ficHTML.write("<td><b>Version: %s</b></td>" % self.ver)
        self.ficHTML.write("</tr></table>")
        self.ficHTML.write("<center><table cellspacing=2 width=\"95%\" border=1>")
        self.imprime(item)
        self.ficHTML.write("</table></center>")
        self.ficHTML.writeFooter()



class EWUneDateTousModulesToutesVersionsToutesPlateformesToutesConfigs(EWBase):
    def __init__(self, date, pkg, version, module, platform, config):
        EWBase.__init__(self, date, pkg, version, module, platform, config)

    def assembleFichier(self, item):
        dbg.printd("assembleFichier : %s  %s" % (item.type, item.nom))
        if (item.type != 'Fichier'): return
        cmplr = '%s' % item.parent.compiler
        if not self.dico_ew.has_key(cmplr):
            self.dico_ew.setdefault(cmplr, len(self.lst_cpl_ew))
            self.lst_cpl_ew.append([0, 0, 0])
            self.lst_lnk_ew.append([0, 0, 0])
        idx = self.dico_ew[cmplr]
        self.lst_cpl_ew[idx][0] = self.lst_cpl_ew[idx][0] + item.absent
        self.lst_cpl_ew[idx][1] = self.lst_cpl_ew[idx][1] + item.errors
        self.lst_cpl_ew[idx][2] = self.lst_cpl_ew[idx][2] + item.warnings
        self.sum_cpl_ew[0] = self.sum_cpl_ew[0] + item.absent
        self.sum_cpl_ew[1] = self.sum_cpl_ew[1] + item.errors
        self.sum_cpl_ew[2] = self.sum_cpl_ew[2] + item.warnings

    def assembleLink(self, item):
        dbg.printd("assembleLink : %s  %s" % (item.type, item.nom))
        if (item.type != 'TypeLink'): return
        if (item == None): return
        cmplr = '%s' % item.parent.compiler
        if not self.dico_ew.has_key(cmplr):
            self.dico_ew.setdefault(cmplr, len(self.lst_cpl_ew))
            self.lst_cpl_ew.append([0, 0, 0])
            self.lst_lnk_ew.append([0, 0, 0])
        idx = self.dico_ew[cmplr]
        self.lst_lnk_ew[idx][0] = self.lst_lnk_ew[idx][0] + item.absent
        self.lst_lnk_ew[idx][1] = self.lst_lnk_ew[idx][1] + item.errors
        self.lst_lnk_ew[idx][2] = self.lst_lnk_ew[idx][2] + item.warnings
        self.sum_lnk_ew[0] = self.sum_lnk_ew[0] + item.absent
        self.sum_lnk_ew[1] = self.sum_lnk_ew[1] + item.errors
        self.sum_lnk_ew[2] = self.sum_lnk_ew[2] + item.warnings

    def assemble(self, item):
        dbg.printd("assemble: %s %s" % (item.type, item.nom))
        if (item.type == 'Fichier'):
            self.assembleFichier(item)
        elif (item.type == 'TypeLink'):
            self.assembleLink(item)
        else:
            for i in item: self.assemble(i)

    def imprimeEnteteTable(self):
        self.ficHTML.write("<tr>")
        self.ficHTML.write("<th align=left>%s" % "Module")
        self.ficHTML.write("<th>%s" % "Compilation")
        self.ficHTML.write("<th>%s" % "Link")
        #self.ficHTML.write("<th>%s" % "Dep")

    def imprimeVersion(self, item):
        dbg.printd("imprimeVersion : %s %s" % (item.type, item.nom))
        if (item.type != 'Version'): return
        self.dico_ew = {}
        self.lst_cpl_ew = []
        self.lst_lnk_ew = []
        self.sum_cpl_ew = [0, 0, 0]
        self.sum_lnk_ew = [0, 0, 0]
        self.assemble(item)
        href = "EW_GenHtmlErWr.cgi?pkg=%s&dte=%s&mdl=%s&vrs=%s" % (self.pkg, self.date, self.mdl_actu, item.nom)
        if (len(self.lst_cpl_ew) > 0):
            self.ficHTML.write("<td align=center width=89 " +
                               "background=\"" + self.ficHTML.getBgColor(self.lst_cpl_ew) + "\"" +
                               ">" +
                               "<a href=\"%s\">%d : %d</a>" % (href, self.sum_cpl_ew[1], self.sum_cpl_ew[2]))
        else:
            self.ficHTML.write("<td >")
        if (len(self.lst_lnk_ew) > 0):
            self.ficHTML.write("<td align=center width=89 " +
                               "background=\"" + self.ficHTML.getBgColor(self.lst_lnk_ew) + "\"" +
                               ">" +
                               "<a href=\"%s\">%d : %d : %d</a>" % (href, self.sum_lnk_ew[0], self.sum_lnk_ew[1], self.sum_lnk_ew[2]))
        else:
            self.ficHTML.write("<td >")

    def imprimeModule(self, item):
        dbg.printd("imprimeModule: %s" % item.nom)
        if (item.type != 'Module'): return
        nVer = 0
        for i in item:
            if (i.type == 'Module'): self.imprimeModule(i)
            if (i.type == 'Version'): nVer += 1
        if (nVer > 0):
            self.mdl_actu = item.nom
            self.ficHTML.write("<tr>")
            self.ficHTML.write("<th align=left>%s" % item.nom)
            for i in item: self.imprimeVersion(i)

    def imprime(self, item):
        dbg.printd("imprimeDate")

        # ---  Monte l'info dans la table
        tbl = Table()
        tbl.build(item, 'Module', 'Version')
        tbl.asm(item, 'Fichier')
        print tbl

        self.ficHTML.write("<hr><b>Date: %s</b>" % self.dte)
        self.ficHTML.write("<br><br><center><table cellspacing=2 width=\"95%\" border=1>")
        self.imprimeEnteteTable()
        for i in item: self.imprimeModule(i)
        self.ficHTML.write("</table></center>")

    def xeq(self, dates, versions, path):
        # ---  Charge les données
        item = self.load(dates, version, path)

        # ---  Génère le fichier HTML
        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Rapport complet: %s %s %s  Logiciel: %s" % (self.dte, "", "", self.pkg))
        menu = EWMenu()
        menu.xeq(self.ficHTML, self.pkg, dates, versions)
        self.ficHTML.write("<br>")
        self.imprime(item)
        self.ficHTML.writeFooter()


class EW_Global_UneDateTousModulesUneVersionToutesPlateformesToutesConfigs:
    def __init__(self):
        self.os = None

    def assembleFichier(self, item):
        dbg.printd("assembleFichier : %s  %s" % (item.type, item.nom))
        if (item.type != 'Fichier'): return
        cmplr = '%s' % item.parent.compiler
        if not self.dico_ew.has_key(cmplr):
            self.dico_ew.setdefault(cmplr, len(self.lst_cpl_ew))
            self.lst_cpl_ew.append([0, 0, 0])
            self.lst_lnk_ew.append([0, 0, 0])
        idx = self.dico_ew[cmplr]
        self.lst_cpl_ew[idx][0] = self.lst_cpl_ew[idx][0] + item.absent
        self.lst_cpl_ew[idx][1] = self.lst_cpl_ew[idx][1] + item.errors
        self.lst_cpl_ew[idx][2] = self.lst_cpl_ew[idx][2] + item.warnings
        self.sum_cpl_ew[0] = self.sum_cpl_ew[0] + item.absent
        self.sum_cpl_ew[1] = self.sum_cpl_ew[1] + item.errors
        self.sum_cpl_ew[2] = self.sum_cpl_ew[2] + item.warnings

    def assembleLink(self, item):
        dbg.printd("assembleLink : %s  %s" % (item.type, item.nom))
        if (item.type != 'TypeLink'): return
        if (item == None): return
        cmplr = '%s' % item.parent.compiler
        if not self.dico_ew.has_key(cmplr):
            self.dico_ew.setdefault(cmplr, len(self.lst_cpl_ew))
            self.lst_cpl_ew.append([0, 0, 0])
            self.lst_lnk_ew.append([0, 0, 0])
        idx = self.dico_ew[cmplr]
        self.lst_lnk_ew[idx][0] = self.lst_lnk_ew[idx][0] + item.absent
        self.lst_lnk_ew[idx][1] = self.lst_lnk_ew[idx][1] + item.errors
        self.lst_lnk_ew[idx][2] = self.lst_lnk_ew[idx][2] + item.warnings
        self.sum_lnk_ew[0] = self.sum_lnk_ew[0] + item.absent
        self.sum_lnk_ew[1] = self.sum_lnk_ew[1] + item.errors
        self.sum_lnk_ew[2] = self.sum_lnk_ew[2] + item.warnings

    def assemble(self, item):
        dbg.printd("assemble: %s %s" % (item.type, item.nom))
        if (item.type == 'Fichier'):
            self.assembleFichier(item)
        elif (item.type == 'TypeLink'):
            self.assembleLink(item)
        else:
            for i in item: self.assemble(i)

    def imprimeEnteteTable(self):
        self.ficHTML.write("<tr>")
        self.ficHTML.write("<th align=left bgcolor=\"#FFE9BF\">%s" % "Module")
        self.ficHTML.write("<th bgcolor=\"#FFE9BF\">%s" % "Compilation")
        self.ficHTML.write("<th bgcolor=\"#FFE9BF\">%s" % "Link")
        #self.ficHTML.write("<th>%s" % "Dep")

    def imprimeVersion(self, item):
        dbg.printd("imprimeVersion : %s %s" % (item.type, item.nom))
        if (item.type != 'Version'): return
        if (item.nom != self.version): return
        self.dico_ew = {}
        self.lst_cpl_ew = []
        self.lst_lnk_ew = []
        self.sum_cpl_ew = [0, 0, 0]
        self.sum_lnk_ew = [0, 0, 0]
        self.assemble(item)
        href = "EW_GenHtmlErWr.cgi?pkg=%s&dte=%s&mdl=%s&vrs=%s" % (self.pkg, self.date, self.mdl_actu, item.nom)
        if (len(self.lst_cpl_ew) > 0):
            self.ficHTML.write("<td align=center width=89 " +
                               "background=\"" + self.ficHTML.getBgColor(self.lst_cpl_ew) + "\"" +
                               ">" +
                               "<a href=\"%s\">%d : %d</a>" % (href, self.sum_cpl_ew[1], self.sum_cpl_ew[2]))
        else:
            self.ficHTML.write("<td >")
        if (len(self.lst_lnk_ew) > 0):
            self.ficHTML.write("<td align=center width=89 " +
                               "background=\"" + self.ficHTML.getBgColor(self.lst_lnk_ew) + "\"" +
                               ">" +
                               "<a href=\"%s\">%d : %d : %d</a>" % (href, self.sum_lnk_ew[0], self.sum_lnk_ew[1], self.sum_lnk_ew[2]))
        else:
            self.ficHTML.write("<td >")

    def imprimeModule(self, item):
        dbg.printd("imprimeModule: %s %s" % (item.type, item.nom))
        if (item.type != 'Module'): return
        nVer = 0
        for i in item:
            if (i.type == 'Module'): self.imprimeModule(i)
            if (i.type == 'Version'): nVer += 1
        if (nVer > 0):
            self.mdl_actu = item.nom
            self.ficHTML.write("<tr> </tr>")
            self.ficHTML.write("<tr>")
            self.ficHTML.write("<th align=left>%s" % item.nom)
            for i in item: self.imprimeVersion(i)

    def imprimeDate(self, item):
        dbg.printd("imprimeDate: %s %s" % (item.type, item.nom))
        if (item.type != 'Date'): return
        self.date = item.nom
        self.ficHTML.write("<hr><b>Date: %s</b>" % self.date)
        self.ficHTML.write("<br><br><center><table cellspacing=2 width=\"95%\" border=1>")
        self.imprimeEnteteTable()
        for i in item: self.imprimeModule(i)
        self.ficHTML.write("</table></center>")
        self.ficHTML.writeFooter()

    def xeq(self, ficHTML, pkg, date, ver):
        self.ficHTML = ficHTML
        self.pkg = pkg
        self.version = ver
        self.imprimeDate(date)


class EW_UneDateTousModulesUneVersionToutesPlateformesToutesConfigs:
    def __init__(self, date, pkg, version, module, platform, config):
        self.pkg = pkg
        self.dte = date
        self.ver = version
        self.ficHTML = None

    def xeq(self, dates, versions, path):

        # ---  Charge l'infodate
        f = "EW_" + self.dte + ".xml"
        e = os.path.join(self.pkg, f)
        info_date = creeInfoDatedeXML(os.path.join(path, e))

        # ---  Génère le fichier HTML
        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Rapport complet: %s %s %s  Logiciel: %s" % (self.dte, "", "", self.pkg))
        menu = EWMenu()
        menu.xeq(self.ficHTML, self.pkg, dates, versions)
        self.ficHTML.write("<br>")
        comp = EW_Global_UneDateTousModulesUneVersionToutesPlateformesToutesConfigs()
        comp.xeq(self.ficHTML, self.pkg, info_date, self.ver)


class EWMenu:
    def __init__(self):
        pass

    def imprimeMenu(self, os, pkg, dates, versions):
        os.write("<table>")

        os.write("<tr>")
        os.write("<td align=left>")
        adresse = "http://www.intranet.gre-ehn.inrs-ete.uquebec.ca/"
        os.write("<a href= %s > %s </a>" % (adresse, "Page d'accueil"))
        os.write("</td>")

        os.write("<td>")
        os.write("<form method=get action=\"EW_GenHtmlErWr.cgi\">")
        os.write("<input type=hidden name=\"pkg\" value=\"%s\">" % pkg)
        os.write("<select name=\"dte\">")
        for d in dates:
            os.write("<option value=\"%s\">%s" % (d, d))
        os.write("</select>")
        os.write("<input type=submit value=\"Date\">")

        os.write("<select name=\"vrs\">")
        for d in versions:
            os.write("<option value=\"%s\">%s" % (d, d))
        os.write("</select>")
        os.write("<input type=submit value=\"version\">")
        os.write("</form>")
        os.write("</td>")


        os.write("</table>")

    def xeq(self, os, pkg, dates, versions):
        self.imprimeMenu(os, pkg, dates, versions)


if __name__ == '__main__':
    dbg.on = 1      # Debug off

    if (dbg.on):
        if (not rpdb2_imported):
            raise 'rpdb2 must be installed to debug'
        else:
            rpdb2.start_embedded_debugger("inrs_iehss", fAllowUnencrypted = True)

    date = "00000000-000000"
    version = "derniere"
    package = "H2D2"
    #algo = EWUneDateUnPckTousModulesToutesVersionsToutesPlateformesToutesConfigs(date, package, "*", "*", "*", "*")
    #algo = EWUneDateTousModulesToutesVersionsToutesPlateformesToutesConfigs(date, package, "*", "*", "*", "*")
    #algo = EWUneDateTousModulesUneVersionToutesPlateformesToutesConfigs(date, package, version, "*", "*", "*")
    #algo = EWUneDateUnModuleUneVersionToutesPlateformesToutesConfigs(date, package, version, "h2d2", "*", "*")
    #algo.xeq([date], [version], "..") #"..\..\logs")

    algo = EWGenerique______()
    algo.xeq('../H2D2', [], date, 'Package', 'H2D2', 'Global', '')
