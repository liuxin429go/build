# -*- coding: UTF-8 -*-
import ExternalLib
import Util
import os

class cuda(ExternalLib.ExternalLib):
    def __unx32(self, ctx):
        pass

    def __unx64(self, ctx):
        pass

    def __unx64i8(self, ctx):
        pass

    def __win32(self, ctx):
        pass

    def __win64(self, ctx):
        hdir = os.path.join( os.environ["ProgramFiles"], 'NVIDIA GPU Computing Toolkit', 'CUDA' )
        hdir = Util.getPathFromVersion(hdir, 'v', search_join='')
        __inc = [ os.path.join(hdir, 'include') ]
        __lpt = [ os.path.join(hdir, 'lib', 'x64') ]
        __lib = ['cuda']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def __win64i8(self, ctx):
        pass

    def configure(self, ctx, tool, version, plateform):
        fnm = '_cuda__%s' % (plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
