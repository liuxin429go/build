# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import os

class scotch_common(ExternalLib.ExternalLib):
    def go(self, ctx):
        hdir = os.environ['INRS_LXT']
        __scotch_dir = Util.getPathFromVersion(hdir, 'scotch', search_join='_')
        hdir = os.path.join(__scotch_dir, r'_run', Util.addBuildPath(r'External'))

        __inc = [ hdir ]
        __lpt = [ hdir ]
        __lib = [ ]
        return __lpt, __lib, __inc

    def cmc_unx64(self, ctx):
        self.go_unx(ctx)

    def cmc_unx32(self, ctx):
        self.go_unx(ctx)

    def cmcitl_unx64(self, ctx):
        self.go_unx(ctx)

    def cmcitl_unx32(self, ctx):
        self.go_unx(ctx)

    def cmcpgi_unx64(self, ctx):
        self.go_unx(ctx)

    def cmcpgi_unx32(self, ctx):
        self.go_unx(ctx)

    #---------------
    #---    Linux 64
    #---------------
    def gcc_unx64(self, ctx):
        self.go_unx(ctx)

    def gccX64_unx64(self, ctx):
        self.go_unx(ctx)

    def intel_unx64(self, ctx):
        self.go_unx(ctx)

    def itlX64_unx64(self, ctx):
        self.go_unx(ctx)

    def open64_unx64(self, ctx):
        self.go_unx(ctx)

    def sun_unx64(self, ctx):
        self.go_unx(ctx)

    #---------------
    #---    Linux 64i8
    #---------------
    def gcc_unx64i8(self, ctx):
        self.go_unx(ctx)

    def gccX64_unx64i8(self, ctx):
        self.go_unx(ctx)

    def intel_unx64i8(self, ctx):
        self.go_unx(ctx)

    def itlX64_unx64i8(self, ctx):
        self.go_unx(ctx)

    def open64_unx64i8(self, ctx):
        self.go_unx(ctx)

    def sun_unx64i8(self, ctx):
        self.go_unx(ctx)

    #---------------
    #---    Linux 32
    #---------------
    def gcc_unx32(self, ctx):
        self.go_unx(ctx)

    def gccX86_unx32(self, ctx):
        self.go_unx(ctx)

    def intel_unx32(self, ctx):
        self.go_unx(ctx)

    def itlX86_unx32(self, ctx):
        self.go_unx(ctx)

    def sun_unx32(self, ctx):
        self.go_unx(ctx)

    #-----------------
    #---    Windows 32
    #-----------------
    def gcc_win32(self, ctx):
        self.go_win(ctx)

    def gccX86_win32(self, ctx):
        self.go_win(ctx)

    def intel_win32(self, ctx):
        self.go_win(ctx)

    def itlX86_win32(self, ctx):
        self.go_win(ctx)

    #-----------------
    #---    Windows 64
    #-----------------
    def gcc_win64(self, ctx):
        self.go_win(ctx)

    def gccX64_win64(self, ctx):
        self.go_win(ctx)

    def intel_win64(self, ctx):
        self.go_win(ctx)

    def itlX64_win64(self, ctx):
        self.go_win(ctx)

    def itlI64_win64(self, ctx):
        self.go_win(ctx)

    #-----------------
    #---    Windows 64 i8
    #-----------------
    def gcc_win64i8(self, ctx):
        self.go_win(ctx)

    def gccX64_win64i8(self, ctx):
        self.go_win(ctx)

    def intel_win64i8(self, ctx):
        self.go_win(ctx)

    def itlX64_win64i8(self, ctx):
        self.go_win(ctx)

    def itlI64_win64i8(self, ctx):
        self.go_win(ctx)

    #-----------------
    #---    Autres
    #-----------------
    def ftnchek_win32(self, ctx):
        raise SCons.Errors.UserError('scotch.ftncheck_win32 not defined')

    def configure(self, ctx, tool, version, plateform):
        fnm = '%s_%s' % (tool, plateform)
        fnc = getattr(self, fnm)
        fnc(ctx)

class scotch(scotch_common):
    def go_unx(self, ctx):
        __lpt, __lib, __inc = self.go(ctx)
        __lib.extend( [ 'esmumps', 'scotch', 'scotcherr' ] )
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def go_win(self, ctx):
        __lpt, __lib, __inc = self.go(ctx)
        __lib.extend( [ 'libesmumps', 'libscotch', 'libscotcherr', 'libscotcherrexit' ] )
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

class ptscotch(scotch_common):
    def go_unx(self, ctx):
        __lpt, __lib, __inc = self.go(ctx)
        __lib.extend( [ 'ptesmumps', 'ptscotch', 'ptscotcherr' ] )
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def go_win(self, ctx):
        __lpt, __lib, __inc = self.go(ctx)
        __lib.extend( [ 'libptesmumps', 'libptscotch', 'libptscotcherr', 'libptscotcherrexit' ] )
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)
