#! /bin/bash
# Setup

DIR_INP=""
DIR_OUT=""
DIR_BLD=""
FIC_PXD=""
FIC_STP=""

# ---  Execute main
#echo compile_cython Version 19.04a
#echo

#-----------------------------------------------------------------------------
function err_argument
{
    echo Error: "$1" needs a corresponding ".setup.py" file
    echo Error: Input param
    echo Error:    PTH_PXD: $PTH_PXD
    echo Error:    DIR_OUT: $DIR_OUT
    echo Error:    DIR_BLD: $DIR_BLD
    echo Error: Variables
    echo Error:    DIR_INP: $DIR_INP
    echo Error:    FIC_PXD: $FIC_PXD
    echo Error:    FIC_STP: $FIC_STP
}

function parse_args
{
    # ---  Input param
    PTH_PXD=$1
    if [ -z $2 ]; then DIR_OUT="."; else DIR_OUT=$(readlink -f $2); fi
    if [ -z $3 ]; then DIR_BLD="__cython-build"; else DIR_BLD=$(readlink -f $3); fi

    # ---  Transform .pxd in .setup.py
    DIR_INP=$(dirname  -- $1)
    FIC_PXD=$(basename -- $1)
    FIC_STP="${FIC_PXD%.*}.setup.py"
}

function compile
{
    # ---  Compile to pyd
    pushd $DIR_INP > /dev/null
    # cython 0.28.2
    # cython-c-in-temp pyrex-c-in-temp font que la cython ne prend pas
    # en compte le fichier .pxd et la compilation n'est pas optimisée.
    CYTHON_CMD="python $FIC_STP build_ext --build-lib=$DIR_OUT --build-temp=$DIR_BLD" # --cython-c-in-temp --pyrex-c-in-temp"
    $CYTHON_CMD
    popd > /dev/null
}

function main
{
    # ---  Parse les arguments
    parse_args $*
    
    # ---  Controls
    if [ ! -e $DIR_INP/$FIC_STP ]; then 
        err_argument $*
        exit
    fi

    # ---  Compile
    compile
}

main $*

