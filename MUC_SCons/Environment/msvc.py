# -*- coding: UTF-8 -*-
import SCons.Script

from . import Environment
import sys

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

class EnvironmentMsvc(MUCEnv):
    def __init__(self, tool, version, opts, *args, **kwargs):
        plateform = 'win32'
        tools =  ['msvc', 'mslib', 'mslink', 'msvs']
        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform = plateform,
                           MSVS_VERSION = version,
                           MUC_TARGETPLATEFORM = 'win32')
        MUCEnv.__init__(self, tool, version, *args, **kwargs)

        self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/NOLOGO']
        self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/SUBSYSTEM:CONSOLE']
        self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/INCREMENTAL:NO']

    def setupEnv(self, ctx):
        MUCEnv.setupEnv(self, ctx)
        if (ctx.bld == 'debug'):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/DEBUG']
