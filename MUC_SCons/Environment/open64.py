# -*- coding: UTF-8 -*-
import SCons.Script

from . import Environment
import MUC_platform
import os

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

class EnvironmentOpen64(MUCEnv):
    def __init__(self, tool, version, opts, *args, **kwargs):
        if (MUC_platform.is_win()):
            raise SCons.Errors.UserError('Invalid Open64 platform: %s' % MUC_platform.build_platform())
            muc_target_plateform = MUC_platform.build_platform()
            plateform = muc_target_plateform
        elif (MUC_platform.is_unx()):
            tools =  ['cc', 'c++', 'f90', 'link', 'ar']
            muc_target_plateform = MUC_platform.build_platform()
            plateform = muc_target_plateform
        else:
            raise SCons.Errors.UserError('Invalid Open64 platform: %s' % MUC_platform.build_platform())

        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform = plateform,
                           MUC_TARGETPLATEFORM = muc_target_plateform)
        MUCEnv.__init__(self, tool, version, *args, **kwargs)

        def __findBinPath(__pth):
            ver = None
            try:
                for d in os.listdir(__pth):
                    __fullPath = os.path.join(__pth, d)
                    if (os.path.isdir(__fullPath)):
                        __fullPath = os.path.join(__fullPath, 'bin/openf90')
                        if (os.path.exists(__fullPath) and d > ver):
                            ver = d
            except:
                pass
            return os.path.join(__pth, ver, 'bin/openf90')
        try:
            self.binPath = os.path.dirname( __findBinPath('/opt/open64') )
        except:
            raise SCons.Errors.UserError('Invalid Open64 platform: %s' % MUC_platform.build_platform())

    def setupEnv(self, ctx):
        if (MUC_platform.is_win()):
            self.__setupEnv_win(ctx)
        elif (MUC_platform.is_unx()):
            self.__setupEnv_unx(ctx)
        else:
            raise SCons.Errors.UserError('Invalid Open64 platform: %s' % MUC_platform.build_platform())

        if (ctx.bld == 'profil'):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-pg']
        MUCEnv.setupEnv(self, ctx)

    def __setupEnv_unx(self, ctx):
        if (self['docpplink']):
            self['LINK'] = os.path.join(self.binPath, 'openCC')
        else:
            self['LINK'] = os.path.join(self.binPath, 'openf95')

        if (MUC_platform.is_unx32()):   # devrait être target_platform
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-m32']
        elif (MUC_platform.is_unx64()):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-m64']
            self.PrependENVPath('LIBRARY_PATH', '/usr/lib/x86_64-linux-gnu')

        if (True):                                                   # (ctx.bld == 'release'): pas juste
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-openmp']  # en release car acml demande openmp
        if (ctx.lnk == 'static'):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-static']

        syslib = ['stdc++']
        for l in syslib:
            try:
                ctx.sys.index(l)
            except:
                ctx.sys.append(l)

    def __setupEnv_win(self, ctx):
        raise SCons.Errors.UserError('Invalid Open64 platform: %s' % MUC_platform.build_platform())

