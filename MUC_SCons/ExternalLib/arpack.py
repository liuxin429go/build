# -*- coding: UTF-8 -*-
import ExternalLib
import Util
import os

class arpack(ExternalLib.ExternalLib):
    def __cnf_sta(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'ARPACK/_run', Util.addBuildPath(r'External'))
        hdir = os.path.normpath(hdir)
        __lpt = [ hdir ]
        __lib = ['arpack', 'arlapack', 'arutil']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def configure(self, ctx, tool, version, plateform):
        self.__cnf_sta(ctx)

