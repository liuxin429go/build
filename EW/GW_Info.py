# -*- coding: UTF-8 -*-
import os
import sys
import time

from EW_HtmlCmpl import EWHtml_AlgoSummaryLog
from EW_Debug import dbg
from EW_Util import EWHtmlFile

class GWHtml_AlgoAllCompilerLog:
    def __init__(self):
        self.pkg = ""
        self.date = ""
        self.pltfrm = ""
        self.mpi = ""
        self.cmplr  = ""
        self.link   = ""
        self.config = ""
        self.computer = ""
        self.pkgActif = False
        self.ptfActif = False
        self.mpiActif = False
        self.cplActif = False
        self.lnkActif = False
        self.cnfActif = False
        self.info = []

    def xtrDir(self, path):
        for f in os.listdir(path):
            if (f == self.pkg): self.pkgActif = True
            if (f == self.pltfrm): self.ptfActif = True
            if (f == self.mpi): self.mpiActif = True
            if (f == self.cmplr): self.cplActif = True
            if (f == self.link): self.lnkActif = True
            if (f == self.config): self.cnfActif = True
            fullPath = os.path.join(path,f)
            if os.path.isdir(fullPath):
                self.xtrDir(fullPath)
            elif (self.ptfActif and self.mpiActif and self.cplActif and self.lnkActif and self.cnfActif):
                self.xtrFile(fullPath, f)
            if (f == self.pkg): self.pkgActif = False
            if (f == self.pltfrm): self.ptfActif = False
            if (f == self.mpi):    self.mpiActif = False
            if (f == self.cmplr): self.cplActif = False
            if (f == self.link): self.lnkActif = False
            if (f == self.config): self.cnfActif = False

    def xtrFile(self, path, file):
        if (os.path.splitext(file)[1] == ".err"):
            dbg.printd(":: %s" % path)
            #href = "EW_GenHtmlCmpl.cgi?full=yes&file=%s" % path
            #self.ficHTML.write("<br><a href=\"%s\">%s</a><br>" % (href, file))
            algo = EWHtml_AlgoSummaryLog()
            algo(path, self.ficHtml)

    def __call__(self, ficHtml, pkg, date, pltf, mpi, cmplr, link, config, path):
        self.ficHtml = ficHtml

        self.pkg    = pkg
        self.date   = date
        self.pltfrm = pltf
        self.mpi    = mpi
        self.cmplr  = cmplr
        self.link   = link
        self.config = config
        self.computer = os.getenv("COMPUTERNAME", "")

        self.pkgActif = False
        self.ptfActif = False
        self.mpiActif = False
        self.cplActif = False
        self.lnkActif = False
        self.cnfActif = False

        self.xtrDir(path)


class GWHtml_AllCompilerLog:
    def __init__(self):
        pass

    def xeq(self, pkg, pltf, mpi, cmplr, link, config, path):
        date = time.strftime("%Y%m%d-%H%M%S")

        nomFic = "GW_%s_%s_%s_%s_%s_%s_%s.%s" %(pkg, date, pltf, mpi, cmplr, link, config, "htm")
        fic = open(nomFic, "wt")
        sys.stdout = fic           # redirect stdout

        self.ficHTML = EWHtmlFile()

        algo = GWHtml_AlgoAllCompilerLog()
        algo(self.ficHTML, pkg, date, pltf, mpi, cmplr, link, config, path)


if __name__ == '__main__':
    if (len(sys.argv) > 3):
        path   = sys.argv[1]
        pkg    = sys.argv[2]
        pltf   = sys.argv[3]
        mpi    = sys.argv[4]
        cmplr  = sys.argv[5]
        link   = sys.argv[6]
        config = sys.argv[7]
    else:
        path = r"_bin"
        pkg  = "H2D2"
        pltf = "win32"
        mpi  = "deino_mpi"
        cmplr = "gcc-xx"
        link  = "dynamic"
        config= "debug"

    dbg.on = 1      # Debug off
    date = GWHtml_AllCompilerLog()
    date.xeq(pkg, pltf, mpi, cmplr, link, config, path)
