#!/usr/bin/env python

import logging
import os
import platform
import re
import sys
try:
    import hashlib
    hashlib_md5 = hashlib.md5
except ImportError:
    # for Python << 2.5
    import md5
    hashlib_md5 = md5.new
import subprocess

class CIError(Exception):
    pass


def _get_env():
    return ['INRS_DEV', 'INRS_BLD', 'INRS_LXT']

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def getlogger(name):
    logger = logging.getLogger(name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(levelname)s: %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    return logger

#*******************************************************************************
# Summary:
#
# Description:
#   Verifies that needed environment variables are defined and valid.
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def check_env():
    for var in _get_env():
        try:
            path = os.environ[var]
        except KeyError:
            raise CIError('%s undefined' % var)
        path = os.path.abspath(path)
        if not os.path.isdir(path):
            msg = '%s does not define a valid directory: %s' % (var, path)
            raise CIError(msg)
        else:
            os.environ[var] = path

#*******************************************************************************
# Summary:
#
# Description:
#   Verifies that needed environment variables are defined and valid.
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
if not hasattr(os.path, 'relpath'):
    def relpath(path, start=os.curdir):
        """Return a relative version of a path"""

        if not path:
            raise ValueError("no path specified")

        def _abspath_split(path):
            abs = os.path.abspath(os.path.normpath(path))
            if (platform.system() == 'Windows'):
                prefix, rest = os.path.splitunc(abs)
            else:
                prefix, rest = ('', abs)
            is_unc = bool(prefix)
            if not is_unc:
                prefix, rest = os.path.splitdrive(abs)
            return is_unc, prefix, [x for x in rest.split(os.sep) if x]

        start_is_unc, start_prefix, start_list = _abspath_split(start)
        path_is_unc,  path_prefix,  path_list  = _abspath_split(path)

        if path_is_unc ^ start_is_unc:
            raise ValueError("Cannot mix UNC and non-UNC paths (%s and %s)"
                                                                % (path, start))
        if path_prefix.lower() != start_prefix.lower():
            if path_is_unc:
                raise ValueError("path is on UNC root %s, start on UNC root %s"
                                                    % (path_prefix, start_prefix))
            else:
                raise ValueError("path is on drive %s, start on drive %s"
                                                    % (path_prefix, start_prefix))
        # Work out how much of the filepath is shared by start and path.
        i = 0
        for e1, e2 in zip(start_list, path_list):
#            if e1.lower() != e2.lower():
            if e1 != e2:
                break
            i += 1

        rel_list = [os.pardir] * (len(start_list)-i) + path_list[i:]
        if not rel_list:
            return os.curdir
        return os.path.join(*rel_list)
else:
    relpath = os.path.relpath

#*******************************************************************************
# Summary: Truncates specified path in order to display it.
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def trunc(path):
    for var in _get_env():
        basepath = os.environ[var]
        if path == basepath:
            path = '[%s]' % var
        elif path.startswith(basepath):
            path = '[%s]%s'  % (var, path[len(basepath):])
    return path

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def getpaths(search_root, *search_parts):

    paths = []

    if search_parts:
        for p in search_parts[:-1]:
            search_root = os.path.join(search_root, p)
            if not os.path.isdir(search_root):
                raise CIError('invalid path: %s' % trunc(search_root))
        regex = '^%s$' % search_parts[-1]
        for f in os.listdir(search_root):
            if re.search(regex, f):
                paths.append(os.path.join(search_root, f))
    elif os.path.exists(search_root):
        paths.append(search_root)

    return paths

#*******************************************************************************
# Summary:
#
# Description:
#   Returns the path for specified file, folder or regular expression.
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def getpath(search_root, *search_parts):

    paths = getpaths(search_root, *search_parts)

    if len(paths) == 1:
        path = paths[0]
    else:
        searched_path = search_root
        for p in search_parts:
            searched_path = os.path.join(searched_path, p)
        searched_path = trunc(searched_path)
        if not paths:
            raise CIError('could not find %s' % searched_path)
        else:
            raise CIError('multiple matches for %s' % searched_path)

    return path

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def isdir(search_root, *search_parts):
    isdir = False
    try:
        if os.path.isdir(getpath(search_root, *search_parts)):
            isdir = True
    except CIError: pass
    return isdir

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def get_valid_dirs(root, required=[], skipped=[]):

    valid_dirs = []

    for path, dirs, files in os.walk(root):
        try:
            for s in skipped:
                if s in dirs:
                    dirs.remove(s)
            for r in required:
                getpath(path, r)
            valid_dirs.append(path)
        except CIError:
            pass

    return valid_dirs

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def get_valid_files(root, regex):

    valid_files = []

    for path, dirs, files in os.walk(root):
        for f in getpaths(path, regex):
            if os.path.isfile(f):
                valid_files.append(f)

    return valid_files

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def update_file(out, tmp, logger, start=''):
    if os.path.isfile(tmp):
        if os.path.isfile(out):
            tmp_file = open(tmp, 'rb')
            out_file = open(out, 'rb')
            tmp_str = tmp_file.read()
            out_str = out_file.read()
            tmp_start = tmp_str.index(start)
            out_start = out_str.index(start)
            tmp_md5 = hashlib_md5(tmp_str[tmp_start:])
            out_md5 = hashlib_md5(out_str[out_start:])
            tmp_file.close()
            out_file.close()
            if tmp_md5.digest() != out_md5.digest():
                bak = out + '.bak'
                if os.path.isfile(bak): os.remove(bak)
                os.renames(out, bak)
                os.renames(tmp, out)
                logger.info('updating %s' % trunc(out))
            else:
                os.remove(tmp)
        else:
            os.renames(tmp, out)
            logger.info('creating %s' % trunc(out))

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def iswindows():
    return sys.platform in ['win32', 'win64']


#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
# http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python
#*******************************************************************************
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file
    return None

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def rsync(source, destination, excludes=[], port=None):
    if iswindows():
        rsync = which('rsync.exe')
        if not rsync:
            try:
                rsync = os.environ['CWRSYNC_BIN']
            except KeyError:
                try:
                    rsync = getpath(os.environ['ProgramFiles(x86)'], 'cwRsync', 'bin')
                except (CIError, KeyError):
                    rsync = getpath(os.environ['ProgramFiles'], 'cwRsync', 'bin')
            os.environ['PATH'] = ';'.join([rsync, os.environ['PATH']])
        try:
            os.environ['HOME']
        except KeyError:
            os.environ['HOME'] = os.path.join(os.environ['HOMEDRIVE'], os.environ['HOMEPATH'])

    cmd_line = 'rsync -rlt --chmod=ugo=rwX'
    if port:
        cmd_line += ' -e "ssh -p %s"' % port
    cmd_line = ' '.join([cmd_line] + list('--exclude %s' % e for e in excludes))
    cmd_line = ' '.join([cmd_line, source, destination])
    if subprocess.call(cmd_line, shell=True):
        raise CIError('rsync failed: %s> %s' % (os.getcwd(), cmd_line))

