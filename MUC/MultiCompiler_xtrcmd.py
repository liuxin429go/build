# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#
#   Compilateur xtrcmd
#-----------------------------------------------------------------------------------------------------

import MUC_platform

import os

"""
Configuration
"""
class cfg_ftn:
    def getCfg(self, c):
        flags = [
                '--format=text',
                ]
        return flags

"""
xtrcmd générique
"""
class xtrcmd(cfg_ftn):
    def __win32(self, cfg):
        s = r'l:\Python-2.4\python $INRS_DEV\tools\xtrcmd\xtrcmd.py'
        o = os.path.splitext(cfg.fic_out)[0] + '.hlp'
        if (cfg.fic_err):
            c = "%s %s --output=%s %s > %s 2>&1" % (' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), o, cfg.fic_inp, cfg.fic_err)
        else:
            c = "%s %s --output=%s %s" % (' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), o, cfg.fic_inp)
        os.system( ' '.join( [s, c] ) )

    def __lnx32(self, cfg):
        s = r'python $INRS_DEV\tools\xtrcmd\xtrcmd.py'
        if (cfg.fic_err):
            c = "%s %s --output=%s %s > %s 2>&1" % (' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), cfg.fic_out, cfg.fic_inp, cfg.fic_err)
        else:
            c = "%s %s --output=%s %s" % (' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), cfg.fic_out, cfg.fic_inp)
        os.system( ' '.join( [s, c] ) )

    def ftn(self, cfg):
        if (MUC_platform.is_win()):
            self.__win32(cfg)
        else:
            self.__lnx32(cfg)

    def ftn_cfg(self, cfg):
        return cfg_ftn.getCfg(self, cfg.tgt_cfg)

"""
xtrcmd
"""
class xtrcmd_01(xtrcmd):
    def ftn(self, cfg):
        return xtrcmd.ftn(self, cfg)

    def ftn_cfg(self, cfg):
        return xtrcmd.ftn_cfg(self, cfg)
