# -*- coding: UTF-8 -*-
import fileinput
import os
import stat
import string
import sys
import tempfile
import dp_solutions
from string import split, replace

def traite(dir):
    if (not xeqAction(dir)):
        xeqRecursion(dir)

def xeqRecursion(dir):
    for f in os.listdir(dir):
        if os.path.isdir(os.path.join(dir,f)):
            if (dir == "."):
                traite(f)
            else:
                traite(os.path.join(dir,f))

def xeqAction(dir):
    if (dir == "."):
        return False
    path = os.path.join(dir, "build")
    res = os.path.isdir(path)
    if (res):
        path = replace(dir,"\\","/")
        module = split(path, "/")[-1]
        if (module != main):
            print " Traite: %s" % module
            print " Module path: %s" % path
            genereVersion(path)
        #    try:
        #        os.remove(path + "/build/versions.cfg")
        #        os.rename(path + "/build/versions.cfg.$$$", path + "/build/versions.cfg")
        #    except OSError:
        #        pass
    return res

def lisFichierInclude(fichier):
    includes = []
    curpath = os.getcwd().lower().replace("\\", "/")
    nomMod = ""
    pathtmp = ""
    other = ""
    try:
        for line in fileinput.FileInput(fichier):
            lenline = len(line)
            if (lenline > 5 and line[3:5].lower() == "e:"):

                if (lenline > 2 and line[0:2] == "-I"):
                    line = line[2:]
                    line = line.lstrip()
                    lenline = lenline - 2

                if (lenline > 0 and line[0] == "\""):
                    line = line[1:]
                    line = line.lstrip()
                    lenline = lenline - 1

                if (lenline > 0 and line[-1] == "\n"):
                    line = line[0:-1]
                    line = line.rstrip()
                    lenline = lenline - 1

                if (lenline > 0 and line[-1] == "\""):
                    line = line[0:-1]
                    line = line.rstrip()
                    lenline = lenline - 1

                if (lenline > 0):
                    line = os.path.dirname(line)
                    print line
                    print curpath
                    while pathtmp != curpath:
                        line, nomMod = os.path.split(line)
                        pathtmp = line.lower()
                    pathtmp = ""
                    if (not dp_solutions.estDansListe(includes, nomMod)):
                        includes.append(nomMod)
    except IOError:
        pass

    includes.sort()
    return includes

def creeBackup(fichier, backup):
    try:
        if (stat.S_ISREG(os.stat(fichier)[stat.ST_MODE])):
            try:
                if (stat.S_ISREG(os.stat(backup)[stat.ST_MODE])):
                    print " Overwriting existing file %s" % backup
                    os.remove(backup)
            except OSError:
                pass
            print " Creating backup file %s" % backup
            os.rename(fichier, backup)
    except OSError:
        pass

def genereVersion(module):

    source = module + "/build/cpp-all-win32-base.inc"
    #source = "cpp-all-win32-base.inc"
    build = module + "/build/versions.cfg"
    buildBackup = build + ".$$$"
    print "Working on module %s" % module

    modules = lisFichierInclude(source)

    creeBackup(build, buildBackup)
    print " Creating new include configuration file %s" % build
    fileNew = file(build, 'w')
    for line in modules:
        fileNew.write("%s = derniere\n" % line)
    fileNew.close()

if __name__ == '__main__':
   sys.exit(traite("."))
