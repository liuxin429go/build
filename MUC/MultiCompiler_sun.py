# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#
#   Compilateur SUN
#-----------------------------------------------------------------------------------------------------

import MUC_platform

import os
import subprocess

"""
SUN - Configuration
"""
class cfg_c:
    def getCfg_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_bse = [
                     '-c',                    # Compile only; produce .o files but suppress linking
###                     '-xport64=full',         # Enable extra checking for code ported from 32-bit to 64-bit platforms; <a>={full|implicit|no}
                     '-mt',                   # Specify options needed when compiling multi-threaded code
                     ]
        flags_cfg = {
                    'debug'     :
                    [
                     '-g',                    # Compile for debugging
###                     '-xO1',                  # Generate optimized code; <n>={1|2|3|4|5}
                     '-xbuiltin=%none',       # Enable profitable inline or substitute intrinsic functions; <a>={%all|%none}
                     '-fsimple=0',            # Select floating-point optimization level; <n>={1|0|2}
                    ],
                    'profil'    :
                    [
                     '-pg',                   # Compile for profiling with gprof
                     '-xO3',                  # Generate optimized code; <n>={1|2|3|4|5}
                     '-xbuiltin=%all',        # Enable profitable inline or substitute intrinsic functions; <a>={%all|%none}
###                     '-xprofile=collect[:<a>]',   # Compile for profile feedback data collection
                     '-xprofile=tcov',        # Compile for tcov basic block profiling (new format)
                     '-fsimple=1',            # Select floating-point optimization level; <n>={1|0|2}
                     '-xdepend=yes',          # Analyze loops for data dependencies; <b>={yes|no}

                     '-xautopar',             # Enable automatic loop parallelization
                     '-xloopinfo',            # Show loops that parallelized
                     '-xopenmp=parallel',     # Enable OpenMP language extension; <a>={parallel|noopt|none}
                    ],
                    'release'   :
                    [
#                     '-fast',                 # Optimize for speed using a selection of options
                     '-xO2',                  # Generate optimized code; <n>={1|2|3|4|5}
#                     '-fsimple=2',            # Select floating-point optimization level; <n>={1|0|2}
###                     '-mc',                   # Remove duplicate strings from .comment section of output files
###                     '-mr',                   # Remove all strings from .comment section of output files
#                     '-xdepend=yes',          # Analyze loops for data dependencies; <b>={yes|no}
#                     '-xbuiltin=%all',        # Enable profitable inline or substitute intrinsic functions; <a>={%all|%none}
#                     '-xcrossfile=1',         # Enable optimization and inlining across source files, n={1|0}
###                     '-xipo=2',               # Perform interprocedural analysis and optimizations, <n>={1|0|2}
#                     '-xlibmil',              # Inline selected libm math routines for optimization
#                     '-xprefetch=auto',       # Enable prefetch instructions; <a>={yes|no|[no%]auto|[no%]explicit}
#                     '-xreduction',           # Recognize reduction operations in parallelized loops
#                     '-xautopar',             # Enable automatic loop parallelization
#                     '-xloopinfo',            # Show loops that parallelized
#                     '-xopenmp=parallel',     # Enable OpenMP language extension; <a>={parallel|noopt|none}
###                     '-xarch=sse3a',
###                     '-xchip=native',
                    ]
                    }
        return flags_bse + flags_cfg[c]

    def getPtf_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_ptf = {
                    'unx32' :
                    [
                     '-m32',
                    ],
                    'unx64' :
                    [
                     '-m64',
                    ],
                    }
        return flags_ptf[p]

    def getLnk_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_lnk = {
                    'dynamic' :
                    [
                     '-Bdynamic',             # Allow dynamic linking
                     '-G',                    # Build a dynamic shared library
                     '-Kpic',                 # Compile position independent code
                    ],
                    'static' :
                    [
                     '-Bstatic',              # Require static linking
                     '-dn',                   # Allow or disallow dynamic libraries for the entire executable
                    ]
                    }
        return flags_lnk[l]

    def c_cfg(self, cfg):
        return self.getCfg_c(cfg) + self.getLnk_c(cfg) + self.getPtf_c(cfg)


class cfg_cpp(cfg_c):
    def getCfg_p(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        v = int(float(self.version)*10 + 0.5) if self.version else -1   # nint de version*10
        flags_bse = [
                     '-std=c++11',
                     '+p',                    # Ignore non-standard preprocessor asserts
                     '+w',                    # Print warnings about additional questionable constructs
                     '+w2',                   # Emit warnings for code with additional portability problems
                     '-library=%s' % 'stdcpp' if v >= 126 else 'stlport4',     # Use STLport version 4.5.3 instead of the default libCstd
                    ]
        flags_cfg = {
                    'debug'     :
                    [
                     '+d',                    # Do not expand inline functions
                    ],
                    'profil'    :
                    [
                    ],
                    'release'   :
                    [
                    ]
                    }
        return cfg_c.getCfg_c(self, cfg) + flags_bse + flags_cfg[c]

    def cpp_cfg(self, cfg):
        return self.getCfg_p(cfg) + self.getLnk_c(cfg) + self.getPtf_c(cfg)

class cfg_ftn:
    def getCfg_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_bse = [
                     '-c',                    # Compile only; produce .o files but suppress linking
                     '-aligncommon=16',       # Align common block elements to the specified boundary requirement; <a>={1|2|4|8|16}
                     '-ext_names=underscores',  # Use the Sun FORTRAN 90 compiler's naming conventions (add underscores)
                     '-fpover=yes',           # Provide run-time overflow check during READ; <b>={yes|no}
                     '-mt',                   # Specify options needed when compiling multi-threaded code
                     '-w1',                   # Show errors and warnings (default)
                     '-xrecursive',           # Allow recursive calls
#           stackvar generates a Floating point exception at startup ???
                     '-stackvar',             # Allocate all local variables on the memory stack
                     '-dalign',               # Force 8-byte data alignment and enable double-word load/stores
                     '-f77=output',           # Enables Fortran 77 extensions for output
                     '-moddir=' + cfg.mod_dir,
                    ]

        flags_cfg = {
                    'debug'     :
                    [
                     '-g',                    # Compile for debugging
###                     '-xO1',                  # Generate optimized code; <n>={1|2|3|4|5}
                     '-fsimple=0',            # Select floating-point optimization level; <n>={1|0|2}
                     '-xcommonchk=yes',       # Enable run-time taskcommon consistency check; <b>={yes|no}
                     '-xcheck=%all',          # Generate runtime checks for error condition <a>={%all|%none|init_local|no%init_local}
                    ],
                    'profil'    :
                    [
                     '-xpg',                  # Compile for profiling with gprof
                     '-xO3',                  # Generate optimized code; <n>={1|2|3|4|5}
###                     '-xprofile=collect[:<a>]',   # Compile for profile feedback data collection
                     '-xprofile=tcov',        # Compile for tcov basic block profiling (new format)
                     '-fsimple=1',            # Select floating-point optimization level; <n>={1|0|2}
                     '-xdepend=yes',          # Analyze loops for data dependencies; <b>={yes|no}

                     '-xautopar',             # Enable automatic loop parallelization
                     '-xloopinfo',            # Show loops that parallelized
                     '-xopenmp=parallel',     # Enable OpenMP language extension; <a>={parallel|noopt|none}

                    ],
                    'release'   :
                    [
                     '-fast',                 # Optimize for speed using a selection of options
                     '-fsimple=2',            # Select floating-point optimization level; <n>={1|0|2}
                     '-libmil',               # Inline selected libm math routines for optimization
                     '-loopinfo',             # Show loops that parallelized
#                     '-xO3',                  # Generate optimized code; <n>={1|2|3|4|5}
                     '-xO4',                  # Add automatic inlining of routines from same source file
#                     '-xO5',                  # Attempt aggressive optimizations (with profile feedback)
                     '-xdepend=yes',          # Analyze loops for data dependencies; <b>={yes|no}
                     '-xopenmp=parallel',     # Enable OpenMP language extension; <a>={parallel|noopt|none}
                     '-xautopar',             # Enable automatic loop parallelization
                     '-pad=local,common',     # Pad variables for efficient use of cache; <p>={%none|local|common}
###                     '-xreduction',           # Recognize reduction operations in parallelized loops
###                                 xreduction provoques: Fatal error in /opt/sun/developerstudio12.6/lib/compilers/bin/iropt : Signal number = 139
###                     '-unroll=<n>',           # Enable unrolling loops to a depth <n> > 0 where possible
                     '-xcrossfile=1',         # Enable optimization and inlining across source files, n={1|0}
###                     '-xF',                   # Compile for later mapfile reordering
###                     '-xipo=2',               # Perform interprocedural analysis and optimizations, <n>={1|0|2}
                     '-xprefetch=auto',       # Enable prefetch instructions; <a>={yes|no|[no%]auto|[no%]explicit}
###                     '-xarch=sse3a',
###                     '-xchip=native',
                     '-W2,-Rujam',         #  Fatal error in iropt : Signal number = 139 
                    ]
                    }

        return flags_bse + flags_cfg[c]

    def getPtf_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_ptf = {
                    'unx32' :
                    [
                     '-m32',
                    ],
                    'unx64' :
                    [
                     '-m64',
                    ],
                    }
        return flags_ptf[p]


    def getLnk_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_lnk = {
                    'dynamic' :
                    [
                     '-Bdynamic',             # Allow dynamic linking
                     '-dy',                   # Allow or disallow dynamic libraries for the entire executable
                     '-G',                    # Build a dynamic shared library
                     '-Kpic',                 # Compile position independent code
                    ],
                    'static' :
                    [
                     '-Bstatic',              # Require static linking
                     '-dn',                   # Allow or disallow dynamic libraries for the entire executable
                    ]
                    }
        return flags_lnk[l]

    def getIsz_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_isz = {
                    4 : ['-xtypemap=integer:32'],
                    8 : ['-xtypemap=integer:64'],
                    }
        return flags_isz[i]

    def ftn_cfg(self, cfg):
        return self.getCfg_f(cfg) + self.getLnk_f(cfg) + self.getPtf_f(cfg) + self.getIsz_f(cfg)


"""
SUN generic
"""
class sun:
    def __init__(self, version = None):
       self.version = version

    def __go(self, cmplr, cfg):
        c = '%s %s %s %s -o %s %s' % (cmplr, ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn), cfg.fic_out, cfg.fic_inp)

        _stderr = None
        _stdout = None
        _shell  = True
        if (cfg.fic_err):
            _stdout = open(cfg.fic_err, 'w')
            _stderr = subprocess.STDOUT
        if (MUC_platform.is_win()):
            _shell = False

        if (cfg.dmp_cmd):
            print('--- Compiling on %s for %s(i%s)' % (MUC_platform.build_platform(), cfg.tgt_ptf, cfg.tgt_isz))
            print(c)
            print('-------')

        retcode = subprocess.call(c, stdout=_stdout, stderr=_stderr, shell=_shell)

    def cpp(self, cmplr, cfg):
        self.__go(cmplr, cfg)

    def ftn(self, cmplr, cfg):
        self.__go(cmplr, cfg)

"""
Sun Studio 12
"""
class sun_120(sun, cfg_cpp, cfg_ftn):
    def __init__(self, version = '12.0'):
       sun.__init__(self, version)

    def c(self, cfg):
        sun.cpp(self, '/opt/sun/bin/suncc', cfg)

    def cpp(self, cfg):
        sun.cpp(self, '/opt/sun/bin/sunCC', cfg)

    def ftn(self, cfg):
        sun.ftn(self, '/opt/sun/bin/sunf95', cfg)

"""
Sun Studio 12.1
"""
class sun_121(sun_120):
    def __init__(self, version = '12.1'):
       sun_120.__init__(self, version)

    def c(self, cfg):
        sun.cpp(self, '/opt/sun/sunstudio12.1/bin/suncc', cfg)

    def cpp(self, cfg):
        sun.cpp(self, '/opt/sun/sunstudio12.1/bin/sunCC', cfg)

    def ftn(self, cfg):
        sun.cpp(self, '/opt/sun/sunstudio12.1/bin/sunf95', cfg)

"""
Solaris Studio 12.2
"""
class sun_122(sun_120):
    def __init__(self, version = '12.2'):
       sun_120.__init__(self, version)

    def c(self, cfg):
        sun.cpp(self, '/opt/sun/solstudio12.2/bin/suncc', cfg)

    def cpp(self, cfg):
        sun.cpp(self, '/opt/sun/solstudio12.2/bin/sunCC', cfg)

    def ftn(self, cfg):
        sun.cpp(self, '/opt/sun/solstudio12.2/bin/sunf95', cfg)

"""
Solaris Studio 12.3
"""
class sun_123(sun_120):
    def __init__(self, version = '12.3'):
       sun_120.__init__(self, version)

    def c(self, cfg):
        sun.cpp(self, '/opt/sun/solarisstudio12.3/bin/suncc', cfg)

    def cpp(self, cfg):
        sun.cpp(self, '/opt/sun/solarisstudio12.3/bin/sunCC', cfg)

    def ftn(self, cfg):
        sun.cpp(self, '/opt/sun/solarisstudio12.3/bin/sunf95', cfg)

"""
Solaris Studio 12.4
"""
class sun_124(sun_120):
    def __init__(self, version = '12.4'):
       sun_120.__init__(self, version)

    def c(self, cfg):
        sun.cpp(self, '/opt/sun/solarisstudio12.4/bin/suncc', cfg)

    def cpp(self, cfg):
        sun.cpp(self, '/opt/sun/solarisstudio12.4/bin/sunCC', cfg)

    def ftn(self, cfg):
        sun.cpp(self, '/opt/sun/solarisstudio12.4/bin/sunf95', cfg)
"""
Solaris Studio 12.5
"""
class sun_125(sun_120):
    def __init__(self, version = '12.5'):
       sun_120.__init__(self, version)

    def c(self, cfg):
        sun.cpp(self, '/opt/sun/developerstudio12.5/bin/suncc', cfg)

    def cpp(self, cfg):
        sun.cpp(self, '/opt/sun/developerstudio12.5/bin/sunCC', cfg)

    def ftn(self, cfg):
        sun.cpp(self, '/opt/sun/developerstudio12.5/bin/sunf95', cfg)
"""
Solaris Studio 12.6
"""
class sun_126(sun_120):
    def __init__(self, version = '12.6'):
       sun_120.__init__(self, version)

    def c(self, cfg):
        sun.cpp(self, '/opt/sun/developerstudio12.6/bin/suncc', cfg)
        
    def cpp(self, cfg):
        sun.cpp(self, '/opt/sun/developerstudio12.6/bin/sunCC', cfg)
        
    def ftn(self, cfg):
        sun.cpp(self, '/opt/sun/developerstudio12.6/bin/sunf95', cfg)


