# -*- coding: UTF-8 -*-

from . import intel
import MUC_platform


MUCIntel = intel.EnvironmentIntel

class EnvironmentItlX64(MUCIntel):
    def __init__(self, tool, version, opts, *args, **kwargs):
        if (MUC_platform.is_win()):
            muc_target_plateform = 'win64'
        elif (MUC_platform.is_unx()):
            muc_target_plateform = 'unx64'
        else:
            raise SCons.Errors.UserError('Invalid Intel platform: %s' % MUC_platform.platform())

        kwargs.setdefault('MUC_TARGETPLATEFORM', muc_target_plateform)
        MUCIntel.__init__(self, tool, version, opts, *args, **kwargs)

