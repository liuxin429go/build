#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

from collections import OrderedDict
import logging
import logging.handlers
import os
import platform
import xml.etree.ElementTree as ET
from anytree import Node, PostOrderIter

LOGGER = logging.getLogger("vc2scons.vcproj")

def isWindows():
    return platform.system() == 'Windows'

def isUnix():
    return platform.system() == 'Linux'

class MSVCSolution:
    def __init__(self):
        self.m_file = ""
        self.m_dir  = ""
        self.m_prjs = []

    def loadSolution(self, file):
        """
        Load the solution, construct the list of projects
        """
        LOGGER.trace('MSVCSolution.loadSolution(...)')
        LOGGER.info('Loading solution: %s' % file)
        self.m_file = os.path.abspath(file)
        self.m_dir = os.path.dirname(self.m_file)

        with open(self.m_file, 'r') as ifs:
            for l in ifs.readlines():
                l = l.strip()
                if l[:8] == "Project(":
                    _, l = l.split('=')
                    nam, fic, gid = l.split(',')
                    nam = nam.strip()[1:-1]
                    fic = fic.strip()[1:-1]
                    gid = gid.strip()[1:-1].lower()
                    fullPath = os.path.abspath( os.path.join(self.m_dir, fic) )
                    if os.path.isfile(fullPath):
                        self.m_prjs.append( MSVCProject(nam, fullPath, gid) )

    def closeSolution(self):
        pass

    def getFile(self):
        """
        Retourne le path complet de la solution
        """
        return self.m_file

    def getDirectory(self):
        """
        Retourne le répertoire de la solution
        """
        return self.m_dir

    def getProjects(self):
        """
        The list of MSVCProjects
        """
        return self.m_prjs

    def getDependencies(self, prj):
        """
        Retourne la liste des dépendances sous forme de
        liste de MSVCProject
        """
        LOGGER.trace("vcproj.MSVCSolution.getDependencies()")
        prjs = []
        gids = prj.getDependencies()
        for gid in gids:
            f = filter(lambda vcp : vcp.m_gid == gid, self.m_prjs)
            try:
                prjs.append( next(f) )
            except StopIteration:
                pass
        return prjs

    def __getConstructionOrder(self, prj, root):
        prjs = self.getDependencies(prj)
        for p in prjs:
            node = Node(p, parent=root)
            self.__getConstructionOrder(p, node)

    def getConstructionOrder(self, prj):
        """
        Retourne l'ordre de construction
        """
        LOGGER.trace("vcproj.MSVCSolution.getConstructionOrder()")
        root = Node(prj)
        self.__getConstructionOrder(prj, root)

        #for pre, fill, node in RenderTree(root):
        #    print('%s%s' % (pre, node.name))

        nodes = [node.name for node in PostOrderIter(root)]
        nodes = list(OrderedDict.fromkeys(nodes))
        return nodes

class MSVCProject:
    ADDITIONAL_LINK_DEPENDENCIES = [    # Extracted from VS
        'kernel32.lib',
        'user32.lib',
        'gdi32.lib',
        'winspool.lib',
        'comdlg32.lib',
        'advapi32.lib',
        'shell32.lib',
        'ole32.lib',
        'oleaut32.lib',
        'uuid.lib',
        'odbc32.lib',
        'odbccp32.lib',
    ]
    PREPROCESSOR_DEFINITIONS = []       # Extracted from VS
    REFERENCE_BUILD ="\"'$(Configuration)|$(Platform)'=='Debug|x64'\""
    REFERENCE_ENV   = {'$(Configuration)'   : 'Debug',
                       '$(Platform)'        : 'x64'}

    def __init__(self, name, file, gid):
        LOGGER.trace('MSVCProject.__init__(...)')
        LOGGER.info('Loading project: %s' % name)
        self.m_name = name
        self.m_file = file
        self.m_dir  = os.path.dirname( os.path.realpath(self.m_file) )
        self.m_gid  = gid
        self.m_tv   = ''
        self.m_ns   = {}
        self.m_tgtType = "Invalid_Target_Type"
        self.__getCoreInfo()

    def __eq__(self, other):
        return self.m_gid == other.gid

    def __hash__(self):
        return hash(self.m_gid)

    def __repr__(self):
        return '%s' % self.m_name

    def __getCoreInfo(self):
        """
        Get XML namespace,and other info
        """
        tree = ET.parse(self.m_file)
        root = tree.getroot()
        url  = root.tag.split('}')[0][1:]
        self.m_ns = { 'ms': url }
        self.m_tv = root.get('ToolsVersion')

        for item in root.findall('.//ms:PropertyGroup/ms:RootNamespace', self.m_ns):
            nam = item.text
            if nam != self.m_name:
                LOGGER.error('%s: %s', nam, self.m_name)
                assert (nam == self.m_name)
        for item in root.findall('.//ms:PropertyGroup/ms:ProjectGuid', self.m_ns):
            gid = item.text.lower()
            if gid != self.m_gid:
                LOGGER.error('%s: %s', gid, self.m_gid)
                assert (gid == self.m_gid)

    def __substEnv(self, s):
        s_ = s
        for k, v in self.REFERENCE_ENV.items():
            s_ = s_.replace(k, v)
        return s_

    def getDependencies(self):
        deps = []
        tree = ET.parse(self.m_file)
        root = tree.getroot()
        for item in root.findall('.//ms:ProjectReference/ms:Project', self.m_ns):
            deps.append( item.text.lower() )
        return deps

    def getDirectory(self):
        return self.m_dir

    def getDefines(self):
        defs = []
        tree = ET.parse(self.m_file)
        root = tree.getroot()
        pth = ".//ms:ItemDefinitionGroup[@Condition=%s]/ms:ClCompile/ms:PreprocessorDefinitions" % MSVCProject.REFERENCE_BUILD
        for item in root.findall(pth, self.m_ns):
            entries = item.text.split(';')
            for entry in entries:
                if entry in [ '%(PreprocessorDefinitions)' ]:
                    defs.extend( self.getPreprocessorDefinitions().split(';') )
                else:
                    defs.append( entry )
        return defs

    def getFile(self):
        return self.m_file

    def getName(self):
        return self.m_name

    def getFiles(self, incExcluded = False):
        """
        Retourne la liste des fichiers du projet
        """
        # TAGS_EXCLUDED = ('ProjectReference', 'ProjectReference', 'CustomBuild')
        TAGS_INCLUDED = ('INRS_MUC', 'INRS_Cython', 'H2D2_PyInstaller', 'ClInclude', 'ResourceCompile', 'Image', 'Text', 'None')

        files = []
        tree = ET.parse(self.m_file)
        root = tree.getroot()
        pth1 = './/ms:ItemGroup/*[@Include]'
        pth2 = "ms:ExcludedFromBuild[@Condition=%s]" % MSVCProject.REFERENCE_BUILD

        for item in root.findall(pth1, self.m_ns):
            tag = item.tag.split('}')[-1]
            if tag in TAGS_INCLUDED:
                isExcludedFromBuild = item.findtext(pth2, default='false', namespaces=self.m_ns)
                if isExcludedFromBuild == 'false':
                    file = item.attrib['Include']
                    file = file.replace('\\', os.sep)
                    file = os.path.join(self.m_dir, file)
                    files.append( MSVCFile(os.path.normpath(file), tag) )
                else:
                    LOGGER.debug('Skipping excluded file "%s"' % item.attrib['Include'])
            else:
                LOGGER.debug('Ignore files with tag "%s"' % tag)
        return files

    def __getIncMUC_1(self, path, drvPrj):
        """
        Retourne la liste des répertoires include du système de MUC
        """
        lst = []
        try:
            with open(path, 'r') as ifs:
                for ligne in ifs.readlines():
                    ligne = ligne.strip()
                    if not ligne: continue
                    if ligne[0] == "#": continue
                    ligne = ligne[2:].strip()        # Suppress -I
                    ligne = ligne.replace('"', '')   # Remove double quotes
                    if ligne[0] == "/": ligne = drvPrj + ligne
                    lst.append(ligne)
        except:
            pass
        return lst

    def getIncMUC(self):
        """
        Retourne la liste des répertoires include du système de MUC
        """
        FIC_INC = [
            "cpp-all-allp-base.inc",
            "cpp-all-win64-base.inc",
            "cpp-msvc-allp-base.inc",
            "cpp-msvc-win64-base.inc",
            "cpp-all-allp-debug.inc",
            "cpp-all-win64-debug.inc",
            "cpp-msvc-allp-debug.inc",

            "cpp-msvc-win64-debug.inc",

            "c-all-allp-base.inc",
            "c-all-win64-base.inc",
            "c-msvc-allp-base.inc",
            "c-msvc-win64-base.inc",
            "c-all-allp-debug.inc",
            "c-all-win64-debug.inc",
            "c-msvc-allp-debug.inc",
            "c-msvc-win64-debug.inc",

            "ftn-all-allp-base.inc",
            "ftn-all-win64-base.inc",
            "ftn-gcc-allp-base.inc",
            "ftn-gcc-win64-base.inc",
            "ftn-all-allp-debug.inc",
            "ftn-all-win64-debug.inc",
            "ftn-gcc-allp-debug.inc",
            "ftn-gcc-win64-debug.inc",
        ]

        lst = []

        dirPrj = self.getDirectory()
        if isWindows():
            patPrj = os.path.normpath(dirPrj)[3:]
            drvPrj = os.path.normpath(dirPrj)[:3]
        else:
            patPrj = os.path.normpath(dirPrj)[1:]
            drvPrj = os.path.normpath(dirPrj)[:1]

        dirBuild = drvPrj
        for tok in patPrj.split(os.path.sep):
            if not tok: continue

            dirBuild = os.path.join(dirBuild, tok)
            for fic in FIC_INC:
                ficInc = os.path.join(dirBuild, "build", fic)
                lst.extend( self.__getIncMUC_1(ficInc, drvPrj) )

        return lst

    def getIncPath(self):
        """
        Retourne la liste des répertoires d'include
        """
        incs = []

        tree = ET.parse(self.m_file)
        root = tree.getroot()
        pth = ".//ms:ItemDefinitionGroup[@Condition=%s]/ms:ClCompile/ms:AdditionalIncludeDirectories" % MSVCProject.REFERENCE_BUILD
        for item in root.findall(pth, self.m_ns):
            incs.extend( item.text.split(';') )
        return incs

    def getAdditionalDependencies(self):
        deps = []

        if self.isLink():
            tree = ET.parse(self.m_file)
            root = tree.getroot()
            pth = ".//ms:ItemDefinitionGroup[@Condition=%s]/ms:Link/ms:AdditionalDependencies" % MSVCProject.REFERENCE_BUILD
            for item in root.findall(pth, self.m_ns):
                entries = item.text.split(';')
                for entry in entries:
                    if entry in [ '%(AdditionalDependencies)' ]:
                        pass 
                        # deps.extend( self.ADDITIONAL_LINK_DEPENDENCIES )
                    else:
                        deps.append( entry )
        return deps

    def getPreprocessorDefinitions(self):
        return ';'.join(MSVCProject.PREPROCESSOR_DEFINITIONS)

    def getLibraries(self):
        """
        Retourne la liste des librairies
        """
        LOGGER.trace("vcproj.MSVCSolution.getLibraries() for %s" % self.m_file)
        libs = []
        tree = ET.parse(self.m_file)
        root = tree.getroot()
        pth = ".//ms:ItemDefinitionGroup[@Condition=%s]/ms:Link/ms:AdditionalDependencies" % MSVCProject.REFERENCE_BUILD
        for item in root.findall(pth, self.m_ns):
            entries = item.text.split(';')
            for entry in entries:
                if entry in [ '%(AdditionalDependencies)' ]:
                    libs.extend( self.getAdditionalDependencies() )
                else:
                    libs.append( entry )
        return libs

    def getLibPath(self):
        """
        Retourne la liste des répertoires de librairies
        """
        libs = []
        tree = ET.parse(self.m_file)
        root = tree.getroot()
        pth = ".//ms:ItemDefinitionGroup[@Condition=%s]/ms:Link/ms:AdditionalLibraryDirectories" % MSVCProject.REFERENCE_BUILD
        for item in root.findall(pth, self.m_ns):
            for tok in item.text.split(';'):
                tok = tok.strip()
                if not tok in [ '%(AdditionalDependencies)' ]:
                    tok = tok.replace("$(INRS_DEV)", "$INRS_DEV")
                    tok = tok.replace("$(INRS_LXT)", "$INRS_LXT")
                    tok = self.__substEnv(tok)
                    libs.append( tok )
        return libs

    def getName(self):
        return self.m_name

    def getTargetType(self):
        if self.m_tgtType != "Invalid_Target_Type":
            return self.m_tgtType

        tree = ET.parse(self.m_file)
        root = tree.getroot()
        for item in root.findall('.//ms:PropertyGroup/ms:ConfigurationType', self.m_ns):
            self.m_tgtType = item.text
        return self.m_tgtType

    def isMUC(self):
        for f in self.getFiles():
            if f.isMUC(): return True
        return False

    def isExe(self):
        if self.m_tgtType == "Invalid_Target_Type": self.getTargetType()
        return self.m_tgtType == 'Application'
    def isLib(self):
        if self.m_tgtType == "Invalid_Target_Type": self.getTargetType()
        return self.m_tgtType == 'StaticLibrary'
    def isDLL(self):
        if self.m_tgtType == "Invalid_Target_Type": self.getTargetType()
        return self.m_tgtType == 'DynamicLibrary'
    def isPython(self):
        if self.m_tgtType == "Invalid_Target_Type": self.getTargetType()
        return self.m_tgtType == 'Python'
    def isCython(self):
        if self.m_tgtType == "Invalid_Target_Type": self.getTargetType()
        return self.m_tgtType == 'Cython'
    def isPyInstaller(self):
        if self.m_tgtType == "Invalid_Target_Type": self.getTargetType()
        return self.m_tgtType == 'PyInstaller'
    def isLink(self):
        return self.isExe() or self.isDLL() or self.isCython()

class MSVCFile:
    def __init__(self, name, kind):
        self.m_name = name
        self.kind = kind
        #print(self.m_name)
        #assert os.path.isfile(name)

    def __repr__(self):
        return 'MSCVFile: %s' % self.m_name

    def isMUC(self):
        return self.kind == 'INRS_MUC'

    def getExtension(self):
        return os.path.splitext(self.m_name)[-1]

    def getFullPAth(self):
        return os.path.abspath(self.m_name)


def main():
    s = MSVCSolution()
    s.loadSolution(r"E:\h2d2-dev\H2D2\h2d2\prjVisual\h2d2.sln")
    p = s.getProjects()[0]
    print( p.getLibraries() )
    print( s.getConstructionOrder(p) )

if __name__ == "__main__":
    from addLogLevel import addLoggingLevel
    addLoggingLevel('DUMP',  logging.DEBUG + 5)
    addLoggingLevel('TRACE', logging.DEBUG - 5)

    strHndlr = logging.StreamHandler()
    FORMAT = "[%(levelname)-8.8s] %(message)s"
    strHndlr.setFormatter( logging.Formatter(FORMAT) )
    strHndlr.setLevel(logging.INFO)

    LOGGER = logging.getLogger("vc2scons.vcproj")
    LOGGER.addHandler(strHndlr)
    LOGGER.setLevel(logging.DEBUG)

    main()
