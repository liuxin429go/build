function showBrowserPopup()
{
    console.log('ew-main: showBrowserPopup');
    var modal = document.getElementById('myModal');
    modal.style.display = "block";
}

// Get the modal
var modal = document.getElementById('myModal');

// Get the <span> element that closes the modal
var span = document.getElementsByClassName('modal-close')[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


var val = localStorage.getItem('ew_dir');
if (val === null) {
    val = '.';
}
sessionStorage.setItem('ew_dir', val);

var val = localStorage.getItem('ew_file');
if (val !== null) {
    sessionStorage.setItem('ew_file', val);
}
