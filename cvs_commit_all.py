# -*- coding: UTF-8 -*-
from string import split, replace
import os
import subprocess

def traite(dir):
    if (xeqAction(dir) == 0):
        xeqRecursion(dir)

def xeqRecursion(dir):
    for f in os.listdir(dir):
        if os.path.isdir(os.path.join(dir,f)):
            if (dir == "."):
                traite(f)
            else:
                traite(os.path.join(dir,f))


def xeqAction(dir):
    cvsroot = None
    for f in os.listdir(dir):
        fullPath = os.path.join(dir,f)
        if (os.path.isdir(fullPath) and f == "CVS"):
            f = open( os.path.join(fullPath,'Root'), 'r' )
            cvsroot = f.readline().strip()
            f.close()

    if (cvsroot):
        module = replace(dir,"\\","/")
        print "Traite: " + module
        cmd_cd  = "cd %s" % replace(dir,"\\","/")
        cmd_cvs = "cvs -n -d %s commit -n ." % cvsroot
        commande= ' && '.join( [cmd_cd, cmd_cvs] )
        print "commande: " + commande
        retcode = subprocess.call(commande, stdout=None, stderr=None, shell=True)
    return (cvsroot != None)

traite(".")
