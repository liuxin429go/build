# -*- coding: UTF-8 -*-
import fileinput
import re

class EWAlgoLinker_CountErrors:
    def __init__(self):
        self.cpl = None

    def configure(self, cmplr):
        self.cpl = ctrLinker(cmplr)

    def __call__(self, cmplr, path):
        self.configure(cmplr)

        errors = 0
        warnings = 0
        for line in fileinput.FileInput(path):
            if (len(line) < 10): continue
            if (line[0] == " "): continue
            if (line[0:3] == "In "): continue
            if (line.find(self.cpl.spdexpWarning) > 0):
#                print "In warning: ~%s" % line
                if (self.cpl.regexpWarning.match(line)):
                    warnings = warnings + 1
            elif (line.find(self.cpl.spdexpError) > 0):
#                print "In error: ~%s" % line
                if (self.cpl.regexpError.match(line)):
#                    print self.cpl.regexpError.match(line).group()
                    errors = errors + 1
        return errors, warnings


class EWLinker:

    def __init__(self):
        self.spdexpError   = None
        self.regexpError   = None
        self.spdexpWarning = None
        self.regexpWarning = None


class EWLinkerGCC(EWLinker):

    def __init__(self):
        EWLinker.__init__(self)
        cplErr = r"(((gcc|g\+\+|g77|collect2): )?)"
        drvErr = r"(([a-zA-Z]:[\\/]|[\\/])?)"
        drvWrn = r"([eE]:[\\/]?)"           # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        objExp = r"(\(\.(text|rdata).*\):)"
        linExp = r"(:([0-9]+))"
        errExp = r"(([0-9]+:)? (undefined|No such|cannot find|could not|ld terminated .* \[Segmentation fault\]))"
        wrnExp = r"(: warning: )"
        patErr = drvErr + dirExp + "*" + filExp + ":"
        patWrn = drvWrn + dirExp + "*" + filExp
        xprErr = "^" + cplErr + "(" + patErr + objExp + "?" + "){0,2}" + errExp
        xprWrn = "^" + "(?P<file>" + patWrn + ")" + linExp + "{0,2}" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = ": "           # On ne peut pas etre plus precis
        self.spdexpWarning = ": warning:"


class EWLinkerVC(EWLinker):

    def __init__(self):
        EWLinker.__init__(self)
        drvErr = r"(([a-zA-Z]:)?[\\/]?)"
        drvWrn = r"(([eE]:)?[\\/]?)"           # Limit to e: drive for warnings
        filExp = r"([^<>:\"/\\|() ][^<>:\"/\\|()]*)"
        dirExp = r"(" + filExp + r"[\\/])"
        errExp = r"( :( fatal)? error )"
        wrnExp = r"( : warning )"
        patErr = drvErr + dirExp + "*" + filExp
        patWrn = drvWrn + dirExp + "*" + filExp
        xprErr = "^" + "(?P<file>" + patErr + ")" + "(\(" + patErr + "\))?" + errExp
        xprWrn = "^" + "(LINK|" + patWrn + ")" + wrnExp
        self.regexpError   = re.compile(xprErr)
        self.regexpWarning = re.compile(xprWrn)
        self.spdexpError   = " error "
        self.spdexpWarning = ": warning "

class EWLinkerIntel(EWLinkerVC):

    def __init__(self):
        EWLinkerVC.__init__(self)



def ctrLinker(nom):
    if (nom.lower() == "gcc-3.4") or (nom.lower() == "gcc3.3"):
        return EWLinkerGCC()
    if (nom.lower() == "intel7.1"):
        return EWLinkerIntel()
    if (nom.lower() == "intel-win-9.0") or (nom.lower() == "intel9.0"):
        return EWLinkerIntel()
    if (nom.lower() == "intel-win-9.1") or (nom.lower() == "intel9.1"):
        return EWLinkerIntel()
    if (nom.lower() == "msvc-7.0") or (nom.lower() == "vc7.0"):
        return EWLinkerVC()
    if (nom.lower() == "msvc-7.1") or (nom.lower() == "vc7.1"):
        return EWLinkerVC()
    raise "ERR - Unsupported Linker: " + nom


if __name__ == '__main__':
    algo = EWAlgoLinker_CountErrors()
    r = algo("gcc3.3", "e:/dev/build/a.err-lnk")
    print r
