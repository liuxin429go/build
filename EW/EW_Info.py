# -*- coding: UTF-8 -*-

import EW_Compiler
from EW_Compiler import EWAlgoCompiler_CountErrors
from EW_Linker   import EWAlgoLinker_CountErrors
from EW_Util     import dbg

import os
import time
# from xml.marshal import generic
# from xml.dom.ext.reader import Sax2
# from xml.dom.ext import PrettyPrint
import cPickle as pickle

class EWErrorCount(object):
    def __init__(self, absent=0, errors=0, warnings=0):
        self.absent = absent
        self.errors = errors
        self.warnings = warnings

    def __iadd__(self, other):
        self.absent   += other.absent
        self.errors   += other.errors
        self.warnings += other.warnings
        return self

    def __add__(self, other):
        a = self.absent + other.absent
        e = self.errors + other.errors
        w = self.warnings + other.warnings
        return EWErrorCount(a, e, w)

    def __str__(self):
        return 'Missing: {:d}; Errors: {:d}; Warnings: {:d}'.format(self.absent, self.errors, self.warnings)


class EWInfo(object):
    def __init__(self, parent=None, type='TypeUndefined'):
        self.items  = []
        self.nom    = ''
        self.type   = type
        self.parent = parent
        self.counts = EWErrorCount()

    # ---  Functions to emulate a list
    def __len__(self):
        return len(self.items)

    def __iter__(self):
        return self.items.__iter__()

    def __getitem__(self, i):
        return self.items.__getitem__(i)

    def append(self, i):
        return self.items.append(i)

    def sort(self):
        return self.items.sort()

    def sortAll(self):
        self.items.sort()
        for i in self.items:
            i.sortAll()

    def countAll(self):
        for i in self.items:
            i.countAll()
            self.counts += i.counts

    def xtrInfo(self, path):
        dbg.printd("EWInfo.xtrInfo(%s)" % path)
        self.nom = os.path.basename(path)
        dbg.printd("%s: %s" % (self.type, self.nom))
        dbg.level = dbg.level + 1
        # printState(path)

        for f in os.listdir(path):
            fullPath = os.path.join(path, f)
            obj = creeInfo(fullPath, self)
            if (obj is not None):
                obj.xtrInfo(fullPath)
                if len(obj):
                    self.append(obj)
                else:
                    dbg.printd("%s: skipping %s size %d" % (obj.type, obj.nom, len(obj)))

        dbg.printd("%s: %s" % (self.type, self.nom))
        dbg.level = dbg.level - 1

    def __lt__(self, other):
        return str(self) < str(other)

    def __hash__(self):
        return self.__str__().__hash__()

    def __str__(self):
        if (self.parent):
            return '::'.join([self.parent.__str__(), self.nom])
        else:
            return self.nom


class EWInfoDate(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoDate, self).__init__(parent, 'Date')
        self.computer = ""

    def xtrUnInfo(self, path):
        dbg.printd("EWInfoDate.xtrUnInfo(%s)" % path)
        obj = creeInfo(path, self)
        if (obj is not None):
            obj.xtrInfo(path)
            if len(obj):
                self.append(obj)
            else:
                dbg.printd("EWInfoDate.xtrUnInfo: skipping %s" % obj.nom)
        dbg.printd("EWInfoDate.xtrUnInfo done")

    def xtrInfo(self, path):
        dbg.printd("EWInfoDate.xtrInfo(%s)" % path)
        self.nom = time.strftime("%Y%m%d-%H%M%S")
        self.computer = os.getenv("COMPUTERNAME", "")
        dbg.printd("Debut xtrInfo: %s" % time.strftime("%H%M%S"))

        self.xtrUnInfo(path)

        if (not estPackage(path) and not estModule(path)):
            for f in os.listdir(path):
                self.xtrUnInfo(os.path.join(path, f))

        dbg.printd("Fin xtrInfo: %s" % time.strftime("%H%M%S"))

    def ecrisXML(self, nom):
        dbg.printd("Debut ecrisXML: %s" % time.strftime("%H%M%S"))
        # reader = Sax2.Reader()
        # doc = reader.fromString(generic.dumps(self))
        # PrettyPrint(doc, file(nom, 'w'))
        # dbg.printd("Fin ecrisXML-prettyPrint: %s" % time.strftime("%H%M%S"))
        # generic.dump(self, file(nom, 'w'))
        pickle.dump(self, file(nom, 'w'))
        dbg.printd("Fin ecrisXML: %s" % time.strftime("%H%M%S"))

    @staticmethod
    def lisXML(nom):
        dbg.printd("Debut lisXML: %s" % nom)
        info = pickle.load(file(nom, 'r'))
        # info = generic.load(file(nom, 'r'))
        dbg.printd("Fin lisXML: %s" % nom)
        return info


class EWInfoPackage(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoPackage, self).__init__(parent, 'Package')


class EWInfoVersion(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoVersion, self).__init__(parent, 'Version')


class EWInfoModule(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoModule, self).__init__(parent, 'Module')


class EWInfoPlatform(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoPlatform, self).__init__(parent, 'Platform')


class EWInfoCompiler(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoCompiler, self).__init__(parent, 'Compiler')

    def getCompiler(self):
        return self.nom


class EWInfoMpi(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoMpi, self).__init__(parent, 'Mpi')


class EWInfoTypeLink(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoTypeLink, self).__init__(parent, 'TypeLink')


class EWInfoTypeBuild(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoTypeBuild, self).__init__(parent, 'TypeBuild')
        self.compiler = ''

        p = self.parent
        s = ''
        while (p is not None and not self.compiler):
            try:
                self.compiler = p.getCompiler()
            except AttributeError:
                s = os.path.join(p.nom, s)
                p = p.parent
        if (not self.compiler):
            raise RuntimeError('No valid compiler found in %s' % s)

    def boucle(self, build, path):
        for f in os.listdir(path):
            fullPath = os.path.join(path, f)
            if (os.path.isdir(fullPath)):
                self.boucle(build, fullPath)
            else:
                if (os.path.splitext(f)[1] == ".err"):
                    fic = EWInfoFichier(self)
                    fic.xtrInfo(fullPath)
                    self.append(fic)
                elif (os.path.splitext(f)[1] == ".err-lnk"):
                    lnk = EWInfoLink(self)
                    lnk.xtrInfo(fullPath)
                    self.append(lnk)

    def xtrInfo(self, path):
        assert estBuild(path), "Not a build directory"
        self.nom = os.path.basename(path)
        build = self.nom
        dbg.printd("Build: %s" % self.nom)
        dbg.level = dbg.level + 1
        self.boucle(build, path)
        dbg.level = dbg.level - 1


class EWInfoFichier(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoFichier, self).__init__(parent, 'Fichier')
        self.path = ""

    def xtrInfo(self, nomFichier):
        dbg.printd("File: %s" % os.path.basename(nomFichier))
        if (os.path.exists(nomFichier)):
            bas = os.path.basename(nomFichier)  # abcd.for.err
            bas = os.path.splitext(bas)[0]      # abcd.for
            ext = os.path.splitext(bas)[1][1:]  # for
            algo = EWAlgoCompiler_CountErrors()
            cplr = self.parent.compiler
            try:
                errs, wrns = algo(cplr, ext, nomFichier)
                dbg.printd("Errors: %i Warnings: %i" % (errs, wrns))
                self.counts = EWErrorCount(errors=errs, warnings=wrns)
            except:
                pass
        else:
            self.counts = EWErrorCount(absents=1)
        self.path = nomFichier
        self.nom  = os.path.basename(nomFichier)


class EWInfoLink(EWInfo):
    def __init__(self, parent=None):
        super(EWInfoLink, self).__init__(parent, 'Link')
        self.path = ""

    def xtrInfo(self, nomFichier):
        dbg.printd("Link: %s" % os.path.basename(nomFichier))
        if (os.path.exists(nomFichier)):
            algo = EWAlgoLinker_CountErrors()
            cplr = self.parent.compiler
            errs, wrns = algo(cplr, nomFichier)
            self.counts = EWErrorCount(errors=errs, warnings=wrns)
        else:
            self.counts = EWErrorCount(absents=1)
        self.path = nomFichier
        self.nom  = os.path.basename(nomFichier)


def printState(path):
    dbg.level += 1
    dbg.printd("estVersion(%s): %d"  % (path, estVersion(path)))
    dbg.printd("estModule(%s): %d"   % (path, estModule(path)))
    dbg.printd("estPlatform(%s): %d" % (path, estPlatform(path)))
    dbg.printd("estCompiler(%s): %d" % (path, estCompiler(path)))
    dbg.printd("estMpi(%s): %d"      % (path, estMpi(path)))
    dbg.printd("estBuild(%s): %d"    % (path, estBuild(path)))
    dbg.printd("estLink(%s): %d"     % (path, estLink(path)))
    dbg.level -= 1

def estBuild(path):
    builds = ["debug", "release", "profil"]
    nom = os.path.basename(path)
    return os.path.isdir(path) and (nom in builds)

def estMpi(path):
    mpis = ["mpich2", "openmpi", "deinompi", "msmpi"]
    nom = os.path.basename(path)
    nom = nom.split('-')[0]
    return os.path.isdir(path) and (nom in mpis)

def estLink(path):
    links = ["dynamic", "static"]
    nom = os.path.basename(path)
    return os.path.isdir(path) and (nom in links)

def estCompiler(path):
    exist = True
    try:
        nom = os.path.basename(path)
        cplr = EW_Compiler.ctrCompiler(nom)
    except NotImplementedError:
        exist = False
    return exist

def estPlatform(path):
    platforms = ["win32", "win64", "win64i8", "unx32", "unx64", "unx64i8"]
    nom = os.path.basename(path)
    return os.path.isdir(path) and (nom in platforms)

def estVersion(path):
    nom = os.path.basename(path)
    return os.path.isdir(path) and (nom[0:2] == "v_" or nom == "derniere")

def estModule(path):
    return os.path.isdir(path) and not (estVersion(path) or estBuild(path) or estLink(path) or estMpi(path) or estCompiler(path) or estPlatform(path))

def estPackage(path):
    return estModule(path) and not (os.path.isdir(os.path.join(path, 'win32')) or os.path.isdir(os.path.join(path, 'unx32')))

def creeInfo(path, parent):
    if (estPackage(path)): return EWInfoPackage(parent)
    if (estModule(path)): return EWInfoModule(parent)
    if (estVersion(path)): return EWInfoVersion(parent)
    if (estPlatform(path)): return EWInfoPlatform(parent)
    if (estMpi(path)): return EWInfoMpi(parent)
    if (estLink(path)): return EWInfoTypeLink(parent)
    if (estBuild(path)): return EWInfoTypeBuild(parent)
    if (estCompiler(path)): return EWInfoCompiler(parent)
    return None

def creeInfoDatedeXML(nom):
    return EWInfoDate.lisXML(nom)

def dumpStructure(s):
    dbg.printd('%s: %s' % (s.__class__.__name__, s.nom))
    dbg.level += 1
    try:
        for o in s: dumpStructure(o)
    except TypeError:
        pass
    dbg.level -= 1

if __name__ == '__main__':
    dbg.on = False      # Debug off
    # path = r"\\Nabisipi\compil\dev\bin\dev"
    path = os.path.join(os.getenv('INRS_DEV'), r'_bin/H2D2')
    path = os.path.normpath(path)
    date = EWInfoDate()
    date.xtrInfo(path)
    # dbg.on = 1      # Debug off
    # dumpStructure(date)

    f = "H2D2/EW_00000000-000000.xml"
    date.ecrisXML(f)
    # date = creeInfoDatedeXML(os.path.join(path, "a.xml"))
