#!/usr/bin/env python

import os
import sys
import optparse
import subprocess
import util
import logging
import shutil
logger = util.getlogger('INRS.IEHSS.web')

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def call_webpp(web_dir, tmp_lst, trf_lst, doc_tec, dst_dir):
    cmd_line = ''
    if not util.iswindows(): cd_line += 'wine '
    cmd_line += util.getpath(web_dir, 'WebPP.exe')
    cmd_line += ' -dogroup -sortalpha -sortpublicfirst -noenum'
    cmd_line += ' -c=%s' % util.getpath(web_dir, 'WebPP.cfg')
    cmd_line += ' -zd=%s' % util.getpath(web_dir, 'SEGRI.def')
    cmd_line += ' -r=%s' % doc_tec
    cmd_line += ' -e=%s' % os.path.join(dst_dir, 'Segri.typ')
    cmd_line += ' -trfcpp=%s' % trf_lst
    cmd_line += ' @%s' % tmp_lst
    logger.info('calling WebPP.exe')
    logger.info('with: %s' % cmd_line)
    if subprocess.call(cmd_line, shell=True):
        logger.error('WebPP.exe failed')

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def call_trfcpp(trf_dir, tmp_lst, trf_lst, doc_tec):
    cmd_line = ''
    if not util.iswindows(): cd_line += 'wine '
    cmd_line += util.getpath(trf_dir, 'TrfCpp.exe')
    cmd_line += ' -c=%s' % util.getpath(trf_dir, 'TrfCpp.cfg')
    cmd_line += ' -i=%s' % trf_lst
    cmd_line += ' -r=%s' % doc_tec
    cmd_line += ' @%s' % tmp_lst
    logger.info('calling TrfCpp.exe')
    logger.info('with: %s' % cmd_line)
    if subprocess.call(cmd_line, shell=True):
        logger.info(cmd_line)
        logger.error('TrfCpp.exe failed')

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def update_webpp(module, opts):

    inrs_dev = os.environ['INRS_DEV']
    src_dir = util.getpath(inrs_dev, module)
    dst_dir = os.path.join(inrs_dev, 'webpp')
    web_dir = util.getpath(inrs_dev, 'Parser', 'Webpp', 'bin')
    trf_dir = util.getpath(inrs_dev, 'Parser', 'TrfCpp', 'bin')
    tmp_lst = os.path.join(os.environ['TEMP'], 'webpp-lst.rsp')
    trf_lst = os.path.join(os.environ['TEMP'], 'webpp-trf.lst')
    doc_tec = 'doc_technique/%s/webpp' % module
    dest = '%s@' % opts.user if opts.user else ''
    dest += '%s:%s' % (opts.server, doc_tec)

    if not os.path.isdir(dst_dir):
        os.mkdir(dst_dir)
    os.chdir(dst_dir)
    for f in util.get_valid_files(dst_dir, r'.*\.htm'):
        os.remove(f)
    with open(tmp_lst, 'w') as file_list:
        for f in util.get_valid_files(src_dir, r'.*\.(h|hf|hpp|cpp)'):
            file_list.write('%s\n' % f)

    call_webpp(web_dir, tmp_lst, trf_lst, doc_tec, dst_dir)
    call_trfcpp(trf_dir, tmp_lst, trf_lst, doc_tec)

    if opts.server:
        try:
            util.rsync('./*.htm', dest, port=opts.port)
        except util.CIError, msg:
            logger.warning(msg)

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def update_webftn(opts):

    inrs_dev = os.environ['INRS_DEV']
    src_dir = util.getpath(inrs_dev, 'H2D2')
    dst_dir = os.path.join(inrs_dev, 'webftn')
    bin_dir = util.getpath(inrs_dev, 'Parser', 'TrfFtn', 'bin')
    etc_dir = util.getpath(inrs_dev, 'Parser', 'TrfFtn', 'etc')
    tmp_lst = os.path.join(os.environ['TEMP'], 'trfftn-lst.rsp')
    dest = '%s@' % opts.user if opts.user else ''
    dest += '%s:doc_technique/H2D2' % opts.server
    dest += '-%s' % opts.branch
    dest += '/webftn'

    if not os.path.isdir(dst_dir): os.makedirs(dst_dir)
    for f in util.get_valid_files(dst_dir, r'.*\.(htm|js)'):
        os.remove(f)
    for f in util.get_valid_files(etc_dir, r'.*\.(js|gif|jpg|css)'):
        shutil.copy(f, dst_dir)

    os.chdir(dst_dir)
    with open(tmp_lst, 'w') as file_list:
        for f in util.get_valid_files(src_dir, r'.*\.(fi|fc|for)'):
            file_list.write('%s\n' % f)

    args = []
    if not util.iswindows(): args += ['wine ']
    args += [util.getpath(bin_dir, 'TrfFtn.exe'), '-p=/H2D2', '@%s' % tmp_lst]
    logger.info('calling TrfFtn.exe')
    logger.info('with: %s' % args)
    if subprocess.call(args):
        logger.error('TrfFtn.exe failed')

    if opts.server:
        try:
            util.rsync('./*.htm', dest, port=opts.port)
            util.rsync('./*.js',  dest, port=opts.port)
            util.rsync('./*.gif', dest, port=opts.port)
            util.rsync('./*.jpg', dest, port=opts.port)
            util.rsync('./*.css', dest, port=opts.port)
        except util.CIError, msg:
            logger.warning(msg)

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def update_webcmd(opts):

    inrs_dev = os.environ['INRS_DEV']
    src_dir = util.getpath(inrs_dev, 'H2D2')
    dst_dir = os.path.join(inrs_dev, 'webcmd')
    www_dir = util.getpath(inrs_dev, 'tools', 'xtrcmd', 'www')
    dest = '%s@' % opts.user if opts.user else ''
    dest += '%s:doc_technique/H2D2' % opts.server
    dest += '-%s' % opts.branch
    dest += '/cmds'

    if not os.path.isdir(dst_dir):
        os.mkdir(dst_dir)
    os.chdir(dst_dir)
    for f in util.get_valid_files(dst_dir, r'.*\.(html|css|js|gif)'):
        os.remove(f)
    for f in util.get_valid_files(www_dir, r'.*\.(html|css|js|gif)'):
        shutil.copy(f, dst_dir)

    args = ['python', util.getpath(www_dir, 'xtr_all.py'), src_dir]
    logger.info('calling xtr_all.py')
    logger.info('with: %s' % args)
    if subprocess.call(args):
        logger.error('xtr_all.py failed')
    args = ['python', util.getpath(www_dir, 'bld_h2d2_cmd_lst.py')]
    logger.info('calling bld_h2d2_cmd_lst.py')
    logger.info('with: %s' % args)
    if subprocess.call(args):
        logger.error('bld_h2d2_cmd_lst.py failed')

    if opts.server:
        try:
            util.rsync('./*.html', dest, port=opts.port)
            util.rsync('./*.css', dest, port=opts.port)
            util.rsync('./*.js', dest, port=opts.port)
            util.rsync('./*.gif', dest, port=opts.port)
        except util.CIError, msg:
            logger.warning(msg)

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def process_args(args):
    parser = optparse.OptionParser()
    parser.add_option('-s', '--server', help='web server name or address')
    parser.add_option('-p', '--port',   help='web server ssh port')
    parser.add_option('-u', '--user',   help='web server ssh user')
    parser.add_option('-b', '--branch', help='the H2D2 branch, if not HEAD', default='HEAD')
    opts, args = parser.parse_args(args)
    if args:
        parser.error('this script does not expect any argument')
    return opts

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def main(args=None):

    if not args:
        args = sys.argv[1:]
    opts = process_args(args)

    for m in ['Modeleur1', 'Modeleur2']:
        logger.info('--- updating webpp for %s ---' % m)
        update_webpp(m, opts)

    logger.info('--- updating webftn for H2D2 ---')
    update_webftn(opts)

    logger.info('--- updating webcmd for H2D2 ---')
    update_webcmd(opts)


try:
    util.check_env()
    if __name__ == '__main__':
        main()
except util.CIError, msg:
    logger.error(msg)
    sys.exit(1)

