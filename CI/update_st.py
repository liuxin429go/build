#!/usr/bin/env python

import os
import sys
import optparse
import util
import logging
logger = util.getlogger('INRS.IEHSS.Fortran')

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def update_string_tbl(prj_dir):
    print 'scanning %s' % util.trunc(prj_dir)
    data = StringTbl.StringTbl()
    modf = data.scanPath(prj_dir, type = StringTbl.Type.Message)
    if modf:
        data.saveTbl()
        print '    MSG_ modified'
    if not data.checkFull():
        print '    MSG_: Entries missing'

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def process_args(args):

    parser = optparse.OptionParser()
    parser.set_usage('Usage: %prog [directories]')
    opts, args = parser.parse_args(args)

    for a in args:
        a = os.path.abspath(a)
        if not os.path.isdir(a):
            parser.error('invalid directory: %s' % a)
    if not args:
        args.append(os.environ['INRS_DEV'])

    return args

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def main(args=None):

    if not args:
        args = sys.argv[1:]
    args = process_args(args)

    required = ['build', 'prjVisual', 'source']
    for a in args:
        for d in util.get_valid_dirs(a, required):
            update_string_tbl(d)


try:
    util.check_env()
    sys.path.append(util.getpath(os.environ['INRS_DEV'], 'tools', 'STEditor'))
    import StringTbl
    if __name__ == '__main__':
        main()
except util.CIError, msg:
    logger.error(msg)
    sys.exit(1)

