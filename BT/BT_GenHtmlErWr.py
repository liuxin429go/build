# -*- coding: UTF-8 -*-
'''
cgi-bin

'''

from BT_Info import *
import BT_HtmlErWr
import os
import string
import sys

arg_date   = "*"
arg_module = "*"
arg_target = "*"
arg_config = "*"

def erreur(msg, status='500 Internal Server Error'):
    print 'Status:', status
    print
    print msg
    sys.exit(0)

def parseEnvString():
    global arg_date, arg_module, arg_target, arg_config

    inp = os.getenv("QUERY_STRING")
    if (inp == None or inp == ""): inp = "?date=*?module=*?target=*?config=*"
    tokens = string.split(inp, "&")

    for tok in tokens:
        if (tok == ""): continue
        stok = string.split(tok, "=")
        var = string.lower(stok[0])
        val = stok[1]
        if (var == "date"):    arg_date    = val
        if (var == "module"):  arg_module  = val
        if (var == "target"):  arg_target = val
        if (var == "config"):  arg_config  = val


def ctrNomAlgo(module, target, config):

    nom = "BTUneDate"

    if (module == "*"): nom = nom + "TousModules"
    else:               nom = nom + "UnModule"

    if (target == "*"): nom = nom + "TousTargets"
    else:               nom = nom + "UnTarget"

    if (config == "*"): nom = nom + "ToutesConfigs"
    else:               nom = nom + "UneConfig"

    return nom

def main():
    global arg_date, arg_module, arg_target, arg_config

    path = "e:/www/intranet/logs"
#    path = "e:/dev/build"

    #---  Parse les info
    parseEnvString()

    #---  Assemble la liste des dates
    dates = []
    for f in os.listdir(path):
        if (f[0:3] != "BT_"): continue
        if (os.path.splitext(f)[1] != ".xml"): continue
        date = os.path.splitext(f)[0]
        date = date[3:]
        dates.append(date)
    dates.sort()
    dates.reverse()

    #---  Date par défaut
    if (len(dates) > 0):
        if (arg_date == "*"): arg_date = dates[0]

    #---  Construis l'algo d'ecriture
    if (len(dates) > 0):
        nomAlgo = ctrNomAlgo(arg_module, arg_target, arg_config)
        algo = BT_HtmlErWr.__dict__[nomAlgo](arg_date, arg_module, arg_target, arg_config)
        algo.xeq(dates, path)


if __name__ == '__main__':
    main()
