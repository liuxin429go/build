# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import os
import SCons

class parmetis(ExternalLib.ExternalLib):
    def __do(self, ctx, dfn = [] ):
        hdir = os.environ['INRS_LXT']
        hdir = Util.getPathFromVersion(hdir, 'parmetis', search_join='-')
        vers = Util.getVersionFromPath(hdir, 'parmetis')
        hdir = os.path.join(hdir, '_run', Util.addBuildPath(r'External'))
        __inc = [ hdir ]
        __lpt = [ hdir ]
        __lib = ['ParMETISLib', 'METISLib' ]
        if vers[0] > 3: __lib += ['GKlib']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc, dfn, ctx.dfn)

    #---------------
    #---    CMC
    #---------------
    def __cmc_unx32(self, ctx):
        self.__do(ctx)

    def __cmc_unx64(self, ctx):
        self.__do(ctx)

    def __cmcitl_unx32(self, ctx):
        self.__do(ctx)

    def __cmcitl_unx64(self, ctx):
        self.__do(ctx)

    def __cmcpgi_unx32(self, ctx):
        self.__do(ctx)

    def __cmcpgi_unx64(self, ctx):
        self.__do(ctx)

    #---------------
    #---    Linux 32
    #---------------
    def __gcc_unx32(self, ctx):
        self.__do(ctx)

    def __gccX86_unx32(self, ctx):
        self.__do(ctx)

    def __intel_unx32(self, ctx):
        self.__do(ctx)

    def __itlX86_unx32(self, ctx):
        self.__do(ctx)

    def __sun_unx32(self, ctx):
        self.__do(ctx)

    #---------------
    #---    Linux 64
    #---------------
    def __gcc_unx64(self, ctx):
        self.__do(ctx)

    def __gccX64_unx64(self, ctx):
        self.__do(ctx)

    def __intel_unx64(self, ctx):
        self.__do(ctx)

    def __itlX64_unx64(self, ctx):
        self.__do(ctx)

    def __open64_unx64(self, ctx):
        self.__do(ctx)

    def __sun_unx64(self, ctx):
        self.__do(ctx)

    #---------------
    #---    Linux 64i8
    #---------------
    def __gcc_unx64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    def __gccX64_unx64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    def __intel_unx64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    def __itlX64_unx64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    def __open64_unx64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    def __sun_unx64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    #-----------------
    #---    Windows 32
    #-----------------
    def __gcc_win32(self, ctx):
        self.__do(ctx)

    def __gccX86_win32(self, ctx):
        self.__do(ctx)

    def __intel_win32(self, ctx):
        self.__do(ctx)

    def __itlX86_win32(self, ctx):
        self.__do(ctx)

    #-----------------
    #---    Windows 64
    #-----------------
    def __gcc_win64(self, ctx):
        self.__do(ctx)

    def __gccX64_win64(self, ctx):
        self.__do(ctx)

    def __intel_win64(self, ctx):
        self.__do(ctx)

    def __itlX64_win64(self, ctx):
        self.__do(ctx)

    def __itlI64_win64(self, ctx):
        self.__do(ctx)

    #---------------
    #---    Linux 64i8
    #---------------
    def __gcc_win64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    def __gccX64_win64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    def __intel_win64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    def __itlX64_win64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    def __itlI64_win64i8(self, ctx):
        self.__do(ctx, dfn = [ 'IDXTYPE_INT64' ])

    #-----------------
    #---    Autres
    #-----------------
    def __ftnchek_win32(self, ctx):
        raise SCons.Errors.UserError('parmetis.__ftncheck_win32 not defined')

    def configure(self, ctx, tool, version, plateform):
        fnm = '_parmetis__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
