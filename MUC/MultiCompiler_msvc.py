# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#-----------------------------------------------------------------------------------------------------

import MUC_platform

import os
import subprocess

ProgramFiles = ''
try:
    ProgramFiles = os.environ['ProgramFiles(x86)']
except:
    try:
        ProgramFiles = os.environ['ProgramFiles']
    except:
        pass

def pJoin(*args):
    res = ''
    for a in args:
        if (a[0] == '"' or a[0] == "'"): a = a[1:-1]
        res = os.path.join(res, a)
    return '"%s"' % os.path.normpath(res)

"""
msvc - Configuration
"""
"""
http://www.viva64.com/articles/Wp64_VS.html#IDARJU2E
Why is /Wp64 key declared deprecated in Visual Studio 2008?
There is a wrong opinion that /Wp64 key is declared deprecated because diagnosis of 64-bit errors has
become much better in Visual Studio 2008. But it is not so.
/Wp64 key is declared deprecated in Visual Studio 2008 only because it has become unnecessary. The time
to prepare for 64-bit code has passed and now it's high time to create 64-bit programs. For that there
is a 64-bit compiler in Visual Studio 2008 (as well as in Visual Studio 2005).
/Wp64 key is useful only in mode of compilation of 32-bit programs. It was created to detect some errors
in time which the program will face in future after migration on 64-bit systems.
During compilation of a 64-bit program /Wp64 key is of no purpose. The compiler of 64-bit applications carries
automatic checks similar to /Wp64 but more accurate. While compiling 32-bit programs /Wp64 mode glitched and it
resulted in false error messages. It is not very pleasant and many developers complained of it and asked to
upgrade this mode. Visual C++ developers have acted, in my opinion, very reasonably. Instead of wasting time
on upgrading /Wp64 they declared it outdated. By this they:
encourage programmers to compile their programs with the help of the 64-bit compiler;
simplify the system of the compiler's commands (which is overload enough) by removing the temporary auxiliary key;
get rid of requests to upgrade this key.
"""

class cfg_c:
    def getCfg(self, cplr, c):
        flags_bse = [
                    '-c',
                    '-nologo',
                    '-W3',                  # print only errors and warnings (DEFAULT)
#                    '-Wp64',                # print diagnostics for 64-bit porting
                    '-Zp16',                # specify alignment constraint for structures
                    '-EHsc',                # enable synchronous C++ exception handling model / assume extern "C" functions do not throw exceptions
                    '-Zc:wchar_t',          # specify that wchar_t is a native data type
                    '-Zc:forScope',         # specify standard conformance
                    '-GR',                  # enable C++ RTTI
                    '-Gd'                   # make __cdecl the default calling convention
                    ]
        flags_cfg = {
                    'debug'   : [
                                '-MD',      # use dynamically-loaded, multithread C runtime
                                '-Od',      # disable optimizations
                                '-Ob0',     # disables inlining
                                '-Zi',      # produce symbolic debug information in object file
                                '-RTCsu'    # enable runtime checks
                                ],
                    'release' : [
                                '-MD',      # use dynamically-loaded, multithread C runtime
                                '-O2',      # optimize for maximum speed
                                #'-O3',      # optimize for maximum speed and enable high-level optimizations
                                '-Ob1',     # inline functions declared with __inline, and perform C++ inlining
                                #'-Ob2',     # inline any function, at the compiler's discretion
                                #'-QxW',     # Intel Pentium 4 and compatible Intel processors
                                '-Qip',     # enable single-file IP optimizations
                                #'-Qopenmp', # enable the compiler to generate multi-threaded code based on the OpenMP directives
                                #'-Qparallel'# enable the auto-parallelizer
                                '-Zi'       # produce symbolic debug information in object file
                                ]
                    }
        return flags_bse + flags_cfg[c]

    def getPtf(self, cplr, p):
        flags_ptf = { 'unx32' : [], 'win32' : [] }
        return flags_ptf[p]

class cfg_cpp(cfg_c):
    def getCfg(self, cplr, c):
        flags_bse = ['-TP' ]
        flags_cfg = { 'debug' : [], 'release' : [] }
        return cfg_c.getCfg(self, cplr, c) + flags_bse + flags_cfg[c]

    def getPtf(self, cplr, p):
        flags_ptf = { 'unx32' : [], 'win32' : [] }
        return cfg_c.getPtf(self, cplr, p) + flags_ptf[p]

"""
Msvc générique
"""
class msvc:
    def __win(self, setup, cmplr, cfg):
        cfg.bldResponseFiles()
        s = '%s > nul' % setup
        c = '%s -Fo%s @%s @%s @%s %s' % (cmplr, cfg.fic_out, cfg.cpl_fcfg, cfg.cpl_finc, cfg.cpl_fdfn, cfg.fic_inp)

        _stderr = None
        _stdout = None
        _shell  = True
        if (cfg.fic_err):
            _stdout = open(cfg.fic_err, 'w')
            _stderr = subprocess.STDOUT
        if (MUC_platform.is_win()):
            _shell = False

        retcode = subprocess.call(' && '.join( [s, c] ), stdout=_stdout, stderr=_stderr, shell=_shell)

    def c(self, setup, cmplr, cfg):
        if (MUC_platform.is_win()):
            self.__win(setup, cmplr, cfg)
        else:
            raise 'Invalid msvc platform: %s' % MUC_platform.build_platform()

    def cpp(self, setup, cmplr, cfg):
        if (MUC_platform.is_win()):
            self.__win(setup, cmplr, cfg)
        else:
            raise 'Invalid msvc platform: %s' % MUC_platform.build_platform()

    def c_cfg(self, cfg):
        return cfg_c().getCfg(self, cfg.tgt_cfg) + cfg_c().getPtf(self, cfg.tgt_ptf)

    def cpp_cfg(self, cfg):
        return cfg_cpp().getCfg(self, cfg.tgt_cfg) + cfg_cpp().getPtf(self, cfg.tgt_ptf)

"""
VC C++ 7.0
"""
class msvc_70(msvc):
    def c(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'Microsoft Visual Studio .NET\Vc7\bin')
        setup = pJoin(bin_dir, 'vcvars32.bat')
        cmplr = pJoin(bin_dir, 'cl.exe')
        msvc.c(self, setup, cmplr, cfg)

    def cpp(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'Microsoft Visual Studio .NET\Vc7\bin')
        setup = pJoin(bin_dir, 'vcvars32.bat')
        cmplr = pJoin(bin_dir, 'cl.exe')
        msvc.cpp(self, setup, cmplr, cfg)

"""
VC C++ 7.1
"""
class msvc_71(msvc):
    def c(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'Microsoft Visual Studio .NET 2003\Vc7\bin')
        setup = pJoin(bin_dir, 'vcvars32.bat')
        cmplr = pJoin(bin_dir, 'cl.exe')
        msvc.c(self, setup, cmplr, cfg)

    def cpp(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'Microsoft Visual Studio .NET 2003\Vc7\bin')
        setup = pJoin(bin_dir, 'vcvars32.bat')
        cmplr = pJoin(bin_dir, 'cl.exe')
        msvc.cpp(self, setup, cmplr, cfg)

"""
VC C++ 9.0
"""
class msvc_90(msvc):
    def c(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'Microsoft Visual Studio 9.0\VC\bin')
        setup = pJoin(bin_dir, 'vcvars32.bat')
        cmplr = pJoin(bin_dir, 'cl.exe')
        msvc.c(self, setup, cmplr, cfg)

    def cpp(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'Microsoft Visual Studio 9.0\VC\bin')
        setup = pJoin(bin_dir, 'vcvars32.bat')
        cmplr = pJoin(bin_dir, 'cl.exe')
        msvc.cpp(self, setup, cmplr, cfg)

"""
VC C++ 14.0
"""
class msvc_140(msvc):
    def c(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'Microsoft Visual Studio 14.0\VC\bin')
        setup = pJoin(bin_dir, 'vcvars32.bat')
        cmplr = pJoin(bin_dir, 'cl.exe')
        msvc.c(self, setup, cmplr, cfg)

    def cpp(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'Microsoft Visual Studio 14.0\VC\bin')
        setup = pJoin(bin_dir, 'vcvars32.bat')
        cmplr = pJoin(bin_dir, 'cl.exe')
        msvc.cpp(self, setup, cmplr, cfg)
        