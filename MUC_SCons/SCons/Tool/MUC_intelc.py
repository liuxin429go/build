"""SCons.Tool.icl

Tool-specific initialization for the Intel C/C++ compiler.
Supports Linux and Windows compilers, v7 and up.

There normally shouldn't be any need to import this module directly.
It will usually be imported through the generic SCons.Tool.Tool()
selection method.

"""

#
# Copyright (c) 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010 The SCons Foundation
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#

__revision__ = "src/engine/SCons/Tool/intelc.py 4629 2010/01/17 22:23:21 scons"

import math, sys, os, glob, string, re
import tempfile
import shutil
import subprocess

#---  Code for Python 2.4 (i.e Lachine)"
try:
    check_output = subprocess.check_output
except AttributeError:
    def check_output(*popenargs, **kwargs):
        # lines 537-545 copied and pasted from 2.7 source
        # https://hg.python.org/cpython/file/2.7/Lib/subprocess.py
        if 'stdout' in kwargs:
            raise ValueError('stdout argument not allowed, it will be overridden.')
        process = subprocess.Popen(stdout=subprocess.PIPE, *popenargs, **kwargs)
        output, unused_err = process.communicate()
        retcode = process.poll()
        if retcode:
            cmd = kwargs.get("args")
            if cmd is None:
                cmd = popenargs[0]
            raise subprocess.CalledProcessError(retcode, cmd, output=output)
        return output

is_windows = sys.platform == 'win32'
is_win64 = is_windows and (os.environ['PROCESSOR_ARCHITECTURE'] == 'AMD64' or
                           ('PROCESSOR_ARCHITEW6432' in os.environ and
                            os.environ['PROCESSOR_ARCHITEW6432'] == 'AMD64'))
is_linux = sys.platform in [ 'linux', 'linux2' ]
is_mac   = sys.platform == 'darwin'

if is_windows:
    import SCons.Tool.msvc
    import RegistryWalker
elif is_linux:
    import SCons.Tool.gcc
elif is_mac:
    import SCons.Tool.gcc
import SCons.Util
import SCons.Warnings

# Exceptions for this tool
class IntelCError(SCons.Errors.InternalError):
    pass
class MissingRegistryError(IntelCError):  # missing registry entry
    pass
class MissingDirError(IntelCError):       # dir not found
    pass
class NoRegistryModuleError(IntelCError): # can't read registry at all
    pass

class IntelVersion:
    def __init__(self, ver, inst_dir = None, reg_key = 'Not set', reg_sub = []):
        self.ver = ver
        self.nrm = linux_ver_normalize(ver)
        if (math.modf(self.nrm)[0] == 0.0):
            self.fuz = 0.001
        else:
            self.fuz = 0.1
        self.reg_key = reg_key
        self.reg_sub = reg_sub
        self.reg_abi = None
        self.dir_cpl = inst_dir
        self.dir_abi = None

    def __findPath(self, __pth, __what):
      __CCPath = None
      try:
         for d in os.listdir(__pth):
            __fullPath = os.path.join(__pth, d)
            if (not __CCPath) and os.path.isdir(__fullPath):
               __CC = os.path.join(__fullPath, __what)
               if os.path.exists(__CC):
                  __CCPath = __CC
                  break
               else:
                  __CCPath = self.__findPath(__fullPath, __what)
      except:
         pass
      return __CCPath

    def __genTmpFic(self, v):
        icc_path = self.__findPath(self.dir_cpl, 'iccvars.sh')
        fd, fc = tempfile.mkstemp('.sh', '~muc_', None, 'w+t')
        f = os.fdopen(fd, "w+t")
        f.write('#!/bin/bash\n')
        f.write('. %s %s \n' % (icc_path, self.dir_abi))
        f.write('echo $%s\n' %v)
        f.close()
        os.chmod(fc, 0o750)
        return fc

    def binDir(self):
        if is_windows: ptf = 'windows'
        if is_linux:   ptf = 'linux'
        if is_mac:     ptf = 'darwin'
        return os.path.join(self.dir_cpl, ptf, 'bin', self.dir_abi)

    def pthDir(self):
        fc = self.__genTmpFic('PATH')
        try:
            r = check_output(fc, shell=True).strip()
        finally:
            if fc: os.remove(fc)
        return r

    def libDir(self):
        fc = self.__genTmpFic('LIBRARY_PATH')
        try:
            r = check_output(fc, shell=True).strip()
        finally:
            if fc: os.remove(fc)
        return r

    def incDir(self):
        fc = self.__genTmpFic('INCLUDE')
        try:
            r = check_output(fc, shell=True).strip()
        finally:
            if fc: os.remove(fc)
        return r

    def __hash__(self):
        return hash(self.ver)

    def __eq__(self, other):
        return math.fabs(self.nrm - other.nrm) < max(self.fuz, other.fuz)

    def __lt__(self, other):
        return self.nrm < other.nrm

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        if is_windows:
            return '%s in %s (%f)' % (self.ver, self.reg_key, self.nrm)
        else:
            return '%s (%s)' % (self.ver, self.nrm)

def uniquify(s):
    """Return a sequence containing only one copy of each unique element from input sequence s.
    Does not preserve order.
    Input sequence must be hashable (i.e. must be usable as a dictionary key)."""
    u = {}
    for x in s:
        u[x] = 1
    return list(u.keys())

def linux_ver_normalize(vstr):
    """Normalize a Linux compiler version number.
    Intel changed from "80" to "9.0" in 2005, so we assume if the number
    is greater than 60 it's an old-style number and otherwise new-style.
    Always returns an old-style float like 80 or 90 for compatibility with Windows.
    Shades of Y2K!"""
    f = 0.0
    # Check for version number like 9.1.026: return 91.026
    v = vstr.split('.')
    vmaj = int(v[0])
    if (vmaj < 9):
        f = float(v[0]) * 10
    elif (vmaj in range(9, 19+1)):
        try:
            f += float(v[0]) * 10.          # maj
            f += float(v[1])                # min
            if (len(v) == 3):
                f += float(v[2]) / 10000.   # rev
            else:
                f += float(v[2]) / 100.     # update
                f += float(v[3]) / 100000.  # rev
        except:
            pass
    else:
        f = float(vstr)
    return f

def check_abi(abi):
    """Check for valid ABI (application binary interface) name,
    and map into canonical one"""
    if not abi:
        return None
    abi = abi.lower()
    # valid_abis maps input name to canonical name
    if is_windows:
        valid_abis = {'ia32'  : 'ia32',
                      'x86'   : 'ia32',
                      'ia64'  : 'ia64',
                      'em64t' : 'em64t',
                      'amd64' : 'em64t'}
    if is_linux:
        valid_abis = {'ia32'   : 'ia32',
                      'x86'    : 'ia32',
                      'x86_64' : 'x86_64',
                      'em64t'  : 'x86_64',
                      'amd64'  : 'x86_64'}
    if is_mac:
        valid_abis = {'ia32'   : 'ia32',
                      'x86'    : 'ia32',
                      'x86_64' : 'x86_64',
                      'em64t'  : 'x86_64'}
    try:
        abi = valid_abis[abi]
    except KeyError:
        raise SCons.Errors.UserError("Intel compiler: Invalid ABI %s, valid values are %s"% \
              (abi, list(valid_abis.keys())))
    return abi

def get_version_from_list(v, vlist):
    """See if we can match v (string) in vlist (list of strings)"""

    try:
        r = None
        v_ = IntelVersion(v)
        for vi in reversed(vlist):
            if vi == v_: return vi
    except:
        pass
    return None

def get_intel_registry_value(valuename, key=None, skey=None):
    """
    Return a value from the Intel compiler registry tree. (Windows only)
    """

    v = None
    try:
        k = key
        if (skey): k = '\\'.join( (k, skey) )
        for (key_name, key), subkey_names, values in RegistryWalker.walk(k):
            for name, data, datatype in values:
                if (name == valuename):
                    v = data
                    break
    except WindowsError:
        pass

    if (not v):
        msg = "%s was not found in the registry, for Intel compiler version %s, abi='%s'" % (valuename, key, skey)
        raise MissingRegistryError(msg)

    return v

def get_all_compiler_versions():
    """Returns a sorted list of strings, like "70" or "80" or "9.0"
    with most recent compiler version first.
    """
    versions=[]
    if is_windows:
        if is_win64:
            keypaths = [
                        'HKEY_LOCAL_MACHINE\\Software\\WoW6432Node\\Intel\\Compilers\\C++',
                        'HKEY_LOCAL_MACHINE\\Software\\WoW6432Node\\Intel\\Suites',
                       ]
        else:
            keypaths = [
                        'HKEY_LOCAL_MACHINE\\Software\\Intel\\Compilers\\C++',
                        'HKEY_LOCAL_MACHINE\\Software\\Intel\\Suites',
                       ]
        for k in keypaths:
            kname, skeys = None, None
            ver = [-1]*4
            try:
                isWindows = True
                for (key_name, key), subkey_names, values in RegistryWalker.walk(k):
                    for name, data, datatype in values:
                        if (name == 'DisplayString'):
                            m = re.search( r'(?P<v>(([0-9]+)(\.[0-9]+)+))', data)
                            if m:
                                v = '.'.join(['%d' % v for v in ver if v >= 0])
                                inst_dir = get_intel_registry_value('ProductDir', kname)[:-1]
                                if isWindows:
                                    versions.append( IntelVersion(v, inst_dir=inst_dir, reg_key=kname, reg_sub=skeys) )
                            isWindows = True
                        elif (name == 'Major Version'):
                            ver[0] = data
                            kname = key_name
                            skeys = subkey_names
                            isWindows = True
                        elif (name == 'Minor Version'):
                            ver[1] = data
                        elif (name == 'Revision'):
                            ver[2] = data
                        elif (name == 'Target'):
                            if (data != 'Windows'): isWindows = False

            except WindowsError as e:
                pass
    elif is_linux:
        linux_dir_name_mapping = {
            'composer_xe_2011_sp1.0.*'          : '12.0.0.',
            'composer_xe_2011_sp1.1.*'          : '12.0.1.',
            'composer_xe_2011_sp1.2.*'          : '12.0.2.',
            'composer_xe_2011_sp1.3.*'          : '12.0.3.',
            'composer_xe_2011_sp1.4.*'          : '12.0.4.',
            'composer_xe_2011_sp1.5.*'          : '12.0.5.',
            'composer_xe_2011_sp1.6.*'          : '12.1.0.',
            'composer_xe_2011_sp1.7.*'          : '12.1.1.',
            'composer_xe_2011_sp1.8.*'          : '12.1.2.',
            'composer_xe_2011_sp1.9.*'          : '12.1.3.',
            'composer_xe_2011_sp1.10.*'         : '12.1.4.',
            'composer_xe_2011_sp1.11.*'         : '12.1.5.',
            'composer_xe_2011_sp1.12.*'         : '12.1.6.',
            'composer_xe_2011_sp1.13.*'         : '12.1.7.',
            'composer_xe_2013.0.*'              : '13.0.0.',
            'composer_xe_2013.1.*'              : '13.0.1.',
            'composer_xe_2013.2.*'              : '13.1.0.',
            'composer_xe_2013.3.*'              : '13.1.1.',
            'composer_xe_2013.4.*'              : '13.1.2.',
            'composer_xe_2013.5.*'              : '13.1.3.',
            'composer_xe_2013_sp1.0.*'          : '14.0.0.',
            'composer_xe_2013_sp1.1.*'          : '14.0.1.',
            'composer_xe_2013_sp1.2.*'          : '14.0.2.',
            'composer_xe_2013_sp1.3.*'          : '14.0.3.',
            'composer_xe_2013_sp1.4.*'          : '14.0.4.',
            'composer_xe_2015.*'                : '15.0.1.',
            'compilers_and_libraries_2016.1.*'  : '16.0.1.',
            'compilers_and_libraries_2016.3.*'  : '16.0.3.',
            'compilers_and_libraries_2017.2.*'  : '17.0.2.',
            'compilers_and_libraries_2017.4.*'  : '17.0.4.',
            'compilers_and_libraries_2017.5.*'  : '17.0.5.',
            'compilers_and_libraries_2017.6.*'  : '17.0.6.',
            'compilers_and_libraries_2018.2.*'  : '18.0.2.',
            'compilers_and_libraries_2018.3.*'  : '18.0.3.',
            'compilers_and_libraries_2019.1.*'  : '19.0.1.',
            'compilers_and_libraries_2019.2.*'  : '19.0.2.',
            'compilers_and_libraries_2019.3.*'  : '19.0.3.',
            'compilers_and_libraries_2019.4.*'  : '19.0.4.',
            'compilers_and_libraries_2019.5.*'  : '19.0.5.',
            'compilers_and_libraries_2020.4.*'  : '19.1.3.',
        }
        for d in glob.glob('/opt/intel_cc_*'):
            # Typical dir here is /opt/intel_cc_80.
            m = re.search(r'cc_(.*)$', d)
            if m:
                v = m.group(1)
                versions.append( IntelVersion(v, inst_dir=d) )
        for d in glob.glob('/opt/intel/cc*/*'):
            # Typical dir here is /opt/intel/cc/9.0 for IA32,
            # /opt/intel/cce/9.0 for EMT64 (AMD64)
            m = re.search(r'([0-9.]+)$', d)
            if m:
                v = m.group(1)
                versions.append( IntelVersion(v, inst_dir=d) )
        for d in glob.glob('/opt/intel/Compiler/*/*'):
            #Typical dir here is /opt/intel/Compiler/11.0/069
            m = re.search( r'((?P<v>[0-9.]+)/(?P<s>[0-9]+))', d)
            if m:
                v = '.'.join( [m.group('v'), m.group('s')] )
                versions.append( IntelVersion(v, inst_dir=d) )
        for d in glob.glob('/opt/intel/compilerpro-*'):
            #Typical dir here is /opt/intel/compilerpro-12.0.0.084
            m = re.search( r'((?P<v>[0-9.]+))', d)
            if m:
                v = m.group('v')
                versions.append( IntelVersion(v, inst_dir=d) )
        for r, v in linux_dir_name_mapping.items():
            for d in glob.glob( os.path.join('/opt/intel', r) ):
                m = re.search( r'(\.(?P<s>[0-9]+))$', d)
                if m:
                    v = '%s%s' % (v, m.group('s'))
                    versions.append( IntelVersion(v, inst_dir=d) )
    elif is_mac:
        for d in glob.glob('/opt/intel/cc*/*'):
            # Typical dir here is /opt/intel/cc/9.0 for IA32,
            # /opt/intel/cce/9.0 for EMT64 (AMD64)
            m = re.search(r'([0-9.]+)$', d)
            if m:
                v = m.group(1)
                versions.append( IntelVersion(v, inst_dir=d) )

    versions = uniquify(versions)       # remove dups
    versions.sort()
    return versions

def get_compiler_top(version, abi):
    """
    Load into version the information specific to abi.
    """

    if is_windows:
        abi_skeys = {'ia32' : ['ia32'],
                     'ia64' : ['Itanium', 'ia64', 'ia32_ia64'],
                     'em64t': ['intel64', 'em64t', 'em64t_native', 'ia32_intel64', 'amd64']}

        inst_dir = version.dir_cpl
        found = False
        for a in abi_skeys[abi]:
            if os.path.exists(os.path.join(inst_dir, "bin", a, "icl.exe")):
                version.dir_abi = a
                found = True
                break
        for a in abi_skeys[abi]:
            for a2 in version.reg_sub:
                if (a.lower() == a2.lower()):
                    chk_val = get_intel_registry_value('LibDir', version.reg_key, a2)
                    version.reg_abi = a2
                    break
        if not found:
            raise MissingDirError(\
                  "Can't find version %s Intel compiler in %s (abi='%s')"%(version.dir_abi, version.dir_cpl, abi))
    elif is_mac or is_linux:
        abi_skeys = {'ia32'   : ['ia32'],
                     'x86_64' : ['x86_64', 'intel64']}
        inst_dir = version.dir_cpl
        found = False
        for a in abi_skeys[abi]:
            if os.path.exists(os.path.join(inst_dir, "bin", a, "icc")):
                version.dir_abi = a
                found = True
                break
            if os.path.exists(os.path.join(inst_dir, "linux", "bin", a, "icc")):
                version.dir_abi = a
                found = True
                break
        if not found:
            raise MissingDirError(\
                  "Can't find version %s Intel compiler in %s (abi='%s')"%(version.dir_abi, version.dir_cpl, abi))

    return version

def generate(env, version=None, abi=None, topdir=None, verbose=0):
    """Add Builders and construction variables for Intel C/C++ compiler
    to an Environment.
    args:
      version: (string) compiler version to use, like "80"
      abi:     (string) 'win32' or whatever Itanium version wants
      topdir:  (string) compiler top dir, like
                         "c:\Program Files\Intel\Compiler70"
                        If topdir is used, version and abi are ignored.
      verbose: (int)    if >0, prints compiler version used.
    """
    if not (is_mac or is_linux or is_windows):
        # can't handle this platform
        return

    if is_windows:
        SCons.Tool.msvc.generate(env)
    elif is_linux:
        SCons.Tool.gcc.generate(env)
    elif is_mac:
        SCons.Tool.gcc.generate(env)

    # if version is unspecified, use latest
    vlist = get_all_compiler_versions()
    if not version:
        if vlist:
            version = vlist[-1]
    else:
        # User may have specified '90' but we need to get actual dirname '9.0'.
        # get_version_from_list does that mapping.
        v = get_version_from_list(version, vlist)
        if not v:
            print (__file__)
            raise SCons.Errors.UserError(
                  "Invalid Intel compiler version %s: " % version +
                  "installed versions are %s" % (', '.join([str(v) for v in vlist])) )
        version = v

    # if abi is unspecified, use ia32
    # alternatives are ia64 for Itanium, or amd64 or em64t or x86_64 (all synonyms here)
    abi = check_abi(abi)
    if abi is None:
        if is_mac or is_linux:
            # Check if we are on 64-bit linux, default to 64 then.
            uname_m = os.uname()[4]
            if uname_m == 'x86_64':
                abi = 'x86_64'
            else:
                abi = 'ia32'
        else:
            if is_win64:
                abi = 'em64t'
            else:
                abi = 'ia32'
        abi = check_abi(abi)

    if version and not topdir:
        try:
            version = get_compiler_top(version, abi)
            topdir = version.dir_cpl
        except (SCons.Util.RegError, IntelCError):
            topdir = None

    if not topdir:
        # Normally this is an error, but it might not be if the compiler is
        # on $PATH and the user is importing their env.
        class ICLTopDirWarning(SCons.Warnings.SConsWarning):
            pass
        if (is_mac or is_linux) and not env.Detect('icc') or \
           is_windows and not env.Detect('icl'):

            SCons.Warnings.enableWarningClass(ICLTopDirWarning)
            SCons.Warnings.warn(ICLTopDirWarning,
                                "Failed to find Intel compiler for version='%s', abi='%s'"%
                                (str(version), str(abi)))
        else:
            # should be cleaned up to say what this other version is
            # since in this case we have some other Intel compiler installed
            SCons.Warnings.enableWarningClass(ICLTopDirWarning)
            SCons.Warnings.warn(ICLTopDirWarning,
                                "Can't find Intel compiler top dir for version='%s', abi='%s'"%
                                    (str(version), str(abi)))

    if topdir:
        if verbose:
            print(("Intel C compiler: using version %s (%s), abi %s, in '%s'"%\
                  (repr(version), str(version.fuz), abi, topdir)))
            # Show the actual compiler version by running the compiler.
            if is_windows:
                cplr = os.path.normpath( os.path.join(version.binDir(), "icl") )
                subprocess.call([cplr, "/QV"])
            elif is_linux:
                cplr = os.path.normpath( os.path.join(version.binDir(), "icc") )
                subprocess.call([cplr, "--version"])
            elif is_mac:
                cplr = os.path.normpath( os.path.join(topdir, "bin", abi, "icc") )
                subprocess.call([cplr, "--version"])

        env['INTEL_C_COMPILER_TOP'] = topdir
        if is_linux:
            env.PrependENVPath('INCLUDE', version.incDir())
            env.PrependENVPath('LIBRARY_PATH', version.libDir())
            env.PrependENVPath('PATH',    version.pthDir())
            env.PrependENVPath('LD_LIBRARY_PATH', version.libDir())
        if is_mac:
            env.PrependENVPath('INCLUDE', version.incDir())
            env.PrependENVPath('LIB',     version.libDir())
            env.PrependENVPath('PATH',    version.pthDir())
            env.PrependENVPath('LD_LIBRARY_PATH', version.libDir())
        if is_windows:
            #       env key    reg valname   default subdir of top
            paths=(('INCLUDE', 'IncludeDir', 'Include'),
                   ('LIB'    , 'LibDir',     'Lib'),
                   ('PATH'   , 'BinDir',     'Bin'))
            # We are supposed to ignore version if topdir is set, so set
            # it to the emptry string if it's not already set.
            if version is None:
                version = ''
            # Each path has a registry entry, use that or default to subdir
            for p in paths:
                try:
                    path=get_intel_registry_value(p[1], version.reg_key, version.reg_abi)
                    # These paths may have $(ICInstallDir)
                    # which needs to be substituted with the topdir.
                    path = path.replace('$(ICInstallDir)', version.dir_cpl + os.sep)
                except IntelCError as e:
                    # Couldn't get it from registry: use default subdir of topdir
                    env.PrependENVPath(p[0], os.path.join(topdir, p[2]))
                else:
                    env.PrependENVPath(p[0], path.split(os.pathsep))
                    # print("ICL %s: %s, final=%s"%(p[0], path, str(env['ENV'][p[0]])))

    if is_windows:
        if shutil.which('xilink'):
            iclvars = None
        else:
            env['INTEL_ICLVARS_CMD']  = os.path.join(version.binDir(), '..', 'iclvars.bat')
            env['INTEL_ICLVARS_KIND'] = 'ia32' if abi == 'ia32' else 'intel64'
            iclvars = '"$INTEL_ICLVARS_CMD" $INTEL_ICLVARS_KIND > NUL'
        env['CC']        = 'icl'
        env['CXX']       = 'icl'
        env['LINK']      = ' && '.join( [iclvars, 'xilink'] ) if iclvars else 'xilink'
        env['AR']        = ' && '.join( [iclvars, 'xilib'] )  if iclvars else 'xilib'

    else:
        env['INTEL_ICLVARS_CMD']  = os.path.join(version.binDir(), '..', 'iccvars.sh')
        env['INTEL_ICLVARS_KIND'] = 'ia32' if abi == 'ia32' else 'intel64'
        iclvars = '"$INTEL_ICLVARS_CMD" $INTEL_ICLVARS_KIND > /dev/null'
        env['CC']        = 'icc'
        env['CXX']       = 'icpc'
        # Don't reset LINK here;
        # use smart_link which should already be here from link.py.
        #env['LINK']      = '$CC'
        env['AR']        = ' && '.join( [iclvars, 'xiar'] )
        env['LD']        = ' && '.join( [iclvars, 'xild'] ) # not used by default

    # This is not the exact (detailed) compiler version,
    # just the major version as determined above or specified
    # by the user.  It is a float like 80 or 90, in normalized form for Linux
    # (i.e. even for Linux 9.0 compiler, still returns 90 rather than 9.0)
    if version:
        env['INTEL_C_COMPILER_VERSION'] = version.ver # linux_ver_normalize(version)

    if is_windows:
        # Look for license file dir
        # in system environment, registry, and default location.
        envlicdir = os.environ.get("INTEL_LICENSE_FILE", '')
        K = ('SOFTWARE\Intel\Licenses')
        try:
            k = SCons.Util.RegOpenKeyEx(SCons.Util.HKEY_LOCAL_MACHINE, K)
            reglicdir = SCons.Util.RegQueryValueEx(k, "w_cpp")[0]
        except (AttributeError, SCons.Util.RegError):
            reglicdir = ""
        defaultlicdir = os.environ.get('CommonProgramFiles', r'C:\Program Files\Common Files') + r'\Intel\Licenses'

        licdir = None
        alldir = envlicdir.split(';')
        alldir.append(reglicdir)
        for ld in alldir:
            # If the string contains an '@', then assume it's a network
            # license (port@system) and good by definition.
            if ld and (ld.find('@') != -1 or os.path.exists(ld)):
                licdir = ld
                break
        if not licdir:
            licdir = defaultlicdir
            if not os.path.exists(licdir):
                class ICLLicenseDirWarning(SCons.Warnings.SConsWarning):
                    pass
                SCons.Warnings.enableWarningClass(ICLLicenseDirWarning)
                SCons.Warnings.warn(ICLLicenseDirWarning,
                                    "Intel license dir was not found."
                                    "  Tried using the INTEL_LICENSE_FILE environment variable (%s), the registry (%s) and the default path (%s)."
                                    "  Using the default path as a last resort."
                                        % (envlicdir, reglicdir, defaultlicdir))
        env['ENV']['INTEL_LICENSE_FILE'] = licdir

def exists(env):
    if not (is_mac or is_linux or is_windows):
        # can't handle this platform
        return 0

    try:
        versions = get_all_compiler_versions()
    except (SCons.Util.RegError, IntelCError):
        versions = None
    detected = versions is not None and len(versions) > 0
    if not detected:
        # try env.Detect, maybe that will work
        if is_windows:
            return env.Detect('icl')
        elif is_linux:
            return env.Detect('icc')
        elif is_mac:
            return env.Detect('icc')
    return detected

# end of file
