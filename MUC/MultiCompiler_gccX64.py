# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#-----------------------------------------------------------------------------------------------------

import MultiCompiler_gcc

gccx64_xx = MultiCompiler_gcc.gcc_xx

gccx64_42 = MultiCompiler_gcc.gcc_42
gccx64_43 = MultiCompiler_gcc.gcc_43
gccx64_44 = MultiCompiler_gcc.gcc_44
gccx64_45 = MultiCompiler_gcc.gcc_45
gccx64_46 = MultiCompiler_gcc.gcc_46

gccx64_9  = MultiCompiler_gcc.gcc_9
gccx64_91 = MultiCompiler_gcc.gcc_91
gccx64_92 = MultiCompiler_gcc.gcc_92
gccx64_93 = MultiCompiler_gcc.gcc_93

