# -*- coding: UTF-8 -*-
import SCons

pyinstAction = SCons.Action.Action("$PYINST_COM")

def pyinst_src_suffix_emitter(env, source):
    return "$PYINST_SRCSUFFIX"

def create_builder(env):
    try:
        pyinst = env['BUILDERS']['PyInstaller']
    except KeyError:
        pyinst = SCons.Builder.Builder(
                  action = pyinstAction,
                  emitter = {},
                  src_suffix = pyinst_src_suffix_emitter,
                  single_source = 1)
        env['BUILDERS']['PyInstaller'] = pyinst

    return pyinst

def generate(env):
    env["PYINST"]               = "pyinstaller"
    env["PYINST_FLAGS"]         = "--log-level=INFO --noconfirm"
    env['PYINST_LIBFLAGS']      = "${_concat('-L', PYINST_LIBPATH, SOURCE.dir, __env__)}"
    env['PYINST_DISTTGT']       = "${TARGET.abspath}"
    env['PYINST_DISTDIR']       = "${TARGET.dir.abspath}"

    if env['PLATFORM'] == 'win32':
        env["PYINST_COM_SEP" ]  = " && "
        env["PYINST_COM_00PSH"] = 'pushd ${SOURCE.dir}'
        env["PYINST_COM_01SET"] = 'set PYINST_LIBPATH="${PYINST_LIBFLAGS}"'
        env["PYINST_COM_02CMD"] = '${PYINST} ${PYINST_FLAGS} --distpath ${PYINST_DISTDIR} ${SOURCE.file}'
        env["PYINST_COM_03TCH"] = 'echo . > ${PYINST_DISTTGT}'
        env["PYINST_COM_09POP"] = 'popd'
    elif env['PLATFORM'] == 'posix':    # We are in a shell script (not bash), so no pushd
        env["PYINST_COM_SEP" ]  = " && "
        env["PYINST_COM_00PSH"] = 'cd ${SOURCE.dir}'
        env["PYINST_COM_01SET"] = 'export PYINST_LIBPATH="${PYINST_LIBFLAGS}"'
        env["PYINST_COM_02CMD"] = '${PYINST} ${PYINST_FLAGS} --distpath ${PYINST_DISTDIR} ${SOURCE.file}'
        env["PYINST_COM_03TCH"] = 'touch ${PYINST_DISTTGT}'
        env["PYINST_COM_09POP"] = 'pwd'
    else:
        raise SCons.Errors.UserError('pyinstaller: Invalid platform "%s"' % env['PLATFORM'])
    env["PYINST_COM_BITS"]      = ['${PYINST_COM_00PSH}', '${PYINST_COM_01SET}', '${PYINST_COM_02CMD}', '${PYINST_COM_03TCH}'] #, '${PYINST_COM_09POP}']
    env["PYINST_COM"]           = '(%s)' % '${PYINST_COM_SEP} '.join( env["PYINST_COM_BITS"] )

    env["PYINST_SRCSUFFIX"]     = ".spec"
    env["PYINST_LIBPATH"]       = [ ]

    create_builder(env)

def exists(env):
    try:
#        import pyinstaller
        return True
    except ImportError:
        return False


