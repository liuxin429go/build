# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import MUC_platform
import SCons
import os.path

# https://stackoverflow.com/questions/43865291/import-function-from-a-file-in-the-same-folder?rq=1
from ExternalLib import intel_path

class mkl(ExternalLib.ExternalLib):
    # https://software.intel.com/en-us/articles/intel-mkl-link-line-advisor

    #---------------
    #---    Linux 32
    #---------------
    def __intel_unx32(self, ctx):
        self.__itlX86_unx32(ctx)

    def __itlX86_unx32(self, ctx):
        __lpt = [ intel_path.get_intel_path('unx32', self.vers) ]
        __lib = [ 'mkl_intel', 'mkl_intel_thread', 'mkl_core', 'iomp5', 'pthread' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    #---------------
    #---    Linux 64
    #---------------
    def __intel_unx64(self, ctx):
        self.__itlX64_unx64(ctx)

    def __itlX64_unx64(self, ctx):
        __lpt = [ intel_path.get_intel_path('unx64', self.vers) ]
        __lib = [ 'mkl_intel_lp64', 'mkl_intel_thread', 'mkl_core', 'iomp5', 'pthread' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    #---------------
    #---    Linux 64i8
    #---------------
    def __intel_unx64i8(self, ctx):
        self.__itlX64_unx64i8(ctx)

    def __itlX64_unx64i8(self, ctx):
        __lpt = [ intel_path.get_intel_path('unx64', self.vers) ]
        __lib = [ 'mkl_intel_ilp64', 'mkl_intel_thread', 'mkl_core', 'iomp5', 'pthread' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    #---------------
    #---    CMC
    #---------------
    def __cmcitl(self, ctx):
        __lpt = []  # Path are set by s.f90
        __lib = [ 'mkl_intel_lp64', 'mkl_intel_thread', 'mkl_core', 'iomp5' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __cmc_unx64(self, ctx):
        self.__cmcitl(ctx)

    def __cmcitl_unx64(self, ctx):
        self.__cmcitl(ctx)

    #-----------------
    #---    Windows 32
    #-----------------
    def __msvc_win32(self, ctx):
        __lpt = [ intel_path.get_intel_path('win32', self.vers) ]
        __lib = ['mkl_intel_c', 'mkl_intel_thread', 'mkl_core']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __intel_win32(self, ctx):
        self.__itlX86_win64(ctx)

    def __itlX86_win32(self, ctx):
        __lpt = [ intel_path.get_intel_path('win32', self.vers) ]
        __lib = ['mkl_intel_c_dll', 'mkl_intel_thread_dll', 'libiomp5md', 'mkl_core_dll']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    #-----------------
    #---    Windows 64
    #-----------------
    def __intel_win64(self, ctx):
        self.__itlX64_win64(ctx)

    def __itlX64_win64(self, ctx):
        __lpt = [ intel_path.get_intel_path('win64', self.vers) ]
        __lib = ['mkl_intel_lp64_dll', 'mkl_intel_thread_dll', 'libiomp5md', 'mkl_core_dll']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    #-----------------
    #---    Windows 64 i8
    #-----------------
    def __intel_win64i8(self, ctx):
        self.__itlX64_win64i8(ctx)

    def __itlX64_win64i8(self, ctx):
        __lpt = [ intel_path.get_intel_path('win64', self.vers) ]
        __lib = ['mkl_intel_ilp64_dll', 'mkl_intel_thread_dll', 'libiomp5md', 'mkl_core_dll']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_mkl__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class sun(ExternalLib.ExternalLib):
    def __get_sun_path_unx32(self):
        vmaj = '.'.join( self.vers.split('.')[0:2])
        try:
            base = {
                   '12.1' : 'sunstudio',
                   '12.2' : 'solstudio',
                   '12.3' : 'solarisstudio',
                   '12.4' : 'solarisstudio',
                   '12.5' : 'developerstudio',
                   '12.6' : 'developerstudio',
                   }
            name = base[vmaj] + vmaj
        except:
            raise SCons.Errors.UserError('blas: Invalid sun version: %s' % self.vers)
        p = os.path.join(r'/opt/sun', name, 'lib')
        return os.path.normpath(p)

    def __get_sun_path_unx64(self):
        p = os.path.join(self.__get_sun_path_unx32(), 'amd64')
        return os.path.normpath(p)

    #---------------
    #---    Linux 64
    #---------------
    def __sun_unx64(self, ctx):
         blas_path = self.__get_sun_path_unx64()
         h2bl_path = os.path.join(os.environ['INRS_LXT'], 'H2D2-BLAS-LAPACK/_run', Util.addBuildPath('External'))
         __lpt = [ blas_path, h2bl_path ]
         __lib = [ 'sunperf', 'h2d2-blas-lapack' ]
         self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    #---------------
    #---    Linux 64i8
    #---------------
    def __sun_unx64i8(self, ctx):
        self.__sun_unx64(ctx)

    #---------------
    #---    Linux 32
    #---------------
    def __sun_unx32(self, ctx):
         blas_path = self.__get_sun_path_unx32()
         h2bl_path = os.path.join(os.environ['INRS_LXT'], 'H2D2-BLAS-LAPACK/_run', Util.addBuildPath('External'))
         __lpt = [ blas_path, h2bl_path ]
         __lib = [ 'sunperf', 'h2d2-blas-lapack' ]
         self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_sun__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class atlas(ExternalLib.ExternalLib):
    def __get_atlas_path(self, ctx):
        try:
            p = Util.getPathFromVersion( os.environ['INRS_LXT'], 'ATLAS' )
            p = os.path.join(p, '_' + ctx.lnk)
        except:
            try:
                p = Util.getPathOfFile('/usr/lib/atlas', 'libblas.a')
                p = os.path.dirname(p)
            except:
                p = Util.getPathOfFile('/usr/lib', 'libblas.a')
                p = os.path.dirname(p)
        return os.path.normpath(p)

    def __atlas(self, ctx):
        path = self.__get_atlas_path(ctx)
        blas_path = os.path.join(path, 'lib')
        h2bl_path = os.path.join(os.environ['INRS_LXT'], 'H2D2-BLAS-LAPACK/_run', Util.addBuildPath('External'))
        __lpt = [ blas_path, h2bl_path ]
        __lib = [ 'f77blas', 'lapack', 'cblas', 'atlas', 'h2d2-blas-lapack' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def configure(self, ctx, tool, version, plateform):
        fnc = self.__atlas
        fnc(ctx)

class acml(ExternalLib.ExternalLib):
    def __acml(self, ctx, subdir):
        blas_path = Util.getPathFromVersion('/opt', 'acml', search_join=None)
        blas_path = os.path.join(blas_path, subdir, 'lib')
        blas_path = os.path.normpath(blas_path)
        if not os.path.isdir(blas_path):
            raise SCons.Errors.UserError("Path does not exist: %s" % blas_path)
        h2bl_path = os.path.join(os.environ['INRS_LXT'], 'H2D2-BLAS-LAPACK/_run', Util.addBuildPath('External'))
        __lpt = [ blas_path, h2bl_path ]
        __lib = [ 'acml_mp', 'h2d2-blas-lapack' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    #---------------
    #---    Linux 64
    #---------------
    def __gcc_unx64(self, ctx):
        self.__acml(ctx, 'gfortran64_mp')

    def __gcc_unx64i8(self, ctx):
        self.__acml(ctx, 'gfortran64_mp_int64')

    def __intel_unx64(self, ctx):
        self.__acml(ctx, 'ifort64_mp')

    def __intel_unx64i8(self, ctx):
        self.__acml(ctx, 'ifort64_mp_int64')

    def __sun_unx64(self, ctx):
        self.__acml(ctx, 'gfortran64_mp')

    def __sun_unx64i8(self, ctx):
        self.__acml(ctx, 'gfortran64_mp_int64')

    def __open64_unx64(self, ctx):
        self.__acml(ctx, 'open64_64_mp')

    def __open64_unx64i8(self, ctx):
        self.__acml(ctx, 'open64_64_mp_int64')

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_acml__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class blas(ExternalLib.ExternalLib):
    #---------------
    #---    last chance
    #---------------
    def __h2d2_blas_lapack(self, ctx):
        h2bl_path = os.path.join(os.environ['INRS_LXT'], 'H2D2-BLAS-LAPACK/_run', Util.addBuildPath('External'))
        __lpt = [ h2bl_path ]
        __lib = [ 'h2d2-blas-lapack' ]
        return __lpt, __lib

    #---------------
    #---    CMC
    #---------------
    def __cmc_unx32(self, ctx):
        raise SCons.Errors.UserError("Invalide platform combination: %s" % "cmc_unx32")

    def __cmcitl_unx32(self, ctx):
        raise SCons.Errors.UserError("Invalide platform combination: %s" % "cmc_unx32")

    def __cmcpgi_unx32(self, ctx):
        raise SCons.Errors.UserError("Invalide platform combination: %s" % "cmc_unx32")

    def __cmc_unx64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __cmcitl_unx64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __cmcpgi_unx64(self, ctx):
        raise SCons.Errors.UserError("Invalide platform combination: %s" % "cmc_unx32")

    #---------------
    #---    Linux 64
    #---------------
    def __gcc_unx64(self, ctx):
        try:
            acml().configure(ctx, self.tool, self.vers, self.pltf)
        except:
            __lpt, __lib = self.__h2d2_blas_lapack(ctx)
            self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __gcc_unx64i8(self, ctx):
        try:
            acml().configure(ctx, self.tool, self.vers, self.pltf)
        except:
            __lpt, __lib = self.__h2d2_blas_lapack(ctx)
            self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __intel_unx64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX64_unx64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX64_unx64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __open64_unx64(self, ctx):
        acml().configure(ctx, self.tool, self.vers, self.pltf)

    def __open64_unx64i8(self, ctx):
        acml().configure(ctx, self.tool, self.vers, self.pltf)

    def __sun_unx64(self, ctx):
        sun().configure(ctx, self.tool, self.vers, self.pltf)

    def __sun_unx64i8(self, ctx):
        sun().configure(ctx, self.tool, self.vers, self.pltf)

    #---------------
    #---    Linux 32
    #---------------
    def __gcc_unx32(self, ctx):
        try:
            atlas().configure(ctx, self.tool, self.vers, self.pltf)
        except SCons.Errors.UserError:
            __lpt, __lib = self.__h2d2_blas_lapack(ctx)
            self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __intel_unx32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX86_unx32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __sun_unx32(self, ctx):
        sun().configure(ctx, self.tool, self.vers, self.pltf)

    #-----------------
    #---    Windows 32
    #-----------------
    def __gcc_win32(self, ctx):
        try:
            atlas().configure(ctx, self.tool, self.vers, self.pltf)
        except SCons.Errors.UserError:
            __lpt, __lib = self.__h2d2_blas_lapack(ctx)
            self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __gccX86_win32(self, ctx):
        try:
            atlas().configure(ctx, self.tool, self.vers, self.pltf)
        except SCons.Errors.UserError:
            __lpt, __lib = self.__h2d2_blas_lapack(ctx)
            self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __msvc_win32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __intel_win32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX86_win32(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    #-----------------
    #---    Windows 64
    #-----------------
    def __gcc_win64(self, ctx):
        try:
            atlas().configure(ctx, self.tool, self.vers, self.pltf)
        except SCons.Errors.UserError:
            __lpt, __lib = self.__h2d2_blas_lapack(ctx)
            self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __gccX64_win64(self, ctx):
        try:
            atlas().configure(ctx, self.tool, self.vers, self.pltf)
        except SCons.Errors.UserError:
            __lpt, __lib = self.__h2d2_blas_lapack(ctx)
            self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __intel_win64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __intel_win64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX64_win64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlX64_win64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlI64_win64(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

    def __itlI64_win64i8(self, ctx):
        mkl().configure(ctx, self.tool, self.vers, self.pltf)

        
    def __ftnchek_win32(self, ctx):
        pass

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_blas__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)


