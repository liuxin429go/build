#!/usr/bin/env python

import os
import sys
import optparse
import subprocess
import logging

#*******************************************************************************
# Summary: Calls cvs to execute an update or commit.
#
# Description:
#
# Input:
#
# Output:
#
# Note: This script is meant to replace Hudson cvs plugin until bug #7833 is
# resolved : http://issues.hudson-ci.org/browse/HUDSON-7833
#*******************************************************************************
def main(args=None):

    ret = 0
    logging.basicConfig(format='%(message)s', level=logging.INFO)

    if not args:
        args = sys.argv[1:]
    parser = optparse.OptionParser('%prog <commit|update> [modules]')
    opts, args = parser.parse_args(args)

    if args and args[0] == 'commit':
        cmd_line = 'cvs -Q -d :ext:compil@white.ete.inrs.ca:/cvs/lib_internes commit -m "Hudson CI"'
        for m in args[1:]:
            cmd_line = ' '.join([cmd_line, m])
        logging.info(cmd_line)
        if subprocess.call(cmd_line, shell=True): ret = 1

    elif args and args[0] == 'update':
        topdir = os.getcwd()
        for m in args[1:]:
            os.chdir(topdir)
            os.chdir(m)
            cmd_line = 'cvs -Q update -PdC'
            logging.info('[%s] %s' % (m, cmd_line))
            if subprocess.call(cmd_line, shell=True): ret = 1

    else:
        parser.error('specify commit or update')

    return ret


if __name__ == '__main__':
    sys.exit(main())

