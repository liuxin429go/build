#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import datetime
import enum
import hashlib
import logging
import os

from config import Config
from writer import Writer
from util   import path2unix, xtrNomModule, xtrPathProjet
from vcproj import MSVCProject

LOGGER = logging.getLogger("vc2scons.writer.scons")

ETAPES = enum.Enum('ETAPES', [
    'Indefinie',
    'ExeBld',
    'ExeSrc',
    'ExePkg',
    'ExeInc',
    'ExeDfn',
    'ExeLpt',
    'ExeLib',
    'ExeLxt',
    'ExeDps',
    'StgBin',
    'StgDoc',
    ])

class Scons(Writer):
    """
    Specialize Writer for SCons
    """

    INDENT = "        "

    def __init__(self, prms):
        super(Scons, self).__init__(prms)

        self.classeCourante = ""
        self.libExternesCourantes = ""
        self.etape  = ETAPES.Indefinie
        self.fname  = ''        # File name
        self.ofs    = None      # Output file stream
        self.has    = {}

    def writeln(self, l):
        self.ofs.write("%s\n" % l)

    def traiteSol(self, solution, *args):
        """
        Génère une solution
        """
        # ---  Récupère les projets à écrire
        prjs = [ prj for prj in solution.getProjects() if self.doisEcrire(solution, prj) ]

        # ---  Ouvre le fichier
        self.ouvreFichier(*args, prj=prjs[0] if len(prjs) == 1 else None)

        # ---  Boucle sur les projets à écrire
        for prj in prjs:

            # ---  Écris entête de classe
            if not self.prms.noExeTarget or not self.prms.noRunTarget or not self.prms.noStageTarget:
                self.ecrisClasseDebut(solution, prj)

            # ---  Écris le target
            if not self.prms.noExeTarget:
                self.ecrisExeTarget(solution, prj)

            # ---  Écris le stage
            if not self.prms.noRunTarget or not self.prms.noStageTarget:
                self.ecrisStageTarget(solution, prj)

            # ---  Écris la fin de la classe
            if not self.prms.noExeTarget or not self.prms.noRunTarget or not self.prms.noStageTarget:
                self.ecrisClasseFin(solution, prj)

        # ---  Ferme le fichier
        self.fermeFichier()

    def traitePrj(self, projet, *args):
        """
        Génère un projet
        """
        # ---  Ouvre le fichier
        self.ouvreFichier(*args, prj=projet)

        # ---  Écris entête de classe
        if not self.prms.noExeTarget or not self.prms.noStageTarget:
            self.ecrisClasseDebut(None, projet)

        # ---  Écris le target
        if not self.prms.noExeTarget:
            self.ecrisExeTarget(None, projet)

        # ---  Écris le stage
        if not self.prms.noStageTarget:
            self.ecrisStageTarget(None, projet)

        # ---  Écris la fin de la classe
        if not self.prms.noExeTarget or not self.prms.noStageTarget:
            self.ecrisClasseFin(None, projet)

        # ---  Ferme le fichier
        self.fermeFichier()

    def ouvreFichier(self, *args, prj=None):
        """
        Ouvre le fichier de sortie et prend soin de l'entête
        """
        if prj:
            nomPth = xtrNomModule (prj.getDirectory(), self.prms.projectRoot)
            nomMdl = nomPth.split('/', 1)[-1]
            nomTgt = prj.getName()
        else:
            nomMdl = '___module_name_not_known___'
            nomTgt = '___target_name_not_known___'

        nameArgs = {'sys_base' : 'SConscript', 'sys_ext' : '', 'module' : nomMdl, 'target' : nomTgt}
        outputName = self.prms.outputFileFormat.format(**nameArgs)
        outputFile = os.path.join(self.prms.outputDir, outputName)
        self.fname = outputFile

        # ---  Ouvre le fichier
        self.ofs = None
        if self.prms.append_to_existing_outfile:
            fileExist = os.path.isfile(outputFile)
            if fileExist:
                self.ofs = open(outputFile, 'at', encoding='utf-8')
            else:
                self.ofs = open(outputFile, 'wt', encoding='utf-8')
        else:
            outputFile += ".new"
            self.ofs = open(outputFile, 'wt', encoding='utf-8')
            self.ecrisEntete(*args)

    def ecrisEntete(self, *args):
        """
        Écris l'entête du fichier
        """
        self.writeln("# -*- coding: UTF-8 -*-")
        self.writeln("#-----------------------------------------------------------")
        self.writeln("# Generated: %s" % datetime.datetime.now())
        self.writeln("# Program:   %s" % Config.nomLogiciel)
        self.writeln("# Version:   %s" % Config.versionLogiciel)
        self.writeln("#")
        self.writeln("# File generated automatically. All changes will be lost.")
        self.writeln("#")
        self.writeln("# Build System: %s" % self.prms.bldSystem)
        self.writeln("# Command line:")
        for arg in args:
            if arg:
                self.writeln("#         %s" % arg)
        self.writeln("#-----------------------------------------------------------")
        self.writeln("import MUC_SCons")
        self.writeln("Import('env')")
        self.writeln("Import('bld')")
        self.writeln("Import('mpi')")
        self.writeln("")

    def fermeFichier(self):
        """
        Ferme le fichier et prend soin du pied de page
        """
        self.ecrisPiedDePage()
        self.ofs.close()
        self.ofs = None

        # ---  Si on édite directement le fichier, quitte
        if self.prms.append_to_existing_outfile: return

        fOld = self.fname
        fNew = fOld + ".new"
        fBck = fOld + ".bak"

        if os.path.isfile(fNew):
            if os.path.isfile(fOld):
                tmp_fic = open(fNew, 'rb')
                out_fic = open(fOld, 'rb')
                tmp_md5 = hashlib.md5( tmp_fic.read()[250:] )
                out_md5 = hashlib.md5( out_fic.read()[250:] )
                tmp_fic.close()
                out_fic.close()
                if (tmp_md5.digest() != out_md5.digest()):
                    if (os.path.isfile(fBck)): os.remove(fBck)
                    os.renames(fOld, fBck)
                    os.renames(fNew, fOld)
                    LOGGER.info(' --> Updating %s' % fOld)
                else:
                    os.remove(fNew)
            else:
                os.renames(fNew, fOld)
                LOGGER.info(' --> Creating %s' % fOld)

    def ecrisPiedDePage(self):
        """
        Écris le pied de page du fichier
        """
        pass

    def ecrisClasseDebut(self, sol, prj):
        """
        Écris l'entête d'une classe qui correspond à un target
        """
        nomTgt = prj.getName()

        self.classeCourante = nomTgt
        self.writeln("class %s:" % nomTgt)
        self.writeln("    def __init__(self):")

    def ecrisClasseFin(self, sol, prj):
        """
        Écris la fin d'une classe
        """
        args = ["bld", "mpi", "pth", "prj", "grp" ]
        for item in ["src", "pkg", "inc", "dfn", "lpt", "lib", "lxt", "dps"]:
            if self.has.get(item, False): args.append("{item:s}={item:s}".format(item=item))
        self.writeln(Scons.INDENT + "ctx = MUC_SCons.Context(%s)" % ", ".join(args))
        if not self.prms.noExeTarget:
            if prj.isLib():
                self.writeln(Scons.INDENT + "tgt = env.MUC_StaticLibrary(ctx)")
            elif prj.isDLL():
                self.writeln(Scons.INDENT + "tgt = env.MUC_SharedLibrary(ctx)")
            elif prj.isExe():
                self.writeln(Scons.INDENT + "tgt = env.MUC_Program(ctx)")
            elif prj.isPython():
                self.writeln(Scons.INDENT + "tgt = []")
                if self.has.get('spt', False): self.writeln(Scons.INDENT + "tgt += env.MUC_Python(ctx, spt)")
            elif prj.isCython():
                self.writeln(Scons.INDENT + "tgt = []")
                if self.has.get('spt', False): self.writeln(Scons.INDENT + "tgt += env.MUC_Python(ctx, spt)")
                if self.has.get('src', False): self.writeln(Scons.INDENT + "tgt += env.MUC_Cython(ctx)")
            elif prj.isPyInstaller():
                self.writeln(Scons.INDENT + "tgt = []")
                if self.has.get('spt', False): self.writeln(Scons.INDENT + "tgt += env.MUC_Python(ctx, spt)")
                if self.has.get('src', False): self.writeln(Scons.INDENT + "tgt += env.MUC_Cython(ctx)")
                if self.has.get('pkg', False): self.writeln(Scons.INDENT + "tgt += env.MUC_PyInstaller(ctx)")
            else:
                self.writeln(Scons.INDENT + "tgt = []")
        else:
            self.writeln(Scons.INDENT + "tgt = []")

        if not self.prms.noRunTarget:
            run = ""
            if prj.isPython():
                run += "env.MUC_Run(ctx, tgt)"
            elif prj.isCython():
                run += "env.MUC_Run(ctx, tgt)"
            elif prj.isPyInstaller():
                run += "env.MUC_Run(ctx, tgt, 'script')"
            else:
                run += "env.MUC_Run(ctx, tgt, 'bin')"
            if self.has.get('bin', False): run += " + env.MUC_Run(ctx, bin, 'bin')"
            if self.has.get('etc', False): run += " + env.MUC_Run(ctx, etc)"
            if self.has.get('doc', False): run += " + env.MUC_Run(ctx, doc)"
            if self.has.get('lcl', False): run += " + env.MUC_Run(ctx, lcl)"
            if self.has.get('spt', False): run += " + env.MUC_Run(ctx, spt)"
            self.writeln(Scons.INDENT + "run = " + run)
        if not self.prms.noStageTarget:
            stg = ""
            stg += "env.MUC_Stage(ctx, tgt, 'bin')"
            if self.has.get('bin', False): stg += " + env.MUC_Stage(ctx, bin, 'bin')"
            if self.has.get('etc', False): stg += " + env.MUC_Stage(ctx, etc)"
            if self.has.get('doc', False): stg += " + env.MUC_Stage(ctx, doc)"
            if self.has.get('lcl', False): stg += " + env.MUC_Stage(ctx, lcl)"
            if self.has.get('spt', False): stg += " + env.MUC_Stage(ctx, spt)"
            self.writeln(Scons.INDENT + "stg = " + stg)
            self.writeln(Scons.INDENT + "bz2 = env.MUC_TarBz2(ctx, stg)")
        self.writeln("")
        self.writeln("%s_ = %s()" % (self.classeCourante, self.classeCourante))
        self.writeln("")
        self.writeln("")

        self.classeCourante = ""

    def doisEcrire(self, sol, prj):
        """
        Retourne True si il y a quelque chose à écrire
        """
        r = False
        if self.prms.genProjectsAll: r = True
        if (os.path.splitext(os.path.basename(sol.getFile()))[0] ==
            os.path.splitext(os.path.basename(prj.getFile()))[0]): r = True
        return r

    def ecrisExeTarget(self, sol, prj):
        """
        Écris le target de type exe-dll-lib d'une solution
        """
        if sol and not self.prms.genProjectsAll:
            if (os.path.dirname(sol.getFile()) != \
                os.path.dirname(prj.getFile())): return

        self.ecrisExeTargetEx(sol, prj)

    def ecrisExeTargetEx(self, sol, prj):
        """
        Écris le target de type exe-dll-lib
        """
        nomDir = xtrPathProjet(prj.getDirectory())
        nomPth = xtrNomModule (prj.getDirectory(), self.prms.projectRoot)
        nomMdl = nomPth.split('/', 1)[-1]
        nomTgt = prj.getName()

        self.writeln(Scons.INDENT + 'pth = "' + nomMdl + '"')
        self.writeln(Scons.INDENT + 'prj = "' + nomTgt + '"')
        self.writeln(Scons.INDENT + 'grp = "' + self.prms.outputGroupName + '"')
        self.writeln("")

        doitEcrire = prj.isExe() or prj.isDLL() or prj.isLib() or prj.isPython() or prj.isCython() or prj.isPyInstaller()

        self.etape = ETAPES.ExeSrc
        self.has["src"] = False
        if doitEcrire:
            items = self.reqFichiers(prj, nomDir, Config.ExtensionsSrc)
            if items:
                self.writeln(Scons.INDENT + "src = [")
                for item in items:
                    self.ecrisUnFichier(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["src"] = True

        self.etape = ETAPES.ExePkg
        self.has["pkg"] = False
        if doitEcrire:
            items = self.reqFichiers(prj, nomDir, Config.ExtensionsPkg)
            if items:
                self.writeln(Scons.INDENT + "pkg = [")
                for item in items:
                    self.ecrisUnFichier(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["pkg"] = True

        self.etape = ETAPES.ExeInc
        self.has["inc"] = False
        if doitEcrire:
            items = self.reqRepertoiresInclude(prj)
            if items:
                self.writeln(Scons.INDENT + "inc = [")
                for item in items:
                    self.ecrisUnInclude(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["inc"] = True

        self.etape = ETAPES.ExeDfn
        self.has["dfn"] = False
        if doitEcrire:
            items = self.reqDefine(prj)
            if items:
                self.writeln(Scons.INDENT + "dfn = [")
                for item in items:
                    self.ecrisUnDefine(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["dfn"] = True

        self.etape = ETAPES.ExeLpt
        self.has["lpt"] = False
        if doitEcrire:
            libExcl = [ MSVCProject.isPython, MSVCProject.isCython ]
            deps = self.reqDependances(sol, prj, exclude=libExcl) if sol else []
            libs = self.reqLibPath(prj)
            if True: # deps or libs:
                self.writeln(Scons.INDENT + "lpt = [")
                for dep in deps:
                    self.ecrisUneDependance(*dep)
                self.ecrisUneDependance(nomPth, nomTgt)      # Ajoute le chemin à self
                for lib in libs:
                    self.ecrisUnLibPath(lib)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["lpt"] = True

        self.etape = ETAPES.ExeLib
        self.has["lib"] = False
        if doitEcrire:
            libExcl = [ MSVCProject.isPython, MSVCProject.isCython ]
            deps = self.reqDependances(sol, prj, exclude=libExcl) if sol else []
            libs = self.reqLibrairies(prj)
            fics = self.reqFichiers(prj, nomDir, Config.ExtensionsObj)
            if deps or libs or fics:
                self.writeln(Scons.INDENT + "lib = [")
                for fic in fics:
                    self.ecrisUnFichier(fic)
                for dep in deps:
                    self.ecrisUneDependance(*dep)
                for lib in libs:
                    self.ecrisUneLib(lib)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["lib"] = True

        self.etape = ETAPES.ExeLxt
        self.has["lxt"] = False
        libExternesCourantes = ""
        if doitEcrire:
            items = self.reqLibXtrn(prj)
            items = [ item for item in items if item[Config.LXT_IDX_SCONS]]
            if items:
                self.writeln(Scons.INDENT + "lxt = [")
                for item in items:
                    self.ecrisUneLibXtrn(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["lxt"] = True

        self.etape = ETAPES.ExeDps
        self.has["dps"] = False
        if doitEcrire:
            libIncl = [ MSVCProject.isPython, MSVCProject.isCython ]
            deps = self.reqDependances(sol, prj, include=libIncl) if sol else []
            if deps:
                self.writeln(Scons.INDENT + "dps = [")
                for dep in deps:
                    self.ecrisUneDependance(*dep)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["dps"] = True

        # ecrisProperties(prj)
        self.etape = ETAPES.Indefinie

    def ecrisStageTarget(self, sol, prj):
        """
        Écris le target de type stage d'un projet
        """
        if sol and not self.prms.genProjectsAll:
            if (os.path.dirname(sol.getFile()) != \
                os.path.dirname(prj.getFile())): return

        self.ecrisStageTargetScr(prj)
        self.ecrisStageTargetBin(prj)

    def ecrisStageTargetScr(self, prj):
        """
        Écris le target de type stage d'un projet
        """

        nomDir = xtrPathProjet(prj.getDirectory())
        doitEcrire = True

        self.has["etc"] = False
        if doitEcrire:
            items = self.reqFichiers(prj, nomDir, Config.ExtensionsStageEtc)
            if items:
                self.writeln(Scons.INDENT + "etc = [")
                for item in items:
                    self.ecrisUnFichier(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["etc"] = True

        self.has["doc"] = False
        if doitEcrire:
            items = self.reqFichiers(prj, nomDir, Config.ExtensionsStageDoc)
            if items:
                self.writeln(Scons.INDENT + "doc = [")
                for item in items:
                    self.ecrisUnFichier(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["doc"] = True

        self.has["lcl"] = False
        if doitEcrire:
            items = self.reqFichiers(prj, nomDir, Config.ExtensionsStageLcl)
            if items:
                self.writeln(Scons.INDENT + "lcl = [")
                for item in items:
                    self.ecrisUnFichier(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["lcl"] = True

        self.has["spt"] = False
        if doitEcrire:
            items = self.reqFichiers(prj, nomDir, Config.ExtensionsStageScr)
            if items:
                self.writeln(Scons.INDENT + "spt = [")
                for item in items:
                    self.ecrisUnFichier(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["spt"] = True

    def ecrisStageTargetBin(self, prj):
        """
        Écris le target de type stage d'un projet
        """

        nomDir = xtrPathProjet(prj.getDirectory())
        doitEcrire = True

        self.has["bin"] = False
        if doitEcrire:
            items = self.reqFichiers(prj, nomDir, Config.ExtensionsStageBin)
            if items:
                self.writeln(Scons.INDENT + "bin = [")
                for item in items:
                    self.ecrisUnFichier(item)
                self.writeln(Scons.INDENT + "      ]")
                self.writeln("")
                self.has["bin"] = True

    def ecrisUneDependance(self, nomMdl, nomTgt):
        """
        Écris une dépendance
        """
        if self.etape == ETAPES.ExeLpt:
            nom = path2unix(nomMdl)
            if '/' in nom:
                l, r = nom.split('/', 1)
                r = '${MUC_GRIST_BIN:%s}' % r if r else '${MUC_GRIST_BIN}'
                nom = '/'.join( ('#..', l, r) )
                # nom = '//'.join(nom.split('/', 1))
            self.writeln(Scons.INDENT + "      r'" + nom + "',")
        elif self.etape == ETAPES.ExeLib:
            self.writeln(Scons.INDENT + "      r'" + nomTgt + "',")
        elif self.etape == ETAPES.ExeDps:
            self.writeln(Scons.INDENT + "      *env.getAllTokenFiles(r'" + nomTgt + "'),")

    def ecrisUnFichier(self, nom):
        """
        Écris un fichier
        """
        if nom: self.writeln(Scons.INDENT + "      r'" + nom + "',")

    def ecrisUnInclude(self, nom):
        """
        Écris un include
        """
        if nom: self.writeln(Scons.INDENT + "      r'" + nom + "',")

    def ecrisUnDefine(self, nom):
        """
        Écris un define
        """
        if nom: self.writeln(Scons.INDENT + "      r'" + nom + "',")

    def ecrisUneLib(self, nom):
        """
        Écris une lib
        """
        if nom: self.writeln(Scons.INDENT + "      r'" + nom + "',")

    def ecrisUneLibXtrn(self, libs):
        """
        Écris une lib externe
        """
        libExternesCourantes = ""
        nom = libs[Config.LXT_IDX_SCONS]
        if nom:
            tag = nom + ";"
            if tag not in libExternesCourantes:
                self.writeln(Scons.INDENT + "      r'" + nom + "',")
                libExternesCourantes = libExternesCourantes + tag

    def ecrisUnLibPath(self, nom):
        """
        Écris un lib path
        """
        if nom: self.writeln(Scons.INDENT + "      r'" + nom + "',")


if __name__ == "__main__":
    s = Scons()
