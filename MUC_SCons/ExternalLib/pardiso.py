# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import os
import SCons

class pardiso(ExternalLib.ExternalLib):
    #--- Same code as in blas.py
    def __get_path_unx32(self):
        p = '/opt/intel/compilerpro-' + self.vers + '/mkl/lib/ia32'
        if (not os.path.isdir(p)):
            v, s = self.vers.rsplit('.', 1)
            p = os.path.join('/opt/intel/Compiler', v, s, 'mkl/lib/32')
        return p

    def __get_path_unx64(self):
        p = '/opt/intel/compilerpro-' + self.vers + '/mkl/lib/intel64'
        if (not os.path.isdir(p)):
            v, s = self.vers.rsplit('.', 1)
            p = os.path.join('/opt/intel/Compiler', v, s, 'mkl/lib/em64t')
        return p

    #---------------
    #---    CMC
    #---------------
    def __cmc_unx32(self, ctx):
        raise SCons.Errors.UserError('pardiso.__cmc_unx32 not defined')

    def __cmc_unx64(self, ctx):
        raise SCons.Errors.UserError('pardiso.__cmc_unx64 not defined')

    def __cmcitl_unx32(self, ctx):
        raise SCons.Errors.UserError('pardiso.__cmcitl_unx32 not defined')

    def __cmcitl_unx64(self, ctx):
        raise SCons.Errors.UserError('pardiso.__cmcitl_unx64 not defined')

    def __cmcpgi_unx32(self, ctx):
        raise SCons.Errors.UserError('pardiso.__cmcpgi_unx32 not defined')

    def __cmcpgi_unx64(self, ctx):
        raise SCons.Errors.UserError('pardiso.__cmcpgi_unx64 not defined')

    #---------------
    #---    Linux 64
    #---------------
    def __gcc_unx64(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'pardiso-3.3')
        __lpt = [ hdir ]
        __lib = ['pardiso_GNU42_INTEL32_P', 'gomp']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __gccX64_unx64(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'pardiso-3.3')
        __lpt = [ hdir ]
        __lib = ['pardiso_GNU42_INTEL32_P', 'gomp']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __intel_unx64(self, ctx):
        __lpt = [ self.__get_path_unx64() ]
        __lib = ['mkl_solver']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __itlX64_unx64(self, ctx):
        __lpt = [ self.__get_path_unx64() ]
        __lib = ['mkl_solver']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    #---------------
    #---    Linux 32
    #---------------
    def __gcc_unx32(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'pardiso-3.3')
        __lpt = [ hdir ]
        __lib = ['pardiso_GNU42_INTEL32_P', 'gomp']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __gccX86_unx32(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'pardiso-3.3')
        __lpt = [ hdir ]
        __lib = ['pardiso_GNU42_INTEL32_P', 'gomp']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __intel_unx32(self, ctx):
        __lpt = [ self.__get_path_unx32() ]
        __lib = ['mkl_solver']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __itlX86_unx32(self, ctx):
        __lpt = [ self.__get_path_unx32() ]
        __lib = ['mkl_solver']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __sun_unx32(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'pardiso-3.3')
        __lpt = [ hdir ]
        __lib = ['pardiso_GNU42_INTEL32_P']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    #-----------------
    #---    Windows 32
    #-----------------
    def __win32(self, ctx):
        __lpt = [ os.path.join(os.environ['ProgramFiles'], r'Intel\ComposerXE-2011\mkl\lib\ia32') ]
        __lib = ['libguide40']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __gcc_win32(self, ctx):
        self.__win32(ctx)

    def __gccX86_win32(self, ctx):
        self.__win32(ctx)

    def __msvc_win32(self, ctx):
        raise SCons.Errors.UserError('pardiso.__msvc_win32 not defined')

    def __intel_win32(self, ctx):
        self.__win32(ctx)

    def __itlX86_win32(self, ctx):
        self.__win32(ctx)

    #-----------------
    #---    Windows 64
    #-----------------
    def __win64(self, ctx):
        __lpt = [ os.path.join(os.environ['ProgramFiles'], r'Intel\ComposerXE-2011\mkl\lib\intel64') ]
        __lib = ['mkl_solver']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __gcc_win64(self, ctx):
        self.__win64(ctx)

    def __gccX64_win64(self, ctx):
        self.__win64(ctx)

    def __intel_win64(self, ctx):
        self.__win64(ctx)

    def __itlX64_win64(self, ctx):
        self.__win64(ctx)

    def __itlI64_win64(self, ctx):
        __lpt = [ os.path.join(os.environ['ProgramFiles'], r'Intel\ComposerXE-2011\mkl\lib\ia64') ]
        __lib = ['libguide40']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)


    def __ftnchek_win32(self, ctx):
        raise SCons.Errors.UserError('pardiso.__ftnchek_win32 not defined')

    def configure(self, ctx, tool, version, plateform):
        fnm = '_pardiso__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
