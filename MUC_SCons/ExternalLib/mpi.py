# -*- coding: UTF-8 -*-
import ExternalLib
import SCons

import Util
import MUC_platform
import os

# https://stackoverflow.com/questions/43865291/import-function-from-a-file-in-the-same-folder?rq=1
from ExternalLib import intel_path

libs = [
    'mpich2',    'mpich2-1.3.2',    'mpich2-1.3.2p1',    'mpich2-1.4.1p1',
    'mpich2x64', 'mpich2x64-1.3.2', 'mpich2x64-1.3.2p1', 'mpich2x64-1.4.1p1',
    'openmpi',
    'openmpi-1.4', 'openmpi-1.4.1', 'openmpi-1.4.3', 'openmpi-1.4.5',
    'openmpi-1.6', 'openmpi-1.6.1', 'openmpi-1.6.3', 'openmpi-1.6.5',
    'openmpi-1.8', 'openmpi-1.8.1', 'openmpi-1.8.2', 'openmpi-1.8.3',
    'openmpi-1.10','openmpi-1.10.6',
    'openmpi-2.1', 'openmpi-2.1.0',
    'openmpi-3.1', 'openmpi-3.1.0',
    'openmpi-4.0', 'openmpi-4.0.5',
    'intelmpi',
    'msmpi',
    'deinompi', 'deinompi-2.0.1',
    'mpi_dummy',
    '_none_'
    ]

ProgramFiles = ''
try:
    ProgramFiles = os.environ['ProgramFiles(x86)']
except:
    try:
        ProgramFiles = os.environ['ProgramFiles']
    except:
        pass

class mpich2(ExternalLib.ExternalLib):
    def __cmc(self, ctx):
        pass

    def __cmc_unx32(self, ctx):
        self.__cmc(ctx)

    def __cmc_unx64(self, ctx):
        self.__cmc(ctx)

    def __cmcitl_unx32(self, ctx):
        self.__cmc(ctx)

    def __cmcitl_unx64(self, ctx):
        self.__cmc(ctx)

    def __cmcpgi_unx32(self, ctx):
        self.__cmc(ctx)

    def __cmcpgi_unx64(self, ctx):
        self.__cmc(ctx)

    def __unx(self, ctx):
        mpi_path = os.environ['INRS_LXT']
        if (ctx.mpi == 'mpich2'):
            mpi_path = Util.getPathFromVersion(mpi_path, 'mpich2') #, last_chance='MPICH2')
        else:
            mpi_path = os.path.join(mpi_path, ctx.mpi)
        mpi_path = os.path.join(mpi_path, '_install')
        __inc = [ os.path.join(mpi_path, 'include') ]
        __lpt = [ os.path.join(mpi_path, 'lib') ]
        __lib = ['fmpich', 'mpich', 'mpichcxx', 'rt', 'pthread']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def __unx32(self, ctx):
        self.__unx(ctx)

    def __unx64(self, ctx):
        self.__unx(ctx)

    def __win32(self, ctx):
        mpi_path = os.environ['INRS_LXT']
        if (ctx.mpi == 'mpich2'):
            mpi_path = Util.getPathFromVersion(mpi_path, 'mpich2', last_chance='MPICH2')
        else:
            mpi_path = os.path.join(mpi_path, ctx.mpi)
        __inc = [ os.path.join(mpi_path, 'include') ]
        __lpt = [ os.path.join(mpi_path, 'lib') ]
        __lib = ['fmpich2', 'mpi']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def __win64(self, ctx):
        mpi_path = os.environ['INRS_LXT']
        if (ctx.mpi == 'mpich2'):
            mpi_path = Util.getPathFromVersion(mpi_path, 'mpich2x64', last_chance='MPICH2x64')
        else:
            mpi_path = os.path.join(mpi_path, ctx.mpi)
        __inc = [ os.path.join(mpi_path, 'include') ]
        __lpt = [ os.path.join(mpi_path, 'lib') ]
        __lib = ['fmpich2', 'mpi']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def __win64i8(self, ctx):
        self.__win64(ctx)

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        if (self.tool in ['cmc', 'cmcitl', 'cmcpgi']):
            fnm = '_mpich2__%s_%s' % (self.tool, self.pltf)
        else:
            fnm = '_mpich2__%s' % (self.pltf)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class openmpi(ExternalLib.ExternalLib):
    def __cmc(self, ctx):
        __inc = []
        __lpt = []
        __lib = []
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def __cmc_unx32(self, ctx):
        self.__cmc(ctx)

    def __cmc_unx64(self, ctx):
        self.__cmc(ctx)

    def __cmcitl_unx32(self, ctx):
        self.__cmc(ctx)

    def __cmcitl_unx64(self, ctx):
        self.__cmc(ctx)

    def __cmcpgi_unx32(self, ctx):
        self.__cmc(ctx)

    def __cmcpgi_unx64(self, ctx):
        self.__cmc(ctx)

    def __unx(self, ctx):
#            mmt_path = os.path.join(os.environ['INRS_LXT'], 'Marmot-2.3.1')
#            mpi_path = os.path.join(os.environ['INRS_LXT'], 'openmpi-1.3.3')
#            __inc = [ os.path.join(mmt_path, 'include'), os.path.join(mpi_path, 'include') ]
#            __lpt = [ os.path.join(mmt_path, 'lib'), os.path.join(mpi_path, 'lib') ]
#            __lib = ['marmot-fortran', 'marmot-core', 'marmot-profile', 'mpi_cxx', 'mpi_f77', 'mpi' ]

#            mpi_path = os.path.join(os.environ['INRS_LXT'], 'openmpi-1.3.3')
#            mpe_path = os.path.join(os.environ['HOME'], 'tools', 'mpe2-1.0.6p1')
#            __inc = [ os.path.join(mpi_path, 'include') ]
#            __lpt = [ os.path.join(mpe_path, 'lib'), os.path.join(mpi_path, 'lib') ]
#            __lib = [ 'mpe_collchk', 'mpe', 'mpi_cxx', 'mpi_f77', 'mpi' ]

#        mpi_path = '/opt/openmpi'
        mpi_root = os.environ['INRS_LXT']
        mpi_sufx = '-'+self.pltf
        if (ctx.mpi == 'openmpi'):
            try:
               mpi_path = Util.getPathFromVersion(mpi_root, 'openmpi', mpi_sufx, last_chance='openmpi'+mpi_sufx)
            except SCons.Errors.UserError:
               mpi_path = Util.getPathFromVersion(mpi_root, 'openmpi')
        else:
            mpi_path = os.path.join(mpi_root, ctx.mpi) + mpi_sufx
            if (not os.path.isdir(mpi_path)):
               mpi_path = os.path.join(mpi_root, ctx.mpi)
        lpt  = os.path.join(mpi_path, 'lib')
        libs = [ 'mpi_cxx', 'mpi_f77', 'mpi_usempi', 'mpi_mpifh', 'mpi', 'open-rte', 'open-pal']
        __inc = [ os.path.join(mpi_path, 'include'), os.path.join(mpi_path, 'lib') ]      # lib added for .mod files
        __lpt = [ lpt ]
        __lib = [ p for p in libs if os.path.isfile( os.path.join(lpt, 'lib%s.so' % p) ) ]
        #try:
        #    isVersionPre18 = Util.getPathOfFile(__lpt[0], '*mpi_f77*')
        #    __lib = [ 'mpi_cxx', 'mpi_f77', 'mpi', 'open-rte', 'open-pal']
        #except SCons.Errors.UserError:
        #    __lib = [ 'mpi_cxx', 'mpi_usempi', 'mpi_mpifh', 'mpi', 'open-rte', 'open-pal']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def __unx32(self, ctx):
        self.__unx(ctx)

    def __unx64(self, ctx):
        self.__unx(ctx)

    def __unx64i8(self, ctx):
        self.__unx(ctx)

    def __win(self, ctx):
        mpi_path = os.environ['INRS_LXT']
        if (ctx.mpi == 'openmpi'):
            mpi_path = Util.getPathFromVersion(mpi_path, 'openmpi')
        else:
            mpi_path = os.path.join(mpi_path, ctx.mpi)
        __inc = [ os.path.join(mpi_path, 'include') ]
        __lpt = [ os.path.join(mpi_path, 'lib') ]
        __lib = [ 'mpi_f77', 'mpi' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def __win32(self, ctx):
        self.__win(ctx)

    def __win64(self, ctx):
        self.__win(ctx)

    def __win64i8(self, ctx):
        self.__win(ctx)

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        if (self.tool in ['cmc', 'cmcitl', 'cmcpgi']):
            fnm = '_openmpi__%s_%s' % (self.tool, self.pltf)
        else:
            fnm = '_openmpi__%s' % (self.pltf)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class msmpi(ExternalLib.ExternalLib):
    def __win(self, ctx, ptf):
        inc_path = os.path.normpath( os.environ['MSMPI_INC'] )
        if   ptf == 'x86':
            lib_path = os.path.normpath( os.environ['MSMPI_LIB32'] )
        elif ptf == 'x64':
            lib_path = os.path.normpath( os.environ['MSMPI_LIB64'] )
        else:
            raise SCons.Errors.UserError('Invalid MS-MPI platform: %s not in [x86, x64]' % ptf)
        __inc = [ inc_path, os.path.join(inc_path, ptf) ]
        __lpt = [ lib_path ]
        __lib = ['msmpifec', 'msmpi']
        __dfn = [ ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc, __dfn, ctx.dfn)

    def __win32(self, ctx):
        self.__win(ctx, 'x86')

    def __win64(self, ctx):
        self.__win(ctx, 'x64')

    def __win64i8(self, ctx):
        raise SCons.Errors.UserError('Invalid MPI system: %s' % ctx.mpi)

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_msmpi__%s' % (self.pltf)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class intelmpi(ExternalLib.ExternalLib):
    def __win32(self, ctx):
        raise SCons.Errors.UserError('Intel-MPI platform x86 never tested')
        mpi_path = os.path.normpath( os.environ['I_MPI_ROOT'] )
        if os.path.isdir(os.path.join(mpi_path, 'ia32')):
            mpi_path = os.path.join(mpi_path, 'ia32')
        inc_path = os.path.join(mpi_path, 'include')
        lib_path = os.path.join(mpi_path, 'lib')
        __inc = [ inc_path ]
        __lpt = [ os.path.join(lib_path, 'release'), lib_path ]
        __lib = ['impi', 'impicxx']
        __dfn = [ ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc, __dfn, ctx.dfn)

    def __win64(self, ctx):
        mpi_path = os.path.normpath( os.environ['I_MPI_ROOT'] )
        if os.path.isdir(os.path.join(mpi_path, 'intel64')):
            mpi_path = os.path.join(mpi_path, 'intel64')
        inc_path = os.path.join(mpi_path, 'include')
        lib_path = os.path.join(mpi_path, 'lib')
        __inc = [ inc_path ]
        __lpt = [ os.path.join(lib_path, 'release'), lib_path ]
        __lib = ['impi', 'impicxx']
        __dfn = [ ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc, __dfn, ctx.dfn)

    def __win64i8(self, ctx):
        mpi_path = os.path.normpath( os.environ['I_MPI_ROOT'] )
        if os.path.isdir(os.path.join(mpi_path, 'intel64')):
            mpi_path = os.path.join(mpi_path, 'intel64')
        inc_path = os.path.join(mpi_path, 'include')
        lib_path = os.path.join(mpi_path, 'lib')
        __inc = [ os.path.join(inc_path, 'ilp64'),   inc_path ]
        __lpt = [ os.path.join(lib_path, 'release'), lib_path ]
        __lib = ['libmpi_ilp64', 'impi', 'impicxx']
        __dfn = [ ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc, __dfn, ctx.dfn)

    def __unx32(self, ctx):
        self.__unx(ctx, 'x86')

    def __unx64(self, ctx):
        self.__unx(ctx, 'x64')

    def __unx64i8(self, ctx):
        self.__unx(ctx, 'x64i8')

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_intelmpi__%s' % (self.pltf)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class deinompi(ExternalLib.ExternalLib):
    def __win(self, ctx):
        mpi_path = os.environ['INRS_LXT']
        if (ctx.mpi == 'deinompi'):
            mpi_path = Util.getPathFromVersion(mpi_path, 'DeinoMPI')
        else:
            mpi_path = os.path.join(mpi_path, 'DeinoMPI'+ctx.mpi[8:])
        __inc = [ os.path.join(mpi_path, 'include') ]
        __lpt = [ os.path.join(mpi_path, 'lib') ]
        __lib = ['DeinFMPI', 'mpi']
        __dfn = ['MPICH_SKIP_MPICXX']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc, __dfn, ctx.dfn)

    def __win32(self, ctx):
        self.__win(ctx)

    def __win64(self, ctx):
        self.__win(ctx)

    def __win64i8(self, ctx):
        self.__win64(ctx)

    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        fnm = '_deinompi__%s' % (self.pltf)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

class mpi_dummy(ExternalLib.ExternalLib):
    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform
        mpi_path = os.path.join(os.environ['INRS_LXT'], 'MPICH2-Dummy')
        __inc = [ os.path.join(mpi_path, 'include') ]
        __lpt = [ os.path.join(mpi_path, '_bin', Util.addBuildPath(r'fmpich2')) ]
        __lib = ['fmpich2']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

class mpi_none(ExternalLib.ExternalLib):
    def configure(self, ctx, tool, version, plateform):
        self.tool = tool
        self.vers = version
        self.pltf = plateform

class mpi(ExternalLib.ExternalLib):
    def configure(self, ctx, tool, version, plateform):
        if   (ctx.mpi[0:6] == 'mpich2'):
            mpich2().configure(ctx, tool, version, plateform)
        elif (ctx.mpi[0:7] == 'openmpi'):
            openmpi().configure(ctx, tool, version, plateform)
        elif (ctx.mpi[0:5] == 'msmpi'):
            msmpi().configure(ctx, tool, version, plateform)
        elif (ctx.mpi[0:8] == 'intelmpi'):
            intelmpi().configure(ctx, tool, version, plateform)
        elif (ctx.mpi[0:8] == 'deinompi'):
            deinompi().configure(ctx, tool, version, plateform)
        elif (ctx.mpi[0:9] == 'mpi_dummy'):
            mpi_dummy().configure(ctx, tool, version, plateform)
        elif (ctx.mpi[0:6] == '_none_'):
            mpi_none().configure(ctx, tool, version, plateform)
        else:
            raise SCons.Errors.UserError('Invalid MPI system: %s' % ctx.mpi)
