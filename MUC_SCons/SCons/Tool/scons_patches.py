# -*- coding: UTF-8 -*-

# Copie de SCons.Tool.FortranCommon (2.5.1)
# La regex d'origine ne prend pas en compte 
#   MODULE FUNCTION et 
#   MODULE SUBROUTINE
import SCons.Tool.FortranCommon
import SCons.Util
import re
def MUC_ifl___fortranEmitter(target, source, env):
    node = source[0].rfile()
    if not node.exists() and not node.is_derived():
       print(("Could not locate %s" % str(node.name)))
       return ([], [])
    ### mod_regex = """(?i)^\s*MODULE\s+(?!PROCEDURE)(\w+)"""
    mod_regex = """(?i)^\s*MODULE\s+(?!(?:PROCEDURE|FUNCTION|SUBROUTINE))(\w+)"""
    cre = re.compile(mod_regex,re.M)
    # Retrieve all USE'd module names
    modules = cre.findall(node.get_text_contents())
    # Remove unique items from the list
    modules = SCons.Util.unique(modules)
    # Convert module name to a .mod filename
    suffix = env.subst('$FORTRANMODSUFFIX', target=target, source=source)
    moddir = env.subst('$FORTRANMODDIR', target=target, source=source)
    modules = [x.lower() + suffix for x in modules]
    for m in modules:
       target.append(env.fs.File(m, moddir))
    return (target, source)

SCons.Tool.FortranCommon._fortranEmitter = MUC_ifl___fortranEmitter

