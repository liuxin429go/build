# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#
#   Compilateur open64
#-----------------------------------------------------------------------------------------------------

import MUC_platform

import glob
import os
import shutil
import subprocess

"""
Open64 - Configuration
"""
class cfg_cpp:
    def getCfg_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_bse = [
                    '-c',                       #  produce a .o and stop
                    '-W',                       #  Enable extra warnings
                    '-Wall',                    #  Enable most warning messages
                    '-Wno-long-long',           #
                    '-align64',                 #  align data in common blocks to 64-bit boundaries
#?                    '-fcheck-new',              #  Check result of operator new for NULL
#                    '-fno-second-underscore',   #  Do not append a second underscore to symbols that already contain an underscore
                    '-ffor-scope',              #  Variables declared in a for-init loop are constrained to that scope
                    '-frtti',                   #  Generate runtime type information
#                    '-pedantic',                #  Issue warnings needed by strict compliance to ANSI C
#?                    '-fpack-struct',            #  Pack structure members together without holes
#?                    '-mieee-fp',                #  Use IEEE floating point comparisons
                    '-pthread',                 #  Compile with pthreads support
                    ]
        flags_cfg = {
                    'debug'     :
                    [
                    '-O0',                      #  no optimization
                    '-g',                       #  full debug info
                    '-fno-fast-math',           #  Conform to ANSI & IEEE math rules at the expense of speed
                    '-fno-unsafe-math-optimizations', #  Conform to ANSI & IEEE math rules at the expense of speed
                    '-noinline',                #  suppress inline processing
                    '-trapuv',                  #  trap uninitialized variables
                    '-fno-fast-stdlib',         #  Do not use faster versions of standard library functions
                    '-fno-inline-functions',    #  Do not automatically integrate simple functions into their callers
                    ],
                    'profil'    :
                    [
                    '-O0', '-g3', '-fno-inline', '-pg'
                    ],
                    'release'   :
                    [
                    '-O3',                      #  full optimization
                    '-g',                       #  full debug info
#                    '-g0',                      #  no debug info
#                    '-ipa',                     #  Perform interprocedural analysis and optimization
                    '-inline',                  #  request inline processing
                    '-finline-functions',       #  Automatically integrate simple functions into their callers
                    '-fkeep-inline-functions',  #  Generate code for funcs even if they are fully inlined
                    '-fcxx-openmp',             #  Do OpenMP processing under C++
                    '-fstrength-reduce',        #  Perform strength reduction optimisations
                    '-fomit-frame-pointer',     #  When possible do not generate stack frames
                    '-mp',                      #  enable the multiprocessing directives (same as -openmp)
                    '-ffast-stdlib',            #  Use faster versions of some standard library functions, when available
                    '-ffast-math',              #  Improve FP speed by violating ANSI & IEEE rules
                    '-funsafe-math-optimizations',   #  Improve FP speed by violating ANSI & IEEE rules
                    '-funroll-all-loops',        #  Peform loop onrolling for all loops
                    '-funroll-loops',            #  unroll-loops
                    ]
                    }
        return flags_bse + flags_cfg[c]

    def getLnk_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        if (p == 'unx32' or p == 'unx64'):
            flags_lnk = {
                         'debug' :
                           {
                              'static' :  [ '-fno-pic' ],
                              'dynamic' : [ '-fPIC', '-shared' ],
                           },
                         'release' :
                           {
                              'static' :  [ '-fno-pic' ],
                              'dynamic' : [ '-fPIC', '-shared' ],
                           },
                        }
        else:
            raise 'Invalid Open64 platform: %s' % p
        return flags_lnk[c][l]

    def getPtf_c(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_ptf = {
                     'unx32' : ['-m32'],
                     'unx64' : ['-m64'],
                     'win32' : [],
                     'win64' : [],
                     }
        return flags_ptf[p]

    def c_cfg(self, cfg):
        return self.cpp_cfg(cfg)

    def cpp_cfg(self, cfg):
        return self.getCfg_c(cfg) + self.getLnk_c(cfg) + self.getPtf_c(cfg)


class cfg_ftn:
    def getCfg_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_bse = [
                    '-c',                       #  produce a .o and stop
                    '-align64',                 #  align data in common blocks to 64-bit boundaries
#                    '-ff90',                    #  Allow Fortran 90 constructs
                    '-fdollars-in-identifiers', #  Accept `$' in identifiers
                    '-fno-second-underscore',   #  Do not append a second underscore to symbols that already contain an underscore
#                    '-fullwarn',                #  give more warnings, especially about missing prototypes
                    '-module' + cfg.mod_dir,
                    '-I' + cfg.mod_dir,
                    ]
        flags_cfg = {
                    'debug'     :
                    [
                    '-O0',                      #  no optimization
                    '-g',                       #  full debug info
                    '-d-lines',                 #  Compile lines with D in 1 column
                    '-fno-directives',          #  Ignore Fortran compiler directives inside comments
                    '-fno-fast-math',           #  Conform to ANSI & IEEE math rules at the expense of speed
                    '-fno-unsafe-math-optimizations', #  Conform to ANSI & IEEE math rules at the expense of speed
                    '-noinline',                #  suppress inline processing
                    '-trapuv',                  #  trap uninitialized variables
                    '-fno-fast-stdlib',         #  Do not use faster versions of standard library functions
                    '-fno-inline-functions',    #  Do not automatically integrate simple functions into their callers
                    '-fno-optimize-regions',    #  Enable optimization of EH regions formation
                    ],
                    'profil'    :
                    [
                    '-O0', '-g3', '-fno-inline', '-pg'
                    ],
                    'release'   :
                    [
                    '-O3',                      #  full optimization
                    '-g',                       #  full debug info
#                    '-g0',                      #  no debug info
#  au link aussi      '-ipa',                     #  Perform interprocedural analysis and optimization
                    '-inline',                  #  request inline processing
                    '-finline-functions',       #  Automatically integrate simple functions into their callers
                    '-fkeep-inline-functions',  #  Generate code for funcs even if they are fully inlined
#                    '-fcxx-openmp',             #  Do OpenMP processing under C++
                    '-fivopts',                 #  Perform induction variable optimizations (for options compatibility only, performed by default by Open64)
                    '-fstrength-reduce',        #  Perform strength reduction optimisations
                    '-fomit-frame-pointer',     #  When possible do not generate stack frames
                    '-ffast-stdlib',            #  Use faster versions of some standard library functions, when available
                    '-ffast-math',              #  Improve FP speed by violating ANSI & IEEE rules
                    '-funsafe-math-optimizations',   #  Improve FP speed by violating ANSI & IEEE rules
                    '-funroll-all-loops',        #  Peform loop onrolling for all loops
                    '-funroll-loops',            #  unroll-loops
                    '-mp',                      #  enable the multiprocessing directives (same as -mp)
                    '-apo',
                    '-march=auto',
                    ]
                    }
        return flags_bse + flags_cfg[c]

    def getLnk_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        if (p == 'unx32' or p == 'unx64'):
            flags_lnk = {
                         'debug' :
                           {
                              'static' :  [ ],
                              'dynamic' : [ '-fPIC', '-shared' ],
                           },
                         'release' :
                           {
                              'static' :  [ ],
                              'dynamic' : [ '-fPIC', '-shared' ],
                           },
                        }
        else:
            raise 'Invalid Open64 platform: %s' % p
        return flags_lnk[c][l]

    def getPtf_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_ptf = {
                     'unx32' : ['-m32'],
                     'unx64' : ['-m64'],
                     'win32' : [],
                     'win64' : [],
                     }
        return flags_ptf[p]

    def getIsz_f(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_isz = {
                    4 : ['-i4'],
                    8 : ['-i8'],
                    }
        return flags_isz[i]

    def ftn_cfg(self, cfg):
        return self.getCfg_f(cfg) + self.getLnk_f(cfg) + self.getPtf_f(cfg) + self.getIsz_f(cfg)


"""
Open64 générique
"""
class open64_gen:
    def __fixModules(self, cfg):
        for f in glob.glob( os.path.join(cfg.mod_dir, '*.mod') ):
            nam, ext = os.path.splitext( os.path.basename(f) )
            if nam != nam.lower():
                modSrc = '.'.join( (nam,'mod') )
                modDst = '.'.join( (nam.lower(),'mod') )
                pthSrc = os.path.join(cfg.mod_dir, modSrc)
                pthDst = os.path.join(cfg.mod_dir, modDst)
                if os.path.isfile(pthSrc):
                    if not os.path.isfile(pthDst) or os.path.getmtime(pthSrc) > os.path.getmtime(pthDst):
#                        print 'Copied to: ', pthDst
                        shutil.copyfile(pthSrc, pthDst)
                        shutil.copystat(pthSrc, pthDst)

    def __go(self, cmplr, cfg):
        c = '%s %s %s %s -o%s %s' % (cmplr, ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn), cfg.fic_out, cfg.fic_inp)

        _stderr = None
        _stdout = None
        _shell  = True
        if (cfg.fic_err):
            _stdout = open(cfg.fic_err, 'w')
            _stderr = subprocess.STDOUT
        if (MUC_platform.is_win()):
            _shell = False

        if (cfg.dmp_cmd):
            print('--- Compiling on %s for %s(i%s)' % (MUC_platform.build_platform(), cfg.tgt_ptf, cfg.tgt_isz))
            print(c)
            print('-------')
        retcode = subprocess.call(c, stdout=_stdout, stderr=_stderr, shell=_shell)
        self.__fixModules(cfg)

    def c(self, cmplr, cfg):
        self.__go(cmplr, cfg)

    def cpp(self, cmplr, cfg):
        self.__go(cmplr, cfg)

    def ftn(self, cmplr, cfg):
        self.__go(cmplr, cfg)


"""
Open64
"""
class open64(open64_gen, cfg_cpp, cfg_ftn):
    def __init__(self, v = None):
        if (v and len(v.strip()) > 0):
            self.open64_dir = 'open64-%s' % v
        else:
            self.open64_dir = 'open64'

    def c(self, cfg):
        if (MUC_platform.is_unx()):
            cmplr = os.path.join('/opt/open64', self.open64_dir, 'bin', 'opencc')
        else:
            raise 'Invalid Open64 build platform: %s' % MUC_platform.build_platform()
        open64_gen.c(self, cmplr, cfg)

    def cpp(self, cfg):
        if (MUC_platform.is_unx()):
            cmplr = os.path.join('/opt/open64', self.open64_dir, 'bin', 'openCC')
        else:
            raise 'Invalid Open64 build platform: %s' % MUC_platform.build_platform()
        open64_gen.cpp(self, cmplr, cfg)

    def ftn(self, cfg):
        if (MUC_platform.is_unx()):
            cmplr = os.path.join('/opt/open64', self.open64_dir, 'bin', 'openf95')
        else:
            raise 'Invalid Open64 build platform: %s' % MUC_platform.build_platform()
        open64_gen.ftn(self, cmplr, cfg)

"""
Open64 4.x
"""
class open64_424(open64):
    def __init__(self):
      open64.__init__(self, '4.2.4')

class open64_45(open64):
    def __init__(self):
      open64.__init__(self, '4.5')

class open64_452(open64):
    def __init__(self):
      open64.__init__(self, '4.5.2')

class open64_4521(open64):
    def __init__(self):
      open64.__init__(self, '4.5.2.1')

class open64_50(open64):
    def __init__(self):
      open64.__init__(self, '5.0')

"""
Open64 version par défaut
"""
class open64_xx(open64):
    def __init__(self):
      open64.__init__(self)

