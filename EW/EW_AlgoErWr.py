# -*- coding: UTF-8 -*-
from EW_Info import EWInfoDate
from EW_Info import creeInfoDatedeXML
import os

class EWFichierHTML:
    def __init__(self):
        pass

    def ecrisTetePage(self, titre):
        print("Content-type: text/html")
        print
        print("<html>")
        # fic.write(EQCopyright());
        print("<head>")
        print("<title>" + titre + "</title>")
        print("</head>")
        print("<body>")
        # (*osP) << EWMenu();

    def ecrisPiedPage(self):
        # (*osP) << WBMenu();
        print("</body>")
        print("</html>")

    def ecris(self, ligne):
        print(ligne)

    def reqCouleur(self, err, wrn):
        if (err == 0):
            if (wrn == 0):
                return "\"#00FF00\""
            else:
                return "\"#FF8800\""
        else:
            return "\"#FF0000\""

class EWUneDateUnModuleUneVersionToutesConfigs:
    def __init__(self, date, mdl, ver, config):
        self.dte = date
        self.mdl = mdl
        self.ver = ver
        self.ficHTML = None

    def assembleFichierFichiers(self, item):
        self.lstFichiers[item.fichier] = item

    def imprimeFichier(self, item):
        if (item.fichier != self.ficActu.fichier): return
        href = "file:%s" % item.path
        self.ficHTML.ecris("<TD ALIGN=CENTER" +
                              " BGCOLOR=" + self.ficHTML.reqCouleur(item.errors, item.warnings) +
                              ">" +
                              "<A HREF=\"%s\">%d - %d</A>" % (href, item.errors, item.warnings))

    def imprimeFichierEnteteTable1(self, item):
        if (item.fichier != self.ficActu.fichier): return
        self.ficHTML.ecris("<TH COLSPAN=1>%s" % item.compil)

    def imprimeFichierEnteteTable2(self, item):
        if (item.fichier != self.ficActu.fichier): return
        self.ficHTML.ecris("<TH>%s" % item.config)

    def assembleTargetFichiers(self, item):
        item.pourChaqueFichier(self.assembleFichierFichiers)

    def imprimeTarget(self, item):
        item.pourChaqueFichier(self.imprimeFichier)

    def imprimeTargetEnteteTable1(self, item):
        item.pourChaqueFichier(self.imprimeFichierEnteteTable1)

    def imprimeTargetEnteteTable2(self, item):
        item.pourChaqueFichier(self.imprimeFichierEnteteTable2)

    def imprimeVersion(self, item):
        if (item.nom != self.ver): return
        self.lstFichiers = {}
        item.pourChaqueTarget(self.assembleTargetFichiers)

        f = self.lstFichiers.values()[0]
        self.ficActu = f
        self.ficHTML.ecris("<TR>")
        self.ficHTML.ecris("<TH ROWSPAN=2 ALIGN=LEFT>%s" % "Fichier")
        item.pourChaqueTarget(self.imprimeTargetEnteteTable1)
        self.ficHTML.ecris("<TR>")
        item.pourChaqueTarget(self.imprimeTargetEnteteTable2)

        for f in self.lstFichiers.values():
            self.ficHTML.ecris("<TR>")
            self.ficHTML.ecris("<TH ALIGN=LEFT>%s" % os.path.splitext(f.fichier)[0])
            self.ficActu = f
            item.pourChaqueTarget(self.imprimeTarget)

    def imprimeModule(self, item):
        item.pourChaqueModule(self.imprimeModule)
        if (item.nom != self.mdl): return
        item.pourChaqueVersion(self.imprimeVersion)

    def imprimeDate(self, item):
        # if (item.date != self.dte): return
        item.pourChaqueModule(self.imprimeModule)

    def xeq(self, dates):
        self.ficHTML = EWFichierHTML()
        self.ficHTML.ecrisTetePage("%s %s %s" % (self.dte, self.mdl, self.ver))
        self.ficHTML.ecris("<br><b>Date: %s</b>" % self.dte)
        self.ficHTML.ecris("<br><b>Module: %s</b>" % self.mdl)
        self.ficHTML.ecris("<br><b>Version: %s</b>" % self.ver)
        self.ficHTML.ecris("<br><br><center><TABLE CELLSPACING=2 WIDTH=\"95%\" BORDER=1>")
        for d in dates:
            self.imprimeDate(d)
        self.ficHTML.ecris("</TABLE></center>")
        self.ficHTML.ecrisPiedPage()


class EW_Comp_UneDateTousModulesToutesVersionsToutesConfigs:
    def __init__(self):
        self.os = None

    def assembleFichierFichiers(self, item):
        self.somme_ew[0] = self.somme_ew[0] + item.errors
        self.somme_ew[1] = self.somme_ew[1] + item.warnings

    def assembleTargetFichiers(self, item):
        item.pourChaqueFichier(self.assembleFichierFichiers)

    def imprimeEnteteTable(self):
        self.ficHTML.ecris("<TR>")
        self.ficHTML.ecris("<TH ROWSPAN=2 ALIGN=LEFT>%s" % "Module")
        self.ficHTML.ecris("<TH COLSPAN=2>%s" % "Compilation")
        self.ficHTML.ecris("<TH COLSPAN=2>%s" % "Link")
        self.ficHTML.ecris("<TH COLSPAN=2>%s" % "Dep")
        self.ficHTML.ecris("<TR>")
        self.ficHTML.ecris("<TH>Erreur<TH>Warning")
        self.ficHTML.ecris("<TH>Erreur<TH>Warning")
        self.ficHTML.ecris("<TH>Erreur<TH>Warning")

    def imprimeVersion(self, item):
        self.somme_ew = [0, 0]
        href = "/cgi-bin/EW_GenHtmlErWr.cgi?mdl=%s&vrs=%s" % (self.mdl_actu, item.nom)
        item.pourChaqueTarget(self.assembleTargetFichiers)
        self.ficHTML.ecris("<TD ALIGN=CENTER" +
                            " BGCOLOR=" + self.ficHTML.reqCouleur(self.somme_ew[0], self.somme_ew[1]) +
                            ">" +
                            "<A HREF=\"%s\">%d</A>" % (href, self.somme_ew[0]))
        self.ficHTML.ecris("<TD ALIGN=CENTER" +
                            " BGCOLOR=" + self.ficHTML.reqCouleur(self.somme_ew[0], self.somme_ew[1]) +
                            ">" +
                            "<A HREF=\"%s\">%d</A>" % (href, self.somme_ew[1]))

    def imprimeModule(self, item):
        item.pourChaqueModule(self.imprimeModule)
        if (len(item.versions) != 0):
            self.mdl_actu = item.nom
            self.ficHTML.ecris("<TR>")
            self.ficHTML.ecris("<TH ALIGN=LEFT>%s" % item.nom)
            item.pourChaqueVersion(self.imprimeVersion)

    def imprimeDate(self, item):
        self.ficHTML.ecris("<hr><b>Date: %s</b>" % item.date)
        self.ficHTML.ecris("<br><br><center><TABLE CELLSPACING=2 WIDTH=\"95%\" BORDER=1>")
        self.imprimeEnteteTable()
        item.pourChaqueModule(self.imprimeModule)
        self.ficHTML.ecris("</TABLE></center>")
        self.ficHTML.ecrisPiedPage()

    def xeq(self, ficHTML, date):
        self.ficHTML = ficHTML
        self.imprimeDate(date)


class EWMenu:
    def __init__(self):
        pass

    def imprimeMenu(self, os, dates):
        os.ecris("<form method=post action=\"/cgi-bin/EW_GenHtmlErWr.cgi\">")
        os.ecris("<p>")
        os.ecris("<select name=\"dte\">")
        for d in dates:
            os.ecris("<option value=\"%s\">%s" % (d.date, d.date))
        os.ecris("</select>")
        os.ecris("<input type=submit value=\"Date\">")

    def xeq(self, os, dates):
        self.imprimeMenu(os, dates)


class EWUneDateTousModulesToutesVersionsToutesConfigs:
    def __init__(self, date, mdl, ver, config):
        self.dte = date
        self.ficHTML = None

    def xeq(self, dates):
        self.ficHTML = EWFichierHTML()
        print "Rapport complet: %s %s %s" % (self.dte, "", "")
        self.ficHTML.ecrisTetePage("Rapport complet: %s %s %s" % (self.dte, "", ""))
        # self.ficHTML.ecris("<frameset rows=\"45, *\">\n");
        # self.ficHTML.ecris("  <frame name=\"menu\"      scrolling=\"no\" noresize src=\"/WebPP_Menu.htm\">\n");
        menu = EWMenu()
        menu.xeq(self.ficHTML, dates)
        self.ficHTML.ecris("<br>")
        # self.ficHTML.ecris("  <frame name=\"affichage\" scrolling=\"yes\"         src=\"%s\">\n", CarP(fic));
        comp = EW_Comp_UneDateTousModulesToutesVersionsToutesConfigs()
        comp.xeq(self.ficHTML, date)
        # self.ficHTML.ecris("</frameset>\n");


if __name__ == '__main__':
    path = "EW_20040225-142952.xml"
    date = creeInfoDatedeXML(path)
    dates = [date]
    algo = EWUneDateTousModulesToutesVersionsToutesConfigs(date.date, "*", "*", "*")
    algo.xeq(dates)
