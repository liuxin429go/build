# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#-----------------------------------------------------------------------------------------------------

import MultiCompiler_intel

itli64_110061 = MultiCompiler_intel.intel_110061
itli64_110066 = MultiCompiler_intel.intel_110066
itli64_110069 = MultiCompiler_intel.intel_110069
itli64_110072 = MultiCompiler_intel.intel_110072
itli64_110074 = MultiCompiler_intel.intel_110074

itli64_111046 = MultiCompiler_intel.intel_111046
itli64_11106  = MultiCompiler_intel.intel_11106
itli64_111060 = MultiCompiler_intel.intel_111060
itli64_111065 = MultiCompiler_intel.intel_111065
itli64_111069 = MultiCompiler_intel.intel_111069
itli64_111072 = MultiCompiler_intel.intel_111072

itli64_1200   = MultiCompiler_intel.intel_1200
itli64_1200084= MultiCompiler_intel.intel_1200084
itli64_1203174= MultiCompiler_intel.intel_1203174
itli64_1203175= MultiCompiler_intel.intel_1203175

itli64_1400   = MultiCompiler_intel.intel_1400
itli64_1402144= MultiCompiler_intel.intel_1402144
itli64_1402176= MultiCompiler_intel.intel_1402176
itli64_1403174= MultiCompiler_intel.intel_1403174
itli64_1403202= MultiCompiler_intel.intel_1403202
itli64_1404211= MultiCompiler_intel.intel_1404211
itli64_1404237= MultiCompiler_intel.intel_1404237

itli64_1500   = MultiCompiler_intel.intel_1500
itli64_1501133= MultiCompiler_intel.intel_1501133
itli64_1502179= MultiCompiler_intel.intel_1502179
itli64_1503208= MultiCompiler_intel.intel_1503208
itli64_1504221= MultiCompiler_intel.intel_1504221
itli64_1506285= MultiCompiler_intel.intel_1506285

itli64_1600   = MultiCompiler_intel.intel_1600
itli64_1601150= MultiCompiler_intel.intel_1601150
itli64_1603207= MultiCompiler_intel.intel_1603207
itli64_1603210= MultiCompiler_intel.intel_1603210
itli64_1604246= MultiCompiler_intel.intel_1604246

itli64_1700   = MultiCompiler_intel.intel_1700
itli64_1702187= MultiCompiler_intel.intel_1702187
itli64_1704210= MultiCompiler_intel.intel_1704210
itli64_1705267= MultiCompiler_intel.intel_1705267
itli64_1706270= MultiCompiler_intel.intel_1706270
itli64_1707272= MultiCompiler_intel.intel_1707272

itli64_1800   = MultiCompiler_intel.intel_1800
itli64_1802185= MultiCompiler_intel.intel_1802185
itli64_1802199= MultiCompiler_intel.intel_1802199
itli64_1803210= MultiCompiler_intel.intel_1803210
itli64_1803222= MultiCompiler_intel.intel_1803222

itli64_1900   = MultiCompiler_intel.intel_1900
itli64_1901144= MultiCompiler_intel.intel_1901144
itli64_1902190= MultiCompiler_intel.intel_1902190
itli64_1903203= MultiCompiler_intel.intel_1903203
itli64_1904243= MultiCompiler_intel.intel_1904243
itli64_1904245= MultiCompiler_intel.intel_1904245
itli64_1905281= MultiCompiler_intel.intel_1905281
itli64_191    = MultiCompiler_intel.intel_191
itli64_191254 = MultiCompiler_intel.intel_191254
itli64_191304 = MultiCompiler_intel.intel_191304
itli64_191311 = MultiCompiler_intel.intel_191311
itli64_192    = MultiCompiler_intel.intel_192
itli64_192616 = MultiCompiler_intel.intel_192616
