# -*- coding: UTF-8 -*-
import SCons.Script
import SCons.Environment

from . import Environment
import MUC_platform

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

class EnvironmentIntel(MUCEnv):
    def __init__(self, tool, version, opts, *args, **kwargs):
        target_arch =    {'win32'  : 'x86',
                          'win64'  : 'amd64',
                          'win64i8': 'amd64',
                          'unx32'  : 'x86',
                          'unx64'  : 'amd64',
                          'unx64i8': 'amd64'}
        intel_platform = {'win32'  : 'ia32',
                          'win64'  : 'em64t',
                          'win64i8': 'em64t',
                          'unx32'  : 'ia32',
                          'unx64'  : 'em64t',
                          'unx64i8': 'em64t'}

        if (MUC_platform.is_win()):
            tools =  [ 'mslib', 'mslink', 'fortran', 'MUC_icl', 'MUC_ifl' ]
        elif (MUC_platform.is_unx()):
            tools =  [ 'c++', 'MUC_icl', 'MUC_ifl', 'gnulink', 'ar', 'gas' ]
        else:
            raise SCons.Errors.UserError('Invalid Intel platform: %s' % MUC_platform.platform())

        try:
            muc_target_plateform = kwargs['MUC_TARGETPLATEFORM']
        except:
            muc_target_plateform = MUC_platform.build_platform()
            kwargs['MUC_TARGETPLATEFORM'] = muc_target_plateform
        muc_build_plateform = MUC_platform.build_platform()

        SConsEnv.__init__( self,
                           tools            = tools,
                           options          = opts,
                           plateform        = muc_build_plateform,
                           TARGET_ARCH      = target_arch[muc_target_plateform],
                           INTEL_VERSION    = version,
                           INTEL_PLATEFORM  = intel_platform[muc_target_plateform],
                           *args,
                           **kwargs)
        # self.printEnv()
        self['MUC_VERSION_FULL'] = self['INTEL_C_COMPILER_VERSION']
        self['MUC_VERSION_MAIN'] = version
        self['INTEL_VERSION'] = self['MUC_VERSION_FULL']
        MUCEnv.__init__(self, tool, version, *args, **kwargs)

        if (MUC_platform.is_win()):
            # Ici ou dans intelc.py ?? ou dans mslink.py??
            self['LINKCOM']   = [self['LINKCOM'],
                                 SCons.Script.Action(WaitForWritable, None),
                                 'mt.exe -nologo -manifest ${TARGET}.manifest -outputresource:$TARGET;1']
            self['SHLINKCOM'] = [self['SHLINKCOM'],
                                 SCons.Script.Action(WaitForWritable, None),
                                'mt.exe -nologo -manifest ${TARGET}.manifest -outputresource:$TARGET;2']
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/NOLOGO']
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/SUBSYSTEM:CONSOLE']
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/INCREMENTAL:NO']
            #self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/NODEFAULTLIB:libcmt']
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/MANIFEST']
            # scons ajoute des "" et split les commandes sur des espaces
            # ce qui rend la commande illisible.
            # Comme les options MANIFESTUAC sont celles par défaut,
            # met simplement le flag en commentaire
            #self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/MANIFESTUAC:"level=\'asInvoker\' uiAccess=\'false\'"']
            # Pour contourner une erreur de link static
            # ldexpf already defined in libmmt.lib
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/FORCE:MULTIPLE']
        elif (MUC_platform.is_unx()):
            if (self['docpplink']):
                lnk = 'icpc'
            else:
                lnk = 'ifort'
            if (muc_target_plateform == 'unx32'):
                lnk += ' -m32'
            else:
                lnk += ' -m64'
            iclvars = '"$INTEL_ICLVARS_CMD" $INTEL_ICLVARS_KIND > /dev/null'
            self['LINK'] = ' && '.join( [iclvars, lnk] )
        else:
            raise SCons.Errors.UserError('Invalid Intel platform: %s' % MUC_platform.platform())

    def setupEnv(self, ctx):
        MUCEnv.setupEnv(self, ctx)
        if (MUC_platform.is_win()):
            if (ctx.bld == 'debug'):
                self['LINKFLAGS'] = self.get('LINKFLAGS') + ['/DEBUG']

        if (MUC_platform.is_unx()):
            if (ctx.bld == 'release'):
                self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-qopenmp']
#            if (ctx.lnk == 'static'):
#                self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-static']
            if (not self['docpplink']):
               try:
                   ctx.sys.index('stdc++')
               except:
                   ctx.sys.append('stdc++')
            # ---  Set again, as docpplink might have been changed
            if (self['docpplink']):
                lnk = 'icpc'
            else:
                lnk = 'ifort'
            if (self['MUC_TARGETPLATEFORM'] == 'unx32'):
                lnk += ' -m32'
            else:
                lnk += ' -m64'
            iclvars = '"$INTEL_ICLVARS_CMD" $INTEL_ICLVARS_KIND > /dev/null'
            self['LINK'] = ' && '.join( [iclvars, lnk] )

import os
import time
def WaitForWritable(target, source, env):
    """Waits for the target to become writable.
    http://src.chromium.org/native_client/trunk/src/native_client/site_scons/site_tools/target_platform_windows.py

    Args:
    target: List of target nodes.
    source: List of source nodes.
    env: Environment context.

    Returns:
    Zero if success, nonzero if error.

    This is a necessary hack on Windows, where antivirus software can lock exe
    files briefly after they're written.  This can cause subsequent reads of the
    file by env.Install() to fail.  To prevent these failures, wait for the file
    to be writable.
    """
    target_path = target[0].abspath
    if not os.path.exists(target_path):
        return 0            # Nothing to wait for

    for unused_retries in range(10):
        try:
            f = open(target_path, 'a+b')
            f.close()
            return 0    # Successfully opened file for write, so we're done
        except (IOError, OSError):
            print('Waiting for access to %s...', target_path)
            time.sleep(1)

    # If we're still here, fail
    print('Timeout waiting for access to %s.', target_path)
    return 1


