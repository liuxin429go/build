#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ***********************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
# ***********************************************************************

import logging
import os

from config import Config
from util   import (ajouteVersion, path2unix,
                    remplaceEnvINRS,
                    xtrNomRelatif, xtrNomModule)

LOGGER = logging.getLogger("vc2scons.writer")

class Writer:
    """
    Virtual class. Does the heavy lifting, child shall implement
    the elements writing for a specific format.
    """

    # ---  Interface
    def traiteSol(self, sol, *args):
        raise NotImplementedError
    def traitePrj(self, prj, *args):
        raise NotImplementedError
    def ecrisUneDependance(self, m, t):
        raise NotImplementedError
    def ecrisUnFichier(self, n):
        raise NotImplementedError
    def ecrisUnInclude(self, n):
        raise NotImplementedError
    def ecrisUnDefine(self, n):
        raise NotImplementedError
    def ecrisUneLib(self, n):
        raise NotImplementedError
    def ecrisUneLibXtrn(self, l):
        raise NotImplementedError
    def ecrisUnLibPath(self, n):
        raise NotImplementedError

    def __init__(self, prms):
        """
        Constructeur
        """
        self.prms = prms

    def reqDependances(self, sol, prj, exclude=[], include=None):
        """
        Retourne la liste des dépendances d'un projet
        """
        deps = []
        if not sol: return deps

        co = sol.getConstructionOrder(prj)[:-1]     # skip prj
        for p in reversed(co):
            typTgt = p.getTargetType()
            if typTgt == "Invalid_Target_Type": continue
            if include is not None and not any([ f(p) for f in include ]): continue
            if any([ f(p) for f in exclude ]): continue

            nomMdl = xtrNomModule(p.getDirectory(), self.prms.projectRoot)
            nomTgt = p.getName()
            deps.append( (nomMdl, nomTgt) )
        return deps

    def ecrisDependances(self, sol, prj):
        """
        Écris les dépendances d'un projet
        """
        deps = self.reqDependances(sol, prj)
        for dep in deps:
            self.ecrisUneDependance(*dep)

    def reqFichiers(self, prj, rep, typFic):
        """
        Retourne la liste les fichiers d'un projet selon le type demandé
        """
        lst = []
        fichiers = prj.getFiles(self.prms.incExcludedFromBuild)
        for fichier in fichiers:
            ext = fichier.getExtension().lower() + ";"
            if ext in typFic:
                nomRel = xtrNomRelatif(fichier.getFullPAth(), rep)
                nomRel = remplaceEnvINRS(nomRel)
                lst.append(nomRel)
        return lst

    def ecrisFichiers(self, prj, rep, typFic):
        """
        Écris les fichiers d'un projet selon le type demandé
        """
        items = self.reqFichiers(prj, rep, typFic)
        for item in items:
            self.ecrisUnFichier(item)

    def reqRepertoiresInclude(self, prj):
        """
        Retourne la liste les includes d'un projet.
        Les répertoires externes connus sont repris par la configuration
        des lib externes.
        """
        incs = []

        fichiers = prj.getIncMUC() if prj.isMUC() else prj.getIncPath()
        for rep in fichiers:
            rep = rep.strip()
            rep = path2unix(rep)

            doitEcrire = False
            if not rep:
                doitEcrire = False
            elif Config.estPathInterne(rep):
                doitEcrire = True
            else:
                estLibInterne = Config.estLibInterne(rep)
                estLibExterne = Config.estLibExterne(rep)
                if estLibInterne or not estLibExterne: doitEcrire = True

            if doitEcrire:
                incs.append(ajouteVersion(rep, self.prms))

        return incs

    def ecrisRepertoiresInclude(self, prj):
        """
        Écris les fichiers d'un projet selon le type demandé
        """
        items = self.reqRepertoiresInclude(prj)
        for item in items:
            self.ecrisUnInclude(item)

    def reqDefine(self, prj):
        """
        Retourne la liste des defines d'un projet
        """
        defs = []
        if prj.isMUC():
            # Nothing to do, this is taken care with the .dfn files
            pass
        else:
            defines = prj.getDefines()
            for df in defines:
                df = df.strip()
                if df:
                    if df not in Config.SystemDefine or self.prms.addSystemDefine:
                        defs.append(df)
        return defs

    def ecrisDefine(self, prj):
        """
        Écris les defines d'un projet
        """
        items = self.reqDefine(prj)
        for item in items:
            self.ecrisUnDefine(item)

    def reqLibXtrn(self, prj):
        """
        Retourne la liste des librairies externes d'un projet.
        """
        libs = []
        # ---  Accumule les librairies
        fichiers = prj.getLibraries()
        for fic in fichiers:
            fic = fic.strip()
            if fic:
                if not Config.estLibInterne(fic):
                    if Config.estLibExterne(fic):
                        k, v = Config.reqItemExterne(fic)
                        if v not in libs:
                            libs.append(v)

        # ---  Accumule les includes
        if prj.isMUC():
            fichiers = prj.getIncMUC()
        else:
            fichiers = prj.getIncPath()
        for fic in fichiers:
            fic = fic.strip()
            if fic:
                if not Config.estLibInterne(fic):
                    if Config.estLibExterne(fic):
                        k, v = Config.reqItemExterne(fic)
                        if v not in libs:
                            libs.append(v)

        return libs

    def ecrisLibXtrn(self, prj):
        """
        Recherche les librairies externes d'un projet et
        appelle la fonction ecrisUneLibXtrn pour les écrire
        """
        libs = self.reqLibXtrn(prj)
        for lib in libs:
            self.ecrisUneLibXtrn(lib)

    def reqLibrairies(self, prj):
        """
        Retourne la liste des librairies d'un projet.
        On n'écris pas les lib externes qui sont
        reprises par la configuration des lib externes.
        """
        libs = []
        fichiers = prj.getLibraries()
        for fic in fichiers:
            fic = fic.strip()
            if fic and not Config.estLibExterne(fic):
                fic = path2unix(fic)
                fic = os.path.splitext(fic)[0]
                libs.append(fic)
        return libs

    def ecrisLibrairies(self, prj):
        """
        Recherche les librairies d'un projet et
        appelle la fonction ecrisUneLib pour les écrire
        On n'écris pas les lib externes qui sont
        reprises par la configuration des lib externes.
        """
        libs = self.reqLibrairies(prj)
        for lib in libs:
            self.ecrisUneLib(lib)

    def reqLibPath(self, prj):
        """
        Retourne la liste des repertoires de librairies.
        On n'écris que les lib internes, les externes sont
        reprises par la configuration des lib externes.
        """
        lpts = []
        for rep in prj.getLibPath():
            rep = rep.strip()
            if not rep: continue

            if not Config.estPathExterne(rep):
                estLibExterne = Config.estLibExterne(rep)
                estLibInterne = Config.estLibInterne(rep)

                if estLibExterne:
                    pass
                elif "PostgreSQL" in rep:
                    pass
                elif rep:
                    if estLibInterne:
                        nomMdl = xtrNomModule(rep, self.prms.projectRoot)
                        nomTgt = ""
                        self.ecrisUneDependance(nomMdl, nomTgt)
                    else:
                        rep = rep.replace("\\", "/")
                        lpts.append(rep)
        return lpts

    def ecrisLibPath(self, prj):
        """
        Recherche les repertoires de librairies et
        appelle la fonction ecrisUnLibPath pour les écrire
        On n'écris que les lib internes, les externes sont
        reprises par la configuration des lib externes.
        """
        for rep in prj.getLibPath():
            rep = rep.strip()
            if not rep: continue

            if not Config.estPathExterne(rep):
                estLibExterne = Config.estLibExterne(rep)
                estLibInterne = Config.estLibInterne(rep)

                if estLibExterne:
                    pass
                elif "PostgreSQL" in rep:
                    pass
                elif rep:
                    if estLibInterne:
                        nomMdl = xtrNomModule(rep, self.prms.projectRoot)
                        nomTgt = ""
                        self.ecrisUneDependance(nomMdl, nomTgt)
                    else:
                        rep = rep.replace("\\", "/")
                        self.ecrisUnLibPath(rep)

    @staticmethod
    def libContains(libs, v):
        return v in libs
