# -*- coding: UTF-8 -*-
import SCons.Script

from . import Environment
import MUC_platform
import os

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

class EnvironmentSun(MUCEnv):
    def __init__(self, tool, version, opts, *args, **kwargs):
        if (MUC_platform.is_win()):
            raise SCons.Errors.UserError('Invalid SUN platform: %s' % MUC_platform.build_platform())
            muc_target_plateform = MUC_platform.build_platform()
            plateform = muc_target_plateform
        elif (MUC_platform.is_unx()):
            tools =  ['suncc', 'sunc++', 'sunf90', 'sunlink', 'ar']
            muc_target_plateform = MUC_platform.build_platform()
            plateform = muc_target_plateform
        else:
            raise SCons.Errors.UserError('Invalid SUN platform: %s' % MUC_platform.build_platform())

        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform = plateform,
                           MUC_TARGETPLATEFORM = muc_target_plateform)
        MUCEnv.__init__(self, tool, version, *args, **kwargs)

        def __findPath(__pth, __what, __version):
            __CCPath = None
            try:
                for d in os.listdir(__pth):
                    __fullPath = os.path.join(__pth, d)
                    if (not __CCPath) and os.path.isdir(__fullPath):
                        __CC = os.path.join(__fullPath, __what)
                        if os.path.exists(__CC) and __version in __CC:
                            __CCPath = __CC
                            break
                        else:
                            __CCPath = __findPath(__fullPath, __what, __version)
            except:
                pass
            return __CCPath
        try:
            self.binPath = os.path.dirname( __findPath('/opt/sun', 'bin/CC', version) )
        except:
            raise SCons.Errors.UserError('Invalid SUN platform: %s' % MUC_platform.build_platform())
        try:
            self.stlPath = None
            if version < '12.6':
                self.stlPath = os.path.dirname( __findPath('/opt/sun', 'stlport4/libstlport.a', version) )
                if (MUC_platform.is_unx64()): self.stlPath = os.path.join(self.stlPath, 'amd64')
        except:
            raise SCons.Errors.UserError('Invalid SUN platform: %s' % MUC_platform.build_platform())

    def setupEnv(self, ctx):
        if (MUC_platform.is_win()):
            self.__setupEnv_win(ctx)
        elif (MUC_platform.is_unx()):
            self.__setupEnv_unx(ctx)
        else:
            raise SCons.Errors.UserError('Invalid SUN platform: %s' % MUC_platform.build_platform())

        if (ctx.bld == 'profil'):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-pg']
        MUCEnv.setupEnv(self, ctx)

    def __setupEnv_unx(self, ctx):
        # -Bdynamic: Prefer dynamic linking (try for shared libraries).
        # –Bstatic:  Require static linking (no shared libraries).
#        if (ctx.lnk == 'static'):
#            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-Bdynamic']

        if (MUC_platform.is_unx32()):   # devrait être target_platform
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-m32', '-L/usr/lib32']
        elif (MUC_platform.is_unx64()):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-m64', '-L/usr/lib/x86_64-linux-gnu']

        if self['MUC_VERSION_MAIN'] < '12.6':
            self.__setupEnv_unx_126moins(ctx)
        else:
            self.__setupEnv_unx_126plus(ctx)

        syslib = ['Crun']
        for l in syslib:
            try:
                ctx.sys.index(l)
            except:
                ctx.sys.append(l)

    def __setupEnv_unx_126moins(self, ctx):
        if (self['docpplink']):
            self['LINK'] = os.path.join(self.binPath, 'sunCC')
        else:           
            self['LINK'] = os.path.join(self.binPath, 'sunf95')

        if (self['docpplink']):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-library=stlport4']
        else:
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-L%s' % self.stlPath, '-lstlport']
        
    def __setupEnv_unx_126plus(self, ctx):
        self['LINK'] = os.path.join(self.binPath, 'sunCC')

        if (self['docpplink']):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-library=stdcpp']
        else:
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-library=stdcpp', '-xlang=f95']
        
    def __setupEnv_win(self, ctx):
	    raise SCons.Errors.UserError('Invalid SUN platform: %s' % MUC_platform.build_platform())

