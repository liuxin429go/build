# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import os

class superlu_sp(ExternalLib.ExternalLib):
    def __go(self, ctx):
        root = Util.getPathFromVersion( os.environ['INRS_LXT'], 'SuperLU', search_join='_' )
        root = os.path.normpath(root)
        __inc = [ os.path.join(root, r'SRC') ]
        __lpt = [ os.path.join(root, r'_run', Util.addBuildPath(r'External')) ]
        __lib = ['SuperLU']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    #---------------
    #---    CMC
    #---------------
    def __cmc_unx32(self, ctx):
        self.__go(ctx)
    def __cmc_unx64(self, ctx):
        self.__go(ctx)
    def __cmcitl_unx32(self, ctx):
        self.__go(ctx)
    def __cmcitl_unx64(self, ctx):
        self.__go(ctx)
    def __cmcpgi_unx32(self, ctx):
        self.__go(ctx)
    def __cmcpgi_unx64(self, ctx):
        self.__go(ctx)

    #---------------
    #---    Linux 32
    #---------------
    def __gcc_unx32(self, ctx):
        self.__go(ctx)

    def __gccX86_unx32(self, ctx):
        self.__go(ctx)

    def __intel_unx32(self, ctx):
        self.__go(ctx)

    def __itlX86_unx32(self, ctx):
        self.__go(ctx)

    def __sun_unx32(self, ctx):
        self.__go(ctx)

    #---------------
    #---    Linux 64
    #---------------
    def __gcc_unx64(self, ctx):
        self.__go(ctx)

    def __gccX64_unx64(self, ctx):
        self.__go(ctx)

    def __intel_unx64(self, ctx):
        self.__go(ctx)

    def __itlX64_unx64(self, ctx):
        self.__go(ctx)

    def __open64_unx64(self, ctx):
        self.__go(ctx)

    def __sun_unx64(self, ctx):
        self.__go(ctx)

    #---------------
    #---    Linux 64i8
    #---------------
    def __gcc_unx64i8(self, ctx):
        self.__go(ctx)

    def __gccX64_unx64i8(self, ctx):
        self.__go(ctx)

    def __intel_unx64i8(self, ctx):
        self.__go(ctx)

    def __itlX64_unx64i8(self, ctx):
        self.__go(ctx)

    def __open64_unx64i8(self, ctx):
        self.__go(ctx)

    def __sun_unx64i8(self, ctx):
        self.__go(ctx)

    #-----------------
    #---    Windows 32
    #-----------------
    def __gcc_win32(self, ctx):
        self.__go(ctx)

    def __gccX86_win32(self, ctx):
        self.__go(ctx)

    def __msvc_win32(self, ctx):
        self.__go(ctx)

    def __intel_win32(self, ctx):
        self.__go(ctx)

    def __itlX86_win32(self, ctx):
        self.__go(ctx)

    #-----------------
    #---    Windows 64
    #-----------------
    def __gcc_win64(self, ctx):
        self.__go(ctx)

    def __gccX64_win64(self, ctx):
        self.__go(ctx)

    def __intel_win64(self, ctx):
        self.__go(ctx)

    def __itlX64_win64(self, ctx):
        self.__go(ctx)

    def __itlI64_win64(self, ctx):
        self.__go(ctx)

    #-----------------
    #---    Windows 64 i8
    #-----------------
    def __gcc_win64i8(self, ctx):
        self.__go(ctx)

    def __gccX64_win64i8(self, ctx):
        self.__go(ctx)

    def __intel_win64i8(self, ctx):
        self.__go(ctx)

    def __itlX64_win64i8(self, ctx):
        self.__go(ctx)

    def __itlI64_win64i8(self, ctx):
        self.__go(ctx)

    #-----------------
    #---    Autres
    #-----------------
    def __ftnchek_win32(self, ctx):
        pass

    def configure(self, ctx, tool, version, plateform):
        fnm = '_superlu_sp__%s_%s' % (tool, plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)

