#!/usr/bin/env python

import os
import sys
import optparse
import subprocess
import util
import logging
logger = util.getlogger('INRS.IEHSS.v2b')

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
class CallVcproj2bjam:

    def __init__(self, opts):
        self.opts = opts
        self._def_common_args()

    def _def_common_args(self):

        v2b = util.getpath(os.environ['INRS_BLD'], 'vcproj2bjam', 'bin',
                              'vcproj2bjam.exe')

        output_systems = '-output_systems='
        if self.opts.setup:
            output_systems += 'setup;'
        if self.opts.scons:
            output_systems += 'scons;'

        project_root = '-project_root=%s' % os.environ['INRS_DEV']

        self.base_args = [v2b, output_systems, project_root]


    def __call__(self, prj_dir):

        def get_sln():
            return util.getpaths(prj_dir, 'prjVisual', '((?!__@@__).)*\.sln')

        def get_ver():
            return util.getpath(prj_dir, 'build', 'versions.cfg')

        def get_stp():
            return util.getpath(prj_dir, 'build', 'setup.cfg')

        def get_grp():
            group = util.relpath(prj_dir, os.environ['INRS_DEV'])
            group = group.split(os.sep)[0]
            if 'H2D2' in group: group = 'H2D2'
            return group

        args = list(self.base_args)

        for p in get_sln():
            args.append('-input_project_file=%s' % p)
            if self.opts.version:
                args.append('-input_versions_file=%s' % get_ver())
            args.append('-input_setup_config_file=%s' % get_stp())
            args.append('-output_group_name=%s' % get_grp())
            args.append('-output_dir=%s' % prj_dir)

            if subprocess.call(args) != 0:
                raise util.CIError('vcproj2bjam.exe failed')


#*******************************************************************************
# Summary: Extracts then validates this script's argument(s).
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def process_args(args):

    parser = optparse.OptionParser()
    parser.set_usage('Usage: %prog [options] [directories]')

    parser.add_option('--setup', action='store_true', dest='setup',
                      default=False, help='create setup file')
    parser.add_option('--scons', action='store_true', dest='scons',
                      default=False, help='create scons file')
    parser.add_option('--version', action='store_true', dest='version',
                      default=False, help='use versions.cfg')

    opts, args = parser.parse_args(args)

    for a in args:
        a = os.path.abspath(a)
        if not os.path.isdir(a):
            parser.error('invalid directory: %s' % a)
    if not args:
        args.append(os.environ['INRS_DEV'])
    if not opts.setup and not opts.scons:
        parser.error('specify one or both options: --setup, --scons')

    return opts, args

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def main(args=None):

    if not args:
        args = sys.argv[1:]
    opts, args = process_args(args)

    call_vcproj2bjam = CallVcproj2bjam(opts)
    required = ['build', 'prjVisual']
    zero_successful = True
    for a in args:
        for d in util.get_valid_dirs(a, required):
            try:
                call_vcproj2bjam(d)
                zero_successful = False
            except util.CIError, msg:
                logger.warning(msg)
    if zero_successful:
        raise util.CIError('every calls failed')


try:
    if not util.iswindows():
        raise util.CIError('invalid platform: must run under windows')
    util.check_env()
    if __name__ == '__main__':
        main()
except util.CIError, msg:
    logger.error(msg)
    sys.exit(1)

