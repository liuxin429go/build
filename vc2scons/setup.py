#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import datetime
import enum
import hashlib
import logging
import os

from config import Config
from writer import Writer
from util   import path2unix, xtrNomModule, xtrPathProjet

LOGGER = logging.getLogger("vc2scons.writer.setup")

class CygwinSetup(Writer):

    def __init__(self, prms):
        super(CygwinSetup, self).__init__(prms)
        
        self.ofs = None

    def writeln(self, l):
        self.ofs.write("%s\n" % l)

    def traiteSol(self, solution, *args):
        """
        Génère le fichier setup d'une solution
        """
        self.ouvreFichier(*args)
        self.__ecrisSolution(solution)
        self.fermeFichier()

    def traitePrj(self, projet, *args):
        """
        Génère le fichier d'un projet
        """
        self.ouvreFichier(*args)
        self.__ecrisProjet(projet)
        self.fermeFichier()

    def __ecrisSolution(self, solution):
        """
        Écris le fichier de setup
        """
        depSet  = {}
        solPath = solution.getDirectory()

        # Pour chaque projet de la solution et qui se trouve dans le même répertoire
        for prj in solution.getProjects():
            if prj.getDirectory() != solPath:
                continue

            # Monte la liste des dépendances via un dico pour l'unicité
            # 1. Les projets dont on dépend qui ne sont pas des lib
            for p in solution.getDependencies(prj):
                if p.getDirectory() != solPath:
                    if p.isLink():
                        depSet[p.getName()] = "-"

            # 2. Les librairies externes
            for fic in prj.getLibraries():
                if "boost_python" in fic:
                    depSet.Add("boost", "-")

        dep = "system %s" % " ".join(depSet.keys())
        dep = dep.strip()

        nomMdl = xtrNomModule(solution.getDirectory(), self.prms.projectRoot)
        nomMdl = nomMdl.split('/', 1)[-1]
        self.writeln("# ---  %s" % nomMdl)
        self.writeln("sdesc: %s" % nomMdl)
        self.writeln("ldesc:")
        if self.prms.ficSetupCfg:
            try:
                with open(self.prms.ficSetupCfg, 'r') as ifs:
                    for l in ifs.readlines():
                        self.writeln("category: %s" % l.strip())
            except:
                LOGGER.error("erreur sur le fichier de setup.")
        else:
            self.writeln("category: Base")

        self.writeln("requires: %s" % dep)
        self.writeln("#curr: 0.2.0")

    def __ecrisProjet(self, prj):
        """
        Écris le fichier de setup
        """
        self.writeln("# ---  " & prj.reqNomProjet())
        self.writeln("sdesc: " & prj.reqNomProjet())
        self.writeln("ldesc:")
        self.writeln("category: ALL Base")
        self.writeln("requires: ")
        self.writeln("curr: 0.2.0")

    def ecrisUneDependance(self, m, t):
        pass
    def ecrisUnFichier(self, n):
        pass
    def ecrisUnInclude(self, n):
        pass
    def ecrisUnDefine(self, n):
        pass
    def ecrisUneLib(self, n):
        pass
    def ecrisUneLibXtrn(self, l):
        pass
    def ecrisUnLibPath(self, n):
        pass

    def ecrisEntete(self, *args):
        """
        Écris l'entête du fichier
        """
        pass

    def ecrisPiedDePage(self):
        """
        Écris le pied de page du fichier
        """
        pass

    def fermeFichier(self):
        """
        Ferme le fichier et prend soin du pied de page
        """
        self.ecrisPiedDePage()
        self.ofs.close()
        self.ofs = None

        # ---  Si on édite directement le fichier, quitte
        if self.prms.append_to_existing_outfile: return

        fOld = os.path.join(self.prms.outputDir, "setup.hint.in")
        fNew = fOld + ".new"
        fBck = fOld + ".bak"

        if os.path.isfile(fNew):
            if os.path.isfile(fOld):
                tmp_fic = open(fNew, 'rb')
                out_fic = open(fOld, 'rb')
                tmp_md5 = hashlib.md5( tmp_fic.read() )
                out_md5 = hashlib.md5( out_fic.read() )
                tmp_fic.close()
                out_fic.close()
                if (tmp_md5.digest() != out_md5.digest()):
                    if (os.path.isfile(fBck)): os.remove(fBck)
                    os.renames(fOld, fBck)
                    os.renames(fNew, fOld)
                    LOGGER.info(' --> Updating %s' % fOld)
                else:
                    os.remove(fNew)
            else:
                os.renames(fNew, fOld)
                LOGGER.info(' --> Creating %s' % fOld)

    def ouvreFichier(self, *args):
        """
        Ouvre le fichier de sortie
        """
        outputFile = os.path.join(self.prms.outputDir, "setup.hint.in")

        # ---  Ouvre le fichier
        self.ofs = None
        if self.prms.append_to_existing_outfile:
            fileExist = os.path.isfile(outputFile)
            if fileExist:
                self.ofs = open(outputFile, 'at', encoding='utf-8')
            else:
                self.ofs = open(outputFile, 'wt', encoding='utf-8')
        else:
            outputFile += ".new"
            self.ofs = open(outputFile, 'wt', encoding='utf-8')
            self.ecrisEntete(*args)
