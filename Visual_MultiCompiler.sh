#!/bin/bash
TGT_CFG=debug
TGT_PTF=unx64
FIC_INP=$1
FIC_OUT=$2
TGT_CPL=sun
TGT_CPL_VER=12.2

python $INRS_BLD/MUC/MultiCompiler.py --compiler=$TGT_CPL-$TGT_CPL_VER --config=$TGT_CFG --platform=$TGT_PTF --output=$FIC_OUT $FIC_INP
