# -*- coding: UTF-8 -*-

from Context import Context
import os.path
import re
import string

MUC_MACROS = {
    '${MUC_TGTDIR_BIN}': re.compile(r'\$\{MUC_GRIST_BIN(:(?P<dir>\w*))?\}'),
    '${MUC_TGTDIR_RUN}': re.compile(r'\$\{MUC_GRIST_RUN(:(?P<dir>\w*))?\}'),
    '__mod__': re.compile(r'\$\{MUC_GRIST_MOD(:(?P<dir>\w*))?\}'),
    }

"""
Add platform, tool, version, etc.. info to a path
"""
def addBuildPath(root):
    ptf = '${MUC_TARGETPLATEFORM}${MUC_TARGETPLATEFORMSUFFIX}'
    mpi = '${MUC_MPI}'
    cpl = '${MUC_TOOL_MAIN}'
    lnk = '${MUC_LINK_G}'
    bld = '${MUC_BUILD}'
    bld_dir = os.path.join(root, ptf, mpi, cpl, lnk, bld)
    bld_dir = os.path.normpath(bld_dir)
    return bld_dir

def expandMUCMacros(ctx, path):
    path = path.replace('\\', '/')
    for key, macro in MUC_MACROS.items():
        m = re.search(macro, path)
        if m:
            d = m['dir'] if m['dir'] else ''
            d = os.path.join(key, addBuildPath(d))
            d = d.replace('\\', '/')    # in macro.sub, \x are re escape sequences
            path = macro.sub(d, path)
    path = os.path.normpath(path)
    return path

def gristSrc(ctx):
    srcs = getattr(ctx, 'src', ctx)
    bld_dir = '${MUC_BUILDDIR}'
    src = []
    for x in srcs:
        doGrist = True
        if x[0] in r'/\$': doGrist = False
        if x[1] in r':':   doGrist = False
        if doGrist: x = os.path.join(bld_dir,x).replace('\\', '/')
        src.append(x)
    return src

def gristIncPath(ctx):
    res = []
    for l in ctx.inc:
        if l.find(r'//') != -1:
            l, r = l.split('//', 1)
            d = '${MUC_TGT_ROOT}'
            if l: d = os.path.join(d, '..', l)
            l = os.path.join(d, '${MUC_TGTDIR_BIN}', addBuildPath(r))
        l = expandMUCMacros(ctx, l)
        res.append(l)
    return res

def gristLibPath(ctx):
    res = []
    for l in ctx.lpt:
        if l.find('//') != -1:
            l, r = l.split('//', 1)
            d = '${MUC_TGT_ROOT}'
            if l: d = os.path.join(d, '..', l)
            l = os.path.join(d, '${MUC_TGTDIR_BIN}', addBuildPath(r))
        l = expandMUCMacros(ctx, l)
        l = l.strip('"').strip("'")
        res.append(l)
    return res

def gristLib(ctx):
#    res = []
#    if (ctx.lnk == 'static'):
#        for l in ctx.lib:
#            if (l not in ['libh2d2', 'getDumpPath'] and l[-3:] != '_xx'): l += '_xx'
#            res.append(l)
#    else:
#        res = ctx.lib
#    return res
    return ctx.lib

def ungristFile(fic, ctx):
    try:
        _ctx = ctx.filterNone()
        # mimic addBuildPath
        root = ''
        ptf = _ctx.env.subst('${MUC_TARGETPLATEFORM}${MUC_TARGETPLATEFORMSUFFIX}')
        mpi = _ctx.mpi
        cpl = _ctx.env.subst('${MUC_TOOL_MAIN}')
        lnk = _ctx.lnk
        bld = _ctx.bld
        bld_dir = os.path.join(root, ptf, mpi, cpl, lnk, bld)
        bld_dir = bld_dir[:-1] if bld_dir[-1] == os.sep else bld_dir

        tl = len(bld_dir)+1
        return fic[ fic.index(bld_dir)+tl: ]
    except ValueError:
        return fic

def gristStage(ctx):
    return os.path.join('${MUC_TGT_ROOT}', '${MUC_TGTDIR_STGE}', addBuildPath(ctx.grp), ctx.prj)

def gristRun(ctx):
    return os.path.join('${MUC_TGT_ROOT}', '${MUC_TGTDIR_RUN}', addBuildPath(ctx.grp))

def gristInstall(ctx):
    instDir = addBuildPath(ctx.grp)
    instDir = instDir.replace('\\', '/').replace('/', r'+')
    instDir = os.path.join('${MUC_TGT_ROOT}', '${MUC_TGTDIR_INST}', ctx.grp, instDir)
    return instDir

def force_posix_path(ctx):
    def __to_posix(dir):
        if os.sep == '/':
            return dir
        else:
            return string.replace(dir, os.sep, '/')

    ctx.lpt = [ __to_posix(c) for c in ctx.lpt ]

"""
  Remove duplicates from a sequence
"""
def seq_uniquer_on_first(seq):
    seen = set()
    result = []
    for item in seq:
        if item not in seen:
            seen.add(item)
            result.append(item)
    return result

def seq_uniquer_on_last(seq):
    seen = set()
    result = []
    for i in range(len(seq)-1, -1, -1):
        item = seq[i]
        if item not in seen:
            seen.add(item)
            result.insert(0,item)
    return result


"""
  Get path from version
"""
import glob
import re
import SCons
#
#   lib_externes/scotch-1.11.2p3_esmumps
#   search_path = 'lib_externes'
#   search_pre  = 'scotch'
#   search_pst  = '_esmumps'
#   search_join = '-'
#   search_sep  = '.'
#   search_fic  = ''
#   last_chance = scotch
def getPathFromVersion(search_path,
                       search_pre,
                       search_pst = '',
                       search_join = '-',
                       search_sep = '.',
                       search_fic = '',
                       last_chance = None):
    def to_float(v):
        try:
            return float(v)
        except:
            try:
                p = v.split('rc')
                return float(p[0]) + 0.001*float(p[1])
            except:
                p = v.split('p')
                return float(p[0]) + 0.001*float(p[1])

    vp = []
    tg = None

    sep  = r'\%s' % search_sep  if search_sep  in ['.'] else search_sep
    jnc  = r'\%s' % search_join if search_join in ['.'] else search_join
    prc  = r'((p|rc|a|b)\d*?)?'
    pver = r'((\d+)(%s\d+)*)' % sep
    pfic = r'(\%s%s)' % (os.sep, search_fic) if search_fic else ''
    if (jnc):
        ptrn = r'[\%s^]%s%s(?P<v>%s)%s%s%s$' % (os.sep, search_pre, jnc, pver, prc, search_pst, pfic)
    else:
        ptrn = r'[\%s^]%s(?P<v>%s)%s%s%s$' % (os.sep, search_pre, pver, prc, search_pst, pfic)

    is_search_path_string = isinstance(search_path, str)
    all_root= None
    all_dir = []
    if is_search_path_string:
        for d in glob.glob( os.path.join(search_path, '*') ):
            if os.path.isdir( os.path.join(search_path, d) ):
                all_dir.append(d)
        all_root = search_path
    else:
        all_dir  = search_path
        all_root = ''

    for d in all_dir:
        m = re.search(ptrn, d)
        if m:
            n  = [ to_float(x) for x in m.group('v').split(search_sep) ]
            if (n > vp):
                vp = n
                tg = d

    if (not tg and is_search_path_string):
        d = last_chance if last_chance else search_pre
        if os.path.isdir( os.path.join(all_root, d) ): tg = d
    if (not tg):
        raise RuntimeError('Could not detect version from file pattern %s in directory %s' % ('xxx'.join([search_pre, search_pst]), search_path))
        raise SCons.Errors.UserError('Could not detect version from file pattern %s in directory %s' % ('xxx'.join([search_pre, search_pst]), search_path))
    return os.path.join(all_root, tg)

#
#   lib_externes/scotch-1.11.2p3_esmumps
#   search_path = 'lib_externes/scotch-1.11.2p3_esmumps'
#   search_pre  = 'scotch'
#   search_pst  = '_esmumps'
#   search_join = '-'
#   search_sep  = '.'
def getVersionFromPath(search_path,
                       search_pre,
                       search_pst = '',
                       search_join = '-',
                       search_sep = '.'):
    def to_float(v):
        try:
            return float(v)
        except:
            try:
                p = v.split('rc')
                return float(p[0]) + 0.001*float(p[1])
            except:
                p = v.split('p')
                return float(p[0]) + 0.001*float(p[1])

    def if_expr(e_if, p, e_else):
        if (p):
            return e_if
        else:
            return e_else

    vp = []

    sep  = if_expr((r'\%s'% search_sep), (search_sep  in ['.']), search_sep)
    jnc  = if_expr((r'\%s'% search_join),(search_join in ['.']), search_join)
    prc  = r'((p|rc|a|b)\d*?)?'
    pver = r'((\d+)(%s\d+)*)' % sep
    if (jnc):
        ptrn = r'%s%s(?P<v>%s)%s%s$' % (search_pre, jnc, pver, prc, search_pst)
    else:
        ptrn = r'%s(?P<v>%s)%s%s$' % (search_pre, pver, prc, search_pst)

    d = os.path.basename(search_path)
    m = re.search(ptrn, d)
    if m:
        vp  = [ to_float(x) for x in m.group('v').split(search_sep) ]

    if not vp:
        raise SCons.Errors.UserError('Could not detect version from path %s' % (search_path))
    return vp

"""
  Get first matching path
"""
def getPathOfFile(search_path, search_ptrn):
    ret = None

    ptrn = r'[\%s^]%s' % (os.sep, search_ptrn)
    for d in glob.glob( os.path.join(search_path, '*') ):
        fullPath = os.path.join(search_path, d)
        if os.path.isdir(fullPath):
            try:
                ret = getPathOfFile(fullPath, search_ptrn)
            except SCons.Errors.UserError:
                pass
        else:
            if re.search(ptrn, d):
                ret = fullPath
                break

    if (ret):
        return ret
    else:
        raise SCons.Errors.UserError('Could not find %s in directory %s' % (search_ptrn, search_path))

"""
  Get all matching path as a list
"""
def getAllPathOfFile(search_path, search_ptrn):
    ret = []

    ptrn = r'[\%s^]%s' % (os.sep, search_ptrn)
    for d in glob.glob( os.path.join(search_path, '*') ):
        fullPath = os.path.join(search_path, d)
        if os.path.isdir(fullPath):
            try:
                ret.extend(getAllPathOfFile(fullPath, search_ptrn))
            except SCons.Errors.UserError:
                pass
        else:
            if re.search(ptrn, d):
                ret.append(fullPath)
                break

    if (ret):
        return ret
    else:
        raise SCons.Errors.UserError('Could not find %s in directory %s' % (search_ptrn, search_path))

if __name__ == "__main__":
    env = {
        'MUC_TARGETPLATEFORM': 'win64',
        'MUC_TOOL_MAIN' : 'intel'
    }
    Context.env = env

    bld, mpi, dir, prj, grp, src, inc, dfn = ('',)*8
    inc = [r'H2D2/__mod__//..']
    ctx = Context(bld, mpi, dir, prj, grp, src, inc, dfn)
    gristIncPath(ctx)
