# -*- coding: UTF-8 -*-
import SCons
import os.path

ProgramFiles = ''
try:
    ProgramFiles = os.environ['ProgramFiles(x86)']
except:
    try:
        ProgramFiles = os.environ['ProgramFiles']
    except:
        pass

def get_intel_path(platform, version):
    if platform == 'unx32': return get_intel_path_unx32(version)
    if platform == 'unx64': return get_intel_path_unx64(version)
    if platform == 'win32': return get_intel_path_win32(version)
    if platform == 'win64': return get_intel_path_win64(version)

def get_intel_path_unx32(version):
    p = '/opt/intel/compilerpro-' + version + '/mkl/lib/ia32'
    if (not os.path.isdir(p)):
        v, s = version.rsplit('.', 1)
        p = os.path.join('/opt/intel/Compiler', v, s, 'mkl/lib/32')
    return os.path.normpath(p)

def get_intel_path_unx64(version):
    p = '/opt/intel/compilerpro-' + version + '/mkl/lib/intel64'
    if (not os.path.isdir(p)):
        v, s = version.rsplit('.', 1)
        p = os.path.join('/opt/intel/Compiler', v, s, 'mkl/lib/em64t')
    return os.path.normpath(p)

def get_intel_path_win32(version):
    vmaj = '.'.join( version.split('.')[0:2])
    vmin = '.'.join( version.split('.')[2:])
    if   (vmaj in ['11.0']):
        p = os.path.join(ProgramFiles, r'Intel/Compiler', vmaj, vmin, r'fortran/mkl/ia32/lib')
    elif (vmaj in ['11.1']):
        p = os.path.join(ProgramFiles, r'Intel/Compiler', vmaj, vmin, r'mkl/ia32/lib')
    elif (vmaj in ['12.0']):
        p = os.path.join(ProgramFiles, r'Intel/ComposerXE-2011', r'mkl/lib/ia32')
    elif (vmaj in ['14.0']):
        p = os.path.join(ProgramFiles, r'Intel/Composer XE 2013 SP1', r'mkl/lib/ia32')
    elif (vmaj in ['15.0']):
        p = os.path.join(ProgramFiles, r'Intel/Composer XE 2015', r'mkl/lib/ia32')
    elif (vmaj in ['16.0']):
        p = os.path.join(ProgramFiles, r'Intel/compilers_and_libraries_2016/windows', r'mkl/lib/ia32')
    elif (vmaj in ['17.0']):
        p = os.path.join(ProgramFiles, r'IntelSWTools/compilers_and_libraries_2017/windows', r'mkl/lib/ia32')
    elif (vmaj in ['18.0']):
        p = os.path.join(ProgramFiles, r'IntelSWTools/compilers_and_libraries_2018/windows', r'mkl/lib/ia32')
    elif (vmaj in ['19.0']):
        p = os.path.join(ProgramFiles, r'IntelSWTools/compilers_and_libraries_2019/windows', r'mkl/lib/ia32')
    elif (vmaj in ['19.1']):
        p = os.path.join(ProgramFiles, r'IntelSWTools/compilers_and_libraries_2020/windows', r'mkl/lib/ia32')
    elif (vmaj in ['19.2']):
        p = os.path.join(ProgramFiles, r'Intel/oneAPI', r'mkl/2021.2.0/lib/ia32')
    else:
        raise SCons.Errors.UserError('blas: Invalid intel version: %s' % version)
    return os.path.normpath(p)

def get_intel_path_win64(version):
    vmaj = '.'.join( version.split('.')[0:2])
    vmin = '.'.join( version.split('.')[2:])
    if   (vmaj in ['11.0']):
        p = os.path.join(ProgramFiles, r'Intel/Compiler', vmaj, vmin, r'fortran/mkl/em74t/lib')
    elif (vmaj in ['11.1']):
        p = os.path.join(ProgramFiles, r'Intel/Compiler', vmaj, vmin, r'mkl/em64t/lib')
    elif (vmaj in ['12.0']):
        p = os.path.join(ProgramFiles, r'Intel/ComposerXE-2011', r'mkl/lib/intel64')
    elif (vmaj in ['14.0']):
        p = os.path.join(ProgramFiles, r'Intel/Composer XE 2013 SP1', r'mkl/lib/intel64')
    elif (vmaj in ['15.0']):
        p = os.path.join(ProgramFiles, r'Intel/Composer XE 2015', r'mkl/lib/intel64')
    elif (vmaj in ['16.0']):
        p = os.path.join(ProgramFiles, r'Intel/compilers_and_libraries_2016/windows', r'mkl/lib/intel64')
    elif (vmaj in ['17.0']):
        p = os.path.join(ProgramFiles, r'IntelSWTools/compilers_and_libraries_2017/windows', r'mkl/lib/intel64')
    elif (vmaj in ['18.0']):
        p = os.path.join(ProgramFiles, r'IntelSWTools/compilers_and_libraries_2018/windows', r'mkl/lib/intel64')
    elif (vmaj in ['19.0']):
        p = os.path.join(ProgramFiles, r'IntelSWTools/compilers_and_libraries_2019/windows', r'mkl/lib/intel64')
    elif (vmaj in ['19.1']):
        p = os.path.join(ProgramFiles, r'IntelSWTools/compilers_and_libraries_2020/windows', r'mkl/lib/intel64')
    elif (vmaj in ['19.2']):
        p = os.path.join(ProgramFiles, r'Intel/oneAPI', r'mkl/2021.2.0/lib/intel64')
    else:
        raise SCons.Errors.UserError('blas: Invalid intel version: %s' % version)
    return os.path.normpath(p)
