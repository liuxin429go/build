# -*- coding: UTF-8 -*-
import fileinput
import logging
import os
import sets
import stat
import string
import sys
import tempfile

log = logging.getLogger("DP_GenInclude")

class DP_AlgoGenInclude:

    def __init__(self):
        self.includes = []

    def estFichierDependance(self, path):
        ext = os.path.splitext(path)[1]
        return not os.path.isdir(path) and (ext == ".d")

    def estFichierSource(self, path):
        if os.path.isdir(path): return 0
        ext = os.path.splitext(path)[1]
        if (ext == ".c"): return 1
        if (ext == ".cc"): return 1
        if (ext == ".cpp"): return 1
        return 0

    def lisUneDependanceExterne(self, file, includes):
        file = file.strip()
        ext = os.path.splitext(file)[1]
        if (ext == ".o"): return
        if (ext == ".c"): return
        if (ext == ".cpp"): return

        file = file[2:]
        file = file.strip()
        if (file[0] == "/"): file = file[1:]
        file = file.strip()
        if (file[0] == "\\"): file = file[1:]
        file = file.strip()

        if (file[0:6] == "boost-"): includes.add("l:/boost-1.33.0")
        if (file[0:7] == "Python-"): includes.add("l:/Python-2.4/include")
        if (file[0:4] == "vtk-"): includes.add("l:/vtk-4.2.2/include/vtk")
        if (file[0:4] == "otl-"): includes.add("l:/otl-4.0")
        if (file[0:5] == "geos-"): includes.add("l:/geos-2.0.1/source/headers")
        if (file[0:9] == "TerraLib-"):
            includes.add("l:/TerraLib-2.0/src/terralib/kernel")
            includes.add("l:/TerraLib-2.0/src/terralib/functions")
        if (file[0:10] == "PostgreSQL"): includes.add("l:/PostgreSQL/8.0/include")
        if (file[0:9] == "pgstream-"): includes.add("l:/pgstream-1.0")
        if (file[0:10] == "xmlParser-"): includes.add("l:/xmlParser-1.08/src")

    def lisUneDependanceInterne(self, file, includes):
        file = file.strip()
        ext = os.path.splitext(file)[1]
        if (ext == ".o"): return
        if (ext == ".c"): return
        if (ext == ".cpp"): return

        include = os.path.split(file)[0]
        if (os.path.split(include)[1] != "include"):
            include = os.path.split(include)[0]
        includes.add(include)

    def lisUneDependance(self, file, includes):
        if (file[0:2] == "l:" or file[0:2] == "L:"):
            self.lisUneDependanceExterne(file, includes)
        else:
            self.lisUneDependanceInterne(file, includes)

    def lisFichierDependance(self, fichier, includes):
        log.debug(" Reading dependencies")

        try:
            for line in fileinput.FileInput(fichier):
                line = line.strip()
                if (line[-1] == "\\"): line = line[0:-1]
                line = line.strip()
                if (line[-1] == ":"): line = line[0:-1]
                line = line.strip()
                if (len(line) < 1): continue

                for tok in line.split(" "):
                    self.lisUneDependance(tok, includes)
        except IOError:
            pass

    def compile(self, inFile, outFile):
        cmpl= "e:/dev/build/Visual_MultiCompiler.bat"
        ext = os.path.splitext(inFile)[1]
        inp = os.path.abspath(inFile).replace("\\", "/")
        out = os.path.abspath(outFile).replace("\\", "/")
        log.debug(" Compiling %s" % inp)
        command = "@call %s %s Depend win32 %s %s" % (cmpl, ext, inp, out)
        os.system(command)

    def execute(self, path):
        includesSet = sets.Set()
        for f in os.listdir(path):
            if self.estFichierSource(os.path.join(path, f)):
                outName = tempfile.mktemp()
                self.compile(os.path.join(path, f), outName)
                self.lisFichierDependance(outName, includesSet)
                try:
                    os.remove(outName)
                except OSError:
                    pass

        self.includes = []
        for i in includesSet:
            if (len(i) > 0):
                self.includes.append(i)
        self.includes.sort()

        log.debug(" Sorted include directories:")
        for i in self.includes:
            log.debug("    %s" % i)


def lisFichierInclude(fichier):

    includes = []
    try:
        for line in fileinput.FileInput(fichier):
            if (len(line) > 2 and line[0:2] == "-I"): line = line[2:]
            line = line.lstrip()
            if (len(line) > 0 and line[0] == "\""): line = line[1:]
            line = line.lstrip()
            if (len(line) > 0 and line[-1] == "\n"): line = line[0:-1]
            line = line.rstrip()
            if (len(line) > 0 and line[-1] == "\""): line = line[0:-1]
            line = line.rstrip()
            if (len(line) > 0):
                includes.append(line)
    except IOError:
        pass

    includes.sort()
    return includes

def creeBackup(fichier, backup):
    try:
        if (stat.S_ISREG(os.stat(fichier)[stat.ST_MODE])):
            try:
                if (stat.S_ISREG(os.stat(backup)[stat.ST_MODE])):
                    log.warning(" Overwriting existing file %s" % backup)
                    os.remove(backup)
            except OSError:
                pass
            log.info(" Creating backup file %s" % backup)
            os.rename(fichier, backup)
    except OSError:
        pass


def genereInclude(module):

    logging.basicConfig()
    log.setLevel(logging.INFO)

    sources= module + "/source"
    build  = module + "/build/cpp-all-win32-base.inc"
    buildBackup = build + ".$$$"
    log.info(" Working on module %s" % module)

    algo = DP_AlgoGenInclude()
    algo.execute(sources)

    includesOld = lisFichierInclude(build)

    if (includesOld != algo.includes):
        creeBackup(build, buildBackup)
        log.info(" Creating new include configuration file %s" % build)
        fileNew = file(build, 'w')
        for line in algo.includes:
            fileNew.write("-I\"%s\"\n" % line)
        fileNew.close()
    else:
        log.info(" Nothing to change")


if __name__ == '__main__':
#   if (len(sys.argv) != 2): Usage()

    module = sys.argv[1]
    sys.exit(genereInclude(module))

