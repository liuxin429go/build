# -*- coding: UTF-8 -*-
import SCons.Script

from . import Environment
from .python import EnvironmentPython
import MUC_platform
import Util

import os

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

MUC_CYTHON_TKN = 'dummy_target_for_cython_construction'

class EnvironmentCython(EnvironmentPython):
    def __init__(self, tool, version, opts, *args, **kwargs):
        if (MUC_platform.is_win()):
            muc_target_plateform = 'win64'
        elif (MUC_platform.is_unx()):
            muc_target_plateform = 'unx64'
        else:
            raise SCons.Errors.UserError('Invalid Cython platform: %s' % MUC_platform.platform())

        muc_build_plateform = MUC_platform.build_platform()
        tools =  ['python', 'cython']

        kwargs.setdefault('MUC_TARGETPLATEFORM', muc_target_plateform)
        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform = muc_build_plateform,
                           MUC_TARGETPLATEFORM = muc_target_plateform)
        MUCEnv.__init__(self, tool, version, *args, **kwargs)

    def setupEnv(self, ctx):
        MUCEnv.setupEnv(self, ctx)

    @staticmethod
    def getTokenFile(prj):
        return '%s_of___%s___.txt' % (MUC_CYTHON_TKN, prj)

    def getAllTokenFiles(self, prj):
        return [ EnvironmentCython.getTokenFile(prj), EnvironmentPython.getTokenFile(prj) ]

    """
    Rule to build an Cython file
    """
    def MUC_Cython(self, ctx):
        """
        Use a dummy NoOp builder to pack all the files as one target.
        If this is not done, dependencies are not detected globally.
        """
        def build_function(target, source, env):
            env.Execute("echo . > %s" % env.subst(target)[0])
            return None     # None for success

        NoOpBuilder = SCons.Builder.Builder(action = build_function)

        localEnv = self.Clone()
        localEnv.setupEnv(ctx)
        localEnv.Append(BUILDERS = {'NoOpCython' : NoOpBuilder})
        localEnv.Decider('make')        # token file has no content, just timestamp

        r0 = []
        for src in ctx.src:
            if os.path.splitext(src)[1] in localEnv["CYTHON_SRCSUFFIX"]:
                tgt = src
                tgt = os.path.splitext(tgt)[0] + localEnv["CYTHON_TGTSUFFIX"]   # modify extension
                tgt = os.path.join('${MUC_BUILDDIR}', tgt)                      # prepend build dir
                r0_ = localEnv.Cython(tgt, src)
                r0.append(r0_)
        tkn = EnvironmentCython.getTokenFile(ctx.prj)           # token file for dependency
        tgt = os.path.join('${MUC_BUILDDIR}', tkn)
        _ = localEnv.NoOpCython(tgt, r0)

        return r0
