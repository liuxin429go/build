#!/usr/bin/env python
# -*- coding: utf-8 -*-
from distutils.core      import setup
from distutils.extension import Extension
from Cython.Build        import cythonize
from Cython.Distutils    import build_ext
import numpy as np

import glob
import os
import platform
import sys

INRS_DEV = os.environ.get('INRS_DEV')
INCS_CPP = ['cpp-all-allp-base.inc', 'cpp-all-win64-base.inc', 'cpp-all-allp-release.inc', 'cpp-all-win64-release.inc']

"""
With a folder tree as
 .../py-H2D2/h2d2_krnl/src/*.pxd

Working directory is
    src
"""

def isWindows():
    return platform.system() == 'Windows'

def isUnix():
    return platform.system() == 'Linux'

def xtrCythonRootDir(pth=None):
    """
    From CWD, extract the root directory of the
    cython module
    """
    if not pth: pth = os.path.abspath(os.getcwd())  # .../py-H2D2/py-h2d2_krnl/src
    pth = os.path.dirname(pth)                      # .../py-H2D2/py-h2d2_krnl
    pth = os.path.dirname(pth)                      # .../py-H2D2
    return pth

def xtrCythonModuleDir(pth=None):
    """
    From CWD, extract the root directory of the
    cython module
    """
    if not pth: pth = os.path.abspath(os.getcwd())  # .../py-H2D2/py-h2d2_krnl/src
    pth = os.path.dirname(pth)                      # .../py-H2D2/py-h2d2_krnl
    return pth

def xtrH2D2RootDir(pth=None):
    """
    Extract the root directory of the H2D2 module
    """
    pth = os.path.join(INRS_DEV, 'H2D2')
    return pth

def xtrH2D2ModuleDir(pth=None):
    """
    Extract the root directory of the H2D2 module
    """
    mdl = xtrH2D2Module(pth)
    pth = os.path.join(INRS_DEV, 'H2D2', mdl)
    return pth

def xtrH2D2Module(pth=None):
    """
    Extract the H2D2 module name
    """
    if not pth: pth = os.path.abspath(os.getcwd())  # .../H2D2/py-h2d2_krnl/src
    pth = os.path.dirname(pth)                      # .../H2D2/py-h2d2_krnl
    mdl = os.path.basename(pth)                     # py-h2d2_krnl
    return mdl[3:]if mdl.startswith('py_') else mdl # h2d2_krnl

def xtrCythonModule(pth=None):
    """
    Extract the cython module name
    """
    if not pth: pth = os.path.abspath(os.getcwd())  # .../H2D2/py-h2d2_krnl/src
    pth = os.path.dirname(pth)                      # .../H2D2/py-h2d2_krnl
    mdl = os.path.basename(pth)                     # py-h2d2_krnl
    return mdl

def getH2D2DependenciesFromBuild():
    """
    Extract the library dependencies from build
    """
    root_dir = xtrH2D2ModuleDir()
    deps = set()
    for inc in INCS_CPP:
        finc = os.path.join(root_dir, 'build', inc)
        if os.path.isfile(finc):
            with open(finc, 'r') as ifs:
                for l in ifs:
                    l = l.split('#', 1)[0]
                    l = l.strip()
                    if l[:2] == '-I' and '$INRS_DEV' in l:
                        l = l[2:]
                        l = l.strip('"')
                        l = l.replace('$INRS_DEV', INRS_DEV)
                        l = xtrH2D2Module(l)
                        deps.add( l )
    return list(deps)

def getCythonDependenciesFromBuild():
    """
    Extract the library dependencies from build
    """
    root_dir = xtrCythonModuleDir()
    deps = set()
    for inc in INCS_CPP:
        finc = os.path.join(root_dir, 'build', inc)
        if os.path.isfile(finc):
            with open(finc, 'r') as ifs:
                for l in ifs:
                    l = l.split('#', 1)[0]
                    l = l.strip()
                    if l[:2] == '-I' and '$INRS_DEV' in l:
                        l = l[2:]
                        l = l.strip('"')
                        l = l.replace('$INRS_DEV', INRS_DEV)
                        l = xtrCythonModule(l)
                        deps.add( l )
    return list(deps)

def getCppIncludesFromBuild():
    """
    Get all include directories for H2D2
    """
    root_dir = xtrH2D2ModuleDir()
    incs = set()
    for inc in INCS_CPP:
        finc = os.path.join(root_dir, 'build', inc)
        if os.path.isfile(finc):
            with open(finc, 'r') as ifs:
                for l in ifs:
                    l = l.split('#', 1)[0]
                    l = l.strip()
                    if l[:2] == '-I' and '$INRS_DEV' in l:
                        l = l[2:]
                        l = l.strip('"')
                        l = l.replace('$INRS_DEV', INRS_DEV)
                        incs.add( os.path.normpath(l) )
    return list(incs)

def getPxdIncludesFromDependencies():
    """
    Get all include directories from dependencies for
    Cython/Python
    """
    dependencies = getCythonDependenciesFromBuild()
    root = xtrCythonRootDir()
    incs = set()
    for dep in dependencies:
        inc = os.path.join(root, dep, 'src')
        for fpath in glob.glob( os.path.join(inc, '*.pxd') ):
            incs.add(inc)
            break
    return list(incs)

def getLibDirFromCython(includes):
    libs = set()
    for inc in includes:
        for fpath in glob.glob( os.path.join(inc, '*.pxd') ):
            libs.add( xtrH2D2Module(inc) )
            break
    libs.add( xtrH2D2Module() )
    return list(libs)

def getH2D2LibrariesFromDependencies():
    dependencies = getH2D2DependenciesFromBuild()
    libs = []
    lib = xtrH2D2Module()
    libs.append( lib )
    for dep in dependencies:
        lib = dep
        libs.append( lib )
    return libs

def getCythonLibrariesFromDependencies():
    dependencies = getCythonDependenciesFromBuild()
    libs = []
    lib = xtrCythonModule()
    libs.append( lib )
    for dep in dependencies:
        lib = dep
        libs.append( lib )
    return libs

def cleanLibraries(libs, paths):
    def removeLibPrefix(lib):
        return lib[3:] if isUnix() and lib.startswith('lib') else lib
    def ptfLibs(lib):
        if isUnix():
            if lib.startswith('lib'):
                return ['%s.a' % lib, '%s.so' % lib]
            else:
                return ['lib%s.a' % lib, 'lib%s.so' % lib]
        else:
            return ['%s.lib' % lib]
    libsKeep = []
    for lib in libs:
        found = False
        for lpath in paths:
            for pLib in ptfLibs(lib):
                if os.path.isfile(os.path.join(lpath, pLib)):
                    found = True
                    break
        if found:
            lib = removeLibPrefix(lib)
            libsKeep.append(lib)
    return libsKeep

def do_build(pkg, h2d2_args):
    includes_cpp     = getCppIncludesFromBuild()
    includes_pxd     = getPxdIncludesFromDependencies()
    librairies_cpp   = getH2D2LibrariesFromDependencies()
    librairies_pxd   = getCythonLibrariesFromDependencies()
    #libdir_cpp       = [ os.path.join(INRS_DEV, 'Debug', 'x64', 'bin') ]
    #libdir_pxd       = [ os.path.join(INRS_DEV, 'Debug', 'x64', 'bin') ]
    if isWindows():
        grist_path = os.path.join(INRS_DEV, '{0}', '_run', '{0}', 'win64', 'intelmpi', 'itlX64-19.2', 'dynamic', '{1}', 'bin')
    else:
        grist_path = os.path.join(INRS_DEV, '{0}', '_run', '{0}', 'unx64', 'openmpi', 'itlX64', 'dynamic', '{1}', 'bin')
    libdir_cpp       = [ grist_path.format('H2D2',   h2d2_args['build']) ] + h2d2_args['libdir']
    libdir_pxd       = [ grist_path.format('pyH2D2', h2d2_args['build']) ]
    #print(includes_cpp)
    #print(includes_pxd)

    librairies_cpp   = cleanLibraries(librairies_cpp, libdir_cpp)
    librairies_pxd   = cleanLibraries(librairies_pxd, libdir_pxd)
    #print(librairies_cpp)
    #print(librairies_pxd)
    #raise

    # ---  Module definition
    pkg_name = pkg
    mdl_file = pkg
    ext_modules=[
        Extension('%s'      % mdl_file,
                  ['%s.pyx' % mdl_file],
                  include_dirs = includes_cpp + includes_pxd + [np.get_include()],
                  libraries    = librairies_pxd + librairies_cpp,
                  library_dirs = libdir_pxd + libdir_cpp,
                  define_macros= [("MODE_DYNAMIC", 1), ("CYTHON_TRACE", 1)],
                  # msvs - Produce separate debug pdb
                  extra_compile_args=['-Zi', '-Od'] if isWindows() else ['-std=c++17', '-m64'],
                  extra_link_args=['-debug'] if isWindows() else ['-debug', '-m64'],
               ),
    ]

    # ---  Setup
    setup(
      name = pkg_name,
      cmdclass = {'build_ext': build_ext},
      ext_modules = cythonize(ext_modules,
                              include_path  = includes_pxd,
                              annotate      = True,
                              compiler_directives={
                                'linetrace': True,
                                'profile'  : True,
                              },
                              language_level= 3,
                              gdb_debug     = True),
    )


if __name__ == "__main__":
    """
    File to cythonize is local to CWD
    CWD is expected in the format:
    .../h2d2_module/python_module/[src, source, sources]
    """
    # ---  Extract pxd file from argv
    pxd_file = sys.argv[1]
    del sys.argv[1]
    
    # ---  Extract arguments targeting h2d2
    h2d2_args = {
        'build': 'release',
        'libdir': [],
        }
    args_removed = []
    for ia, arg in enumerate(sys.argv):
        if arg.startswith('--h2d2-build='):
            h2d2_args['build'] = arg.split('=')[1]
            args_removed.append(ia)
        elif arg.startswith('--h2d2-libdir='):
            h2d2_args['libdir'].append( arg.split('=')[1] )
            args_removed.append(ia)
    for ia in reversed(args_removed):
        del sys.argv[ia]

    # ---  Get package from pxd file
    pkg = os.path.splitext(pxd_file)[0]

    # ---  XEQ
    sys.exit( do_build(pkg, h2d2_args) )
