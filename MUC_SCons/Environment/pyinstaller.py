# -*- coding: UTF-8 -*-
import SCons.Script

from . import Environment
from . import cython
import MUC_platform
import Util

import os

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

isIterable = Environment.isIterable

MUC_PYINST_DIR = '__scons_dir_for_pyinstaller'
MUC_PYINST_TKN = 'dummy_target_for_spec_construction'
MUC_CYTHON_TKN = cython.MUC_CYTHON_TKN

class PyinstallerWarning(SCons.Warnings.SConsWarning):
    pass

class EnvironmentPyInstaller(cython.EnvironmentCython):
    def __init__(self, tool, version, opts, *args, **kwargs):
        if (MUC_platform.is_win()):
            muc_target_plateform = 'win64'
        elif (MUC_platform.is_unx()):
            muc_target_plateform = 'unx64'
        else:
            raise SCons.Errors.UserError('Invalid PyInstaller platform: %s' % MUC_platform.platform())

        muc_build_plateform = MUC_platform.build_platform()
        tools =  ['cython', 'pyinstaller']

        kwargs.setdefault('MUC_TARGETPLATEFORM', muc_target_plateform)
        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform = muc_build_plateform,
                           MUC_TARGETPLATEFORM = muc_target_plateform)
        MUCEnv.__init__(self, tool, version, *args, **kwargs)

    def setupEnv(self, ctx):
        MUCEnv.setupEnv(self, ctx)

    """
    Rule to build an Cython file.
    It is intended that cythonized files will be incorporated
    by pyinstall.
    Return an empty list of targets to bypass install rules.
    """
    def MUC_Cython(self, ctx):
        _ = super().MUC_Cython(ctx)
        return []

    """
    Rule to build a PyInstaller file
    """
    def MUC_PyInstaller(self, ctx):
        """
        scons needs a file as target. Provide here a token file, which timestamp
        shall be updated in the PyInstaller command. The update decision will thus
        be taken on the timestamp and not on the MD5.
        Also, PyInstaller will populate the target directory, directory of the token file.
        As we don't know yet the names of the sub-directories created by the command,
        we can't return them. We return the token file as target.
        """
        localEnv = self.Clone()
        localEnv.setupEnv(ctx)
        localEnv.MergeFlags( {'PYINST_LIBPATH': '${MUC_BUILDDIR}'} )
        localEnv.Decider('make')                     # token file has no content, just timestamp

        dps = [ os.path.join('${MUC_BUILDDIR}', d) for d in ctx.dps ]               # prepend build dir

        r0 = []
        for pkg in ctx.pkg:
            tgt = pkg
            if os.path.splitext(tgt)[1] == self["PYINST_SRCSUFFIX"]:
                tkn = '%s_of___%s___.txt' % (MUC_PYINST_TKN, ctx.prj)
                tgt = os.path.join(ctx.prj, os.path.dirname(tgt), tkn)              # token file for dependency
                tgt = os.path.join('${MUC_BUILDDIR}', MUC_PYINST_DIR, tgt)          # prepend build dir and inst dir
                r0_ = localEnv.PyInstaller(tgt, pkg)
                # print('%s -> %s' % (pkg, tgt))
                if dps:
                    localEnv.MUC_MakeChildDepends(r0_, dps)
                r0.append(r0_)
        return r0

    """
    Rule to copy files to the run directory

    Specialize rule from parent:
        - take care of added directories by rule MUC_PyInstaller.
        - disable rule for cython targets
    """
    def MUC_Run_One(self, dst, src):
        # --- Traitements spéciaux
        res_ = []
        if isIterable(src) and len(src) == 1:
            src_ = str(self.subst(src[0]))
            if MUC_PYINST_TKN in src_:
                if os.path.isfile(src_):
                    # https://stackoverflow.com/questions/141291/how-to-list-only-top-level-directories-in-python
                    pth_ = os.path.dirname(src_)
                    for d in next(os.walk(pth_))[1]:
                        p = os.path.join(pth_, d)
                        res_ += self.Install(dst, p)
                else:
                    SCons.Warnings.enableWarningClass(PyinstallerWarning)
                    SCons.Warnings.warn(PyinstallerWarning,
                                        "Run pyinstaller twice to ensure correct MUC_Run results")
            elif MUC_CYTHON_TKN in src_:
                SCons.Warnings.enableWarningClass(PyinstallerWarning)
                SCons.Warnings.warn(PyinstallerWarning,
                                    "Skipping rule MUC_RUN for cython part of pyinstaller builder")
            else:
                res_ += super().MUC_Run_One(dst, src)
        else:
            res_ += super().MUC_Run_One(dst, src)
        return res_
