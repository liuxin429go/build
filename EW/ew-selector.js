function onStorageChange(evt)
{
    var fic = sessionStorage.getItem('ew_file');
    console.log('ew-selector.onStorageChange: new file: ' + fic);
    if (fic !== null)
    {
        document.getElementById('active_file').innerHTML = fic;
    }
}

window.addEventListener('storage', onStorageChange);
