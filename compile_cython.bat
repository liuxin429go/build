:: Setup
@echo off
setlocal EnableExtensions EnableDelayedExpansion

::-----------------------------------------------------------------------------
:: Main
::-----------------------------------------------------------------------------
:main

:: ---  Input param
set PTH_PXD=%1
if [%2]==[] ( set DIR_OUT="." ) else ( set DIR_OUT=%CD%/%2 )
if [%3]==[] ( set DIR_BLD="__cython-build" ) else ( set DIR_BLD=%CD%/%3 )

:: ---  Transform .pxd in .setup.py
set DIR_INP=%~dp1
set FIC_PXD=%~nx1
set FIC_STP=%~n1.setup.py

:: ---  Controls
if not exist %DIR_INP%%FIC_STP% goto err_argument

:: ---  Unix-ify the paths to get rid of \ problems
call set DIR_OUT=%%DIR_OUT:\=/%%
call set DIR_BLD=%%DIR_BLD:\=/%%

:: ---  Compile to pyd
pushd %DIR_INP%
:: cython 0.28.2
:: cython-c-in-temp pyrex-c-in-temp font que la cython ne prend pas
:: en compte le fichier .pxd et la compilation n'est pas optimisée.
set CYTHON_CMD=python %FIC_STP% build_ext --build-lib=%DIR_OUT% --build-temp=%DIR_BLD%
echo %CYTHON_CMD%
%CYTHON_CMD%
popd

goto :EOF

::-----------------------------------------------------------------------------
:err_argument
echo Usage:
echo    compile_cython.bat PTH_PXD [DIR_OUT] [DIR_BLD]
echo
echo Error: %1 needs a corresponding ".setup.py" file
echo
echo Error: Input param
echo Error:    PTH_PXD: %PTH_PXD%
echo Error:    DIR_OUT: %DIR_OUT%
echo Error:    DIR_BLD: %DIR_BLD%
echo Error: Variables
echo Error:    DIR_INP: %DIR_INP%
echo Error:    FIC_STP: %FIC_STP%
goto error

::-----------------------------------------------------------------------------
:error
exit /b 2


