# -*- coding: UTF-8 -*-
'''
cgi-bin

'''

import os
import string
import sys

import EW_Tree
import EW_HtmlTree

date = "*"

def erreur(msg, status='500 Internal Server Error'):
    print 'Status:', status
    print
    print msg
    sys.exit(0)

def parseEnvString():
    global date

    inp = os.getenv("QUERY_STRING")
    if (inp == None or inp == ""): inp = "?date=*"
    tokens = string.split(inp, "&")

    for tok in tokens:
        if (tok == ""): continue
        stok = string.split(tok, "=")
        var = string.lower(stok[0])
        val = stok[1]
        if (var == "date"):  date = val


def main():

    #---  Parse les info
    parseEnvString()

    #---  Assemble les info
    trees = []
    path = "c:/www/intranet/logs"
    for f in os.listdir(path):
        if (f[0:3] != "DT_"): continue
        if (os.path.splitext(f)[1] != ".xml"): continue
        tree = EW_Tree.createTreeFromXML(os.path.join(path, f))
        trees.append(tree)

    #---  Construit l'algo d'ecriture
    if (len(trees) > 0):
        algo = EW_HtmlTree.EWArbreDependanceDirecte()
        algo.xeq(trees[-1])


if __name__ == '__main__':
    main()
