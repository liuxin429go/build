# -*- coding: UTF-8 -*-
import os
import sys
import SCons.Tool

supPath = os.path.normpath(os.path.join(os.environ['INRS_BLD'], 'MUC'))
sys.path.insert(0, supPath)

supPath = os.path.normpath(os.path.join(os.environ['INRS_BLD'], 'MUC_SCons'))
sys.path.insert(0, supPath)
SCons.Tool.DefaultToolpath.insert(0, supPath)

supPath = os.path.normpath(os.path.join(os.environ['INRS_BLD'], 'MUC_SCons/SCons/Tool'))
sys.path.insert(0, supPath)
SCons.Tool.DefaultToolpath.insert(0, supPath)

from . import Context
import Environment
import ExternalLib
import SCons.Environment

rpdb2_imported = False
vcpdb_imported = False
try:
    import rpdb2
    rpdb2_imported = True
except:
    pass
try:
	import debugpy
	vcpdb_imported = True
except:
	pass

clean = Context.delContextTmpFiles
Context = Context.Context

# ---  Initialise les var globales du module
environments = None
builds = None
dirs = []

# ---  Récupère les param de la ligne de commande
cmdLineOpts = {}
cmdLineSupl = {}
for a in sys.argv[1:]:
    try:
        o, v = a.split('=')
        if (o in ['compilers', 'builds', 'mpilibs', 'fintegersize', 'doinstall', 'dostage', 'docpplink', 'forcestatic', 'winpdb', 'vscpdb', 'targetbasedir']):
            cmdLineOpts[o] = v
        elif (o in ['topdir']):
            dirs.append(v)
        elif (o[1] != '-'):
            cmdLineSupl[o] = v
            print("Supplementary option passed uninterpreted: %s", a)
    except:
        pass

# ---  Configure les options
opts = SCons.Variables.Variables(args = cmdLineOpts)
opts.Add( SCons.Variables.ListVariable('compilers', 'Available compilers [none]', 'none', Environment.compilers) )
opts.Add( SCons.Variables.ListVariable('builds', 'Builds [debug]', 'debug', ['debug', 'release', 'profil', '_none_']) )
opts.Add( SCons.Variables.ListVariable('mpilibs', 'mpi library [openmpi, msmpi, _none_]', 'openmpi', ExternalLib.mpi.libs) )
opts.Add( SCons.Variables.EnumVariable('fintegersize', 'FORTRAN integer size [4]', '4', ['4', '8']) )
opts.Add( SCons.Variables.BoolVariable('doinstall', 'Do install phase [true]', 1) )
opts.Add( SCons.Variables.BoolVariable('dostage', 'Do stage phase [true]', 1) )
opts.Add( SCons.Variables.BoolVariable('docpplink', 'Do a C++ link [false]', 0) )
opts.Add( SCons.Variables.BoolVariable('forcestatic', 'Force a static build [false]', 0) )
opts.Add( SCons.Variables.BoolVariable('winpdb', 'Debug run with Winpdb [false]', 0) )
opts.Add( SCons.Variables.BoolVariable('vscpdb', 'Debug run with Visual Studio Code [false]', 0) )
#opts.Add( SCons.Variables.PathVariable('targetbasedir', 'Directory path to the target base dir', '.') )
opts.Add( ('targetbasedir', 'Directory path to the target base dir', '.') )
opts.Add( SCons.Variables.PathVariable('topdir', 'Directory path to the top SConscript file', '.') )

# ---  Crée un environment temp
envTmp = SCons.Environment.Environment(options = opts)

# ---  Si demandé, démarre WinPDB
if (envTmp['winpdb']):
    if (not rpdb2_imported):
        raise 'rpdb2 must be installed to debug'
    else:
        rpdb2.start_embedded_debugger("inrs_iehss", fAllowUnencrypted = True)
if (envTmp['vscpdb']):
    if (not vcpdb_imported):
        raise 'debugpy must be installed to debug'
    else:
        # 5678 is the default attach port in the VS Code debug configurations
        attachPort = 5678
        print("Waiting for debugger to attach on port=%d; pid=%s" % (attachPort, os.getpid()))
        debugpy.listen(('localhost', attachPort))
        debugpy.wait_for_client()
        debugpy.breakpoint()

# ---  Crée les environnements et les builds (variable utiles)
environments = Environment.createEnvironments( envTmp['compilers'], opts , **cmdLineSupl)
builds  = envTmp['builds']
if (str(builds) == 'all'): builds = envTmp['builds'].allowedElems
mpilibs = envTmp['mpilibs']

# ---  Assigne l'attribut dynamic/static au Context
if str(envTmp['builds']) == '_none_':
    Context.lnk_g = '_none_'
else:
    Context.lnk_g = 'static' if envTmp['forcestatic'] else 'dynamic'


del(envTmp)
del(opts)
