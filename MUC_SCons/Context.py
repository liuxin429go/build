# -*- coding: UTF-8 -*-
import os.path
import glob
import atexit

tmpDir = None
if (not tmpDir):
    try:
        tmpDir = os.environ['TEMP']
    except:
        pass
if (not tmpDir):
    try:
        tmpDir = os.environ['TMP']
    except:
        pass
if (not tmpDir):
    try:
        tmpDir = os.environ['TMPDIR']
    except:
        pass
if (not tmpDir):
    raise 'Environment variable TEMP, TMP or TMPDIR must be defined'

class Context:
    fstk  = []          # File stack
    ctr   = 1           # File counter
    env   = None        # Environment : valeur mise dans SConstruct
    lnk_g = 'dynamic'   # Valeur par défaut: va être changée dans __init__.py

    def __init__(self, bld, mpi, dir, prj, grp, src=[], pkg=[], inc=[], dfn=[], lpt=[], lib=[], lxt=[], dps=[]):
        self.bld = bld  # debug/release
        self.mpi = mpi  # mpi lib
        self.dir = dir  # partial path of targets
        self.prj = prj  # project name
        self.tgt = prj  # target name (_xx added for static lib)
        self.grp = grp  #
        self.src = src
        self.pkg = pkg  # packages
        self.dps = dps  # explicit dependencies
        self.inc = inc
        self.dfn = dfn
        self.lpt = lpt
        self.lib = lib
        self.lxt = lxt
        self.sys = []
        self.lnk = Context.lnk_g

        self.__gen_temp_files()

    def __gen_temp_files(self):
        ptf = Context.env['MUC_TARGETPLATEFORM'] # + Context.env['MUC_TARGETPLATEFORMSUFFIX']
        mpi = self.mpi
        too = Context.env['MUC_TOOL_MAIN']
        env = '%s_%s_%s' % (ptf, mpi, too)
        grp = self.grp.replace('/', '@').replace('\\', '@').replace('.', '@')
        prj = self.prj.replace('/', '@').replace('\\', '@').replace('.', '@')
        self.tmpInc = os.path.join(tmpDir, '~scons_%s_%s_%s_%03d.inc' % (env, grp, prj, Context.ctr))
        self.tmpDfn = os.path.join(tmpDir, '~scons_%s_%s_%s_%03d.dfn' % (env, grp, prj, Context.ctr))

        Context.fstk.append(self.tmpInc)
        Context.fstk.append(self.tmpDfn)
        Context.ctr += 1

    def filterNone(self):
        def fltr(s):
            return '' if s in ['none', '_none_'] else s
        ctx = self
        ctx.bld = fltr(ctx.bld)
        ctx.mpi = fltr(ctx.mpi)
        ctx.lnk = fltr(ctx.lnk)
        ctx.lnk_g = fltr(ctx.lnk_g)
        return ctx

    def __repr__(self):
        r = [ "Context.Context:" ]
        for k, w in self.__dict__.items():
           r.append("    %s = %s" % (k, w))
        return "\n".join(r)

def delContextTmpFiles():
    def rm(f):
        try:
            os.close(f)
        except:
            pass
        try:
            os.remove(f)
        except:
            pass

    for f in Context.fstk:
        if os.path.isfile(f): rm(f)

atexit.register(delContextTmpFiles)
