function onBrowserOpen(evt)
{
    console.log('ew-files: onBrowserOpen');
    parent.showBrowserPopup();
}

//  Handler for the onChange event on the browse button
function onButton(evt)
{
    var selection = evt.target.files;
    var ficPath   = selection[0].webkitRelativePath;
    var ficDir    = ficPath.split('/')[0];

    console.log('ew-files.onButton: new directory: ' + ficPath);
    console.log('ew-files.onButton: new directory: ' + ficDir);
    // ---  Change ew_dir
    sessionStorage.setItem('ew_dir', ficDir);

    // ---  Change display
    document.getElementById('files.active_dir').innerHTML = sessionStorage.getItem('ew_dir');

    // Events are not fired for the same window
    // Direct call
//    onStorageChange(0);
}

//  Handler for the onStorageChange event
function onStorageChange(evt)
{
    console.log('ew-files.onStorageChange: event: ' + evt);
    var path = sessionStorage.getItem('ew_dir');
    var page = '/cgi-bin/EW_GenHtmlDir.py?top={0}&filter=EW_*.xml'.replace('{0}', path);
    console.log('ew-files.onStorageChange: loading ' + page);

    // ---  Update content
    document.getElementById('files.content').src = page;

    // ---  Select first item
    var subdoc = document.getElementById('files.content').contentDocument;
    var entry  = subdoc.getElementById('entry_000');
    if (entry !== null)
    {
        console.log('ew-files.onStorageChange: first entry: ' + entry);
        entry.onclick();
    }
}

// Events are not fired for the same window
window.addEventListener('storage', onStorageChange);
console.log('ew-files: on entry: sessionStorage.ew_dir:  ' + sessionStorage.getItem('ew_dir'));
console.log('ew-files: on entry: sessionStorage.ew_file: ' + sessionStorage.getItem('ew_file'));
