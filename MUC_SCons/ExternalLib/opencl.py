# -*- coding: UTF-8 -*-
import ExternalLib

class opencl(ExternalLib.ExternalLib):
    def __unx(self, ctx):
        __lpt = [ ]
        __lib = ['OpenCL']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __unx32(self, ctx):
        self.__unx(ctx)

    def __unx64(self, ctx):
        self.__unx(ctx)

    def __unx64i8(self, ctx):
        self.__unx(ctx)

    def __win32(self, ctx):
        pass

    def __win64(self, ctx):
        #__lpt = [r'C:\Intel\OpenCL\sdk\lib\x64']
        ##__lpt = [r'C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v7.5\lib\x64__']
        #__lib = ['OpenCL']
        #self.assign(__lpt, ctx.lpt, __lib, ctx.lib)
        pass

    def __win64i8(self, ctx):
        pass

    def configure(self, ctx, tool, version, plateform):
        fnm = '_opencl__%s' % (plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
