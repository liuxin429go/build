# -*- coding: UTF-8 -*-
import SCons.Script

from . import Environment
import MUC_platform
import os
import sys

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

class EnvironmentGcc(MUCEnv):
    def __init__(self, tool, version, opts, *args, **kwargs):
        if (MUC_platform.is_win()):
            tools =  ['mingw', 'gfortran']
        elif (MUC_platform.is_unx()):
            tools =  ['gcc', 'g++', 'gfortran', 'gnulink', 'ar', 'gas']
        else:
            raise SCons.Errors.UserError('Invalid GCC platform: %s' % MUC_platform.build_platform())

        try:
            muc_target_plateform = kwargs['MUC_TARGETPLATEFORM']
            del kwargs['MUC_TARGETPLATEFORM']
        except:
            muc_target_plateform = MUC_platform.build_platform()
        muc_build_plateform = MUC_platform.build_platform()

        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform           = muc_build_plateform,
                           TARGET_ARCH         = muc_target_plateform,
                           MUC_TARGETPLATEFORM = muc_target_plateform,
                           WINDOWS_INSERT_DEF  = 'Do_insert_def')
        MUCEnv.__init__(self, tool, version, *args, **kwargs)

        if (self['docpplink']):
            self['LINK'] = ['g++']
        else:
            self['LINK'] = ['gfortran']
        if self['VERSION']:
            self['LINK'] = self['LINK'] + '-%s' % self['VERSION']
        if   muc_target_plateform in ['unx32', 'win32']:
            self['LINK'] = self.get('LINK') +  ['-m32']
        elif muc_target_plateform in ['unx64', 'win64']:
            self['LINK'] = [ self.get('LINK') ] +  ['-m64']
        else:
            raise SCons.Errors.UserError('Invalid GCC platform: %s' % muc_target_plateform)

    def setupEnv(self, ctx):
        if   (MUC_platform.is_win()):
            self.__setupEnv_win(ctx)
        elif (MUC_platform.is_unx()):
            self.__setupEnv_unx(ctx)
        else:
            raise SCons.Errors.UserError('Invalid GCC platform: %s' % MUC_platform.build_platform())

        if self['MUC_VERSION_MAIN'] and self['MUC_VERSION_MAIN'] < '4.0':
            self.__setupEnv_4moins(ctx)
        else:
            self.__setupEnv_4plus(ctx)

        if   (ctx.bld == 'debug'):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-g']
        elif (ctx.bld == 'profil'):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-pg']

        MUCEnv.setupEnv(self, ctx)

    def __setupEnv_4moins(self, ctx):
        if (not self['docpplink']):
           syslib = ['frtbegin', 'g2c']
           for l in syslib:
               try:
                   ctx.sys.index(l)
               except:
                   ctx.sys.append(l)

    def __setupEnv_4plus(self, ctx):
        if (ctx.bld == 'release'):
            self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-fopenmp']

        if (not self['docpplink']):
            syslib = ['gfortran']
            if self['MUC_VERSION_MAIN'] and self['MUC_VERSION_MAIN'] < '6.0': 
               syslib = ['gfortranbegin'] + syslib
            for l in syslib:
                try:
                    ctx.sys.index(l)
                except:
                    ctx.sys.append(l)

    def __setupEnv_unx(self, ctx):
        # if (ctx.lnk == 'dynamic'):
        #     self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-fPIC']

        if (not self['docpplink']):
            syslib = ['stdc++']
            for l in syslib:
                try:
                    ctx.sys.index(l)
                except:
                    ctx.sys.append(l)

    def __mingw_fix_exports(self, ctx):
        # - Avec un mélange C++,FTN, les symboles ne sont pas exportés correctement
        # - En les exportant tous, on a de multiples définitions des symboles des
        # - lib systeme
        if (not self.WhereIs('mingw32-gcc')): return
        if (not self['MUC_VERSION_MAIN'] in ['4.2', '4.3', '4.4', '4.5', '4.6', '']): return

        libs = [ ]
        libs.extend( [ SCons.Util.adjustixes(l, 'lib', '.a') for l in ctx.lib] )
        libs.extend( [ SCons.Util.adjustixes(l, '', '.lib')  for l in ctx.lib] )
        libs.extend( [ SCons.Util.adjustixes(l, '', '.dll')  for l in ctx.lib] )
        libs = ':'.join( libs )

        linkFlags = self['SHLINKFLAGS']
        linkFlags = linkFlags + ['-Wl,--export-all-symbols']
        linkFlags = linkFlags + ['-Wl,--allow-multiple-definition']
        linkFlags = linkFlags + ['-Wl,--exclude-libs,%s' % libs]
        self['SHLINKFLAGS'] = linkFlags

    def __setupEnv_win(self, ctx):
        """
        Lorsque la ligne de commande est trop longue, mingw-dos la coupe.
        La stratégie normale est d'utilise TEMPFILE, mais gcc n'accèpte
        pas les \ dans les noms de fichier dans un fichier de commande.
        Utiliser .posix tronque la commmande et ne fonctionne pas pour les
        LIBPATH.
        La seule combinaison fonctionnelle est de redéfinir spawn qui utilise
        subprocess et qui n'a plus la même limitation de taille de ligne.
        De plus spawn permet de récupérer stdout et stderr.
        """

        # --- Flags
        # self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-mno-cygwin']
        self['LINKFLAGS'] = self.get('LINKFLAGS') + ['-mconsole']
        self.__mingw_fix_exports(ctx)

        # --- sys libs
        if (not self['docpplink']):
            syslib = ['stdc++']
            for l in syslib:
                try:
                    ctx.sys.index(l)
                except:
                    ctx.sys.append(l)

