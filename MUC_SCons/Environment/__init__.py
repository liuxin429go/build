# -*- coding: UTF-8 -*-
import SCons.Script

from . import cmc
from . import cython
from . import ftncheck
from . import gcc
from . import gccX86
from . import gccX64
from . import itlX64
from . import msvc
from . import open64
from . import pyinstaller
from . import sun

"""
Known compilers
"""
compilers = [
    'cmc', 'cmcitl', 'cmcpgi',
    'cython',
    'ftnchek-3.3',
    'gcc', 'gcc-4.4', 'gcc-4.5', 'gcc-4.6', 'gcc-9', 'gcc-9.1', 'gcc-9.2', 'gcc-9.3',
    'gccX86', 'gccX86-4.4', 'gccX86-4.5', 'gccX86-4.6', 'gccX86-9', 'gccX86-9.1', 'gccX86-9.2', 'gccX86-9.3',
    'gccX64', 'gccX64-4.4', 'gccX64-4.5', 'gccX64-4.6', 'gccX64-9', 'gccX64-9.1', 'gccX64-9.2', 'gccX64-9.3',
    'itlX64', 'itlX64-18.0', 'itlX64-19.0', 'itlX64-19.1', 'itlX64-19.2',
    'msvc-7.0', 'msvc-7.1', 'msvc-9.0',
    'open64', 'open64-4.2.4', 'open64-4.5', 'open64-4.5.2', 'open64-4.5.2.1', 'open64-5.0',
    'python',
    'pyinstaller',
    'sun', 'sun-12.0', 'sun-12.1', 'sun-12.2', 'sun-12.3', 'sun-12.4', 'sun-12.5', 'sun-12.6',
    ]

"""
Environment constructor: helper function
"""
def createEnvironment(tool, version, opts, *args, **kwargs):
    constructors = \
    {
        'cmc'     : cmc.EnvironmentCMC,
        'cmcitl'  : cmc.EnvironmentCMC,
        'cmcpgi'  : cmc.EnvironmentCMC,
        'cython'  : cython.EnvironmentCython,
        'ftnchek' : ftncheck.EnvironmentFtnchek,
        'gcc'     : gcc.EnvironmentGcc,
        'gccX86'  : gccX86.EnvironmentGccX86,
        'gccX64'  : gccX64.EnvironmentGccX64,
        'itlX64'  : itlX64.EnvironmentItlX64,
        'msvc'    : msvc.EnvironmentMsvc,
        'open64'  : open64.EnvironmentOpen64,
        'python'  : python.EnvironmentPython,
        'pyinstaller' : pyinstaller.EnvironmentPyInstaller,
        'sun'     : sun.EnvironmentSun,
    }
    return constructors[tool](tool, version, opts, *args, **kwargs)

"""
Environment constructor: helper function
"""
def createEnvironments(tools, opts, *args, **kwargs):
    envs = []
    for t in tools:
        tool = t
        version = ''
        try:
           tool, version = t.split('-')
        except:
           pass
        envs.append(createEnvironment(tool, version, opts, *args, **kwargs))
    return envs
