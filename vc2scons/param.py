#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import logging
import os

from config import Config
from util   import path2unix

LOGGER = logging.getLogger("vc2scons.param")

class Parametres:
    def __init__(self):
        self.ficPrjVisual    = ""
        self.ficVersions     = ""
        self.ficSetupCfg     = ""
        self.outputDir       = ""
        self.outputGroupName = ""
        self.outputFileFormat= "{sys_base}{sys_ext}"
        self.bldSystem       = ""
        self.bldSystems      = ["scons"]
        self.projectRoot     = "."
        self.genProjectsAll  = False
        self.genProjectsSameDir = True
        self.noExeTarget     = False
        self.noRunTarget     = False
        self.noStageTarget   = False
        self.incExcludedFromBuild = False
        self.append_to_existing_outfile = False
        self.addSystemDefine = False
        self.valide          = False
        self.versions        = {}

    def __lisVersions(self):
        """
        Lis les versions des modules
        """
        with open(self.ficVersions, 'rt', encoding='utf-8') as ifs:
            for ligne in ifs.readlines():
                ligne = ligne.strip()
                if ligne:
                    lhs, rhs = ligne.split("=")
                    self.versions[lhs] = rhs

    def __parseParamEntree(self, args):
        """
        Parse les paramètres de la ligne de commande
        """
        for arg in args:
            if '=' in arg:
                var, val = arg.split('=')
            else:
                var, val = arg, ""
            var = var.strip().lower()
            val = val.strip()

            if var == "-input_project_file":
                self.ficPrjVisual = val
            elif var == "-input_versions_file":
                self.ficVersions = val
            elif var == "-input_setup_config_file":
                self.ficSetupCfg = val
            elif var == "-output_systems":
                if val[-1] == ";": val = val[:-1]
                self.bldSystems = val.lower().split(";")
                n = 0
                for a in self.bldSystems:
                    if a in Config.BuildSys: n += 1
                if n != len(self.bldSystems):
                    raise RuntimeError('Invalid output_systems parameter: "%s"' % val)
            elif var == "-output_dir":
                self.outputDir = val
            elif var == "-output_group_name":
                self.outputGroupName = val
            elif var == "-output_file_name_format":
                if val[0] in ('"', "'"): val = eval(val, None, None)
                self.outputFileFormat = val
            elif var == "-project_root":
                val = os.path.normpath(val)
                val = path2unix(val)
                drv, _ = os.path.splitdrive(val)
                if drv: val = drv + val[len(drv):]
                self.projectRoot = val
            elif var == "-no_exe_target":
                self.noExeTarget = True
            elif var == "-no_run_target":
                self.noRunTarget = True
            elif var == "-no_stage_target":
                self.noStageTarget = True
            elif var == "-generate_all_projects_from_solution":
                self.genProjectsAll = True
            elif var == "-include_files_excluded_from_build":
                self.incExcludedFromBuild = True
            elif var == "-append_to_existing_outfile":
                self.append_to_existing_outfile = True
            elif var == "-add_system_define":
                self.addSystemDefine = True
            elif var == "-log_level":
                if val == "info":
                    LOGGER.setLevel(LOGGER.INFO)
                elif val == "warning":
                    LOGGER.setLevel(LOGGER.WARNING)
                elif val == "error":
                    LOGGER.setLevel(LOGGER.ERROR)
                elif val == "debug":
                    LOGGER.setLevel(LOGGER.DEBUG)
                else:
                    raise RuntimeError("Invalid log level")

        if len(self.ficPrjVisual) > 0:
            self.valide = True

        if not self.valide:
            print("vcproj2bjam  - Traduis un projet Visual C++")
            print("    appel: vcproj2bjam  parameters")
            print("      -input_project_file=FileName")
            print("      [-input_versions_file=FileName]")
            print("      [-input_setup_config_file=FileName]")
            print("      [-output_systems=BuildSystems](setup, scons)")
            print("      [-output_dir=DirName]")
            print("      [-output_group_name=Name]")
            print("      [-output_file_name_format=Format] (May use {sys_base},{sys_ext},{module},{target})")
            print("      [-project_root=Path]")
            print("      [-no_exe_target] (False)")
            print("      [-no_run_target] (False)")
            print("      [-no_stage_target] (False)")
            print("      [-generate_all_projects_from_solution] (False)")
            print("      [-include_files_excluded_from_build] (False)")
            print("      [-append_to_existing_outfile] (False)")
            print("      [-add_system_define] (False)")
            print("      [-log_level=level] (error, warning, info, debug)")

    def parseParamEntree(self, args):
        self.__parseParamEntree(args)
        if not self.valide: return

        # ---  Lis la configuration des versions
        if self.ficVersions:
            if not os.path.isfile(self.ficVersions):
                raise RuntimeError("Invalid version file: %s" % self.ficVersions)
            self.__lisVersions()


if __name__ == "__main__":
    #addLoggingLevel('DUMP',  logging.DEBUG + 5)
    #addLoggingLevel('TRACE', logging.DEBUG - 5)

    strHndlr = logging.StreamHandler()
    FORMAT = "[%(levelname)-8.8s] %(message)s"
    strHndlr.setFormatter( logging.Formatter(FORMAT) )
    strHndlr.setLevel(logging.DEBUG)

    LOGGER = logging.getLogger("vc2scons")
    LOGGER.addHandler(strHndlr)
    LOGGER.setLevel(logging.DEBUG)

    args = [
        r"-input_project_file=E:\h2d2-dev\H2D2\algo_remesh\prjVisual\algo_remesh.sln",
        r"-input_setup_config_file=H2D2\algo_remesh\build\setup.cfg",
        r"-output_group_name=H2D2",
        r"-output_dir=E:\h2d2-dev\H2D2\algo_remesh",
        r"-output_systems=scons;setup;",
        r"-project_root=E:\h2d2SurGitHub",
        r"-no_stage_target",
        ]

    prm = Parametres()
    prm.parseParamEntree(args)
