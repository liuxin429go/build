# -*- coding: UTF-8 -*-

from EW_Util     import dbg
from EW_Util     import EWHtmlFile
from EW_Compiler import ctrCompiler

import fileinput
import os
import time


class EWHtml_AlgoBase:
    def __init__(self):
        self.cpl = None

    def configure(self, path):
        cpl = os.path.dirname(path)
        if (os.path.basename(cpl) == "user-interface-gui"):
            cpl = os.path.dirname(cpl)
        while (os.path.basename(cpl) not in ['dynamic', 'static']):
            cpl = os.path.dirname(cpl)
        cpl = os.path.dirname(cpl)
        cpl = os.path.basename(cpl)
        typ = os.path.basename(path)        # algmrs.for.err
        typ = os.path.splitext(typ)[0]      # algmrs.for
        typ = os.path.splitext(typ)[1][1:]  # for
        self.cpl = ctrCompiler(cpl, typ)


class EWHtml_AlgoSummaryLog(EWHtml_AlgoBase):
    def __init__(self):
        pass

    def write(self, htmlFile, line, inError):
        if (inError == 1):
            htmlFile.write("<font color = " + htmlFile.getColor(0, 1, 0) + ">"  + line + "</font>")
        else:
            htmlFile.write("<font color = " + htmlFile.getColor(0, 0, 1) + ">"  + line + "</font>")

    def __call__(self, path, htmlFile):
        self.configure(path)

        htmlFile.write("<pre>")
        inError = 0
        dbg.printd(path)
        for line in fileinput.FileInput(path):
            line = line.rstrip()
            dbg.printd(line)
            if (len(line) < 10):
                if (inError): self.write(htmlFile, msg, inError)
                inError = 0
            elif (line[0:5] == "     "):
                if (inError): self.write(htmlFile, msg, inError)
                inError = 0
            elif (line[0:3] == "In "):
                if (inError): self.write(htmlFile, msg, inError)
                inError = 0
            elif (line[0] == " "):
                if (inError):
                    msg = msg + " " + line.strip()
                else:
                    inError = 0
            elif (line.find(self.cpl.spdexpWarning) > 0) and (self.cpl.regexpWarning.match(line)):
                if (inError): self.write(htmlFile, msg, inError)
                mObj = self.cpl.regexpWarning.match(line)
                srcFile = os.path.basename(mObj.group('file'))
                srcLine = mObj.group('line')
                srcLine = srcLine.replace(':', '')
                srcLine = srcLine.replace('(', '')
                srcLine = srcLine.replace(')', '')
                srcLine = srcLine.strip()
                if (os.path.splitext(srcFile)[1][1] == 'f'):   # Fortran
                    (srcBase, srcExt) = os.path.splitext(srcFile)
                    htmFile = srcBase + '_' + srcExt[1:] + '.htm'
                    if (len(srcLine) > 0):
                        href = "/doc_technique/H2D2/webftn/%s#L%s" % (htmFile, srcLine)
                    else:
                        href = "/doc_technique/H2D2/webftn/%s" % (htmFile)
                else:
                    if (len(srcLine) > 0):
                        href = "/cgi-bin/frm_code.exe?/webpp-segri/%s@L%s" % (srcFile, srcLine)
                    else:
                        href = "/cgi-bin/frm_code.exe?/webpp-segri/%s" % (srcFile)
                msg = line.replace (srcFile, "<a href=\"%s\">%s</a>" % (href, srcFile))
                inError = 2
            elif (line.find(self.cpl.spdexpError) > 0) and (self.cpl.regexpError.match(line)):
                if (inError): self.write(htmlFile, msg, inError)
                mObj = self.cpl.regexpError.match(line)
                srcFile = os.path.basename(mObj.group('file'))
                srcLine = mObj.group('line')
                srcLine = srcLine.replace(':', '')
                srcLine = srcLine.replace('(', '')
                srcLine = srcLine.replace(')', '')
                srcLine = srcLine.strip()
                if (os.path.splitext(srcFile)[1][1] == 'f'):   # Fortran
                    (srcBase, srcExt) = os.path.splitext(srcFile)
                    htmFile = srcBase + '_' + srcExt[1:] + '.htm'
                    if (len(srcLine) > 0):
                        href = "/webftn/%s#L%s" % (htmFile, srcLine)
                    else:
                        href = "/webftn/%s" % (htmFile)
                else:
                    if (len(srcLine) > 0):
                        href = "/cgi-bin/frm_code.exe?/webpp-segri/%s@L%s" % (srcFile, srcLine)
                    else:
                        href = "/cgi-bin/frm_code.exe?/webpp-segri/%s" % (srcFile)
                msg = line.replace (srcFile, "<a href=\"%s\">%s</a>" % (href, srcFile))
                inError = 1
            else:
                if (inError): self.write(htmlFile, msg, inError)
                inError = 0
        if (inError): self.write(htmlFile, msg, inError)
        htmlFile.write("</pre>")


class EWHtml_AlgoCompilerLog(EWHtml_AlgoBase):
    def __init__(self):
        pass

    def writeError(self, htmlFile, line):
#        href = "/cgi-bin/EW_GenHtmlErWr.cgi?mdl=%s&vrs=%s" % (self.mdl_actu, item.nom)
        htmlFile.write("<pre><font color = " + htmlFile.getColor(0, 1, 0) + ">" + line + "</font>")

    def writeWarning(self, htmlFile, line):
#        href = "/cgi-bin/EW_GenHtmlErWr.cgi?mdl=%s&vrs=%s" % (self.mdl_actu, item.nom)
        htmlFile.write("<font color = " + htmlFile.getColor(0, 0, 1) + ">"  + line + "</font>")


    def __call__(self, path, htmlFile):
        self.configure(path)

        htmlFile.write("<pre>")
        for line in fileinput.FileInput(path):
            line = line.rstrip()
            if (len(line) < 10):
                htmlFile.write(line)
            elif (line[0] == " "):
                htmlFile.write(line)
            elif (line[0:3] == "In "):
                htmlFile.write(line)
            elif (line.find(self.cpl.spdexpWarning) > 0) and (self.cpl.regexpWarning.match(line)):
                mObj = self.cpl.regexpWarning.match(line)
                srcFile = os.path.basename(mObj.group('file'))
                srcLine = mObj.group('line')
                srcLine = srcLine.replace(':', '')
                srcLine = srcLine.replace('(', '')
                srcLine = srcLine.replace(')', '')
                srcLine = srcLine.strip()
                if (os.path.splitext(srcFile)[1][1] == 'f'):   # Fortran
                    (srcBase, srcExt) = os.path.splitext(srcFile)
                    htmFile = srcBase + '_' + srcExt[1:] + '.htm'
                    if (len(srcLine) > 0):
                        href = "/webftn/%s#L%s" % (srcFile, srcLine)
                    else:
                        href = "/webftn/%s" % (srcFile)
                else:
                    if (len(srcLine) > 0):
                        href = "/cgi-bin/frm_code.exe?/webpp-segri/%s@L%s" % (srcFile, srcLine)
                    else:
                        href = "/cgi-bin/frm_code.exe?/webpp-segri/%s" % (srcFile)
                line = line.replace (srcFile, "<a href=\"%s\">%s</a>" % (href, srcFile))
                self.writeWarning(htmlFile, line)
            elif (line.find(self.cpl.spdexpError) > 0) and (self.cpl.regexpError.match(line)):
                mObj = self.cpl.regexpError.match(line)
                srcFile = os.path.basename(mObj.group('file'))
                srcLine = mObj.group('line')
                srcLine = srcLine.replace(':', '')
                srcLine = srcLine.replace('(', '')
                srcLine = srcLine.replace(')', '')
                srcLine = srcLine.strip()
                if (os.path.splitext(srcFile)[1][1] == 'f'):   # Fortran
                    (srcBase, srcExt) = os.path.splitext(srcFile)
                    htmFile = srcBase + '_' + srcExt[1:] + '.htm'
                    if (len(srcLine) > 0):
                        href = "/webftn/%s#L%s" % (srcFile, srcLine)
                    else:
                        href = "/webftn/%s" % (srcFile)
                else:
                    if (len(srcLine) > 0):
                        href = "/cgi-bin/frm_code.exe?/webpp-segri/%s@L%s" % (srcFile, srcLine)
                    else:
                        href = "/cgi-bin/frm_code.exe?/webpp-segri/%s" % (srcFile)
                line = line.replace (srcFile, "<a href=\"%s\">%s</a>" % (href, srcFile))
                self.writeError(htmlFile, line)
            else:
                htmlFile.write(line)
        htmlFile.write("</pre>")


class EWHtml_SummaryLog:
    def __init__(self):
        pass

    def xeq(self, file):
        # dbg.printd("Compiler log: %s" % file)

        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Compiler log summary: %s" % file)

        href = "EW_GenHtmlCmpl.cgi?full=yes&file=%s" % file
        self.ficHTML.write("    Fichier: %s" % file)
        if (os.path.exists(file)):
            self.ficHTML.write("<br>Date: %s" % time.ctime(os.path.getmtime(file)))
            self.ficHTML.write("<br><a href=\"%s\">Full Log</a><br>" % href)
        else:
            self.ficHTML.write("<br><br><b>Fichier inexistant</b>")

        algo = EWHtml_AlgoSummaryLog()
        algo(file, self.ficHTML)

        self.ficHTML.writeFooter()


class EWHtml_CompilerLog:
    def __init__(self):
        pass

    def xeq(self, file):
        # dbg.printd("Compiler log: %s" % file)

        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Compiler log: %s" % file)

        self.ficHTML.write("    Fichier: %s" % file)
        if (os.path.exists(file)):
            self.ficHTML.write("<br>Date: %s" % time.ctime(os.path.getmtime(file)))
            self.ficHTML.write("<br>")
        else:
            self.ficHTML.write("<br><br><b>Fichier inexistant</b>")

        algo = EWHtml_AlgoCompilerLog()
        algo(file, self.ficHTML)

        self.ficHTML.writeFooter()


if __name__ == '__main__':
    dbg.on = True
    path = r"/home/secretyv/bld-1710/_bin/H2D2/libh2d2/unx64/openmpi-1.10/gcc-xx/dynamic/debug/source/c_mm.cpp.err"
    #path = r"E:\dev\bin\dev\boost_sup\derniere\msvc-7.1\debug\interpreter.err-cpl"
    algo = EWHtml_SummaryLog()
    algo.xeq(path)
