# -*- coding: UTF-8 -*-
"""
::------------------------------------------------------
:: I.N.R.S. - Processus de compilation multi-compilateur
::------------------------------------------------------
"""

import os
import platform
import sys

def is_win():
   return (platform.system() == 'Windows')

def is_unx():
   return (platform.system() == 'Linux')

def is_win32():
   return (platform.system() == 'Windows' and platform.architecture()[0] == '32bit')

def is_win64():
   return (platform.system() == 'Windows' and 'PROGRAMFILES(X86)' in os.environ)

def is_unx32():
   return (platform.system() == 'Linux' and platform.architecture()[0] == '32bit')

def is_unx64():
   return (platform.system() == 'Linux' and platform.architecture()[0] == '64bit')

def build_system():
   if (platform.system() == 'Windows'): return 'win'
   if (platform.system() == 'Linux'): return 'unx'
   raise 'Invalid system: %s' % platform.system()

def build_platform():
   if (is_win64()): return 'win64'
   if (is_win32()): return 'win32'
   if (is_unx64()): return 'unx64'
   if (is_unx32()): return 'unx32'
   raise 'Invalid platform: %s - %s' % (platform.system(), platform.architecture()[0])

def build_node():
   return platform.node().split('.')[0]

