# -*- coding: UTF-8 -*-

'''
    La classe EWDebug et la variable globale dbg associée, permet une
    écriture debug sous la forme:
        dbg.printd(msg, condition)
    Le mode debug est contrôlé par l'attribut EWDebug.on
    Le niveau d'intentation est contrôlé par l'attribut EWDebug.level
'''
class EWDebug:
    blank = "                                              "
    def __init__(self):
        self.level = 0
        self.on = 0         # False

    def printd(self, msg, cond = 1):
        if not self.on: return
        if not cond: return
        if (self.level > 0):
            print EWDebug.blank[0: self.level*3], msg
        else:
            print msg

dbg = EWDebug()
