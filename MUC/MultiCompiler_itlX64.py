# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#-----------------------------------------------------------------------------------------------------

import MultiCompiler_intel

itlx64_110061 = MultiCompiler_intel.intel_110061
itlx64_110066 = MultiCompiler_intel.intel_110066
itlx64_110069 = MultiCompiler_intel.intel_110069
itlx64_110072 = MultiCompiler_intel.intel_110072
itlx64_110074 = MultiCompiler_intel.intel_110074

itlx64_111046 = MultiCompiler_intel.intel_111046
itlx64_11106  = MultiCompiler_intel.intel_11106
itlx64_111060 = MultiCompiler_intel.intel_111060
itlx64_111065 = MultiCompiler_intel.intel_111065
itlx64_111069 = MultiCompiler_intel.intel_111069
itlx64_111072 = MultiCompiler_intel.intel_111072

itlx64_1200   = MultiCompiler_intel.intel_1200
itlx64_1200084= MultiCompiler_intel.intel_1200084
itlx64_1203174= MultiCompiler_intel.intel_1203174
itlx64_1203175= MultiCompiler_intel.intel_1203175

itlx64_1400   = MultiCompiler_intel.intel_1400
itlx64_1402144= MultiCompiler_intel.intel_1402144
itlx64_1402176= MultiCompiler_intel.intel_1402176
itlx64_1403174= MultiCompiler_intel.intel_1403174
itlx64_1403202= MultiCompiler_intel.intel_1403202
itlx64_1404211= MultiCompiler_intel.intel_1404211
itlx64_1404237= MultiCompiler_intel.intel_1404237

itlx64_1500   = MultiCompiler_intel.intel_1500
itlx64_1501133= MultiCompiler_intel.intel_1501133
itlx64_1502179= MultiCompiler_intel.intel_1502179
itlx64_1503208= MultiCompiler_intel.intel_1503208
itlx64_1504221= MultiCompiler_intel.intel_1504221
itlx64_1506285= MultiCompiler_intel.intel_1506285

itlx64_1600   = MultiCompiler_intel.intel_1600
itlx64_1601150= MultiCompiler_intel.intel_1601150
itlx64_1603207= MultiCompiler_intel.intel_1603207
itlx64_1603210= MultiCompiler_intel.intel_1603210
itlx64_1604246= MultiCompiler_intel.intel_1604246

itlx64_1700   = MultiCompiler_intel.intel_1700
itlx64_1702187= MultiCompiler_intel.intel_1702187
itlx64_1704210= MultiCompiler_intel.intel_1704210
itlx64_1705267= MultiCompiler_intel.intel_1705267
itlx64_1706270= MultiCompiler_intel.intel_1706270
itlx64_1707272= MultiCompiler_intel.intel_1707272

itlx64_1800   = MultiCompiler_intel.intel_1800
itlx64_1802185= MultiCompiler_intel.intel_1802185
itlx64_1802199= MultiCompiler_intel.intel_1802199
itlx64_1803210= MultiCompiler_intel.intel_1803210
itlx64_1803222= MultiCompiler_intel.intel_1803222

itlx64_1900   = MultiCompiler_intel.intel_1900
itlx64_1901144= MultiCompiler_intel.intel_1901144
itlx64_1902190= MultiCompiler_intel.intel_1902190
itlx64_1903203= MultiCompiler_intel.intel_1903203
itlx64_1904243= MultiCompiler_intel.intel_1904243
itlx64_1904245= MultiCompiler_intel.intel_1904245
itlx64_1905281= MultiCompiler_intel.intel_1905281
itlx64_191    = MultiCompiler_intel.intel_191
itlx64_191254 = MultiCompiler_intel.intel_191254
itlx64_191304 = MultiCompiler_intel.intel_191304
itlx64_191311 = MultiCompiler_intel.intel_191311
itlx64_192    = MultiCompiler_intel.intel_192
itlx64_192616 = MultiCompiler_intel.intel_192616


import MUC_platform
if (MUC_platform.is_unx()):
   import MultiCompiler_gcc
   class intel_gcc_mixer:
      def __init__(self):
         self.prx_gcc = MultiCompiler_gcc.gcc_xx()
         self.prx_itl = MultiCompiler_intel.intel_1203174()
      def c_cfg(self, cfg):
         return self.prx_itl.c_cfg(cfg)
      def cpp_cfg(self, cfg):
         return self.prx_gcc.cpp_cfg(cfg)
      def ftn_cfg(self, cfg):
         return self.prx_itl.ftn_cfg(cfg)
      def c(self, cfg):
         self.prx_itl.c(cfg)
      def cpp(self, cfg):
         self.prx_gcc.cpp(cfg)
      def ftn(self, cfg):
         self.prx_itl.ftn(cfg)
   itlx64_1203174 = intel_gcc_mixer

