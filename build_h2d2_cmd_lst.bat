:: Construis l'arborescence des commandes
::
:: Si utilisé par un autre fichier de commande, utiliser
:: un appel avec call

@echo off

:: ---  Copie pour être sûr d'avoir tous les fichiers
call %INRS_DEV%\build\copy_to_debug.bat %1 %2 %3

:: ---  Le répertoire de doc
set PRJ_ROOT=%1
set SRC_CONF=%2
set PTF_CONF=%3
set DST_DIR=%INRS_DEV%\%SRC_CONF%\%PTF_CONF%
set DST_DOC_DIR=%DST_DIR%\doc

:: ---  Copie les images
pushd %DST_DOC_DIR%
python %PRJ_ROOT%\..\doc\bld_h2d2_cmd_lst.py
popd

:: ---  Nettoie les erreurs
exit /b 0
