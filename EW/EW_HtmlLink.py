# -*- coding: UTF-8 -*-
import fileinput
import os
import time

from EW_Util import dbg
from EW_Util import EWHtmlFile
from EW_Linker import ctrLinker


class EWHtml_AlgoSummaryLog:
    def __init__(self):
        self.lnk = None

    def configure(self, path):
        cmplr = os.path.dirname(path)
        cmplr = os.path.dirname(cmplr)
        cmplr = os.path.basename(cmplr)
        self.lnk = ctrLinker(cmplr)

    def write(self, htmlFile, line, inError):
        if (inError == 1):
            htmlFile.write("<font color = " + htmlFile.getColor(0, 1, 0) + ">"  + line + "</font>")
        else:
            htmlFile.write("<font color = " + htmlFile.getColor(0, 0, 1) + ">"  + line + "</font>")

    def __call__(self, path, htmlFile):
        self.configure(path)

        htmlFile.write("<pre>")
        inError = 0
        for line in fileinput.FileInput(path):
            line = line.rstrip()
            if (len(line) < 10):
                if (inError): self.write(htmlFile, msg, inError)
                inError = 0
            elif (line[0:5] == "     "):
                if (inError): self.write(htmlFile, msg, inError)
                inError = 0
            elif (line[0:3] == "In "):
                if (inError): self.write(htmlFile, msg, inError)
                inError = 0
            elif (line[0] == " "):
                if (inError):
                    msg = msg + " " + line.strip()
                else:
                    inError = 0
            elif (line.find(self.lnk.spdexpWarning) > 0) and (self.lnk.regexpWarning.match(line)):
                if (inError): self.write(htmlFile, msg, inError)
                mObj = self.lnk.regexpWarning.match(line)
                srcFile = os.path.basename(mObj.group('file'))
                if (os.path.splitext(srcFile)[1][1] == 'f'):   # Fortran
                    (srcBase, srcExt) = os.path.splitext(srcFile)
                    htmFile = srcBase + '_' + srcExt[1:] + '.htm'
                    href = "webftn/%s" % (htmFile)
                else:
                    href = "/cgi-bin/frm_code.exe?/webpp-segri/%s" % (srcFile)
                msg = line.replace (srcFile, "<a href=\"%s\">%s</a>" % (href, srcFile))
                inError = 2
            elif (line.find(self.lnk.spdexpError) > 0) and (self.lnk.regexpError.match(line)):
                if (inError): self.write(htmlFile, msg, inError)
                mObj = self.lnk.regexpError.match(line)
                srcFile = os.path.basename(mObj.group('file'))
                if (os.path.splitext(srcFile)[1][1] == 'f'):   # Fortran
                    (srcBase, srcExt) = os.path.splitext(srcFile)
                    htmFile = srcBase + '_' + srcExt[1:] + '.htm'
                    href = "webftn/%s" % (htmFile)
                else:
                    href = "/cgi-bin/frm_code.exe?/webpp-segri/%s" % (srcFile)
                msg = line.replace (srcFile, "<a href=\"%s\">%s</a>" % (href, srcFile))
                inError = 1
            else:
                if (inError): self.write(htmlFile, msg, inError)
                inError = 0
        if (inError): self.write(htmlFile, msg, inError)
        htmlFile.write("</pre>")


class EWHtml_AlgoLinkerLog:
    def __init__(self):
        self.lnk = None

    def configure(self, path):
        cmplr = os.path.dirname(path)
        cmplr = os.path.dirname(cmplr)
        cmplr = os.path.basename(cmplr)
        self.lnk = ctrLinker(cmplr)

    def writeError(self, htmlFile, line):
#        href = "/cgi-bin/EW_GenHtmlErWr.cgi?mdl=%s&vrs=%s" % (self.mdl_actu, item.nom)
        htmlFile.write("<pre><font color = " + htmlFile.getColor(0, 1, 0) + ">" + line + "</font>")

    def writeWarning(self, htmlFile, line):
#        href = "/cgi-bin/EW_GenHtmlErWr.cgi?mdl=%s&vrs=%s" % (self.mdl_actu, item.nom)
        htmlFile.write("<font color = " + htmlFile.getColor(0, 0, 1) + ">"  + line + "</font>")


    def __call__(self, path, htmlFile):
        self.configure(path)

        htmlFile.write("<pre>")
        for line in fileinput.FileInput(path):
            line = line.rstrip()
            if (len(line) < 10):
                htmlFile.write(line)
            elif (line[0] == " "):
                htmlFile.write(line)
            elif (line[0:3] == "In "):
                htmlFile.write(line)
            elif (line.find(self.lnk.spdexpWarning) > 0) and (self.lnk.regexpWarning.match(line)):
                mObj = self.lnk.regexpWarning.match(line)
                srcFile = os.path.basename(mObj.group('file'))
                if (os.path.splitext(srcFile)[1][1] == 'f'):   # Fortran
                    (srcBase, srcExt) = os.path.splitext(srcFile)
                    htmFile = srcBase + '_' + srcExt[1:] + '.htm'
                    href = "webftn/%s" % (htmFile)
                else:
                    href = "/cgi-bin/frm_code.exe?/webpp-segri/%s" % (srcFile)
                line = line.replace (srcFile, "<a href=\"%s\">%s</a>" % (href, srcFile))
                self.writeWarning(htmlFile, line)
            elif (line.find(self.lnk.spdexpError) > 0) and (self.lnk.regexpError.match(line)):
                mObj = self.lnk.regexpError.match(line)
                srcFile = os.path.basename(mObj.group('file'))
                if (os.path.splitext(srcFile)[1][1] == 'f'):   # Fortran
                    (srcBase, srcExt) = os.path.splitext(srcFile)
                    htmFile = srcBase + '_' + srcExt[1:] + '.htm'
                    href = "webftn/%s" % (htmFile)
                else:
                    href = "/cgi-bin/frm_code.exe?/webpp-segri/%s" % (srcFile)
                line = line.replace (srcFile, "<a href=\"%s\">%s</a>" % (href, srcFile))
                self.writeError(htmlFile, line)
            else:
                htmlFile.write(line)
        htmlFile.write("</pre>")


class EWHtml_SummaryLog:
    def __init__(self):
        pass

    def xeq(self, file):
#        dbg.printd("Compiler log: %s" % file)

        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Linker log summary: %s" % file)

        href = "EW_GenHtmlCmpl.cgi?full=yes&file=%s" % file
        self.ficHTML.write("    Fichier: %s" % file)
        if (os.path.exists(file)):
            self.ficHTML.write("<br>Date: %s" % time.ctime(os.path.getmtime(file)))
            self.ficHTML.write("<br><a href=\"%s\">Log complet</a><br>" % href)
        else:
            self.ficHTML.write("<br><br><b>Fichier inexistant</b>")

        algo = EWHtml_AlgoSummaryLog()
        algo(file, self.ficHTML)

        self.ficHTML.writeFooter()


class EWHtml_LinkerLog:
    def __init__(self):
        pass

    def xeq(self, file):
#        dbg.printd("Linker log: %s" % file)

        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Linker log: %s" % file)

        self.ficHTML.write("    Fichier: %s" % file)
        if (os.path.exists(file)):
            self.ficHTML.write("<br>Date: %s" % time.ctime(os.path.getmtime(file)))
            self.ficHTML.write("<br>")
        else:
            self.ficHTML.write("<br><br><b>Fichier inexistant</b>")

        algo = EWHtml_AlgoLinkerLog()
        algo(file, self.ficHTML)

        self.ficHTML.writeFooter()


if __name__ == '__main__':
    path = r"\\Madawaska\compil\dev\bin\dev\boost_sup\derniere\boost_sup.dll\intel7.1\debug\boost_sup.err-lnk"
    algo = EWHtml_SummaryLog()
    algo.xeq(path)

