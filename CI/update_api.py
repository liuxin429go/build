#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os
import sys
import optparse
import re
import subprocess
import util
import logging
logger = util.getlogger('INRS.IEHSS.Fortran')

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
class CallXtrapi:

    def __init__(self):
        self.modules = []
        self._def_common_args()

    def _def_common_args(self):
        inrs_dev = os.environ['INRS_DEV']
        xtrapi = util.getpath(inrs_dev, 'tools', 'xtrapi', 'xtrapi.py')
        inc = '@%s' % util.getpath(os.environ['INRS_DEV'], 'H2D2', 'h2d2.i')
        self.base_args = ['python', xtrapi, inc, '-f', 'cmc']

    def get_for_files(self, prj_dir):
        for_files = []
        script = util.getpath(prj_dir, 'SConscript')
        MUC_SCons.Context.env = env
        execfile(script, globals())
        if env.ctx:
            for f in env.ctx.src:
                if (os.path.splitext(f)[1] == '.for'):
                    f = os.path.join(prj_dir, f)
                    f = os.path.normpath(f)
                    for_files.append(f)
        return for_files

    def __call__(self, for_files, prj_dir):
        mdl = os.path.basename(prj_dir)
        out = os.path.join(prj_dir, 'source', 'fake_dll_%s.cpp' % mdl)
        tmp = out + '.new'
        args = self.base_args + ['-k', 'modul=%s' % mdl, '-o', tmp] + for_files
        if subprocess.call(args) != 0:
            raise util.CIError('xtrapi.py failed')
        util.update_file(out, tmp, logger, start='#include')
        if os.path.isfile(out):
            self.modules.append(mdl)

    def write_modules(self):
        fake_dll = os.path.join(os.environ['INRS_DEV'], 'tools', 'xtrapi', 'cmc',
                                'fake_dlls.cpp')
        w = WriterCmc.Writer(fake_dll)
        w.writeGlobal(self.modules)
        del(w)


#*******************************************************************************
# Summary: Extracts then validates this script's argument(s).
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def process_args(args):

    parser = optparse.OptionParser()
    parser.set_usage('Usage: %prog [directories]')
    opts, args = parser.parse_args(args)

    for a in args:
        a = os.path.abspath(a)
        if not os.path.isdir(a):
            parser.error('invalid directory: %s' % a)
    if not args:
        args.append(os.environ['INRS_DEV'])

    return args

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def main(args=None):

    if not args:
        args = sys.argv[1:]
    args = process_args(args)

    call_xtrapi = CallXtrapi()
    required = ['build', 'prjVisual', 'source']
    skipped = ['test']
    zero_successful = True
    for a in args:
        for d in util.get_valid_dirs(a, required, skipped):
            try:
                for_files = call_xtrapi.get_for_files(d)
                call_xtrapi(for_files, d)
                zero_successful = False
            except util.CIError, msg:
                logger.warning(msg)
    if zero_successful:
        raise util.CIError('every calls failed')
    call_xtrapi.write_modules()


try:
    util.check_env()

    sys.path.append(os.environ['INRS_BLD'])
    sys.path.append(util.getpath(os.environ['INRS_BLD'], 'MUC_SCons'))
    sys.path.append(util.getpath(os.environ['INRS_DEV'], 'tools'))
    sys.path.append(util.getpath(os.environ['INRS_DEV'], 'tools', 'xtrapi'))

    if util.isdir(sys.prefix, 'scons.*'):
        sys.path.append(util.getpath(sys.prefix, 'scons.*'))
    if util.isdir(sys.prefix, 'Lib', 'scons.*'):
        sys.path.append(util.getpath(sys.prefix, 'Lib', 'scons.*'))
    if util.isdir(sys.prefix, 'Lib', 'site-packages', 'scons.*'):
        sys.path.append(util.getpath(sys.prefix, 'Lib', 'site-packages', 'scons.*'))
    try:
        pipe = subprocess.Popen(['scons'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True)
        text = pipe.communicate()[0]
        m = re.search(r'File\ "(?P<p>.+)",\ line', text)
        path = m.group('p')
        path = path[:path.find('SCons')-1]
        sys.path.append(path)
    except:
        pass

    import WriterCmc
    import MUC_SCons.Context
    import SCons_Env

    def Import(v):
        SCons_Env.Import(v, globals())
    Import('env')

    if __name__ == '__main__':
        main()

except util.CIError, msg:
    logger.error(msg)
    sys.exit(1)

