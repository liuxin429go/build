# -*- coding: UTF-8 -*-
import SCons.Script

from . import Environment
import sys

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

class EnvironmentOpenWatcom(MUCEnv):
    def __init__(self, tool, version, opts, *args, **kwargs):
        plateform = 'win32'
        tools =  ['wfc', 'wlib']
        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform = plateform,
                           MUC_TARGETPLATEFORM = 'win32')
        MUCEnv.__init__(self, 'watcom', version, *args, **kwargs)

#        self['LINK']   = 'wlink a definir'
#        self['SHLINK'] = 'wlink a definir'
#        self['LINKFLAGS'] = []
#        self['LINKFLAGS'] = []

    def setupEnv(self, ctx):
        MUCEnv.setupEnv(self, ctx)
