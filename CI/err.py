#!/usr/bin/env python

import os
import re
import sys
import optparse
import subprocess
import util
import logging
import shutil
import fileinput
import time
logger = util.getlogger('INRS.IEHSS.err')

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
class ErrExtract:
    def __init__(self, opts):
        self.opts = opts
        self.log = []
        self.warnings = 0
        self.errors = 0
        self.intRe = re.compile(r'.*?(?P<int>[0-9]+)')

    def __configure(self, path):
        cpl = os.path.dirname(path)
        if (os.path.basename(cpl) == 'user-interface-gui'):
            cpl = os.path.dirname(cpl)
        while (os.path.basename(cpl) not in ['dynamic', 'static']):
            cpl = os.path.dirname(cpl)
        cpl = os.path.dirname(cpl)
        cpl = os.path.basename(cpl)
        typ = os.path.basename(path)        # algmrs.for.err
        typ = os.path.splitext(typ)[0]      # algmrs.for
        typ = os.path.splitext(typ)[1][1:]  # for
        try:
            self.cpl = ctrCompiler(cpl, typ)
        except NotImplementedError:
            self.cpl = None

    def __format_msg(self, line, mObj):
        srcFile = os.path.basename(mObj.group('file'))
        fic, ext = os.path.splitext(srcFile)
        if (ext and ext[1] == 'f'):   # Fortran
            lineSrc = mObj.group('line')
            lineNbr = self.intRe.match(lineSrc).group('int')

            (srcBase, srcExt) = os.path.splitext(srcFile)
            htmFile = srcBase + '_' + srcExt[1:] + '.htm'
            href = r'    http://%s/doc_technique/H2D2' % self.opts.href
            if self.opts.branch:
                href += '-%s' % self.opts.branch
            href += '/webftn/%s' % htmFile
            if lineNbr:
                href += '#L%s' % lineNbr
            msg = line.replace(srcFile, href)
        else:
            msg = '    ' + line
        return msg

    def __call__(self, path):
        self.__configure(path)
        if (not self.cpl): return

        log_index = len(self.log)
        inError = 0
        msg = ''
        for line in fileinput.FileInput(path):
            line = line.rstrip()
            if (len(line) < 10):
                if (inError): self.log.append(msg)
                inError = 0
            elif (line[0:5] == '     '):
                if (inError): self.log.append(msg)
                inError = 0
            elif (line[0:3] == 'In '):
                if (inError): self.log.append(msg)
                inError = 0
            elif (line[0] == ' '):
                if (inError):
                    msg = msg + ' ' + line.strip()
                else:
                    inError = 0
            elif (line.find(self.cpl.spdexpWarning) > 0) and (self.cpl.regexpWarning.match(line)):
                if (inError): self.log.append(msg)
                mObj = self.cpl.regexpWarning.match(line)
                msg = self.__format_msg(line, mObj)
                inError = 2
                self.warnings += 1
            elif (line.find(self.cpl.spdexpError) > 0) and (self.cpl.regexpError.match(line)):
                if (inError): self.log.append(msg)
                mObj = self.cpl.regexpError.match(line)
                msg = self.__format_msg(line, mObj)
                inError = 1
                self.errors += 1
            else:
                if (inError): self.log.append(msg)
                inError = 0
        if (inError): self.log.append(msg)
        if msg: # insert link to .err file before errors it contains
            err_relpath = util.relpath(path, os.environ['INRS_DEV'])
            err_relpath = err_relpath.replace('\\', '/')
            err_href = 'http://%s/CI_global_results/' % self.opts.href
            err_href += self.opts.branch
            err_href += '/%s :' % err_relpath
            self.log.insert(log_index, err_href)
            self.log.append('')

    def print_summary(self):
        for l in self.log:
            print l


def find_version(path):
    version = 0
    while (os.path.basename(path) not in ['debug', 'release']):
        path = os.path.dirname(path)
    if os.path.basename(path) == 'release': version += 1
    path = os.path.dirname(path)
    if os.path.basename(path) == 'static': version += 2
    return version

def print_table(wrn, err):
    if len(wrn) == 4 and len(err) == 4:
        for i in range(4):
            wrn[i] = str(wrn[i]).center(5)
            err[i] = str(err[i]).center(5)
        tbl = ['']
        tbl.append('          -------------------------')
        tbl.append('          |  DYNAMIC  |   STATIC  |')
        tbl.append('          |------------------------')
        tbl.append('          | wrn | err | wrn | err |')
        tbl.append('-----------------------------------')
        tbl.append('|  DEBUG  |%s|%s|%s|%s|' % (wrn[0], err[0], wrn[2], err[2]))
        tbl.append('-----------------------------------')
        tbl.append('| RELEASE |%s|%s|%s|%s|' % (wrn[1], err[1], wrn[3], err[3]))
        tbl.append('-----------------------------------')
        for l in tbl:
            print l.center(80)


def ew_parse(opts, path):

    def get_pf_mpi_cpl(path):
        path = os.path.join(path, os.listdir(path)[0])
        pf = os.listdir(path)[0]
        path = os.path.join(path, pf)
        mpi = os.listdir(path)[0]
        path = os.path.join(path, mpi)
        cpl = os.listdir(path)[0]
        return pf, mpi, cpl

    pf, mpi, cpl = get_pf_mpi_cpl(path)
    out = '_'.join(['EW', time.strftime("%Y%m%d-%H%M%S"), pf, mpi, cpl]) + '.xml'
    dest = ''
    if opts.user: dest = '%s@' % opts.user
    dest += '%s:logs/CI/' % opts.server
    dest += opts.branch
    dest += '/%s' % out

    date = EWInfoDate()
    date.xtrInfo(path)
    date.ecrisXML(out)

    try:
        util.rsync(out, dest, port=opts.port)
    except util.CIError, msg:
        logger.warning(msg)

    os.remove(out)

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def process_args(args):
    parser = optparse.OptionParser()
    parser.add_option('-r', '--href',   help='href url prefix')
    parser.add_option('-s', '--server', help='web server name or address')
    parser.add_option('-p', '--port',   help='web server ssh port')
    parser.add_option('-u', '--user',   help='web server ssh user')
    parser.add_option('-b', '--branch', help='the H2D2 branch, if not HEAD', default='HEAD')
    opts, args = parser.parse_args(args)
    if not opts.href:
        parser.error('must specify href url prefix: --href')
    if not opts.server:
        parser.error('must specify server: --server')
    if args:
        parser.error('this script does not expect any argument')
    return opts

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def main(args=None):

    if not args:
        args = sys.argv[1:]
    opts = process_args(args)

    path = util.getpath(os.environ['INRS_DEV'], '_bin', 'H2D2')
    err_extract = []
    version = ['DYNAMIC-DEBUG', 'DYNAMIC-RELEASE', 'STATIC-DEBUG', 'STATIC-RELEASE']

    # ---  Copy .err files
    if opts.server:
        dest = ''
        if opts.user: dest = '%s@' % opts.user
        dest += '%s:CI_global_results/' % opts.server
        dest += opts.branch
        try:
            util.rsync('_*', dest, excludes=['__mod__'], port=opts.port)
        except util.CIError, msg:
            logger.warning(msg)

    # ---  Extract messages
    for i in version:
        err_extract.append(ErrExtract(opts))
    for f in util.get_valid_files(path, r'.*\.err'):
        print f
        err_extract[find_version(f)](f)
    print_table([i.warnings for i in err_extract], [i.errors for i in err_extract])
    for i, v in enumerate(version):
        print '\n' + v.center(80, '-')
        err_extract[i].print_summary()

    ew_parse(opts, path)


try:
    util.check_env()

    sys.path.append(os.environ['INRS_BLD'])
    from EW_Compiler import ctrCompiler
    from EW_Info import EWInfoDate
    #from EW_Util import dbg

    if __name__ == '__main__':
        main()
except util.CIError, msg:
    logger.error(msg)
    sys.exit(1)

