# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#
#   Compilateur ftnchk
#-----------------------------------------------------------------------------------------------------

import MUC_platform

import os

"""
Configuration
"""
class cfg_ftn:
    def getCfg(self, c):
        flags = [
                '-f77=assignment−stmt,automatic−array',
                '-declare',
                '-noextern',
                '-nolibrary',
                '-nonovice',
                '-portability=tab,common-alignement',
                '-project',
                '-quiet',
                #'-nopure',
                #-'style=all',
                #'-style=nostructured-end',
                '-usage=all',
                ]
        return flags

"""
ftnchek générique
"""
class ftnchek(cfg_ftn):
    def __trfInc(self, inc):
        __inc = []
        for i in inc:
            __inc.append('-include=%s' % i[2:])
        return __inc

    def __win32(self, cmplr, cfg):
        inc = self.__trfInc(cfg.cpl_inc)
        s = r'set PATH=%ProgramFiles(x86)%\ftnchek;%PATH%'
        if (cfg.fic_err):
            c = '%s %s %s %s -o%s %s > %s 2>&1' % (cmplr, ' '.join(cfg.cpl_cfg), ' '.join(inc), ' '.join(cfg.cpl_dfn), cfg.fic_out, cfg.fic_inp, cfg.fic_err)
        else:
            c = '%s %s %s %s -o%s %s' % (cmplr, ' '.join(cfg.cpl_cfg), ' '.join(inc), ' '.join(cfg.cpl_dfn), cfg.fic_out, cfg.fic_inp)
        os.system( ' && '.join( [s, c] ) )

    def __lnx32(self, cmplr, cfg):
        if (cfg.fic_err):
            c = '%s %s %s %s -o%s %s > %s 2>&1' % (cmplr, ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn), cfg.fic_out, cfg.fic_inp, cfg.fic_err)
        else:
            c = '%s %s %s %s -o%s %s' % (cmplr, ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn), cfg.fic_out, cfg.fic_inp)
        os.system(c)

    def ftn(self, cfg):
        if (MUC_platform.is_win()):
            self.__win32('ftncheck_', cfg)
        else:
            self.__lnx32('ftnchek', cfg)

    def ftn_cfg(self, cfg):
        return cfg_ftn.getCfg(self, cfg.tgt_cfg)

"""
ftnchek
"""
class ftnchek_33(ftnchek):
    def ftn(self, cfg):
        return ftnchek.ftn(self, cfg)

    def ftn_cfg(self, cfg):
        return ftnchek.ftn_cfg(self, cfg)

"""
Version par défaut
"""
class ftnchek_xx(ftnchek_33):
    pass
