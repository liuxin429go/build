#!/usr/bin/env python

import os
import sys
import optparse
import subprocess
import util
import logging
logger = util.getlogger('INRS.IEHSS.test')

if util.iswindows():
    XTN_EXE = '.exe'
else:
    XTN_EXE = ''

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def run_tests(opts):

    inrs_dev = os.environ['INRS_DEV']
    bt_gentest_bindir = util.getpath(inrs_dev, 'H2D2', 'test', 'bin')
    cd2d = util.getpath(bt_gentest_bindir, 'gen_cd2d'+XTN_EXE)
    sv2d = util.getpath(bt_gentest_bindir, 'gen_sv2d'+XTN_EXE)
    cd2d_dir = util.getpath(inrs_dev, 'H2D2', 'test', 'convection-diffusion')
    sv2d_dir = util.getpath(inrs_dev, 'H2D2', 'test', 'hydro', 'st-venant2D')
    bt_gentest = ((cd2d, cd2d_dir), (sv2d, sv2d_dir))
    bt_errcnv = util.getpath(bt_gentest_bindir, 'errcnv'+XTN_EXE)

    bt_target_dir = []
    bt_target_exe = []
    tgt_exe = 'h2d2'+XTN_EXE
    for d in util.get_valid_dirs('_run', ['bin', 'doc', 'etc', 'locale', 'script']):
        tgt_dir = os.path.join(os.getcwd(), util.getpath(d, 'bin'))
        bt_target_dir.append(tgt_dir)
        bt_target_exe.append( os.path.join(tgt_dir, tgt_exe) )

    os.environ['BT_GENTEST_BINDIR'] = bt_gentest_bindir.replace('\\', '/')
    os.environ['BT_ERRCNV'] = bt_errcnv.replace('\\', '/')

    for btd, bte in zip(bt_target_dir, bt_target_exe):
        os.environ['BT_TARGET_DIR'] = btd.replace('\\', '/')
        os.environ['BT_TARGET_EXE'] = bte.replace('\\', '/')
        for (test, test_dir) in bt_gentest:
            os.environ['BT_GENTEST'] = test.replace('\\', '/')
            os.chdir(test_dir)
            version = util.relpath(btd, '_run')
            logger.info('running test %s for %s' % (os.path.basename(test), version))
            logger.info('running test in %s' % os.getcwd())
            try:
                logger.info('make clean')
                if subprocess.call(['make', 'clean', '-f', 'makefile.mak']):
                    raise util.CIError('make clean failed')
                logger.info('make clean done')
                logger.info('make')
                if subprocess.call(['make', '-f', 'makefile.mak']):
                    raise util.CIError('make failed')
                logger.info('make done')
                if subprocess.call('cmpltest'+XTN_EXE):
                    raise util.CIError('cmpltest failed')
                if opts.server:
                    dest = ''
                    if opts.user: dest = '%s@' % opts.user
                    dest += '%s:CI_tests/' % opts.server
                    dest += opts.branch
                    dest += '/%s.html' % version
                    util.rsync('test_h2d2.html', dest, port=opts.port)
                os.remove('test_h2d2.html')
            except util.CIError, msg:
                logger.error(msg)


#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def process_args(args):
    parser = optparse.OptionParser()
    parser.add_option('-s', '--server', help='web server name or address')
    parser.add_option('-p', '--port',   help='web server ssh port')
    parser.add_option('-u', '--user',   help='web server ssh user')
    parser.add_option('-b', '--branch', help='the H2D2 branch, if not HEAD', default='HEAD')
    opts, args = parser.parse_args(args)
    if args:
        parser.error('this script does not expect any argument: %s' % args)
    return opts

#*******************************************************************************
# Summary:
#
# Description:
#
# Input:
#
# Output:
#
# Note:
#*******************************************************************************
def main(args=None):
    if not args:
        args = sys.argv[1:]
    opts = process_args(args)
    run_tests(opts)


try:
    util.check_env()
    if __name__ == '__main__':
        main()
except util.CIError, msg:
    logger.error(msg)
    sys.exit(1)

