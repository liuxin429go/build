# -*- coding: UTF-8 -*-

from __future__ import print_function

from PIL import Image
from PIL import ImageColor
from PIL import ImageDraw

import sys

'''
    La fonction génère un fichier bitmap avec une tranche pour chaque
    paire (erreur, warning).

    erwr    : liste de paires (erreur, warning)
    file    : nom du fichier de sortie
'''
def genEWBackgroundColor(erwr):

    def getColor(abs, err, wrn):
        if (abs == 0):
            if (err == 0):
                if (wrn == 0):
                    return "#00ff00"
                else:
                    return "#ff8800"
            else:
                return "#ff0000"
        else:
            return "#ff0000"

    def getCode(abs, err, wrn):
        if (abs == 0):
            if (err == 0):
                if (wrn == 0):
                    return "0"
                else:
                    return "w"
            else:
                return "e"
        else:
            return "e"

    tw = 90
    th = 30
    w = float(tw) / float(max(1, len(erwr)))
    x1 = 0
    x2 = 0
    y1 = 0
    y2 = th

    fic  = "/images/ew_bc_"
    img  = Image.new("RGB", (tw, th))
    draw = ImageDraw.Draw(img)
    i = 0
    for e in erwr:
        i = i + 1
        x1 = x2
        x2 = int(w*float(i) + 0.5)
        fic = fic + getCode(e[0], e[1], e[2])
        clr = getColor(e[0], e[1], e[2])
        draw.rectangle((x1,y1,x2,y2), clr, "#ffffff")

    fic = fic + ".jpg"
    img.save(".." + fic)
    return fic

'''
    La classe EWDebug et la variable globale dbg associée, permet une
    écriture debug sous la forme:
        dbg.printd(msg, condition)
    Le mode debug est contrôlé par l'attribut EWDebug.on
    Le niveau d'intentation est contrôlé par l'attribut EWDebug.level
'''
class EWDebug:
    def __init__(self):
        self.level = 0
        self.on    = False

    def printd(self, msg, cond = 1):
        if not self.on: return
        if not cond: return
        if (self.level > 0):
            print('   '*self.level, msg, file=sys.stderr)
        else:
            print(msg, file=sys.stderr)

dbg = EWDebug()

'''
'''
class EWHtmlFile:
    def __init__(self):
        pass

    def writeHeader(self, title, css=[]):
        print('Content-type: text/html; charset=utf-8')
        print('')
        print('<html>')
        # print(EWCopyright());
        print('<head>')
        print('   <meta charset="utf-8" />')
        for c in css:
            print('   <link rel="stylesheet" href="{}" />'.format(c))
        print('   <title>{}</title>'.format(title))
        print('</head>')
        print('<body>')
        # (*osP) << EWMenu();

    def writeFooter(self):
        # (*osP) << WBMenu();
        print('</body>')
        print('</html>')

    def write(self, ligne):
        print(ligne)

    def getColor(self, abs, err, wrn):
        if (abs == 0):
            if (err == 0):
                if (wrn == 0):
                    return "\"#00FF00\""
                else:
                    return "\"#FF8800\""
            else:
                return "\"#FF0000\""
        else:
            return "\"#FF0000\""

    def getBgColor(self, lst_ew):
        return genEWBackgroundColor(lst_ew)

