# -*- coding: UTF-8 -*-
"""
::------------------------------------------------------
:: I.N.R.S. - Processus de compilation multi-compilateur
::------------------------------------------------------
"""

import sys
if sys.version_info[0:2] < (3, 6):
    print("Python version 3.6 or more required.")
    exit(1)

import MultiCompiler_ftnchek
import MultiCompiler_cmc
import MultiCompiler_cmcitl
import MultiCompiler_cmcpgi
#import MultiCompiler_f2c
import MultiCompiler_gcc
import MultiCompiler_gccX86
import MultiCompiler_gccX64
import MultiCompiler_intel
import MultiCompiler_itlX86
import MultiCompiler_itlX64
import MultiCompiler_itlI64
import MultiCompiler_msvc
import MultiCompiler_open64
import MultiCompiler_sun
import MultiCompiler_watcom
import MultiCompiler_xtrcmd

import datetime
import os
import optparse
import posixpath
import re
import sys
import tempfile
import traceback

InrsDev = ''
try:
    InrsDev = os.environ['INRS_DEV']
except:
    pass

rpdb2_imported = False
try:
    import rpdb2
    rpdb2_imported = True
except:
    pass

# ---  Lien entre les noms externes et les classes
cmc    = MultiCompiler_cmc
cmcitl = MultiCompiler_cmcitl
cmcpgi = MultiCompiler_cmcpgi
ftnchek= MultiCompiler_ftnchek
gcc    = MultiCompiler_gcc
gccx86 = MultiCompiler_gccX86
gccx64 = MultiCompiler_gccX64
intel  = MultiCompiler_intel
itlx86 = MultiCompiler_itlX86
itlx64 = MultiCompiler_itlX64
itli64 = MultiCompiler_itlI64
msvc   = MultiCompiler_msvc
open64 = MultiCompiler_open64
sun    = MultiCompiler_sun
watcom = MultiCompiler_watcom
xtrcmd = MultiCompiler_xtrcmd


class MuCompiler:
    ext2typ = { '.c'  : 'c', '.C'  : 'c',
                '.cpp': 'cpp', '.CPP': 'cpp', '.cc' : 'cpp', '.CC' : 'cpp',
                '.f'  : 'ftn', '.F'  : 'ftn', '.for': 'ftn', '.FOR': 'ftn', '.ftn': 'ftn', '.FTN': 'ftn', '.f90': 'ftn', '.F90': 'ftn'}

    MUC_MACROS = {
        '_bin':    re.compile(r'\$\{MUC_GRIST_BIN(:(?P<dir>\w*))?\}'),
        '_run':    re.compile(r'\$\{MUC_GRIST_RUN(:(?P<dir>\w*))?\}'),
        '__mod__': re.compile(r'\$\{MUC_GRIST_MOD(:(?P<dir>\w*))?\}'),
        }

    def __init__(self, dump, cplr, conf, pltf, link, isiz, nocfg, incl, noinc, defn, nodfn, pckg, dmod, lmpi, fout, ferr, finp):
        self.dmp_cmd = dump
        self.arg_cpl = cplr

        self.tgt_cpl = cplr.split('-')[0]
        self.tgt_ver = 'xx'
        try:
            self.tgt_ver = cplr.split('-')[1].replace('.', '')
        except:
            pass
        self.tgt_cfg = conf
        self.tgt_ptf = pltf
        self.tgt_lnk = link
        self.tgt_isz = isiz
        self.tgt_mpi = lmpi

        self.cpl_cfg   = None
        self.cpl_doCfg = not nocfg
        self.cpl_inc   = None
        self.cpl_doInc = not noinc
        self.cpl_dfn   = None
        self.cpl_doDfn = not nodfn
        self.cpl_fcfg  = None
        self.cpl_finc  = None
        self.cpl_fdfn  = None

        self.mod_dir = os.path.normpath(dmod) if dmod else ''
        if not self.mod_dir:
            self.mod_dir = os.path.join(InrsDev, pckg, '__mod__', pltf, lmpi, cplr, link, conf)
        if not os.path.exists(self.mod_dir): os.makedirs(self.mod_dir)

        if finp.startswith('"') and finp.endswith('"'): finp = finp[1:-1]
        if finp.startswith("'") and finp.endswith("'"): finp = finp[1:-1]
        if finp.startswith('"') and finp.endswith('"'): finp = finp[1:-1]
        self.fic_ext = os.path.splitext(finp)[1]
        self.fic_typ = MuCompiler.ext2typ[ self.fic_ext ]
        self.fic_inp = finp
        self.fic_out = fout
        self.fic_err = ferr

        if incl:
            with open(incl, 'r') as f:
                self.cpl_inc = [x.strip() for x in f.readlines()]
        if (defn):
            with open(defn, 'r') as f:
                self.cpl_dfn = [x.strip() for x in f.readlines()]
        if (isiz):
            if (not self.cpl_dfn): self.cpl_dfn = []
            self.cpl_dfn.insert(0, '-D"INRS_FORTRAN_INTEGER_SIZE=%i"' % isiz)

        cplrNam = '_'.join( [self.tgt_cpl, self.tgt_ver] )
        self.cplr = sys.modules['__main__'].__dict__[self.tgt_cpl].__dict__[cplrNam]()
        self.mthCpl = getattr(self.cplr, self.fic_typ)
        self.mthCfg = getattr(self.cplr, self.fic_typ+'_cfg')

    def __del__(self):
        if (self.cpl_fcfg): os.remove(self.cpl_fcfg)
        if (self.cpl_fdfn): os.remove(self.cpl_fdfn)
        if (self.cpl_finc): os.remove(self.cpl_finc)

    def __expandMacros(self, path):
        for key, macro in self.MUC_MACROS.items():
            m = re.search(macro, path)
            if m:
                d = m['dir'] if m['dir'] else ''
                t = posixpath.join(key, d, self.tgt_ptf, self.tgt_mpi, self.arg_cpl, self.tgt_lnk, self.tgt_cfg)
                path = macro.sub(t, path)
        return path

    def __reqFichiersConfig(self, buildPath, typCfg):
        retour = []

        # "cpp-all-allp-base.inc"
        fic = "%s-%s-%s-%s.%s" % (self.fic_typ, 'all', 'allp', 'base', typCfg)
        path = os.path.join(buildPath, fic)
        if os.path.isfile(path): retour.append(path)

        # "cpp-msvc-allp-base.inc"
        fic = "%s-%s-%s-%s.%s" % (self.fic_typ, self.tgt_cpl, 'allp', 'base', typCfg)
        path = os.path.join(buildPath, fic)
        if os.path.isfile(path): retour.append(path)

        # "cpp-all-win32-base.inc"
        fic = "%s-%s-%s-%s.%s" % (self.fic_typ, 'all', self.tgt_ptf, 'base', typCfg)
        path = os.path.join(buildPath, fic)
        if os.path.isfile(path): retour.append(path)

        # "cpp-msvc-win32-base.inc"
        fic = "%s-%s-%s-%s.%s" % (self.fic_typ, self.tgt_cpl, self.tgt_ptf, 'base', typCfg)
        path = os.path.join(buildPath, fic)
        if os.path.isfile(path): retour.append(path)

        # "cpp-all-allp-debug.inc"
        fic = "%s-%s-%s-%s.%s" % (self.fic_typ, 'all', 'allp', self.tgt_cfg, typCfg)
        path = os.path.join(buildPath, fic)
        if os.path.isfile(path): retour.append(path)

        # "cpp-msvc-allp-debug.inc"
        fic = "%s-%s-%s-%s.%s" % (self.fic_typ, self.tgt_cpl, 'allp', self.tgt_cfg, typCfg)
        path = os.path.join(buildPath, fic)
        if os.path.isfile(path): retour.append(path)

        # "cpp-all-win32-debug.inc"
        fic = "%s-%s-%s-%s.%s" % (self.fic_typ, 'all', self.tgt_ptf, self.tgt_cfg, typCfg)
        path = os.path.join(buildPath, fic)
        if os.path.isfile(path): retour.append(path)

        # "cpp-msvc-win32-debug.inc"
        fic = "%s-%s-%s-%s.%s" % (self.fic_typ, self.tgt_cpl, self.tgt_ptf, self.tgt_cfg, typCfg)
        path = os.path.join(buildPath, fic)
        if os.path.isfile(path): retour.append(path)

        return retour

    def __processPath(self, root, toks, typCfg):
        path = root
        retour = []
        for tok in toks:
            path = os.path.join(path, tok)
            bpth = os.path.join(path, "build")
            retour = retour + self.__reqFichiersConfig(bpth, typCfg)

        return retour

    def __processConfig(self, typCfg):
        fic = os.path.expandvars(self.fic_inp)
        fic = os.path.normpath(fic)
        fic = os.path.abspath(fic)
        dir = os.path.dirname(fic)
        dir = dir.replace('\\', '/')
        toks = dir.split('/')
        root = '/'
        if toks and toks[0] and toks[0][1] == ":":
            root = toks[0] + '/'
            del toks[0]

        cfg = []
        for p in self.__processPath(root, toks, typCfg):
            with open(p, 'r') as ifs:
                for l in ifs:
                    l = l.split('#',1)[0]
                    l = l.strip()
                    if l:
                        cfg.append(l)

        return cfg

    def __passThrough(d):
        return d

    def encode(self, encodeCfg = __passThrough, encodeDfn = __passThrough, encodeInc = __passThrough):
        for i in range(len(self.cpl_cfg)): self.cpl_cfg[i] = encodeCfg(self.cpl_cfg[i])
        for i in range(len(self.cpl_dfn)): self.cpl_dfn[i] = encodeDfn(self.cpl_dfn[i])
        for i in range(len(self.cpl_inc)): self.cpl_inc[i] = encodeInc(self.cpl_inc[i])

    def bldResponseFiles(self):
        fd, self.cpl_fcfg = tempfile.mkstemp('.cfg', '~muc_', None, 'w+t')
        f = os.fdopen(fd, "w+t")
        for l in self.cpl_cfg: f.write('%s\n' % l)
        f.close()

        fd, self.cpl_fdfn = tempfile.mkstemp('.dfn', '~muc_', None, 'w+t')
        f = os.fdopen(fd, "w+t")
        for l in self.cpl_dfn: f.write('%s\n' % l)
        f.close()

        fd, self.cpl_finc = tempfile.mkstemp('.inc', '~muc_', None, 'w+t')
        f = os.fdopen(fd, "w+t")
        for l in self.cpl_inc: f.write('%s\n' % l)
        f.close()

    def getConfig(self):
        self.cpl_cfg = self.mthCfg(self)
        if not self.cpl_cfg: self.cpl_cfg = []
        if self.cpl_doCfg:
            self.cpl_cfg += self.__processConfig('cfg') # + self.cpl_cfg

        if not self.cpl_dfn: self.cpl_dfn = []
        if self.cpl_doDfn:
            self.cpl_dfn = self.__processConfig('dfn') + self.cpl_dfn   # Prepend to preserve undef

        if not self.cpl_inc: self.cpl_inc = []
        if self.cpl_doInc:
            self.cpl_inc += self.__processConfig('inc') # + self.cpl_inc

        # ---  Expand macros
        for i in range(len(self.cpl_inc)):
            self.cpl_inc[i] = self.__expandMacros( self.cpl_inc[i] )

        # ---  Expand ENV var
        for i in range(len(self.cpl_inc)):
            self.cpl_inc[i] = os.path.expandvars( self.cpl_inc[i] )

    def timeStamp(self):
        t = datetime.datetime.now()
        print('%s - %s(%s): %s' % (t.strftime('%Y-%m-%d %H:%M:%S'), self.arg_cpl, self.tgt_cfg, self.fic_inp))

    def compile(self):
        return self.mthCpl(self)



def main(opt_args = None):
    usage  = "MuComp [options] inputFile"
    parser = optparse.OptionParser(usage)
    parser.add_option("-c", "--compiler", dest="cplr", help="Compiler to use")
    parser.add_option("-p", "--platform", dest="pltf", help="Target plateform", default="win64")
    parser.add_option("-g", "--config",   dest="conf", help="Configuration [debug]", default="debug")
    parser.add_option("-l", "--link",     dest="link", help="Link", default="dynamic")
    parser.add_option("-f", "--fortran-integer-size",  dest="isiz", help="Fortran INTEGER size [4]", default=4, type="int")
    parser.add_option("-o", "--output",   dest="fout", help="Output file")
    parser.add_option("-i", "--include",  dest="incl", help="Include path response file", default=None)
    parser.add_option("-m", "--define",   dest="defn", help="Define response file", default=None)
    parser.add_option("-C", "--add-config-from-build", dest="nocfg", help="Add config from build tree [false]", default=True, action="store_false")
    parser.add_option("-D", "--no-define-from-build",  dest="nodfn", help="Do not load defines from build tree [false]", default=False, action="store_true")
    parser.add_option("-I", "--no-include-from-build", dest="noinc", help="Do not load include path from build tree [false]", default=False, action="store_true")
    parser.add_option("-M", "--fortran-modules-directory", dest="dmod", help="Directory where Fortran90 modules will be generated", default=None)
    parser.add_option("-P", "--mpi-library",           dest="mpi",  help="MPI Library, used to build default modules directory", default="intelmpi")
    parser.add_option("-e", "--error",    dest="ferr", help="Error file", default=None)
    parser.add_option("-x", "--dumpcmd",  dest="dump", help="Dump the command [false]", default=False, action="store_true")
    parser.add_option("-d", "--winpdb",   dest="dbug", help="Debug run with Winpdb [false]", default=False, action="store_true")
    parser.add_option("-K", "--package",  dest="pckg", help="Package name, used for default mod directory", default='')

    if (not opt_args): opt_args = sys.argv[1:]
    try:
        v = os.environ['INRS_MUC_DUMPCMD']
        opt_args.insert(0, '--dumpcmd')
    except:
        pass
    (opts, args) = parser.parse_args(opt_args)

    if (opts.dbug):
        if (not rpdb2_imported):
            raise RuntimeError('rpdb2 must be installed to debug')
        else:
            rpdb2.start_embedded_debugger("inrs_iehss", fAllowUnencrypted = True)

    try:
        cplr = MuCompiler(opts.dump,
                          opts.cplr.lower(),
                          opts.conf.lower(),
                          opts.pltf.lower(),
                          opts.link.lower(),
                          opts.isiz,
                          opts.nocfg,
                          opts.incl,
                          opts.noinc,
                          opts.defn,
                          opts.nodfn,
                          opts.pckg,
                          opts.dmod,
                          opts.mpi.lower(),
                          opts.fout,
                          opts.ferr,
                          args[0])
        cplr.timeStamp()
        cplr.getConfig()
        cplr.compile()
    except:
        traceback.print_exc()
        print()
        print('Submitted command:')
        print('   ', sys.argv[0], ' '.join(opt_args))

if __name__ == '__main__':
    #args = "--dumpcmd --compiler=itlX64-19.2 --platform=win64 --config=debug --link=dynamic --fortran-integer-size=4 --add-config-from-build --output=dummy.obj --error=dummy.f90.err %INRS_DEV%\pyH2D2\py_h2d2_svc\src\dbg_m_FWRP.f90"
    #args = "--dumpcmd --compiler=itlX64-19.1.3.311 --platform=win64 --config=debug --link=dynamic --fortran-integer-size=4 --add-config-from-build --fortran-modules-directory=E:\inrs-dev\pyH2D2\.\__mod__\win64\intelmpi\itlX64-19.1\dynamic\debug --output=_bin\py_h2d2_svc\win64\intelmpi\itlX64-19.1\dynamic\debug\src\tr_chrn_m_FWRP.obj --error=_bin\py_h2d2_svc\win64\intelmpi\itlX64-19.1\dynamic\debug\src\tr_chrn_m_FWRP.f90.err py_h2d2_svc\src\tr_chrn_m_FWRP.f90"
    #main(args.split(' '))
    main()
