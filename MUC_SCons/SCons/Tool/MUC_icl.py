# -*- coding: UTF-8 -*-

# MUC_intelc est l'implantation MUC de SCons.Tool.intelc
# Pas trouvé le moyen de faire lire directement un fichier intelc.py!
# Avec un autre nom, il n'y a pas de confusion possible
import MUC_intelc
SCons_Tool_intelc = MUC_intelc
import SCons.Errors
import sys

# This has been completely superceded by intelc.py, which can
# handle both Windows and Linux versions.

def generate(env, *args, **kw):
    """Add Builders and construction variables for icl to an Environment."""
    topdir_ori  = kw.get('topdir',  None)
    version_ori = kw.get('version', None)

    version = None
    if env['INTEL_VERSION']:
        if   sys.platform == 'win32':
            version = env['INTEL_VERSION'].replace('.', '', 1)  #  11.0.061 ==> 110.061
        elif sys.platform.startswith('linux'):
            version = env['INTEL_VERSION']
        else:
            SCons.Errors.UserError('Invalid Intel platform: %s' % sys.platform)

    kw.setdefault('abi',    env['INTEL_PLATEFORM'])
    kw.setdefault('topdir', None)
    kw.setdefault('version', version)
    # kw.setdefault('verbose', 1)
    
    ret = SCons_Tool_intelc.generate(env, *args, **kw)
    
    kw['topdir']  = topdir_ori
    kw['version'] = version_ori

    return ret

def exists(env, *args, **kw):
    return apply(SCons_Tool_intelc.exists, env, args, kw)

