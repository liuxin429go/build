# -*- coding: UTF-8 -*-
import os
import fileinput
from string import lower, strip, split
# from xml.marshal import generic
# from xml.dom.ext.reader import Sax2
# from xml.dom.ext import PrettyPrint
import cPickle

from EW_Util import dbg

class EWNodeTree:

    def __init__(self, mdl="Top"):
        self.mdl = strip(mdl)
        self.mapNodes = {}

    def readNode(self, path):
        buildDir = os.path.join(path, "build")
        if not os.path.isdir(buildDir): return

        for f in os.listdir(buildDir):
            if os.path.splitext(f)[1] != ".inc": continue
            l = os.path.splitext(f)[0].split("-")
            if l[0] != "all" and l[0] != "msvc": continue

            ficInc = os.path.join(buildDir, f)

            for line in fileinput.FileInput(ficInc):
                line = strip(line)
                if len(line) == 0: continue
                if line[0:2] != "-I": continue
                line = strip(line[2:])
                if line[0] == "\"": line = strip(line[1:-1])
                mdl = self.extractmdl(line)
                if len(mdl) > 0 and mdl != self.mdl:
                    dbg.printd("%s" % mdl)
                    self.mapNodes[mdl] = None

    def extractmdl(self, path):
        if lower(path[0:2]) != "e:": return ""
        path = os.path.split(path)[0]
        if os.path.split(path)[1] == "build":
            path = os.path.split(path)[0]
        mdl = os.path.split(path)[1]
        return strip(mdl)

    def readTree(self, path):
        mdl = os.path.basename(path)
        # if (mdl == "derniere" or mdl[0:2] == "v_"):
        #     mdl = os.path.basename(os.path.dirname(path))

        projDir = os.path.join(path, "prjVisual")
        if os.path.isdir(projDir):
            for f in os.listdir(projDir):
                if (os.path.splitext(f)[1] != ".vcproj"): continue
                mdl = os.path.splitext(f)[0]
                dbg.printd("Node: %s" % mdl)
                node = EWNodeTree(mdl)
                dbg.level = dbg.level + 1
                node.readNode(path)
                dbg.level = dbg.level - 1
                self.mapNodes[node.mdl] = node

        for f in os.listdir(path):
            subDir = os.path.join(path, f)
            if os.path.isdir(subDir):
                self.readTree(subDir)

    def linkNodes(self, stack="::", topNode=None):
        dbg.printd("Resoud %s" % self.mdl)
        if (stack.find("::"+self.mdl+"::") > 0): raise "Cycle detected"
        dbg.level = dbg.level + 1
        if topNode is None: topNode = self

        for n in self.mapNodes:
            if self.mapNodes[n] is not None: continue
            dbg.printd("%s" % n)
            self.mapNodes[n] = topNode.mapNodes.get(n, nodeUnknown)
            dbg.printd("%s -- %s" % (self.mdl, n))
            dbg.printd("    Lien %s -- %s" % (n, self.mapNodes[n].mdl))

        stack = stack + self.mdl + "::"
        dbg.printd(" %s" % stack)
        for n in self.mapNodes:
            if self.mapNodes[n] is None: continue
            if self.mapNodes[n] is nodeUnknown: continue
            self.mapNodes[n].linkNodes(stack, topNode)

        dbg.level = dbg.level - 1

    def writeXML(self, name):
        reader = Sax2.Reader()
        doc = reader.fromString(generic.dumps(self))
        PrettyPrint(doc, file(name, 'w'))
        # generic.dump(self, file(nom, 'w'))


def createTreeFromXML(name):
    return generic.load(file(name, 'r'))

nodeUnknown = EWNodeTree("Node unknown")

if __name__ == '__main__':
    dbg.on = 1      # true
    path = r"\\Madawaska\compil\dev"
    tree = EWNodeTree()

    dbg.printd("--- EW_Tree - Read Path")
    dbg.level = dbg.level + 1
    tree.readTree(path)
    dbg.level = dbg.level - 1
    dbg.printd("")

    dbg.printd("--- EW_Tree - Link nodes")
    tree.linkNodes()
    dbg.printd("")

#    tree.writeXML("a.xml")
