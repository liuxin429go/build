# -*- coding: UTF-8 -*-
import ExternalLib

class version(ExternalLib.ExternalLib):
    def __unx(self, ctx):
        pass

    def __win(self, ctx):
        __lpt = [ ]
        __lib = ['version']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)


    def __unx32(self, ctx):
        self.__unx(ctx)

    def __unx64(self, ctx):
        self.__unx(ctx)

    def __unx64i8(self, ctx):
        self.__unx(ctx)

    def __win32(self, ctx):
        self.__win(ctx)

    def __win64(self, ctx):
        self.__win(ctx)

    def __win64i8(self, ctx):
        self.__win(ctx)

    def configure(self, ctx, tool, version, plateform):
        fnm = '_version__%s' % (plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
