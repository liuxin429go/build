# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#-----------------------------------------------------------------------------------------------------

import MUC_platform

import os
import shutil
import subprocess
import tempfile

ProgramFiles = ''
try:
    ProgramFiles = os.environ['ProgramFiles(x86)']
except:
    try:
        ProgramFiles = os.environ['ProgramFiles']
    except:
        pass

def pJoin(*args):
    res = ''
    for a in args:
        if (a[ 0] == '"' or a[ 0] == "'"): a = a[1:]
        if (a[-1] == '"' or a[-1] == "'"): a = a[:-1]
        res = os.path.join(res, a)
    return '"%s"' % os.path.normpath(res)

"""
Intel - Configuration
"""
class cfg_c:
    def __init__(self, v = ''):
        self.version = v

    def getCfg(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        v = int( float(self.version) + 0.5) if self.version else -1 # nint de la version
        if (p == 'win32' or p == 'win64'):
            flags_bse = [
                        '-c',
                        '-nologo',
                        '-W3',                  # print only errors and warnings (DEFAULT)
                        '-Wp64',                # print diagnostics for 64-bit porting
                        '-Zp16',                # specify alignment constraint for structures
                        '-EHsc',                # enable synchronous C++ exception handling model / assume extern "C" functions do not throw exceptions
                        '-Zc:wchar_t',          # specify that wchar_t is a native data type
                        '-Zc:forScope',         # specify standard conformance
                        '-GR',                  # enable C++ RTTI
                        ]
            flags_dbg = [
                        '-Od',      # disable optimizations
                        '-Ob0',     # disables inlining
                        '-Zi',      # produce symbolic debug information in object file
                        '-RTCsu',   # enable runtime checks
                        '-GS',      # generates code that detects some buffer overruns
                        '-Gs0',     # Disables stack-checking for (almost) all routines
                        '-Ge',      # Deprecated
                        ]
            flags_ssa = flags_dbg + \
                        [
                        '-Qdiag-enable:sc-full',    # Static analysis - all diagnostics
                        '-Qdiag-enable:sc3',        # Static analysis - all diagnostics
                        ]
            flags_rel = [
                        '-O2',       # optimize for maximum speed
                        #'-O3',       # optimize for maximum speed and enable high-level optimizations
                        '-Ob1',      # inline functions declared with __inline, and perform C++ inlining
                        #'-Ob2',      # inline any function, at the compiler's discretion
                        #'-QxW',      # Intel Pentium 4 and compatible Intel processors
                        '-Qip',      # enable single-file IP optimizations
                        #'-Qipo',     # additional interprocedural optimizations for multi-file compilation
                        '-Qopenmp',  # enable the compiler to generate multi-threaded code based on the OpenMP directives
                        '-Qparallel',# enable the auto-parallelizer
                        '-Zi',       # produce symbolic debug information in object file
                        ]
            flags_cfg = {
                        'debug'     : flags_dbg,
                        'intel_ssa' : flags_ssa,
                        'release'   : flags_rel
                        }
        elif (p == 'unx32' or p == 'unx64'):
            flags_bse = [
                        '-c',
                        '-std=c++17',           # for unordered_map
                        '-w1',                  # print only errors and warnings (DEFAULT)
                        '-Wp64',                # print diagnostics for 64-bit porting
                        '-Zp16',                # specify alignment constraint for structures
#                        '-EHsc',                # enable synchronous C++ exception handling model / assume extern "C" functions do not throw exceptions
#                        '-Zc:wchar_t',          # specify that wchar_t is a native data type
#                        '-Zc:forScope',         # specify standard conformance
                         # C++ RTTI enabled par défaut
#                        '-Gd',                  # make __cdecl the default calling convention
                        '-no-multibyte-chars',
                        ]
            flags_cfg = {
                        'debug'   : [
                                    '-O0',                   # disable optimizations
                                    '-inline-level=0',       # disables inlining
                                    '-g',                    # produce symbolic debug information in object file
                                    '-check=uninit',         # check for uninitialized variables
                                    '-fstack-security-check',# enable overflow security checks
                                    ],
                        'release' : [
                                    '-O3',               # optimize for maximum speed and enable high-level optimizations
                                    '-inline-level=2',   # inline any function, at the compiler's discretion
                                    '-ip',               # enable single-file IP optimizations
                                    '-qopenmp' if (v >= 16) else '-openmp', # enable the compiler to generate multi-threaded code based on the OpenMP directives
                                    '-parallel',         # enable the auto-parallelizer
                                    '-g',                # produce symbolic debug information in object file
                                    '-debug full',       # generate full debugging information
#                                    '-debug parallel',   # allow
#                                    '-xhost',           # Optimize for host processor
                                    ]
                        }
        else:
            raise ValueError('Invalid Intel platform: %s' % p)
        return flags_bse + flags_cfg[c]

    def getLnk(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        if   (p == 'win32' or p == 'win64'):
            flags_lnk = {
                         'debug' :
                           {                             #  (cf. note)
                              'static' :  [ '-MT' ],     # no debug mode: dll
                              'dynamic' : [ '-MD' ],     # are not redist.
                           },
                         'release' :
                           {                             #  (cf. note)
                              'static' :  [ '-MT' ],     # statically-loaded, multithread C runtime
                              'dynamic' : [ '-MD' ],     # dynamically-loaded, multithread C runtime
                           },
                        }
        elif (p == 'unx32' or p == 'unx64'):
            flags_lnk = {
                         'debug' :
                           {
                              'static' :  [ '-static', '-static-intel' ],
                              'dynamic' : [ '-fpic', '-shared' ]
                           },
                         'release' :
                           {
                              'static' :  [ '-static', '-static-intel' ],
                              'dynamic' : [ '-fpic', '-shared' ]
                           },
                        }
        else:
            raise ValueError('Invalid Intel platform: %s' % p)
        return flags_lnk[c][l]

    def getPtf(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_ptf = {
                    'unx32' : ['-m32'],
                    'unx64' : ['-m64'],
                    'win32' : [],
                    'win64' : [],
                    }
        return flags_ptf[p]

    def getIsz(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_isz = {
                    'unx32' : { 4 : [''], 8 : ['-DMKL_ILP64'] },
                    'unx64' : { 4 : [''], 8 : ['-DMKL_ILP64'] },
                    'win32' : { 4 : [''], 8 : ['-DMKL_ILP64'] },
                    'win64' : { 4 : [''], 8 : ['-DMKL_ILP64'] },
                    }
        return flags_isz[p][i]

    def c_cfg(self, cfg):
        return cfg_c.getCfg(self, cfg) + cfg_c.getLnk(self, cfg) + cfg_c.getPtf(self, cfg) + cfg_c.getIsz(self, cfg)

class cfg_cpp(cfg_c):
    def __init__(self, v = ''):
        cfg_c.__init__(self, v)

    def getCfg(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_bse = [
                    '-TP'
                    ]
        flags_cfg = { 'debug' : [], 'release' : [] }
        if (p == 'unx32'):
            # patch pour bug de compatibilité avec gcc4.2 pour le link, voir http://softwarecommunity.intel.com/isn/Community/en-US/forums/thread/30240835.aspx
            flags_bse.append('-D"__sync_fetch_and_add(ptr,addend)=_InterlockedExchangeAdd(const_cast<void*>(reinterpret_cast<volatile void*>(ptr)), addend);"')
        return cfg_c.getCfg(self, cfg) + flags_bse + flags_cfg[c]

    def getPtf(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_ptf = {
                    'unx32' : [],
                    'unx64' : [],
                    'win32' : [],
                    'win64' : [],
                    }
        return cfg_c.getPtf(self, cfg) + flags_ptf[p]

    def cpp_cfg(self, cfg):
        return cfg_cpp.getCfg(self, cfg) + cfg_cpp.getLnk(self, cfg) + cfg_cpp.getPtf(self, cfg) + cfg_cpp.getIsz(self, cfg)

class cfg_ftn:
    def __init__(self, v = ''):
        self.version = v

    def getCfg(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        v = int( float(self.version) + 0.5) if self.version else -1 # nint de la version
        if (p == 'win32' or p == 'win64'):
            flags_bse = [
                        '-c',
                        '-nologo',
#                        '-preprocess-only',
                        '-fpp',                         # Runs the Fortran preprocessor on source files before compilation
                        '-auto',                        # Causes all local, non-SAVEd variables to be allocated to the run-time stack
                        '-names:uppercase',             # Specifies how source code identifiers and external names are interpreted
                        '-noaltparam',                  # Allows alternate syntax (without parentheses) for PARAMETER statements
#                        '/Qpc64',                      # Enables control of floating-point significant precision
                        '-extend-source:72',            # Specifies the length of the statement field in a fixed-form source file
                        '-warn:all',                    # Specifies diagnostic messages to be issued by the compiler
                        '-warn:nounused',
                        '-warn:notruncated_source',
                        '-warn:nointerface' if (v == 14) else '',
                        '-module:"%s"' % cfg.mod_dir,
                        '-WB',                          # Turns a compile-time bounds check into a warning
                        '-align:rec16byte',             # Tells the compiler how to align certain data items
                        '-align:array64byte',           # Tells the compiler how to align certain data items
                        ]
            flags_dbg = [
                        '-d-lines',         # Compiles debug statements
                        '-Zi',              # generate full debugging information in the object file
                        '-debug:full',      # generate full debugging information
                        '-Od',              # Disables all optimizations
                        '-Ob0',             # level of inline function expansion
                        '-check:all',       # Runtime Error Checking
                        '-check:nobounds',  # Do not Check Array and String Bounds
                        '-traceback',       # Generate extra information in the object file to provide source file traceback
                        '-Qtrapuv',         # Initializes stack local variables to an unusual value to aid error detection
                        '-Qfp-stack-check', # Generate extra code after every function call to check the floating-point stack
                        '-GS',              # generates code that detects some buffer overruns
                        ##'-Gs0',             # Disables stack-checking for (almost) all routines
                        '-Gs',
                        '-Ge',              # Deprecated
                        '-Qopenmp',         # generate multi-threaded code based on the OpenMP* directives
                        '-Qopenmp-simd',    # generate multi-threaded code based on the OpenMP* directives
                        ]
            flags_ssa = flags_dbg + \
                        [
                        '-Qdiag-enable:sc-full',    # Static analysis - all diagnostics
                        '-Qdiag-enable:sc3',        # Static analysis - all diagnostics
                        ]
            flags_rel = [
                        '-O3',              # more aggressive optimizations
                        '-Ob2',             # level of inline function expansion
                        '-Qip',             # additional interprocedural optimizations for single-file compilation
                        #'-Qipo',            # additional interprocedural optimizations for multi-file compilation
                        '-nocheck',         # Disable Runtime Error Checking
                        '-Qopenmp',         # generate multi-threaded code based on the OpenMP* directives
                        '-Qopenmp-simd',    # generate multi-threaded code based on the OpenMP* directives
                        '-Qparallel',       # generate multi-threaded code for loops that can be safely executed in parallel
                        '-Qopt-report:3',   #
                        '-Zi',              # generate full debugging information in the object file
                        '-debug:full',      # generate full debugging information
#                       '-debug:parallel',  # allow
                        '-traceback',       # generate extra information in the object file to provide source file traceback
#                       '-Qxhost',          # generate optimized code specialized for the Intel processor
#                       '-QaxSSE4.2,SSE4.2,SSSE3',       # Multiple code path
#                       '-QxSSE4.2',        # Compatible Intel processors with SSE up to SSE4.2
                        ]
            flags_cfg = {
                        'debug'     : flags_dbg,
                        'intel_ssa' : flags_ssa,
                        'release'   : flags_rel,
                        }
        elif (p == 'unx32' or p == 'unx64'):
            flags_bse = [
                        '-c',
                        '-fpp',                         # Runs the Fortran preprocessor on source files before compilation
                        '-auto',                        # Causes all local, non-SAVEd variables to be allocated to the run-time stack
                        '-noaltparam',                  # Allows alternate syntax (without parentheses) for PARAMETER statements
    #                    '-pc64',
                        '-extend-source 72',            # Specifies the length of the statement field in a fixed-form source file
                        '-warn all',                    # Specifies diagnostic messages to be issued by the compiler
                        '-warn nousage',
                        '-warn nounused',
                        '-warn notruncated_source',
                        '-warn nointerface' if (v == 14) else '',
#                        '-w90',                         # Suppress messages about use of non-standard Fortran
#                        '-w95',
                        '-WB',
                        '-module ' + cfg.mod_dir,
                        '-align rec16byte',             # Tells the compiler how to align certain data items
                        '-align array64byte',           # Tells the compiler how to align certain data items
                        ]
            flags_cfg = {
                        'debug'   : [
                                    '-d_lines',
                                    '-g',
                                    '-O0',
                                    '-inline-level=0',
                                    '-check all',
                                    '-check nobounds',
                                    ],
                        'release' : [
                                    '-O3',
                                    '-inline-level=2',
                                    '-ip' if (v != 19) else '',
                                    '-nocheck',
                                    '-qopenmp' if (v >= 16) else '-openmp',
                                    '-parallel',
                                    '-g',
#                                    '-xhost',         # optimize for host'
#                                    '-ftz',           # flush denormalized to zero
#                                    '-fno-alias',     # no aliasing
#                                    '-no-prec-div',   # no precise division
                                    ],
                        }
        else:
            raise ValueError('Invalid Intel platform: %s' % p)
        return flags_bse + flags_cfg[c]

    def getLnk(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        if   (p == 'win32' or p == 'win64'):
            flags_lnk = {
                         'debug' :
                           {                            #  (cf. note)
                              'static' :  [ '-MT' ],    # no debug mode: dll
                              'dynamic' : [ '-MD' ],    # are not redist.
                           },
                         'release' :
                           {                            #  (cf. note)
                              'static' :  [ '-MT' ],    # statically-loaded, multithread C runtime
                              'dynamic' : [ '-MD' ],    # dynamically-loaded, multithread C runtime
                           },
                        }
        elif (p == 'unx32' or p == 'unx64'):
            flags_lnk = {
                         'debug' :
                           {
                              'static' :  [ '-static', '-static-intel' ],
                              'dynamic' : [ '-fpic', '-shared' ]
                           },
                         'release' :
                           {
                              'static' :  [ '-static', '-static-intel' ],
                              'dynamic' : [ '-fpic', '-shared' ]
                           },
                        }
        else:
            raise ValueError('Invalid Intel platform: %s' % p)
        return flags_lnk[c][l]

    def getPtf(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_ptf = {
                    'unx32' : [ '-m32' ],
                    'unx64' : [ '-m64' ],
                    'win32' : [],
                    'win64' : [],
                    }
        return flags_ptf[p]

    def getIsz(self, cfg):
        c, l, p, i = cfg.tgt_cfg, cfg.tgt_lnk, cfg.tgt_ptf, cfg.tgt_isz
        flags_isz = {
                    'unx32' : { 4 : [ '-i4'], 8 : [ '-i8'] },
                    'unx64' : { 4 : [ '-i4'], 8 : [ '-i8'] },
                    'win32' : { 4 : ['-4I4'], 8 : ['-4I8'] },
                    'win64' : { 4 : ['-4I4'], 8 : ['-4I8'] },
                    }
        return flags_isz[p][i]

    def ftn_cfg(self, cfg):
        return cfg_ftn.getCfg(self, cfg) + cfg_ftn.getLnk(self, cfg) + cfg_ftn.getPtf(self, cfg) + cfg_ftn.getIsz(self, cfg)

"""
Intel command launcher
"""
class intel_launcher:
    def __win(self, setup, cmplr, cfg):
        if (type(setup) == type('')):
            __setup = setup
            __parms = ''
        else:
            __setup = setup[0]
            __parms = setup[1]

        cfg.bldResponseFiles()
        s = None if shutil.which(cmplr) else '%s %s > NUL' % (__setup, __parms)
        c = '%s -Fo%s @%s @%s @%s %s' % (cmplr, pJoin(cfg.fic_out), cfg.cpl_fcfg, cfg.cpl_finc, cfg.cpl_fdfn, pJoin(cfg.fic_inp))

        if (cfg.dmp_cmd):
            d = '%s -Fo%s %s %s %s %s' % (cmplr, pJoin(cfg.fic_out), ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn), pJoin(cfg.fic_inp))
            print('--- Compiling on %s for %s(i%s)' % (MUC_platform.build_platform(), cfg.tgt_ptf, cfg.tgt_isz))
            if s: print(s)
            if d: print(d)
            print('-------')

        _stderr = None
        _stdout = None
        _shell  = False
        if (cfg.fic_err):
            _stdout = open(cfg.fic_err, 'w')
            _stderr = subprocess.STDOUT

        retcode = subprocess.call(' && '.join( [s, c] ) if s else c, stdout=_stdout, stderr=_stderr, shell=_shell)

    def __lnx(self, setup, cmplr, cfg):
        if (type(setup) == type('')):
            __setup = setup
            __parms = ''
        else:
            __setup = setup[0]
            __parms = setup[1]

        s = '%s %s' % (__setup, __parms)
        c = '%s -o%s %s %s %s %s' % (cmplr, pJoin(cfg.fic_out), ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn), pJoin(cfg.fic_inp))

        _stderr = None
        _stdout = None
        _shell  = True
        if (cfg.fic_err):
            _stdout = open(cfg.fic_err, 'w')
            _stderr = subprocess.STDOUT

        fd, fc = tempfile.mkstemp('.sh', '~muc_', None, 'w+t')
        f = os.fdopen(fd, "w+t")
        f.write('#!/bin/bash\n')
        f.write('. %s $1\n' % s)
        f.write('%s\n' % c)
        f.close()
        os.chmod(fc, 0o750)

        if (cfg.dmp_cmd):
            print('--- Compiling on %s for %s(i%s)' % (MUC_platform.build_platform(), cfg.tgt_ptf, cfg.tgt_isz))
            print(s)
            print(c)
            print('-------')
        retcode = subprocess.call(' '.join( [fc, __parms] ), stdout=_stdout, stderr=_stderr, shell=_shell)
        if fc: os.remove(fc)

    def c(self, cfg):
        setup, cmplr = self.getcmds_c(cfg)
        if (MUC_platform.is_win()):
            self.__win(setup, cmplr, cfg)
        else:
            self.__lnx(setup, cmplr, cfg)

    def cpp(self, cfg):
        setup, cmplr = self.getcmds_cpp(cfg)
        if (MUC_platform.is_win()):
            self.__win(setup, cmplr, cfg)
        else:
            self.__lnx(setup, cmplr, cfg)

    def ftn(self, cfg):
        setup, cmplr = self.getcmds_ftn(cfg)
        if (MUC_platform.is_win()):
            self.__win(setup, cmplr, cfg)
        else:
            self.__lnx(setup, cmplr, cfg)


"""
Intel generique
"""
class intel(intel_launcher, cfg_cpp, cfg_ftn):
    def __init__(self, v = '', s = ''):
        #super(intel, self).__init__(v)
        cfg_cpp.__init__(self, v)
        cfg_ftn.__init__(self, v)
        self.version = v
        self.subver  = s

"""
Intel 9
"""
class intel_9(intel):
    def __init__(self, v = '9.0', s = ''):
       intel.__init__(self, v, s)

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'IA32',
                  ('win32','win64') : 'EM64T',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx32','unx64') : 'em64t',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]


    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel\Compiler\C++', self.version, self.__itl_ptf(cfg), 'bin')
            setup = pJoin(bin_dir, 'iclvars.bat')
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            bin_dir = pJoin('/opt/intel/cc', '.'.join(self.version, self.subver), 'bin')
            setup = pJoin(bin_dir, 'iccvars.sh')
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel\Compiler\C++', self.version, self.__itl_ptf(cfg), 'bin')
            setup = pJoin(bin_dir, 'iclvars.bat')
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            bin_dir = pJoin('/opt/intel/cc', '.'.join(self.version, self.subver), 'bin')
            setup = pJoin(bin_dir, 'iccvars.sh')
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel\Compiler\Fortran', self.version, self.__itl_ptf(cfg), 'bin')
            setup = pJoin(bin_dir, 'ifortvars.bat')
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            bin_dir = pJoin('/opt/intel/fc', '.'.join(self.version, self.subver), 'bin')
            setup = pJoin(bin_dir, 'ifortvars.sh')
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

"""
Intel 10
"""
class intel_10(intel_9):
    def __init__(self, s = ''):
       intel_9.__init__(self, '10.0', s)

class intel_101015(intel_10):
    def __init__(self):
       intel_10.__init__(self, '015')

"""
Intel C++ 11.0
"""
class intel_110(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '11.0', s)
       self.tgtptf  = 'ia32'

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Compiler', self.version, self.subver, r'cpp/bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            bin_dir = pJoin(r'/opt/intel/Compiler/', self.version, self.subver, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Compiler', self.version, self.subver, r'cpp/bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            bin_dir = pJoin(r'/opt/intel/Compiler/', self.version, self.subver, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Compiler', self.version, self.subver, r'fortran/bin')
            setup = [ pJoin(bin_dir, 'ifortvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            bin_dir = pJoin(r'/opt/intel/Compiler/', self.version, self.subver, r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_110061(intel_110):
    def __init__(self):
       intel_110.__init__(self, '061')

class intel_110066(intel_110):
    def __init__(self):
       intel_110.__init__(self, '066')

class intel_110069(intel_110):
    def __init__(self):
       intel_110.__init__(self, '069')

class intel_110072(intel_110):
    def __init__(self):
       intel_110.__init__(self, '072')

class intel_110074(intel_110):
    def __init__(self):
       intel_110.__init__(self, '074')

"""
Intel 11.1
"""
class intel_111(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '11.1', s)

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win64','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx64','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Compiler', self.version, self.subver, r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            bin_dir = pJoin(r'/opt/intel/Compiler/', self.version, self.subver, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Compiler', self.version, self.subver, r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            bin_dir = pJoin(r'/opt/intel/Compiler/', self.version, self.subver, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Compiler', self.version, self.subver, r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            bin_dir = pJoin(r'/opt/intel/Compiler/', self.version, self.subver, r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_111046(intel_111):
    def __init__(self):
       intel_111.__init__(self, '046')

class intel_11106(intel_111):
    def __init__(self):
       intel_111.__init__(self, '060')

class intel_111060(intel_111):
    def __init__(self):
       intel_111.__init__(self, '060')

class intel_111065(intel_111):
    def __init__(self):
       intel_111.__init__(self, '065')

class intel_111069(intel_111):
    def __init__(self):
       intel_111.__init__(self, '069')

class intel_111072(intel_111):
    def __init__(self):
       intel_111.__init__(self, '072')

"""
Intel 12.0
"""
class intel_120(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '12.0', s)

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win64','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx64','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/ComposerXE-2011', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [self.version, self.subver] )
            ver_dir = '-'.join( [r'compilerpro', ver_dir] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/ComposerXE-2011', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [self.version, self.subver] )
            ver_dir = '-'.join( [r'compilerpro', ver_dir] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/ComposerXE-2011', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [self.version, self.subver] )
            ver_dir = '-'.join( [r'compilerpro', ver_dir] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_1200(intel_120):
    def __init__(self):
       intel_120.__init__(self, '0')

class intel_1200084(intel_120):
    def __init__(self):
       intel_120.__init__(self, '0.084')

class intel_1203174(intel_120):
    def __init__(self):
       intel_120.__init__(self, '3.174')

class intel_1203175(intel_120):
    def __init__(self):
       intel_120.__init__(self, '3.175')

"""
Intel 14.0
"""
class intel_140(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '14.0', s)

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win64','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx64','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Composer XE 2013 SP1', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'composer_xe_2013_sp1', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Composer XE 2013 SP1', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'composer_xe_2013_sp1', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Composer XE 2013 SP1', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'composer_xe_2013_sp1', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_1400(intel_140):
    def __init__(self):
       intel_140.__init__(self, '0')

class intel_1402144(intel_140):
    def __init__(self):
       intel_140.__init__(self, '2.144')

class intel_1402176(intel_140):
    def __init__(self):
       intel_140.__init__(self, '2.176')

class intel_1403174(intel_140):
    def __init__(self):
       intel_140.__init__(self, '3.174')

class intel_1403202(intel_140):
    def __init__(self):
       intel_140.__init__(self, '3.202')

class intel_1404211(intel_140):
    def __init__(self):
       intel_140.__init__(self, '4.211')

class intel_1404237(intel_140):
    def __init__(self):
       intel_140.__init__(self, '4.237')

"""
Intel 15.0
"""
class intel_150(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '15.0', s)

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win64','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx64','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Composer XE 2015', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'composer_xe_2015', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Composer XE 2015', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'composer_xe_2015', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            bin_dir = pJoin(ProgramFiles, r'Intel/Composer XE 2015', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'composer_xe_2015', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_1500(intel_150):
    def __init__(self):
       intel_150.__init__(self, '0')

class intel_1501133(intel_150):
    def __init__(self):
       intel_150.__init__(self, '1.133')

class intel_1502179(intel_150):
    def __init__(self):
       intel_150.__init__(self, '2.179')

class intel_1503208(intel_150):
    def __init__(self):
       intel_150.__init__(self, '3.208')

class intel_1504221(intel_150):
    def __init__(self):
       intel_150.__init__(self, '4.221')

class intel_1506285(intel_150):
    def __init__(self):
       intel_150.__init__(self, '6.285')

"""
Intel 16.0
"""
class intel_160(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '16.0', s)

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win64','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx64','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2016', self.subver] )
            bin_dir = pJoin(ProgramFiles, r'Intel', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2016', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2016', self.subver] )
            bin_dir = pJoin(ProgramFiles, r'Intel', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2016', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2016', self.subver] )
            bin_dir = pJoin(ProgramFiles, r'Intel', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2016', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_1600(intel_160):
    def __init__(self):
       intel_160.__init__(self, '0')

class intel_1601150(intel_160):
    def __init__(self):
       intel_160.__init__(self, '1.150')

class intel_1603207(intel_160):
    def __init__(self):
       intel_160.__init__(self, '3.207')

class intel_1603210(intel_160):
    def __init__(self):
       intel_160.__init__(self, '3.210')

class intel_1604246(intel_160):
    def __init__(self):
       intel_160.__init__(self, '4.246')



"""
Intel 17.0
"""
class intel_170(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '17.0', s)

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win64','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx64','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2017', self.subver] )
            bin_dir = pJoin(ProgramFiles, r'IntelSWTools', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2017', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2017', self.subver] )
            bin_dir = pJoin(ProgramFiles, r'IntelSWTools', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2017', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2017', self.subver] )
            bin_dir = pJoin(ProgramFiles, r'IntelSWTools', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2017', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_1700(intel_170):
    def __init__(self):
       intel_170.__init__(self, '0')

class intel_1702187(intel_170):
    def __init__(self):
       intel_170.__init__(self, '2.187')

class intel_1704210(intel_170):
    def __init__(self):
       intel_170.__init__(self, '4.210')

class intel_1705267(intel_170):
    def __init__(self):
       intel_170.__init__(self, '5.267')

class intel_1706270(intel_170):
    def __init__(self):
       intel_170.__init__(self, '6.270')

class intel_1707272(intel_170):
    def __init__(self):
       intel_170.__init__(self, '7.272')


"""
Intel 18.0
"""
class intel_180(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '18.0', s)

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win64','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx64','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2018', self.subver] )
            bin_dir = pJoin(ProgramFiles, r'IntelSWTools', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2018', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2018', self.subver] )
            bin_dir = pJoin(ProgramFiles, r'IntelSWTools', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2018', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2018', self.subver] )
            bin_dir = pJoin(ProgramFiles, r'IntelSWTools', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [r'compilers_and_libraries_2018', self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_1800(intel_180):
    def __init__(self):
       intel_180.__init__(self, '0')

class intel_1802185(intel_180):
    def __init__(self):
       intel_180.__init__(self, '2.185')

class intel_1802199(intel_180):
    def __init__(self):
       intel_180.__init__(self, '2.199')

class intel_1803210(intel_180):
    def __init__(self):
       intel_180.__init__(self, '3.210')

class intel_1803222(intel_180):
    def __init__(self):
       intel_180.__init__(self, '3.222')


"""
Intel 19.0
"""
class intel_190(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '19.0', s)
       self.clwin = r'compilers_and_libraries_2019'
       self.clunx = r'compilers_and_libraries_2019'

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win64','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx64','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [self.clwin, self.subver] )
            bin_dir = pJoin(ProgramFiles, r'IntelSWTools', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [self.clunx, self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [self.clwin, self.subver] )
            bin_dir = pJoin(ProgramFiles, r'IntelSWTools', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'iclvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [self.clunx, self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [self.clwin, self.subver] )
            bin_dir = pJoin(ProgramFiles, r'IntelSWTools', ver_dir, r'windows', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [self.clunx, self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_1900(intel_190):
    def __init__(self):
       intel_190.__init__(self, '0')

class intel_1901144(intel_190):
    def __init__(self):
       intel_190.__init__(self, '1.144')

class intel_1902190(intel_190):
    def __init__(self):
       intel_190.__init__(self, '2.190')

class intel_1903203(intel_190):
    def __init__(self):
       intel_190.__init__(self, '3.203')

class intel_1904243(intel_190):
    def __init__(self):
       intel_190.__init__(self, '4.243')

class intel_1904245(intel_190):
    def __init__(self):
       intel_190.__init__(self, '4.245')

class intel_1905281(intel_190):
    def __init__(self):
       intel_190.__init__(self, '5.281')

"""
Intel 19.1
"""
class intel_191(intel_190):
    """ Héritage fonctionnel, on bypass __init__ de 190"""
    def __init__(self, s=''):
       intel.__init__(self, '19.1', s)
       self.clwin = r'compilers_and_libraries_2020'
       self.clunx = r'compilers_and_libraries_2020'

class intel_191254(intel_191):
    def __init__(self):
       intel_191.__init__(self, '254')

class intel_191304(intel_191):
    def __init__(self):
       intel_191.__init__(self, '304')

class intel_191311(intel_191):
    def __init__(self):
       intel_191.__init__(self, '311')

"""
Intel 19.2
"""
class intel_192(intel):
    def __init__(self, s = ''):
       intel.__init__(self, '19.2', s)
       self.clwin = r'2021'
       self.clunx = r'2021'

    def __itl_ptf(self, cfg):
        itl_ptf = {
                  ('win32','win32') : 'ia32',
                  ('win64','win32') : 'ia32',
                  ('win32','win64') : 'ia32_intel64',
                  ('win64','win64') : 'intel64',
                  ('unx32','unx32') : 'ia32',
                  ('unx64','unx32') : 'ia32',
                  ('unx32','unx64') : 'ia32_intel64',
                  ('unx64','unx64') : 'intel64',
                  }
        return itl_ptf[ (MUC_platform.build_platform(), cfg.tgt_ptf) ]

    def getcmds_c(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [self.clwin, self.subver] )
            bin_dir = pJoin(ProgramFiles, 'Intel', 'oneAPI', 'compiler', ver_dir, 'env')
            setup = [ pJoin(bin_dir, 'vars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [self.clunx, self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_cpp(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [self.clwin, self.subver] )
            bin_dir = pJoin(ProgramFiles, 'Intel', 'oneAPI', 'compiler', ver_dir, 'env')
            setup = [ pJoin(bin_dir, 'vars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'icl.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [self.clunx, self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'iccvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'icpc'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

    def getcmds_ftn(self, cfg):
        if (MUC_platform.is_win()):
            ver_dir = '.'.join( [self.clwin, self.subver] )
            bin_dir = pJoin(ProgramFiles, 'Intel', 'oneAPI', 'compiler', ver_dir, 'env')
            setup = [ pJoin(bin_dir, 'vars.bat'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort.exe'
        elif (MUC_platform.is_unx()):
            ver_dir = '.'.join( [self.clunx, self.subver] )
            bin_dir = pJoin(r'/opt/intel', ver_dir, r'linux', r'bin')
            setup = [ pJoin(bin_dir, 'ifortvars.sh'), self.__itl_ptf(cfg) ]
            cmplr = 'ifort'
        else:
            raise ValueError('Invalid Intel build platform: %s' % MUC_platform.build_platform())
        return setup, cmplr

class intel_192616(intel_192):
    def __init__(self):
       super(intel_192616, self).__init__('616')
