# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#-----------------------------------------------------------------------------------------------------

import MultiCompiler_gcc

gccx86_xx = MultiCompiler_gcc.gcc_xx

gccx86_42 = MultiCompiler_gcc.gcc_42
gccx86_43 = MultiCompiler_gcc.gcc_43
gccx86_44 = MultiCompiler_gcc.gcc_44
gccx86_45 = MultiCompiler_gcc.gcc_45
gccx86_46 = MultiCompiler_gcc.gcc_46

gccx86_9  = MultiCompiler_gcc.gcc_9
gccx86_91 = MultiCompiler_gcc.gcc_91
gccx86_92 = MultiCompiler_gcc.gcc_92
gccx86_93 = MultiCompiler_gcc.gcc_93
