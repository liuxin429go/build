#! /bin/bash

# ---  Input param
PTH_PXD=$1
if [ -z $2 ]; then DIR_OUT="."; else DIR_OUT=$PWD/$2; fi
if [ -z $3 ]; then DIR_BLD="__cython-build"; else DIR_BLD=$PWD/$3; fi

# ---  Transform .pxd in .setup.py
DIR_INP=$(dirname  "$1")
FIC_PXD=$(basename "$1")
DIR_BAT=$(dirname  "$0")

# ---  Compile to pyd
pushd $DIR_INP > /dev/null
# cython 0.28.2
# cython-c-in-temp pyrex-c-in-temp font que la cython ne prend pas
# en compte le fichier .pxd et la compilation n'est pas optimisée.
CYTHON_CMD="python ${DIR_BAT}/h2d2_cython.py ${FIC_PXD} build_ext --build-lib=${DIR_OUT} --build-temp=${DIR_BLD}"
echo $CYTHON_CMD
$CYTHON_CMD
popd  > /dev/null
