#!/usr/bin/env python
# -*- coding: UTF-8 -*-
import getopt
import os
import string
import subprocess
import sys

arg_outputFormat = ''
arg_outputSystems = ''
arg_noVersion = False
arg_projRoot = ''

def traite(dir, hasBuildDir):
    hasBuildDir = xeqAction(dir, hasBuildDir)
    hasBuildDir = xeqRecursion(dir, hasBuildDir)
    return hasBuildDir

def xeqRecursion(dir, hasBuildDir):
    for f in os.listdir(dir):
        if os.path.isdir(os.path.join(dir,f)):
            newdir = os.path.join(dir,f)
            newdir = os.path.normpath(newdir)
            hasBuildDir = traite(newdir, hasBuildDir)
    return hasBuildDir

def xeqAction(dir, hasBuildDir):
    global arg_outputFormat
    global arg_outputSystems
    global arg_noVersion
    global arg_projRoot

    prjFound = False
    bldFound = hasBuildDir
    for f in os.listdir(dir):
        if (f == 'prjVisual'): prjFound = True
        if (f == 'build'):     bldFound = True
    if (bldFound): hasBuildDir = True
    if not (bldFound and prjFound): return hasBuildDir

    home = os.environ['INRS_BLD']
    fichierPy = os.path.join(home, 'vc2scons/vc2scons.py')
    fichierPy = os.path.normpath(fichierPy)
    fichierExe = 'python'
    grpName      = os.path.normpath(dir).split(os.sep)[0]
    fichierHnt   = os.path.join(dir,'setup.hint')
    dirPrjVisual = os.path.join(dir,'prjVisual')
    dirBuild     = os.path.join(dir,'build')
    fichierCfg   = os.path.join(dirBuild, 'versions.cfg')
    fichierStp   = os.path.join(dirBuild, 'setup.cfg')
    if 'H2D2-tools' in grpName:
        grpName = 'H2D2-tools'
    elif 'H2D2-Remesh' in grpName:
        grpName = 'H2D2'
    elif 'pyH2D2' in grpName: 
        grpName = 'pyH2D2'
    elif 'H2D2' in grpName:
        grpName = 'H2D2'
    for f in os.listdir(dirPrjVisual):
        ext = os.path.splitext(f)[1]
        ext = ext.lower()
        if ext == '.sln' and f.find('__@@__') < 0:
            fichierInp = os.path.join(dirPrjVisual, f)
            input_project_file  = '-input_project_file=%s' % fichierInp
            if not arg_noVersion:
                input_versions_file = '-input_versions_file=%s' % fichierCfg
            else:
                input_versions_file = ''
            if os.path.exists(fichierStp):
                input_setup_config_file = '-input_setup_config_file=%s' % fichierStp
            else:
                input_setup_config_file = ''
            if arg_outputFormat:
                output_format = "-output_file_name_format='%s'" % arg_outputFormat
            else:
                output_format = ''
            output_dir   = '-output_dir=%s' % dir
            output_format  = "-output_file_name_format='%s'" % arg_outputFormat if arg_outputFormat else ''
            output_systems = '-output_systems=%s' % arg_outputSystems
            output_group_name = '-output_group_name=%s' % grpName
            project_root = '-project_root=%s' % arg_projRoot
            stage_target = '-no_stage_target'
            args = [
                fichierExe,
                fichierPy,
                # '-log-level=debug',
                input_project_file,
                input_versions_file,
                input_setup_config_file,
                output_format,
                output_group_name,
                output_dir,
                output_systems,
                project_root,
                stage_target,
                ]
            print('Traite: %s' % fichierInp)
            retcode = subprocess.run(args)
    return hasBuildDir

def processOptions(opts):
    outFmt = ''
    outSys = ''
    noVersion = False

    for o in opts:
        var = o[0]
        if var == '--no-version':
            noVersion = True
        elif var == '--output_file_name_format':
            outFmt = o[1]
        else:
            outSys = outSys + var[2:] + ';'

    def print_usage():
        print('Options:')
        print('   --setup           Generate a config file for setup')
        print('   --scons           Generate a config file for SCons')
        print('   --no-version      No version sub-directories')
        raise invalid_parameter

    if (outSys== ''): print_usage()

    return outFmt, outSys, noVersion

def main(argv = None):
    global arg_outputFormat
    global arg_outputSystems
    global arg_noVersion
    global arg_projRoot

    if (argv == None): argv = sys.argv
    opts, args = getopt.getopt(argv[1:], '', ['setup', 'scons', 'no-version', 'output_file_name_format='])
    arg_outputFormat, arg_outputSystems, arg_noVersion = processOptions(opts)
    arg_projRoot = os.getcwd()

    for a in args:
        hasBuildDir = False
        traite(a, hasBuildDir)

main()
