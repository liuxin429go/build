# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#-----------------------------------------------------------------------------------------------------

import os
import sys
import tempfile

ProgramFiles = ''
try:
    ProgramFiles = os.environ['ProgramFiles']
except:
    pass

def pJoin(*args):
    res = ''
    for a in args:
        if (a[0] == '"' or a[0] == "'"): a = a[1:-1]
        res = os.path.join(res, a)
    return '"%s"' % os.path.normpath(res)

"""
Open Watcom - Configuration
"""
class cfg_c:
    def getCfg(self, c):
        flags_bse = [
                    '-q',           # Quiet - nologo
#                    '-bc',          # target is a console application
#                    '-bd',          # target is a Dynamic Link Library
                    '-bm',          # target is a multi-threaded environment
                    '-br',          # target uses DLL version of C/C++ run-time library
                    '-bt=nt',       # target for operating system
                    '-e25',         # set error limit number
                    '-wx',          # set warning level to maximum setting
                    '-zp8',         # pack structure members
                    '-6r',          # Pentium register calling conventions
                    '-mf',          # flat memory model
                    ]

        flags_cfg = {
                    'debug'   :
                        [
                        '-d2',      # full symbolic debugging information
                        '-od',      # disable all optimizations
                        ],
                    'profil' :
                        [
                        '-d2',      # full symbolic debugging information
                        '-od',      # disable all optimizations
                        '-et',      # Pentium profiling
                        ],
                    'release' :
                        [
                        '-fpi87',   # in-line 80x87 instructions
                        '-fp6',     # optimize floating-point for Pentium Pro
                        '-d0',      # no debugging information
                        '-oa',      # relax aliasing constraints
                        '-oe',      # expand user functions in-line
                        '-ol+',     # enable loop optimizations with loop unrolling-on
                        '-or',      # reorder instructions for best pipeline usage
                        '-ot',      # favor execution time over code size in optimizations
                        '-ox',      # equivalent to -obmiler -s
                        ]
                    }
        return flags_bse + flags_cfg[c]

    def getPtf(self, p):
        flags_ptf = {
                    'win32' :
                        [
                        ]
                    }
        return flags_ptf[p]

class cfg_cpp(cfg_c):
    def getCfg(self, c):
        flags_bse = [
                    '-xs',          # enable exception handling
                    '-xr',          # enable RTTI
                    ]
        return cfg_c.getCfg(self, c) + flags_bse

    def getPtf(self, p):
        return cfg_c.getPtf(self, p)

class cfg_ftn:
    def getCfg(self, c):
        flags_bse = [
                    '-q',           # Quiet - nologo
                    '-noer',        # No error file
                    '-nor',         # No warning for unreferenced symbols
#                    '-exp',         # Explicit typing required
                    '-mf',          # Flat memory model
                    '-bm',          # Multi-threaded application
                    '-bd',          # Dynamic link library
                    '-te',          # Display diagnostic messages
                    '-au',          # Local variables on stack
                    ]
        flags_cfg = {
                    'debug'   :
                        [
                        '-d2',      # Full debugging
                        '-deb',     # Run-time checking code
                        '-st',      # Stack checking
                        '-for',     # Format type checking
                        '-tr',      # Generate run-time traceback
                        '-bo',      # Generate bounds checking
                        '-od',      # Disable optimization
                        ],
                    'profil' :
                        [
                        ],
                    'release' :
                        [
                        '-6',       # Optimize for Pentium Pro
                        '-fpi87',   # Inline 80x87 instuctions
                        '-fp6',     # Inline Pentium Pro 80387 instructions
                        '-ox',      # Optimization
                        '-ol+',
                        ]
                    }
        return flags_bse  + flags_cfg[c]

    def getPtf(self, p):
        flags_ptf = {
                    'win32' :
                        [
                        ]
                    }
        return flags_ptf[p]

"""
Open Watcom générique
"""
class watcom(cfg_cpp, cfg_ftn):
    """
    Supprime les guillemets doubles si présents
    """
    def __encodeDfnCpp(self, d):
        d = d.strip()
        if (d[0:2].upper() == '-D'):
            p = d[0:2]
            d = d[2:]
            if (d[0] == '"' and d[-1] == '"'): d = d[1:-1]
            d = p + d
        return d
    """
    Supprime les guillemets doubles si présents
    """
    def __encodeDfnFtn(self, d):
        d = d.strip()
        if (d[0:2].upper() == '-D'):
            d = d[2:]
            if (d[0] == '"' and d[-1] == '"'): d = d[1:-1]
            d = '-def=' + d
        return d
    """
    Supprime les guillemets doubles si présents
    Remplace -I par -incp=
    """
    def __encodeIncFtn(self, d):
        d = d.strip()
        if (d[0:2].upper() == '-I'):
            d = d[2:]
            if (d[0] == '"' and d[-1] == '"'): d = d[1:-1]
            if (d.find(' ') != -1): raise 'Include Path with space are not supported: %s' % d
            d = '-incp=' + d
        return d

    def __cpp(self, setup, cmplr, cfg):
        cfg.encode(encodeDfn = self.__encodeDfnCpp)
        cfg.bldResponseFiles()
        s = '%s > nul' % setup
        if (cfg.fic_err):
            c = '%s @%s @%s @%s -fo=%s %s >%s 2>&1' % (cmplr, cfg.cpl_fcfg, cfg.cpl_finc, cfg.cpl_fdfn, os.path.normpath(cfg.fic_out), os.path.normpath(cfg.fic_inp), os.path.normpath(cfg.fic_err))
        else:
            c = '%s @%s @%s @%s -fo=%s %s'          % (cmplr, cfg.cpl_fcfg, cfg.cpl_finc, cfg.cpl_fdfn, os.path.normpath(cfg.fic_out), os.path.normpath(cfg.fic_inp))
        os.system( ' && '.join( [s, c] ) )

    def __ftn(self, setup, cmplr, cfg):
        cfg.encode(encodeDfn = self.__encodeDfnFtn, encodeInc = self.__encodeIncFtn)
        s = '%s > nul' % setup

        # --- Fait compiler vers un fichier tmp pour eviter les problèmes de path avec espaces et -
        objTmp = os.path.join(os.environ['TEMP'], '~muc_wfc.obj')
        if (cfg.fic_err):
            c = "%s -fo=%s %s %s %s %s > %s 2>&1" % (cmplr, objTmp, os.path.normpath(cfg.fic_inp), ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn), os.path.normpath(cfg.fic_err))
        else:
            c = '%s -fo=%s %s %s %s %s'           % (cmplr, objTmp, os.path.normpath(cfg.fic_inp), ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn))
        os.system( ' && '.join( [s, c] ) )

        # --- Transfert le fichier tmp vers la destination
        if (os.stat(objTmp).st_size > 0):
            objOut = os.path.normpath(cfg.fic_out)
            try:
                os.makedirs(os.path.dirname(objOut))
            except (OSError):
                pass
            try:
                os.remove(objOut)
            except (OSError):
                pass
            os.rename(objTmp, objOut)
        else:
            os.remove(objTmp)

    def c(self, setup, cmplr, cfg):
        self.__cpp(setup, cmplr, cfg)

    def cpp(self, setup, cmplr, cfg):
        self.__cpp(setup, cmplr, cfg)

    def ftn(self, setup, cmplr, cfg):
        self.__ftn(setup, cmplr, cfg)

"""
Open Watcom 1.6
"""
class watcom_16(watcom):
    def c(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'WATCOM\binnt')
        setup = pJoin(bin_dir, 'WatcomVars.bat')
        cmplr = 'wcc386.exe'
        watcom.c(self, setup, cmplr, cfg)

    def cpp(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'WATCOM\binnt')
        setup = pJoin(bin_dir, 'WatcomVars.bat')
        cmplr = 'wpp386.exe'
        watcom.cpp(self, setup, cmplr, cfg)

    def ftn(self, cfg):
        bin_dir = pJoin(ProgramFiles, r'WATCOM\binnt')
        setup = pJoin(bin_dir, 'WatcomVars.bat')
        cmplr = 'wfc386.exe'
        watcom.ftn(self, setup, cmplr, cfg)

    def c_cfg(self, cfg):
        return cfg_c.getCfg(self, cfg.tgt_cfg) + cfg_c.getPtf(self, cfg.tgt_ptf)

    def cpp_cfg(self, cfg):
        return cfg_cpp.getCfg(self, cfg.tgt_cfg) + cfg_cpp.getPtf(self, cfg.tgt_ptf)

    def ftn_cfg(self, cfg):
        return cfg_ftn.getCfg(self, cfg.tgt_cfg) + cfg_ftn.getPtf(self, cfg.tgt_ptf)
