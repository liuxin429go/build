@echo off
setlocal EnableExtensions EnableDelayedExpansion

set FIC_PRJ=%1
set TGT_CFG=%2
set TGT_PTF=%3
set FIC_INP=%4
set FIC_OUT=%5

:: ---  Controls
if ["%INRS_DEV%"] == [""] goto Err_INRS_DEV

:: ---  Find Python
set TST_DIR=%HOMEDRIVE%%HOMEPATH%\Miniconda3\
if ["%PYTHON_ROOT%"] == [""] (if exist "%TST_DIR%python.exe" set PYTHON_ROOT=%TST_DIR%)
set TST_DIR=%HOMEDRIVE%%HOMEPATH%\Anaconda3\
if ["%PYTHON_ROOT%"] == [""] (if exist "%TST_DIR%python.exe" set PYTHON_ROOT=%TST_DIR%)
set TST_DIR=%ProgramData%\Miniconda3\
if ["%PYTHON_ROOT%"] == [""] (if exist "%TST_DIR%python.exe" set PYTHON_ROOT=%TST_DIR%)
set TST_DIR=%ProgramData%\Anaconda3\
if ["%PYTHON_ROOT%"] == [""] (if exist "%TST_DIR%python.exe" set PYTHON_ROOT=%TST_DIR%)

:: ---  Load compiler and version
call %INRS_BLD%/Visual_MultiCompiler_cfg.bat %TGT_PTF%

:: ---  Translate x64 to win64
if ["%TGT_PTF%"] == ["x64"] set TGT_PTF=win64
if ["%TGT_PTF%"] == ["X64"] set TGT_PTF=win64

:: ---   Extract package (full-path; remove INRS_xxx; first dir)
for %%f in (%FIC_PRJ%) do ( set FIC_PKG=%%~ff )
set FIC_PKG=!FIC_PKG:%INRS_DEV%=!
for /f "delims=\" %%f in ("%FIC_PKG%") do ( set FIC_PKG=%%f )

:: ---  Mount flags list
set MUC_FLAGS=
set MUC_FLAGS=%MUC_FLAGS% --dumpcmd
::set MUC_FLAGS=%MUC_FLAGS% --fortran-integer-size=8
set MUC_FLAGS=%MUC_FLAGS% --add-config-from-build
set MUC_FLAGS=%MUC_FLAGS% --compiler=%TGT_CPL%-%TGT_CPL_VER%
set MUC_FLAGS=%MUC_FLAGS% --config=%TGT_CFG%
set MUC_FLAGS=%MUC_FLAGS% --platform=%TGT_PTF%
set MUC_FLAGS=%MUC_FLAGS% --output=%FIC_OUT%
set MUC_FLAGS=%MUC_FLAGS% --package=%FIC_PKG%

:: ---  Execute command
echo "%PYTHON_ROOT%python" %INRS_BLD%/MUC/MultiCompiler.py %FIC_INP% %MUC_FLAGS% 
"%PYTHON_ROOT%python" %INRS_BLD%/MUC/MultiCompiler.py %FIC_INP% %MUC_FLAGS% 
goto End:

:Err_INRS_DEV
@echo Environment variable INRS_DEV and INRS_LXT must be defined
goto End:
:Err_PYTHON_ROOT
@echo Environment variable PYTHON_ROOT must be defined
goto End:

:End
endlocal
