# -*- coding: UTF-8 -*-

class ExternalLib:
    def assign(self, __lpt, lpt, __lib, lib, __inc = None, inc = None, __dfn = None, dfn = None):
        try:
            lib.index(__lib[0])
        except:
            if (__lpt): lpt.extend(__lpt)
            if (__lib): lib.extend(__lib)
            if (__inc): inc.extend(__inc)
            if (__dfn): dfn.extend(__dfn)

from . import arpack
from . import boost
from . import blacs
from . import blas
from . import cuda
from . import dl
from . import geos
from . import google_breakpad
from . import mpi
from . import mumps
from . import opencl
from . import pardiso
from . import parmetis
from . import pthread
from . import python
# from . import regex   # scotch 6.0.6
from . import scotch as ptscotch
from . import scotch as scotch
from . import shlwapi
from . import slatec
from . import superlu_sp
from . import version
from . import wininet

import sys
mdl = sys.modules['ExternalLib'].__dict__

"""
Setup external library
"""
import sys
def setupLxt(ctx, env):
    for lib in ctx.lxt:
        libmdl = mdl[lib].__dict__
        cls = libmdl[lib]()
        t = env['MUC_TOOL']
        v = env['MUC_VERSION_FULL']
        try:
            p = env['MUC_TARGETPLATEFORM'] + env['MUC_TARGETPLATEFORMSUFFIX']
        except TypeError:
            p = env['MUC_TARGETPLATEFORM']
        cls.configure(ctx, t, v, p)

