# -*- coding: UTF-8 -*-
import EW_Tree
from EW_Tree import EWNodeTree
from EW_Util import EWHtmlFile


class EWArbreDependanceInverse:
    def __init__(self):
        self.ficHTML = None

    def createTreeRoot(self, root):
        self.ficHTML.write("   var %s = new WebFXTree('%s');" % (root, root))

    def createTreeItems(self, dtNode, node):
        nodeKeys = node.mapNodes.keys()
        nodeKeys.sort()
        for n in nodeKeys:
            var = dtNode + "_" + n
            self.ficHTML.write("   var %s = new WebFXTreeItem('%s');" % (var, n))
        for n in nodeKeys:
            if (node.mapNodes[n] != None):
                var = dtNode + "_" + n
                self.createTreeItems(var, node.mapNodes[n])

    def addNodeToDisplayTree(self, dtNode, node):
        nodeKeys = node.mapNodes.keys()
        nodeKeys.sort()
        for n in nodeKeys:
            if (node.mapNodes[n] != None):
                var = dtNode + "_" + n
                self.addNodeToDisplayTree(var, node.mapNodes[n])
        for n in nodeKeys:
            var = dtNode + "_" + n
            self.ficHTML.write("   %s.add(%s);" % (var, dtNode))

    def writeProlog(self):
        self.ficHTML.write("<script src=\"/js/xtree.js\" language=\"JavaScript\"></script>")
        self.ficHTML.write("<link type=\"text/css\" rel=\"stylesheet\" href=\"/css/xtree.css\">")
        self.ficHTML.write("<style>")
        self.ficHTML.write("   body { background: white; color: black; }")
        self.ficHTML.write("   input { width: 120px; }")
        self.ficHTML.write("</style>")

    def xeq(self, tree):
        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Arbre de dépendance inverse")
        self.writeProlog()
        self.ficHTML.write("<script language=\"JavaScript\">")
        self.ficHTML.write("if (document.getElementById)")
        self.ficHTML.write("{")
        self.createTreeRoot ("Root")
        self.createTreeItems("Root", tree)
        self.ficHTML.write("")
        self.addNodeToDisplayTree("Root", tree)
        self.ficHTML.write("")
        self.ficHTML.write("   document.write(treeRoot);")
        self.ficHTML.write("}")
        self.ficHTML.write("</script>")
        self.ficHTML.writeFooter()


class EWArbreDependanceDirecte:
    def __init__(self):
        self.ficHTML = None

    def createTreeRoot(self, root):
        self.ficHTML.write("   var %s = new WebFXTree('%s');" % (root, root))

    def createTreeItems(self, dtNode, node):
        nodeKeys = node.mapNodes.keys()
        nodeKeys.sort()
        for n in nodeKeys:
            var = dtNode + "_" + n
            self.ficHTML.write("   var %s = new WebFXTreeItem('%s');" % (var, n))
        for n in nodeKeys:
            if (node.mapNodes[n] != None):
                var = dtNode + "_" + n
                self.createTreeItems(var, node.mapNodes[n])

    def addNodeToDisplayTree(self, dtNode, node):
        nodeKeys = node.mapNodes.keys()
        nodeKeys.sort()
        for n in nodeKeys:
            var = dtNode + "_" + n
            self.ficHTML.write("   %s.add(%s);" % (dtNode, var))
        for n in nodeKeys:
            if (node.mapNodes[n] != None):
                var = dtNode + "_" + n
                self.addNodeToDisplayTree(var, node.mapNodes[n])

    def writeProlog(self):
        self.ficHTML.write("<script src=\"/js/xtree.js\" language=\"JavaScript\"></script>")
        self.ficHTML.write("<link type=\"text/css\" rel=\"stylesheet\" href=\"/css/xtree.css\">")
        self.ficHTML.write("<style>")
        self.ficHTML.write("   body { background: white; color: black; }")
        self.ficHTML.write("   input { width: 120px; }")
        self.ficHTML.write("</style>")

    def xeq(self, tree):
        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Arbre de dépendance")
        self.writeProlog()
        self.ficHTML.write("<script language=\"JavaScript\">")
        self.ficHTML.write("if (document.getElementById)")
        self.ficHTML.write("{")
        self.createTreeRoot ("Root")
        self.createTreeItems("Root", tree)
        self.ficHTML.write("")
        self.addNodeToDisplayTree("Root", tree)
        self.ficHTML.write("")
        self.ficHTML.write("   document.write(Root);")
        self.ficHTML.write("}")
        self.ficHTML.write("</script>")
        self.ficHTML.writeFooter()


if __name__ == '__main__':
    path = "DT_20040227-124715.xml"
    tree = EW_Tree.createTreeFromXML(path)
    arbre = EWArbreDependanceDirecte()
    arbre.xeq(tree)

