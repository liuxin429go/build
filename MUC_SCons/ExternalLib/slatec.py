# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import os.path

class slatec(ExternalLib.ExternalLib):
    def __go(self, ctx):
        hdir = os.path.join(os.environ['INRS_LXT'], 'slatec/_run', Util.addBuildPath(r'External'))
        hdir = os.path.normpath(hdir)
        __lpt = [ hdir ]
        __lib = [ 'slatec' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def configure(self, ctx, tool, version, plateform):
        self.__go(ctx)
