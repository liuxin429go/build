# -*- coding: UTF-8 -*-
import os
import string
import time
import sys
from xml.marshal import generic
from xml.dom.ext.reader import Sax2
from xml.dom.ext import PrettyPrint

from EW_Util import dbg

class BTMsg:
    def __init__(self):
        self.type = ""
        self.msg = []

    def lis(self, fic):
        ligne = " "
        while (ligne != "" and ligne.strip() == ""): ligne = fic.readline()
        self.type = self.tueAccents(ligne.strip())

        ligne = fic.readline()
        while (ligne!= "" and ligne.strip() != ""):
            self.msg.append(self.tueAccents(ligne.strip()))
            ligne = fic.readline()

    def tueAccents(self, src):
        dst = ""
        for i in src:
            if   (i == "À"): i = "A"
            elif (i == "Â"): i = "A"
            elif (i == "Ä"): i = "A"
            elif (i == "É"): i = "E"
            elif (i == "È"): i = "E"
            elif (i == "Ê"): i = "E"
            elif (i == "Ë"): i = "E"
            elif (i == "Ç"): i = "C"
            elif (i == "à"): i = "a"
            elif (i == "â"): i = "a"
            elif (i == "ä"): i = "a"
            elif (i == "é"): i = "e"
            elif (i == "è"): i = "e"
            elif (i == "ê"): i = "e"
            elif (i == "ë"): i = "e"
            elif (i == "î"): i = "i"
            elif (i == "ï"): i = "i"
            elif (i == "ô"): i = "o"
            elif (i == "ö"): i = "o"
            elif (i == "ù"): i = "u"
            elif (i == "û"): i = "u"
            elif (i == "ü"): i = "u"
            elif (i == "ç"): i = "c"
            dst = dst + i
        return dst

    def dump(self):
        print self.type
        for m in self.msg:
            print "   - %s" % m

class BTInfoTest:
    def __init__(self):
        self.path = ""
        self.fichier = ""
        self.compil = ""
        self.config = ""
        self.absent = 0
        self.errors = 0
        self.warnings = 0
        self.msgTest = BTMsg()
        self.msgMdlr = BTMsg()

    def lis(self, nomFichier):
        fic = open(nomFichier, "r")

        try:
            self.msgTest.lis(fic)
            self.msgMdlr.lis(fic)
        except:
            pass
        self.msgTest.dump()
        self.msgMdlr.dump()

        if (self.msgTest.type == "Configuration"):
            self.errors = 1
        elif (self.msgTest.type == "Etapes Prealables"):
            self.errors = 1
        elif (self.msgTest.type == "Echec"):
            self.errors = 1
        elif (self.msgTest.type == "Succes"):
            pass
        self.absent = 0
        self.warnings = 0
        fic.close()

    def xtrInfo(self, nomFichier, nomCompilateur, nomConfiguration):
        dbg.printd("Test: %s" % os.path.basename(nomFichier))
        if (os.path.exists(nomFichier)):
            self.lis(nomFichier)
        else:
            self.absent = 1
            self.errors = 0
            self.warnings = 0
        self.path   = nomFichier
        self.fichier= os.path.basename(nomFichier)
        self.compil = nomCompilateur
        self.config = nomConfiguration


class BTInfoTarget:
    def __init__(self):
        self.path = ""
        self.nom = ""
        self.nomComplet = ""
        self.tests = []

    def xtrPourChaqueConfiguration(self, compil, path):
##        config = os.path.basename(path)
        config = "debug"
        dbg.printd("Config: %s" % config)
        dbg.level = dbg.level + 1
##        for f in os.listdir(path):
##            if not estCVS(os.path.join(path,f)):
##                tst = BTInfoTest()
##                tst.xtrInfo(os.path.join(path,f), compil, config)
##                self.tests.append(tst)
        tst = BTInfoTest()
        tst.xtrInfo(os.path.join(path,"fic_log.log"), compil, config)
        self.tests.append(tst)
        dbg.level = dbg.level - 1

    def xtrPourChaqueCompilateur(self, path):
##        compil = os.path.basename(path)
        compil = "msvc"
        dbg.printd("Compiler: %s" % compil)
        dbg.level = dbg.level + 1
##        for f in os.listdir(path):
##            if os.path.isdir(os.path.join(path,f)):
##                self.xtrPourChaqueConfiguration(compil, os.path.join(path,f))
        self.xtrPourChaqueConfiguration(compil, path)
        dbg.level = dbg.level - 1

    def xtrInfo(self, path, nomParent):
        assert estTgt(path), "Not a target directory"
        self.path = path
        self.nom = os.path.basename(path)
        self.nomComplet = string.join([nomParent, self.nom], "@")
        dbg.printd("Target: %s" % self.nom)
        dbg.level = dbg.level + 1
##        for f in os.listdir(self.path):
##            if os.path.isdir(os.path.join(self.path,f)):
##                self.xtrPourChaqueCompilateur(os.path.join(self.path,f))
        self.xtrPourChaqueCompilateur(self.path)
        dbg.level = dbg.level - 1

    def pourChaqueTest(self, cb):
        for item in self.tests:
            cb(item)


class BTInfoModule:
    def __init__(self):
        self.nom = ""
        self.nomComplet = ""
        self.modules = []
        self.targets = []

    def estVide(self):
        return (len(self.targets) == 0 and len(self.modules) == 0)

    def xtrInfoTgt(self, path):
        assert estTgt(path), "Not a target directory"
        tgt = BTInfoTarget()
        tgt.xtrInfo(path, self.nomComplet)
        self.targets.append(tgt)

    def xtrInfoMdl(self, path):
        assert estModule(path), "Not a module directory"
        mdl = BTInfoModule()
        mdl.xtrInfo(path, self.nomComplet)
        if (not mdl.estVide()): self.modules.append(mdl)

    def xtrInfo(self, path, nomParent):
        assert estModule(path), "Not a module directory"
        self.nom = os.path.basename(path)
        self.nomComplet = string.join([nomParent, self.nom], "@")
        dbg.printd("Module: %s" % self.nom)
        dbg.level = dbg.level + 1
        for f in os.listdir(path):
            fullPath = os.path.join(path, f)
            if estModule(fullPath):
                self.xtrInfoMdl(fullPath)
            elif estTgt(fullPath):
                self.xtrInfoTgt(fullPath)
##        dbg.printd("Module: #targets: %d  #modules: %d" % (len(self.targets), len(self.modules)))
        dbg.level = dbg.level - 1

    def pourChaqueModule(self, cb):
        for item in self.modules:
            cb(item)

    def pourChaqueTarget(self, cb):
        for item in self.targets:
            cb(item)


class BTInfoDate:
    def __init__(self):
        self.date = ""
        self.modules = []

    def xtrInfo(self, path):
        self.date = time.strftime("%Y%m%d-%H%M%S")
        dbg.printd("Debut xtrInfo: %s" % time.strftime("%H%M%S"))
        for f in os.listdir(path):
            if not estModule(os.path.join(path,f)): continue
            mdl = BTInfoModule()
            mdl.xtrInfo(os.path.join(path,f), "")
            if not mdl.estVide(): self.modules.append(mdl)
            dbg.printd("Module: skipped %s" % mdl.nom, mdl.estVide())
        dbg.printd("Fin xtrInfo: %s" % time.strftime("%H%M%S"))

    def ecrisXML(self, nom):
        dbg.printd("Debut ecrisXML: %s" % time.strftime("%H%M%S"))
        generic.dump(self, file(nom, 'w'))
        dbg.printd("Fin ecrisXML: %s" % time.strftime("%H%M%S"))
##        reader = Sax2.Reader()
##        doc = reader.fromString(generic.dumps(self))
##        PrettyPrint(doc, file(nom, 'w'))
##        dbg.printd("Fin ecrisXML-prettyPrint: %s" % time.strftime("%H%M%S"))

    def pourChaqueModule(self, cb):
        for item in self.modules:
            cb(item)


def estTgt(path):
    try:
        for f in os.listdir(path):
            if (f == "script_test.py"): return True
    except:
        pass
    return False

def estCVS(path):
    return os.path.split(path)[-1] == "CVS"

def estModule(path):
    return os.path.isdir(path) and not (estCVS(path) or estTgt(path))

def creeInfoDatedeXML(nom):
    return generic.load(file(nom, 'r'))


if __name__ == '__main__':
    dbg.on = 1      # Debug on
    if (len(sys.argv) > 1):
        path = sys.argv[1]
    else:
#    path = r"\\Madawaska\compil\dev\bin\dev"
        path = "."
    date = BTInfoDate()
    date.xtrInfo(path)

    date.ecrisXML("BT_%s.xml" % date.date)
 #   date = creeInfoDatedeXML(os.path.join(path, "a.xml"))

