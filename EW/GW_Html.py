# -*- coding: UTF-8 -*-
from EW_Util import EWHtmlFile

import os

class GWMenu:
    def __init__(self):
        pass

    def imprimeMenu(self, os, dates):
        os.write("<form method=get action=\"GW_GenHtml.cgi\">")
        os.write("<select name=\"date\">")
        for d in dates:
            os.write("<option value=\"%s\">%s" % (d, d))
        os.write("</select>")
        os.write("<input type=submit value=\"Date\">")

    def xeq(self, os, dates):
        self.imprimeMenu(os, dates)


class GW_Html:
    def __init__(self, date):
        composantes = date.split('_')
        self.pkg = composantes[0]
        self.dte = composantes[1]
        self.ptf = composantes[2]
        self.mpi = composantes[3]
        self.cpl = composantes[4]
        self.lnk = composantes[5]
        self.cfg = composantes[6]
        self.ficHTML = None

    def xeq(self, dates, path):

        # ---  Crée le fichier HTML
        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Log global de compilation: %s" % self.dte)

        # ---  Affiche le menu
        menu = GWMenu()
        menu.xeq(self.ficHTML, dates)

        # ---  Affiche l'entête
        self.ficHTML.write("<br>")
        self.ficHTML.write("<hr><b>Date:          %s</b>" % self.dte)
        self.ficHTML.write("<br><b>Logiciel:      %s</b>" % self.pkg)
        self.ficHTML.write("<br><b>Plateforme:    %s</b>" % self.ptf)
        self.ficHTML.write("<br><b>MPI Library:   %s</b>" % self.mpi)
        self.ficHTML.write("<br><b>Compilateur:   %s</b>" % self.cpl)
        self.ficHTML.write("<br><b>Link:          %s</b>" % self.lnk)
        self.ficHTML.write("<br><b>Configuration: %s</b>" % self.cfg)
        self.ficHTML.write("<br><hr>")

        # ---  Charge l'infodate
        nomFic = "GW_%s_%s_%s_%s_%s_%s_%s.%s" % (self.pkg, self.dte, self.ptf, self.mpi, self.cpl, self.lnk, self.cfg, "htm")
        fic = open(os.path.join(path, nomFic), "rt")
        for line in fic:
            print line.rstrip()

        self.ficHTML.writeFooter()

if __name__ == '__main__':
    date = "H2D2_20100524-160323_win32_mpich2_intel-11.1_dynamic_release"
    algo = GW_Html(date)
    algo.xeq([date], ".")
