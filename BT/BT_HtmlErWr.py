# -*- coding: UTF-8 -*-
from BT_Info import *
from EW_Util import EWHtmlFile

import os

class BTUneDateUnModuleUnTargetToutesConfigs:
    def __init__(self, date, module, target, config):
        self.dte = date
        self.mdl = module
        self.tgt = target
        self.ficHTML = None

    def imprimeTest(self, item):
#        if (item.fichier != self.ficActu.fichier): return
        href = "BT_GenHtmlCmpl.cgi?full=no?file=%s" % item.path
        self.ficHTML.write("<td align=center " +
                           "bgcolor=" + self.ficHTML.getColor(item.absent, item.errors, 0) +
                           ">" +
                           "<a href=\"%s\">%d - %d</a>" % (href, item.absent, item.errors))
        ##### PATCH
        self.tstActu = item

    def imprimeEnteteTable1(self, item):
        if (self.tstActu == None): self.tstActu = item
        elif (item.fichier != self.tstActu.fichier): return
        elif (item.compil  == self.tstActu.compil): return
        self.ficHTML.write("<th colspan=2>%s" % item.compil)
        self.tstActu = item

    def imprimeEnteteTable2(self, item):
        if (item.fichier != self.tstActu.fichier): return
        self.ficHTML.write("<th>%s" % item.config)

    def imprimeTarget(self, item):
        if (item.nom != self.tgt): return
        self.lstFichiers = {}

        self.tstActu = None
        self.ficHTML.write("<tr>")
        self.ficHTML.write("<th align=left>%s" % "Test")
##        self.ficHTML.write("<th rowspan=2 align=left>%s" % "Test")
##        item.pourChaqueTest(self.imprimeEnteteTable1)
        self.ficHTML.write("<tr>")
##        item.pourChaqueTest(self.imprimeEnteteTable2)

        self.ficHTML.write("<tr>")
        self.ficHTML.write("<th align=left>%s" % item.nom)
        item.pourChaqueTest(self.imprimeTest)

    def imprimeModule(self, item):
        item.pourChaqueModule(self.imprimeModule)
        if (item.nomComplet != self.mdl): return
        item.pourChaqueTarget(self.imprimeTarget)

    def imprimeDate(self, item):
#        if (item.date != self.dte): return
        item.pourChaqueModule(self.imprimeModule)

    def xeq(self, dates, path):
        # ---  Charge l'infodate
        f = "BT_" + self.dte + ".xml"
        info_date = creeInfoDatedeXML(os.path.join(path, f))

        # ---  Génère le fichier HTML
        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("%s %s %s" % (self.dte, self.mdl, self.tgt))
        self.ficHTML.write("    <b>Date: %s</b>" % self.dte)
        self.ficHTML.write("<br><b>Module: %s</b>" % self.mdl)
        self.ficHTML.write("<br><b>Test: %s</b>" % self.tgt)
        self.ficHTML.write("<br><br><center><table cellspacing=2 width=\"95%\" border=1>")
        self.imprimeDate(info_date)
        self.ficHTML.write("</table></center>")

        ##### PATCH
        if (self.tstActu.msgTest != None and self.tstActu.msgTest.type != ""):
            self.ficHTML.write("<br><br>Résultat du test: <b>" + self.tstActu.msgTest.type + "</b>")
            for m in self.tstActu.msgTest.msg:
                self.ficHTML.write("<br>   - %s" % m)

        if (self.tstActu.msgMdlr != None and self.tstActu.msgMdlr.type != ""):
            self.ficHTML.write("<br><br>Message Modeleur: <b>" + self.tstActu.msgMdlr.type + "</b>")
            for m in self.tstActu.msgMdlr.msg:
                self.ficHTML.write("<br>   - %s" % m)

        self.ficHTML.writeFooter()

class BT_Global_UneDateUnModuleTousTargetsToutesConfigs:
    def __init__(self, module = ""):
        self.os = None
        self.mdl = module
        self.mode = "Table"

    def assembleErreursTest(self, item):
        if not self.dico_ew.has_key(item.compil):
            self.dico_ew.setdefault(item.compil, len(self.lst_tst_ew))
            self.lst_tst_ew.append([0, 0, 0])
        idx = self.dico_ew[item.compil]
        self.lst_tst_ew[idx][0] = self.lst_tst_ew[idx][0] + item.absent
        self.lst_tst_ew[idx][1] = self.lst_tst_ew[idx][1] + item.errors
        self.lst_tst_ew[idx][2] = self.lst_tst_ew[idx][2] + 0
        self.sum_tst_ew[0] = self.sum_tst_ew[0] + item.absent
        self.sum_tst_ew[1] = self.sum_tst_ew[1] + item.errors
        self.sum_tst_ew[2] = self.sum_tst_ew[2] + 1

    def assembleErreursTarget(self, item):
        item.pourChaqueTest(self.assembleErreursTest)

    def assembleErreursModule(self, item):
        item.pourChaqueModule(self.assembleErreursModule)
        item.pourChaqueTarget(self.assembleErreursTarget)

    def imprimeEnteteTable(self):
        self.ficHTML.write("<tr>")
        self.ficHTML.write("<th align=left>%s" % "Module")
        self.ficHTML.write("<th>%s" % "Tests")

    def imprimeModule(self, item):
        self.dico_ew = {}
        self.lst_tst_ew = []
        self.sum_tst_ew = [0, 0, 0]
        item.pourChaqueModule(self.assembleErreursModule)
        item.pourChaqueTarget(self.assembleErreursTarget)
        href = "BT_GenHtmlErWr.cgi?date=%s?module=%s" % (self.date, item.nomComplet)
        self.ficHTML.write("<tr>")
        if (len(self.lst_tst_ew) > 0):
            self.ficHTML.write("<th align=left><a href=\"%s\">%s</a>" % (href, item.nom))
            self.ficHTML.write("<td align=center width=89 " +
                               "background=\"" + self.ficHTML.getBgColor(self.lst_tst_ew) + "\"" +
                               ">" +
                               "<a href=\"%s\">(%d : %d) / %d</a>" % (href, self.sum_tst_ew[0], self.sum_tst_ew[1], self.sum_tst_ew[2]))
        else:
            self.ficHTML.write("<td >")

    def imprimeTarget(self, item):
        self.dico_ew = {}
        self.lst_tst_ew = []
        self.sum_tst_ew = [0, 0, 0]
        item.pourChaqueTest(self.assembleErreursTest)

        hfin = len(item.nomComplet)-len(item.nom)-1
        href = "BT_GenHtmlErWr.cgi?date=%s?module=%s?target=%s" % (self.date, item.nomComplet[0:hfin], item.nom)
        if (self.mode == "Table"):
            if (len(self.lst_tst_ew) > 0):
                self.ficHTML.write("<tr>")
                self.ficHTML.write("<th align=left><i><a href=\"%s\">%s</a></i>" % (href, item.nom))
                self.ficHTML.write("<td align=center width=89 " +
                                   "background=\"" + self.ficHTML.getBgColor(self.lst_tst_ew) + "\"" +
                                   ">" +
                                   "<a href=\"%s\">%d : %d</a>" % (href, self.sum_tst_ew[0], self.sum_tst_ew[1]))
            else:
                self.ficHTML.write("<tr>")
                self.ficHTML.write("<td >")
        else:
            if (len(self.lst_tst_ew) > 0):
                self.ficHTML.write("<background=\"" + self.ficHTML.getBgColor(self.lst_tst_ew) + "\"" +
                                   ">" +
                                   "<a href=\"%s\"> </a>")
            else:
                self.ficHTML.write("<background=\"0x0a0a0a\">" +
                                   "<a href=\"%s\"> </a>")

    def recurseModule(self, item):
        if (self.mdl == "" or self.mdl == item.nomComplet):
            item.pourChaqueTarget(self.imprimeTarget)
            item.pourChaqueModule(self.imprimeModule)
        else:
            item.pourChaqueModule(self.recurseModule)

    def imprimeDate(self, item):
        self.date = item.date
        self.ficHTML.write("<hr><b>Date: %s</b>" % item.date)
        self.ficHTML.write("<br><b>Module: %s</b>" % self.mdl)
        self.ficHTML.write("<br><br><center><table cellspacing=2 width=\"95%\" border=1>")
        self.mode = "Sommaire"
##        item.pourChaqueModule(self.recurseModule)
        self.imprimeEnteteTable()
        self.mode = "Table"
        item.pourChaqueModule(self.recurseModule)
        self.ficHTML.write("</table></center>")
        self.ficHTML.writeFooter()

    def xeq(self, ficHTML, date):
        self.ficHTML = ficHTML
        self.imprimeDate(date)

class BTMenu:
    def __init__(self):
        pass

    def imprimeMenu(self, os, dates):
        os.write("<form method=get action=\"BT_GenHtmlErWr.cgi\">")
        os.write("<select name=\"date\">")
        for d in dates:
            os.write("<option value=\"%s\">%s" % (d, d))
        os.write("</select>")
        os.write("<input type=submit value=\"Date\">")

    def xeq(self, os, dates):
        self.imprimeMenu(os, dates)


class BTUneDateUnModuleTousTargetsToutesConfigs:
    def __init__(self, date, module, target, config):
        self.dte = date
        self.mdl = module
        self.ficHTML = None

    def xeq(self, dates, path):

        # ---  Charge l'infodate
        f = "BT_" + self.dte + ".xml"
        info_date = creeInfoDatedeXML(os.path.join(path, f))

        # ---  Génère le fichier HTML
        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Rapport complet: %s %s %s" % (self.dte, self.mdl, ""))
        menu = BTMenu()
        menu.xeq(self.ficHTML, dates)
        self.ficHTML.write("<br>")
        comp = BT_Global_UneDateUnModuleTousTargetsToutesConfigs(self.mdl)
        comp.xeq(self.ficHTML, info_date)


class BTUneDateTousModulesTousTargetsToutesConfigs:
    def __init__(self, date, module, target, config):
        self.dte = date
        self.mdl = ""
        self.ficHTML = None

    def xeq(self, dates, path):

        # ---  Charge l'infodate
        f = "BT_" + self.dte + ".xml"
        info_date = creeInfoDatedeXML(os.path.join(path, f))

        # ---  Génère le fichier HTML
        self.ficHTML = EWHtmlFile()
        self.ficHTML.writeHeader("Rapport complet: %s %s %s" % (self.dte, "", ""))
        menu = BTMenu()
        menu.xeq(self.ficHTML, dates)
        self.ficHTML.write("<br>")
        comp = BT_Global_UneDateUnModuleTousTargetsToutesConfigs(self.mdl)
        comp.xeq(self.ficHTML, info_date)


if __name__ == '__main__':
    date = "20051219-120144"
    algo = BTUneDateTousModulesTousTargetsToutesConfigs(date, "*", "*", "*")
    algo.xeq([date], ".")
#    algo = BTUneDateUnModuleTousTargetsToutesConfigs(date, "@banc_test", "*", "*")
#    algo.xeq([date], ".")
#    algo = BTUneDateUnModuleTousTargetsToutesConfigs(date, "@banc_test@Module_GestionnaireBD@ChampAnalytiqueScalaire", "*", "*")
#    algo.xeq([date], ".")

#    algo = BTUneDateUnModuleUnTargetToutesConfigs(date, "@banc_test@Module_GestionnaireBD@ChampAnalytiqueScalaire", "ChargeDechargeDecharge", "*")
#    algo.xeq([date], ".")

