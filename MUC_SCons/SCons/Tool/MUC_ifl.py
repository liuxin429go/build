# -*- coding: UTF-8 -*-

import MUC_icl
import SCons.Errors
import SCons.Tool.ifl
import os
import sys

def generate(env):
    envL = env.Clone()
    envL['ENV']['PATH'] = ''
    MUC_icl.generate(envL)

    pathOri = []
    for p in env['ENV']['PATH'].split(os.pathsep):
        pathOri.append(os.path.normpath(p))

    pathNew = []
    for p in envL['ENV']['PATH'].split(os.pathsep):
        pathNew.append(os.path.normpath(p))

    for p in pathNew:
        if sys.platform == 'win32':
            p = p.replace(os.path.normpath('/C++/'), os.path.normpath('/Fortran/'))
        elif sys.platform.startswith('linux'):
            p = p.replace(os.path.normpath('/cc/'), os.path.normpath('/fc/'))
        else:
            raise SCons.Errors.UserError('Invalid Intel platform: %s' % sys.platform)
        if (p not in pathOri):
            env.PrependENVPath('PATH', p)

    ftnext = [ '.f', '.for', '.ftn', '.f90', '.F', '.FOR', '.FTN', '.F90' ]
    if 'FORTRANFILESUFFIXES' not in env:
        env['FORTRANFILESUFFIXES'] = ftnext
    else:
        env['FORTRANFILESUFFIXES'].append(ftnext)
    SCons.Tool.ifl.generate(env)

def exists(env):
    SCons.Tool.ifl.exists(env)

