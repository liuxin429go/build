# -*- coding: UTF-8 -*-
'''
cgi-bin

'''

import GW_Html
import os
import string
import sys

arg_date    = "*"

def erreur(msg, status='500 Internal Server Error'):
    print 'Status:', status
    print
    print msg
    sys.exit(0)

def parseEnvString():
    global arg_date

    inp = os.getenv("QUERY_STRING")
    if (inp == None or inp == ""): inp = "date=*"
    tokens = string.split(inp, "&")

    for tok in tokens:
        if (tok == ""): continue
        stok = string.split(tok, "=")
        var = string.lower(stok[0])
        val = stok[1]
        if (var == "date"):    arg_date    = val

def main():
    global arg_date

    path = "e:/www/intranet/logs"
#    path = "e:/dev/build"

    #---  Parse les info
    parseEnvString()

    #---  Assemble la liste des dates
    dates = []
    for f in os.listdir(path):
        if (f[0:3] != "GW_"): continue
        if (os.path.splitext(f)[1] != ".htm"): continue
        date = os.path.splitext(f)[0]
        date = date[3:]
        dates.append(date)
    dates.sort()
    dates.reverse()

    #---  Date par défaut
    if (len(dates) > 0):
        if (arg_date == "*"):
            arg_date   = dates[0]

    #---  Construis l'algo d'ecriture
    if (len(dates) > 0):
        algo = GW_Html.GW_Html(arg_date)
        algo.xeq(dates, path)


if __name__ == '__main__':
    main()
