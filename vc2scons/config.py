#!/usr/bin/env python
# -*- coding: utf-8 -*-
#************************************************************************
# --- Copyright (c) Yves Secretan 2019
# ---
# --- Distributed under the GNU Lesser General Public License, Version 3.0.
# --- See accompanying file LICENSE.txt.
#************************************************************************

import os

from util import path2unix

class Config:
    """
    Classe statique qui contient la configuration
    """
    
    nomLogiciel = "vc2scons"
    versionLogiciel = "2.2.0dev"

    ExtensionsSrc = ".cpp;.cxx;.cc;.c;.for;.ftn;.f90;.rc;.pxd;"
    ExtensionsObj = ".obj;.lib;"
    ExtensionsExe = ".exe;.dll;"
    ExtensionsPkg = ".spec;"
    ExtensionsStageBin = ".ini;.jpg;.sql;.xml;.bat;.sh;"
    ExtensionsStageEtc = ".h2d2-cfg;.dat;"
    ExtensionsStageDoc = ".pdf;.htm;.html;.css;.gif;.js;.hlp;.lst;.txt;"
    ExtensionsStageLcl = ".stbl;"
    ExtensionsStageScr = ".py;.pyd;.png;"
    ExtensionsAdocumenter = ".h;.hpp;"
    SystemDefine = "WIN32;_DEBUG;_CONSOLE;_WINDOWS;"

    BuildSys = ["flag", "setup", "scons"]
        
    libInternes = [
        "algo_remesh",
        "b_BFGS",
        "b_BGL",
        "b_gbp",
        "b_NetCDF",
        "b_MUMPS",
        "b_ParMetis",
        "b_PETSc",
        "b_Remesh",
        "b_Scotch",
        "b_SuperLU_DS",
        "b_SuperLU_SP",
        "cd2d_age",
        "cd2d_b1l",
        "cd2d_bnl",
        "cd2d_bse",
        "cd2d_ccn",
        "cd2d_clf",
        "cd2d_mes",
        "cd2d_mes1",
        "cd2d_mes3",
        "cd2d_mesn",
        "cd2d_smp",
        "cd2d_tmp",
        "cd2d_tst",
        "ea_cmmn",
        "eg_cmmn",
        "h2d2_algo",
        "h2d2_elem",
        "h2d2_gpu",
        "h2d2_krnl",
        "h2d2_post",
        "h2d2_svc",
        "h2d2_umef",
        "libh2d2",
        "lmgo_l2",
        "lmgo_l3l",
        "lmgo_p12l",
        "lmgo_t3",
        "lmgo_t6l",
        "ni_p12l",
        "ni_t6l",
        "ns_macro3d",
        "numr_bgl",
        "numr_front",
        "numr_petsc",
        "numr_pmetis",
        "numr_scotch",
        "sed2d",
        "slvr_BFGS",
        "slvr_mkl",
        "slvr_mumps",
        "slvr_ptsc",
        "slvr_skit",
        "slvr_slud",
        "slvr_slus",
        "sv2d",
        "tool_gbp",
        "tools",
        "RemailleurH2D2",

        # ---  Modeleur
        "MailleursInternes",
        "Algo",
        "ChampsSeries",
        "ElementsFinis",
        "ToolKit_Geometrie",
        "ToolKit_Collection",
        "ToolKit_Exception",
    ]

    LXT_IDX_FLAG  = 0
    LXT_IDX_SETUP = 1
    LXT_IDX_SCONS = 2
    libExternes = { 
        #        "name"       (flag, setup, scons)
        # ---  Windows
        "Shlwapi"	        : ("", "", "shlwapi"),
        "version"	        : ("", "", "version"),
        "wininet"	        : ("", "", "wininet"),
        "kernel32"	        : ("", "", "pthread"),
        "fakedll"	        : ("", "", "dl"),
        "FakeDLL"	        : ("", "", "dl"),

        # ---  Modeleur
        "boost_python"	    : ("", "", "boost_python"),
        "boost_program_options" : ("", "", "boost_prog"),
        "geos_c"	        : ("", "", "geos"),
        "geos"	            : ("", "", "geos"),
        "vtk"	            : ("", "", "vtk"),
        "terralib.lib"	    : ("", "", "terralib"),
        "pgstream.lib"	    : ("", "", "pgstream"),
        "libpq"	            : ("", "", "postgresql"),
        "xmlParser"	        : ("", "", "xmlParser"),
        "proj.lib"	        : ("", "", "proj"),
        # --- H2D2
        "arpack"	        : ("", "", "arpack"),
        "util"	            : ("", "", "arpack"),
        "boost_"	        : ("", "", "boost"),
        "blacs"	            : ("", "", "blacs"),
        "breakpad"	        : ("", "", "google_breakpad"),
        "crash_generation_client"	: ("", "", "google_breakpad"),
        "crash_generation_server"	: ("", "", "google_breakpad"),
        "crash_report_sender"	    : ("", "", "google_breakpad"),
        "exception_handler"	        : ("", "", "google_breakpad"),
        "common"	        : ("", "", "google_breakpad"),
        "getDumpPath"	    : ("", "", "google_breakpad"),
        "mkl_scalapack"	    : ("", "", "blacs"),
        "mkl_blacs"	        : ("", "", "blacs"),
        "mkl_core"	        : ("", "", "blas"),
        "mkl"	            : ("", "", "blas"),
        "mkl_solver"	    : ("", "", "pardiso"),
        "libguide40.lib"	: ("", "", "blas"),
        "libiomp5md.lib"	: ("", "", "blas"),
        "intelmpi"	        : ("", "", "mpi"),
        "mpich2"	        : ("", "", "mpi"),
        "MPICH2"	        : ("", "", "mpi"),
        "msmpi"	            : ("", "", "mpi"),
        "MPI"	            : ("", "", "mpi"),
        "mpi"	            : ("", "", "mpi"),
        "MUMPS_"	        : ("", "", "mumps"),
        "dmumps"	        : ("", "", "mumps"),
        "mumps_common"	    : ("", "", "mumps"),
        "OpenCL"	        : ("", "", "opencl"),
        "cuda"	            : ("", "", "cuda"),
        "pord"	            : ("", "", "mumps"),
        "ParMetis-"	        : ("", "", "parmetis"),
        "libparmetis"	    : ("", "", "parmetis"),
        "libmetis"	        : ("", "", "parmetis"),
        "ParMETISLib"	    : ("", "", "parmetis"),
        "METISLib"	        : ("", "", "parmetis"),
        "GKlib"	            : ("", "", "parmetis"),
        "esmumps"	        : ("", "", "ptscotch"),
        "ptesmumps"	        : ("", "", "ptscotch"),
        "scotch"	        : ("", "", "ptscotch"),
        "ptscotch"	        : ("", "", "ptscotch"),
        "scalapack"	        : ("", "", "blacs"),
        "SCALAPACK"	        : ("", "", "blacs"),
        "slatec"	        : ("", "", "slatec"),
        "SuperLU_DIST"	    : ("", "", "superlu_ds"),
        "SuperLU_"	        : ("", "", "superlu_sp"),
        # SuperLU est mal détecté. Si la règle suivante est vide
        # on perd l'ordre des librairies
        "SuperLU.lib"	    : ("", "", "superlu_sp"),
    }
    #versions As New SortedList()

    @staticmethod
    def estPathInterne(p):
        path = p
        path = path2unix(path)
        if len(path) >  1 and path[: 1] == '"': path = path[1:]
        if len(path) > 10 and path[:10] == "$HOME/dev/": return True
        if len(path) > 10 and path[:10] == "$INRS_DEV/": return True
        if len(path) > 12 and path[:12] == "$(INRS_DEV)/": return True
        if len(path) >  2 and path[1] == ":": path = path[2:]
        if len(path) >  5 and path[: 5] == "/dev/": return True
        return False

    @staticmethod
    def estPathExterne(p):
        return not Config.estPathInterne(p)

    @staticmethod
    def estLibInterne(v):
        r = os.path.splitext(v)[0]
        return r in Config.libInternes

    @staticmethod
    def estLibExterne(v):
        if Config.estLibInterne(v):
            return False
        for item in Config.libExternes:
            if item in v:
                return True
        return False

    @staticmethod
    def reqItemExterne(v):
        lMax = 0
        iMax = None
        for item in Config.libExternes:
            if item in v and len(item) > lMax:
                lMax = len(item)
                iMax = item
        return iMax, Config.libExternes[iMax]


if __name__ == "__main__":
    print(Config.versionLogiciel)
    print(Config.estLibInterne("libh2d2"))
    print(Config.estLibInterne("libh2d2_"))
    print(Config.estLibExterne("OpenCL"))
    print(Config.estLibExterne("OpenCL_"))
    print(Config.reqItemExterne("OpenCL"))
