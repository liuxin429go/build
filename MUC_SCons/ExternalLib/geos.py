# -*- coding: UTF-8 -*-
import ExternalLib

import Util
import os.path

class geos(ExternalLib.ExternalLib):
    def __go_unx(self, ctx, ptf):
        hdir = os.environ['INRS_LXT']
        hdir = Util.getPathFromVersion(hdir, 'geos')
        hdir = os.path.join(hdir, 'build', ptf)
        __inc = [ os.path.join(hdir, 'include') ]
        __lpt = [ os.path.join(hdir, 'lib') ]
        __lib = [ 'geos_c', 'geos' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def __go_win(self, ctx, ptf):
        hdir = os.environ['INRS_LXT']
        hdir = Util.getPathFromVersion(hdir, 'geos')
        try:
            __inc = []
            idir = Util.getPathOfFile(hdir, r'build\\.*%s\\.*\\geos\\platform\.h' % ptf)
            idir = os.path.dirname( os.path.dirname(idir) )
            __inc.append(idir)
            idir = Util.getPathOfFile(hdir, r'build\\.*\\geos_c\.h')    # version config
            idir = os.path.dirname(idir)
            __inc.append(idir)
            idir = Util.getPathOfFile(hdir, r'.*\\geos\\geom\.h')       # main include
            idir = os.path.dirname( os.path.dirname(idir) )
            __inc.append(idir)

            ldir = Util.getPathOfFile(hdir, r'build\\.*%s\\.*geos\.lib' % ptf)
            ldir = os.path.dirname(ldir)
            __lpt = [ ldir ]
        except:
             SCons.Errors.UserError, 'GEOS: canonical path is "geos-x.x.x/build/win64'
        __lib = [ 'geos_c', 'geos' ]
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib, __inc, ctx.inc)

    def __unx32(self, ctx):
        self.__go_unx(ctx, 'unx32')

    def __unx64(self, ctx):
        self.__go_unx(ctx, 'unx64')

    def __unx64i8(self, ctx):
        self.__go_unx(ctx, 'unx64')

    def __win32(self, ctx):
        self.__go_win(ctx, 'win32')

    def __win64(self, ctx):
        self.__go_win(ctx, 'win64')

    def __win64i8(self, ctx):
        self.__go_win(ctx, 'win64')

    def configure(self, ctx, tool, version, plateform):
        fnm = '_geos__%s' % (plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
