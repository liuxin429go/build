# -*- coding: UTF-8 -*-
import DP_GenInclude
import logging
from string import split, replace
import os
import sys

log = logging.getLogger("DP_GenInclude_all")

def traite(dir):
    logging.basicConfig()
    log.setLevel(logging.INFO)

    if (not xeqAction(dir)):
        xeqRecursion(dir)

def xeqRecursion(dir):
    for f in os.listdir(dir):
        if os.path.isdir(os.path.join(dir,f)):
            if (dir == "."):
                traite(f)
            else:
                traite(os.path.join(dir,f))


def xeqAction(dir):
    trouve = 0
    path = os.path.join(dir, "source")
    if (os.path.isdir(path)): trouve = trouve + 1
    path = os.path.join(dir, "build")
    if (os.path.isdir(path)): trouve = trouve + 1

    if (trouve == 2):
        path = replace(dir,"\\","/")
        module = split(path, "/")[-1]
        log.info (" Traite: %s" % module)
        log.debug(" Module path: %s" % path)
        DP_GenInclude.genereInclude(path)
    return (trouve == 2)


if __name__ == '__main__':
   sys.exit(traite("."))



