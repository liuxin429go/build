# -*- coding: UTF-8 -*-
import ExternalLib

class wininet(ExternalLib.ExternalLib):
    def __unx64(self, ctx):
        pass

    def __unx64i8(self, ctx):
        pass

    def __unx32(self, ctx):
        pass

    def __win32(self, ctx):
        __lpt = [ ]
        __lib = ['wininet', 'shell32']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def __win64(self, ctx):
        __lpt = [ ]
        __lib = ['wininet', 'shell32']
        self.assign(__lpt, ctx.lpt, __lib, ctx.lib)

    def configure(self, ctx, tool, version, plateform):
        fnm = '_wininet__%s' % (plateform)
        fnc = self.__class__.__dict__[fnm]
        fnc(self, ctx)
