# -*- coding: UTF-8 -*-
#-----------------------------------------------------------------------------------------------------
# I.N.R.S. - Processus de compilation multi-compilateur
#
#   Compilateur cmc
#-----------------------------------------------------------------------------------------------------

import MUC_platform

import os
import shutil
import subprocess

import MultiCompiler_intel

"""
Configuration
"""
class cfg_cpp:
    def __init__(self):
        pass

    def c_cfg(self, cfg):
        return MultiCompiler_intel.cfg_c().c_cfg(cfg)

    def cpp_cfg(self, cfg):
        return MultiCompiler_intel.cfg_cpp().cpp_cfg(cfg)

class cfg_ftn:
    def __init__(self):
        pass

    def ftn_cfg(self, cfg):
        return MultiCompiler_intel.cfg_ftn().ftn_cfg(cfg)

"""
cmc générique
"""
class cmc(cfg_cpp, cfg_ftn):
    def __init__(self):
        pass

    def __lnx(self, cmplr, cfg):

        c = '%s -verbose %s -o%s %s %s %s' % (cmplr, cfg.fic_inp, cfg.fic_out, ' '.join(cfg.cpl_cfg), ' '.join(cfg.cpl_inc), ' '.join(cfg.cpl_dfn))

        _stderr = None
        _stdout = None
        _shell  = True
        if (cfg.fic_err):
            _stdout = open(cfg.fic_err, 'w')
            _stderr = subprocess.STDOUT


        if (cfg.dmp_cmd):
            print('--- Compiling on %s for %s(i%s)' % (MUC_platform.build_platform(), cfg.tgt_ptf, cfg.tgt_isz))
            print(c)
            print('-------')
        retcode = subprocess.call(c, stdout=_stdout, stderr=_stderr, shell=_shell)

    def c  (self, cmplr, cfg):
        self.__lnx(cmplr, cfg)

    def cpp(self, cmplr, cfg):
        self.__lnx(cmplr, cfg)

    def ftn(self, cmplr, cfg):
        self.__lnx(cmplr, cfg)


class cmc_xx(cmc):
    def c(self, cfg):
        cmc.cpp(self, 's.cc', cfg)

    def cpp(self, cfg):
        cmc.cpp(self, 's.CC', cfg)

    def ftn(self, cfg):
        cmc.ftn(self, 's.f90', cfg)

class cmc_intel1302(cmc_xx):
    pass

