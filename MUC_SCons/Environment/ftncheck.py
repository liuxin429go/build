# -*- coding: UTF-8 -*-
import SCons.Script

from . import Environment

SConsEnv = SCons.Environment.Environment
MUCEnv   = Environment.Environment

class EnvironmentFtnchek(MUCEnv):
    def __init__(self, tool, version, opts, *args, **kwargs):
        plateform = 'win32'
        tools =  ['f77', 'gnulink', 'ar']

        SConsEnv.__init__( self,
                           tools = tools,
                           options = opts,
                           plateform = plateform,
                           MUC_TARGETPLATEFORM = 'win32')
        MUCEnv.__init__(self, tool, version, *args, **kwargs)

        self['LINK']   = 'fntchek linker a definir'
        self['SHLINK'] = 'fntchek linker a definir'
        self['LINKFLAGS'] = []
        self['LINKFLAGS'] = []

    def setupEnv(self, ctx):
        MUCEnv.MUCEnv.setupEnv(self, ctx)
